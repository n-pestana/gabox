<?php
$mod['name']       = t("Groupes Administrateurs");
$mod['desc']       = t("Module de gestion des groupes administrateurs");
$mod['fa-icon']    = 'users';
$mod['version']    = '1.0';

$mod['bo-hook'] = false;

