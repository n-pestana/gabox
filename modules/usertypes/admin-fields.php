<?php
$form->fields= array(
    '_cat1'    => t("Informations")
    ,'lib'=>array_merge($form->gen_field['lib'],array('label'=>t("Nom du groupe")))
    ,'level'=>array_merge($form->gen_field['lib'],array('label'=>t("Niveau (non utilisé)")))
    ,'is_admin'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Accès super utilisateur ?")))
    ,'updated_at'=>$form->gen_field['updated_at']
	,'is_valid'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Publié ?")))
    ,'module_list' =>array(
        'label' => t("Permission d'accéder aux modules suivants")
        ,'type' =>'relationel_list'
        ,'arrayValues' => $gen->collector['modules']->getKv() + ['media' => 'media']
        ,'multiple'=>true
        ,'linked_object'=>'modules' 
        ,'class'=>'chosen-select','style'=>'width: 600px'
    )
	,'bootstrap_url'=> array_merge($form->gen_field['lib'],array('label'=>t("URL de redirection au login"),'pattern'=>'.*'))


);
