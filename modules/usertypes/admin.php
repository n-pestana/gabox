<?php
include(GABOX_BACK_ROOT.'header.php');

$curr_obj='usertypes';


# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    include('admin-fields.php');
    $form->lib= t("Administrators editing");
    $form->heading= '';
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
# Listing
else{

    $listing['sql']= array(
              'gen_usertypes.id_'.$curr_obj
              ,'gen_usertypes.lib'
              ,'gen_usertypes.level'
              ,'gen_usertypes.is_admin as is_admin'
    );

    $listing['fields']= array(
        'lib' => array(
            'label'         => t("Nom")
            ,'edit_in_place'=>'true'
        )
        ,'level' => array(
            'label'         => t("Niveau")
            ,'edit_in_place'=>'true'
        )
       ,'is_admin' => array(
            'label'         => t("Super Admin ?")
            ,'callback'=>'getEnable'
            ,'edit_in_place'=>'true'
            ,'type'=>'switch'
        )
       
    );


    if(!empty($restrictions)) $listing['join_conditions']  .= ' and gen_usertypes.id_usertypes='.$_SESSION['usertypes']['id_usertypes'];

    $listing['title'] = t("Liste des groupes adminisateurs");
    include(GABOX_GEN_ROOT.'genlist.php');
}

include(GABOX_BACK_ROOT.'footer.php');
