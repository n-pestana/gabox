<?php
$mod['name']       = t("Médiathèque");
$mod['desc']       = t("Module de gestion des medias basé sur MoxieManager");
$mod['fa-icon']    = 'fa-solid fa-photo-film-music';
$mod['version']    = '1.0';
