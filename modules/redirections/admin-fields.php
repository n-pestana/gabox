<?php
$form->fields = [
    '_info' => t('Informations'),
    'origin' => array_merge($form->gen_field['lib'], ['label' => t("Origin")]),
    'destination' => array_merge($form->gen_field['lib'], ['label' => t("Destination")]),
    'enabled' => $form->gen_field['ouinon'],
    'id_sites' => array_merge($form->gen_field['select'], [
        'label' => t("Site"),
        'arrayValues' => $gen->collector['sites']->getKv(),
        'value' => defined('ID_SITE') ? ID_SITE : '',
    ]),
    'updated_at' => $form->gen_field['updated_at'],
];
