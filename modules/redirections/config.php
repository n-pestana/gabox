<?php
$mod['name']       = 'Redirections';
$mod['desc']       = 'Ce module permet de définir des redirections 301';
$mod['fa-icon']    = 'route';
$mod['version']    = '1.0';

$mod['sql'][] = "CREATE TABLE IF NOT EXISTS 
    gen_redirections (
        id_redirections mediumint(8) unsigned auto_increment PRIMARY KEY,
        origin varchar(1024) not null unique,
        destination varchar(1024) not null,
        enabled tinyint(1) default 0 null,
        id_sites mediumint unsigned not null,
        created_at datetime default now() null,
        updated_at datetime null,
        constraint gen_redirections_gen_sites_id_sites_fk
        foreign key (id_sites) references gen_sites (id_sites)
        on delete cascade
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$mod['unsql'] = "DROP TABLE IF EXISTS gen_redirections;";
