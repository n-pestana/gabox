<?php
include(GABOX_BACK_ROOT.'/header.php');
$curr_obj = 'redirections';

# Edit
if (isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])) {
    include('admin-fields.php');
    $form->lib     = t('Edit redirection');
    $form->heading = '';
    include(GABOX_GEN_ROOT.'gen_edit.php');
} else { # Listing
    $listing        = ['sql','table_join','join_conditions','fields','title'];
    $listing['sql'] = [
        'gen_'.$curr_obj.'.id_'.$curr_obj,
        'gen_'.$curr_obj.'.origin',
        'gen_'.$curr_obj.'.destination',
        'gen_'.$curr_obj.'.enabled',
    ];

    $listing['fields'] = [
        'origin' => ['label' => t('Origin'), 'edit_in_place' => true],
        'destination' => ['label' => t('Destination'), 'edit_in_place' => true],
        'enabled' => [
            'label'         => t("Publié ?"),
            'type'          => 'switch',
            'callback'      => 'getEnable',
            'edit_in_place' => 'true',
        ],
    ];
        
    $listing['title'] = t('Redirections list');
    include(GABOX_GEN_ROOT.'genlist.php');
}
include(GABOX_BACK_ROOT.'/footer.php');