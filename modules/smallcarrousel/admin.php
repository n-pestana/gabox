<?php
include(GABOX_BACK_ROOT.'header.php');

$curr_obj='smallcarrousel';

# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    include('admin-fields.php');
    $form->lib= 'Small Carrousel editing';
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
# Listing
else{

    $listing['fields']= array(
        'lib' => array(
            'label'         => ("Name")
	        ,'edit_in_place'=>true
        ) 
        ,'updated_at' => array(
            'label'         => ("Maj")
        ) 
        ,'cat' => array(
            'label'         => ("Catégorie")
        ) 
        ,'media' => array(
            'label'         => ("Media")
            ,'callback' => 'getMedia'
        )
        ,'media_linked' => array(
            'label'         => ("Media lié")
            ,'callback' => 'getMedia'
        )
  	    ,'enabled' => array(	
            'edit_in_place'=>'true'
            ,'callback'=>'getEnable'
            ,'type'=>'switch'
        )
    );

    if(defined('ID_SITE')){ 
        $listing['table_join']      = array('gen_smallcarrousel');
        $listing['join_conditions'] = ' gen_smallcarrousel.id_site='.ID_SITE;
    }

    $listing['title'] = 'Small Carrousel listing';
    include(GABOX_GEN_ROOT.'genlist.php');
}

include(GABOX_BACK_ROOT.'footer.php');
