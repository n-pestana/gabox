<?php
$form->fields= array(
    '_cat1' => t("Informations")
    ,'lib'          => array_merge($form->gen_field['lib'],array('label'=>("Titre"),'tooltip'=>"Non obligatoire"))
    ,'enabled'      => array_merge($form->gen_field['ouinon'],array('label'=>("Activé ?")))
    ,'updated_at'   => $form->gen_field['updated_at']
    ,'content'      => array_merge($form->gen_field['desc'],array('label'=>("Contenu "),'tooltip'=>"Non obligatoire"))
    ,'media'        => array_merge($form->gen_field['file'],array('label'=>("Image affichée"),'tooltip'=>"Il s'agit de l'image qui sera affichée dans le carrousel. \nLe format préconisé est  160x120"))
    ,'media_linked' => array_merge($form->gen_field['file'],array('label'=>("Média lié au click"),'tooltip'=>"Il s'agit de l'image ou le document PDF qui sera affiché au click"))
    ,'cat'          => $form->gen_field['cat'] 
); 
