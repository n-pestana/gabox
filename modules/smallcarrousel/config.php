<?php
$mod['name']       = 'Small Carrousel';
$mod['desc']       = 'Ce module permet de gérer des carrousels simple (lib,  media, media_linked)';
$mod['fa-icon']    = 'fas fa-images';
$mod['version']    = '1.0';
$mod['sql'][]        = "
CREATE TABLE `gen_smallcarrousel` (
  `id_smallcarrousel` int(10) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `id_site` int(11) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `lib` varchar(1024) DEFAULT NULL,
  `content` mediumtext,
  `media` varchar(1024) DEFAULT '',
  `media_linked` varchar(1024) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
$mod['sql'][] = "ALTER TABLE `gen_smallcarrousel` ADD `cat` VARCHAR(255) NOT NULL AFTER `media_linked`;";
$mod['sql'][] = "ALTER TABLE `gen_smallcarrousel` ADD UNIQUE KEY `id_smallcarrousel` (`id_smallcarrousel`), ADD KEY `enabled` (`enabled`,`cat`);";
$mod['sql'][] = "ALTER TABLE `gen_smallcarrousel` CHANGE `id_smallcarrousel` `id_smallcarrousel` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;";
$mod['unsql'] = "DROP TABLE gen_smallcarrousel;";


