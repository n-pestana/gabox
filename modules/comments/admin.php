<?php
$curr_obj='comments';

 // Ajax
if (isset($_POST['action']) && (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    $id_comments = (int) $_POST['id_comments'];
    $result = false;
    if ($_POST['action'] == 'approve') {
        $statut = 'APPROVED';
        $result = $gen->collector[$curr_obj]->set($id_comments, ['moderation' => $statut]);
    } elseif ($_POST['action'] == 'refuse') {
        $statut = 'REFUSED';
        $result = $gen->collector[$curr_obj]->set($id_comments, ['moderation' => $statut]);
    }
    if ($result === false) {
        $result = 'error';
        $msg = t("Erreur lors de la mise à jour...");
    } else {
        $result = 'success';
        $msg = t("Mise à jour effectuée !");
    }
    header('Content-type: application/json');
    echo json_encode(['msg' => $msg, 'result' => $result, 'id_comments' => $id_comments, 'statut' => $gen->collector[$curr_obj]->getStatus($statut)]);
    die;
}

include(GABOX_BACK_ROOT.'/header.php');

# Edit
if (isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])) {
    include('admin-fields.php');
    $form->lib     = t("Edit comment");
    $form->heading = 'Customer comments management & moderation';
    include(GABOX_GEN_ROOT.'gen_edit.php');
        
} else { # Listing
    $listing        = ['sql','table_join','join_conditions','fields','title'];
    $listing['sql'] = ['gen_'.$curr_obj.'.id_'.$curr_obj
        ,'gen_'.$curr_obj.'.name'
        ,'gen_'.$curr_obj.'.rate'
        ,'gen_'.$curr_obj.'.content'
        ,'gen_'.$curr_obj.'.moderation'
        ,'gen_'.$curr_obj.'.id_produits as id_produits'
        ,'gen_sites.lib AS site_lib'
    ];

    $listing['fields'] = ['name'        => ['label' => t("Name"), 'edit_in_place' => true]
                        , 'moderation'  => ['label' => t("Statut"), 'callback' => 'comments::getStatus']
                        , 'id_produits'  => ['label' => t("Pro"), 'callback' => 'produits::getLib']
                        , 'rate'        => ['label' => t("Rate")]
                        , 'content'     => ['label' => t("Comment")]
                        , 'site_lib'    => ['label' => t("Site")]
                        , 'btn_approve' => ['label' => t("Approve"), 'callback' => 'comments::getApproveButton', 'callbackFieldValue' => 'rowset']
                        , 'btn_refuse'  => ['label' => t("Refuse"), 'callback' => 'comments::getRefuseButton', 'callbackFieldValue' => 'rowset']
    ];
        
    $listing['title'] = t("Comments list");
    include(GABOX_GEN_ROOT.'genlist.php');
}
?>
  <script src="js/main.js"></script>
<?php 
include(GABOX_BACK_ROOT.'/footer.php');
