<?php
class Comments extends gen_comments {

    public function getStatus($value) {
        switch ($value) {
            case 'REFUSED':
                $class = 'alert-danger';
                break;
            case 'PENDING':
                $class = 'alert-warning';
                break;
            default:
                $class = 'alert-success';
        }
        return '<div class="' . $class . ' text-center">' . $value . '</div>';
    }
    
    public function getApproveButton($value) {
        return '<a href="#" class="btn btn-outline-success approve"' . (in_array($value['moderation'], ['REFUSED', 'PENDING']) ? '' : ' style="display:none"') . '>' . t("Approve") . '</a>';
    }

    public function getRefuseButton($value) {
        return '<a href="#" class="btn btn-outline-danger refuse"' . (in_array($value['moderation'], ['APPROVED', 'PENDING']) ? '' : ' style="display:none"') . '>' . t("Refuse") . '</a>';
    }
    
    public function getModerationLabel($status) 
    {
        $label = '';
        switch($status) {
            case 'PENDING':
                $label = t("Moderation in progress");
                break;
            case 'APPROVED':
                $label = t("Comment approved");
                break;
             case 'REFUSED':
                $label = t("Comment refused");
                break;
        }
        return $label;
    }
    function getStatusKv(){
	return array(
                'PENDING'=>'PENDING','APPROVED'=>'APPROVED','REFUSED'=>'REFUSED'
        );
    }

    function getModerationList(){
	return array(
		'PENDING','APPROVED','REFUSED'
	);
    }
    
    public function getAverage($id_produit){
          $sql = 'SELECT ( AVG( rate ) ) as avg 
                FROM  `gen_comments` 
                WHERE id_produits ='.(int)$id_produit." and moderation='APPROVED'"
                ;
          $rs = $this->query($sql);

          $temp = $this->GetRows($rs);

          if(empty($temp)) return false;
          if(isset($temp[0]) && isset($temp[0]['avg']) && !$temp[0]['avg'])return false;
          return number_format($temp[0]['avg'], 1, ',', ' ');
    }
}
