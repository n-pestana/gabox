<?php
$mod['name']        = 'Commentaires clients';
$mod['desc']        = 'Ce module permet de gérer les avis clients déposés sur le site';
$mod['fa-icon']     = 'star';
$mod['version']     = '1.0';

$mod['sql'][]      = "
CREATE TABLE IF NOT EXISTS `gen_comments` (
  `id_comments` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sites` mediumint(8) UNSIGNED NOT NULL,
  `id_produits` mediumint(8) UNSIGNED NOT NULL,
  `id_clients` mediumint(8) UNSIGNED DEFAULT NULL,
  `content` mediumtext NOT NULL,
  `moderation` enum('PENDING','APPROVED','REFUSED') NOT NULL,
  `rate` tinyint(4) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  FOREIGN KEY (id_sites) REFERENCES gen_sites(id_sites)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
        
$mod['unsql'][]    = "DROP TABLE gen_comments;";

if (isset($gen->collector['comments'])) {
    $cnt = $gen->collector['comments']->getCount(['moderation'=>'PENDING']);
    if($cnt >= 1) {
        $mod['bo-header-shortcuts'] = [
            URL_LOC . '/modules/comments/admin.php?status=PENDING' => [
                'class' => 'btn-warning text-light',
                'lib' => $cnt . t(' avi(s) en attente'),
            ],
        ];
    }
}

/*$mod['bo-hook'] = ['lib'  => 'FAQ',
                   'icon' => 'question-circle',
                   'links' => [['link' => '/modules/faq/admin.php',  'lib' => 'Liste']
                              ,['link' => '/modules/treeview/treeview.php?parent-hook=' . $mod['parent-hook'],  'lib' => 'Catégories']
                              ]
                  ];*/