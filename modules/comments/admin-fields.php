<?php
$form->fields = [
    '_info'         => t("Informations")
    ,'name'         => array_merge($form->gen_field['lib'], ['label' => t("Nom")])
    ,'rate'         => array_merge($form->gen_field['select'], ['label' => t("Note")],
                        ['arrayValues'=> [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5] ])
    ,'content'      => array_merge($form->gen_field['desc'], ['label' => t("Commentaire")])
    ,'_publication' => t("Publication")
    ,'moderation'   => array_merge($form->gen_field['select'], ['label' => t("Statut")],
                        ['arrayValues'=> ['PENDING' => t("En attente"), 'APPROVED' => t("Approuvé"), 'REFUSED' => t("Refusé")] ])
    ,'id_sites'     => array_merge($form->gen_field['select'], ['label' => t("Site")],
                        ['arrayValues'=>$gen->collector['sites']->getKv()],
                        ['class'=>'chosen-select'],
                        ['value' => defined('ID_SITE') ? ID_SITE : '']) 
    ,'updated_at'   => $form->gen_field['updated_at']
    ];
