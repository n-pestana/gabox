$(document).ready(function() {
    $('a.btn.approve').click(function() {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/modules/comments/admin.php",
            data: "action=approve&id_comments=" + $(this).parent().siblings('.id_comments').children('input').val(),
            success: function(data){
                notify(data['msg'], data['result']);
                if (data['result']) {
                    $('td.id_comments input[value=' + data['id_comments'] + ']').parent().siblings('.btn_approve').children('a').hide();
                    $('td.id_comments input[value=' + data['id_comments'] + ']').parent().siblings('.btn_refuse').children('a').show();
                    $('td.id_comments input[value=' + data['id_comments'] + ']').parent().siblings('.moderation').html(data['statut']);
                }
            }
        });
    });
    $('a.btn.refuse').click(function() {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/modules/comments/admin.php",
            data: "action=refuse&id_comments=" + $(this).parent().siblings('.id_comments').children('input').val(),
            success: function(data){
                notify(data['msg'], data['result']);
                if (data['result']) {
                    $('td.id_comments input[value=' + data['id_comments'] + ']').parent().siblings('.btn_approve').children('a').show();
                    $('td.id_comments input[value=' + data['id_comments'] + ']').parent().siblings('.btn_refuse').children('a').hide();
                    $('td.id_comments input[value=' + data['id_comments'] + ']').parent().siblings('.moderation').html(data['statut']);
                }
            }
        });
    });
});