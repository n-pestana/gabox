<?php
$form->fields = [
    '_info'         => t("Informations")
    ,'lib'          => array_merge($form->gen_field['lib'], ['label'=>t("Titre")])
    ,'content'      => array_merge($form->gen_field['desc'], ['label'=>t("Contenu")])
    ,'media'        => array_merge($form->gen_field['file'], ['label' => t("Media")])
    ,'author'       => array_merge($form->gen_field['lib'], ['label'=>t("Auteur")])
    ,'page'         => array_merge($form->gen_field['select'], ['label' => t("Page")],
                        ['arrayValues'=>['home-et-inscription' => 'home-et-inscription', 'espace-entreprise' => 'espace-entreprise']],
                        ['class'=>'chosen-select'])
    ,'_publication' => t("Publication")
    ,'enabled'      => $form->gen_field['ouinon']
    ,'id_sites'     => array_merge($form->gen_field['select'], ['label' => t("Site")],
                        ['arrayValues'=>$gen->collector['sites']->getKv()],
                        ['class'=>'chosen-select'],
                        ['value' => defined('ID_SITE') ? ID_SITE : '']) 
    ,'updated_at'   => $form->gen_field['updated_at']
    ];
