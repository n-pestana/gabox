<?php
$mod['name']       = 'Témoignages';
$mod['desc']       = 'Gestion des témoignages';
$mod['fa-icon']    = 'comment-dots';
$mod['version']    = '1.0';

$mod['sql'][]        = "
CREATE TABLE IF NOT EXISTS `gen_testimonials` (
  `id_testimonials` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sites` mediumint(8) UNSIGNED NOT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `lib` varchar(255) NOT NULL,
  `content` mediumtext DEFAULT NULL,
  `media` varchar(1024) DEFAULT '',
  `author` varchar(255) DEFAULT NULL,
  `page` enum('home-et-inscription','espace-entreprise') NOT NULL DEFAULT 'home-et-inscription',
  `id_produits` mediumint(9) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  FOREIGN KEY (id_sites) REFERENCES gen_sites(id_sites)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$mod['unsql'] = "DROP TABLE gen_testimonials;";