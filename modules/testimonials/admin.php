<?php
include(GABOX_BACK_ROOT.'header.php');
$curr_obj = 'testimonials';

# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    include('admin-fields.php');
    $form->lib      = t("Edit testimonial");
    $form->heading  = '';
    include(GABOX_GEN_ROOT.'gen_edit.php');
} else { # Listing
    $listing        = ['sql','table_join','join_conditions','fields','title'];
    $listing['sql'] = ['gen_'.$curr_obj.'.id_'.$curr_obj
        ,'gen_'.$curr_obj.'.lib'
        ,'gen_'.$curr_obj.'.enabled'
        ,'gen_'.$curr_obj.'.author'
        ,'gen_'.$curr_obj.'.page'
        ,'gen_'.$curr_obj.'.media'
        ,'gen_sites.lib AS site_lib'
        ];

    $listing['fields'] = ['lib'      => ['label' => t("Title"), 'edit_in_place' => true]
                        , 'author'   => ['label' => t("Author"), 'edit_in_place' => true]
                        , 'media'   => ['label' => t("Image"), 'callback'=>'getMedia']
                        , 'enabled'  => ['label' => t("Publié ?")
                            ,'type'         => 'switch'
                            ,'callback'     => 'getEnable'
                            ,'edit_in_place'=> 'true'
                        ]
                        , 'site_lib' => ['label' => t("Site")]
    ];

    $listing['title'] = t("Testimonials list");
    include(GABOX_GEN_ROOT.'genlist.php');
}
include(GABOX_BACK_ROOT.'footer.php');
