<?php
$faqCategories = ['' => t("Select category")] + $gen->collector['treeview']->getItemsHierarchyForSelect('faq-categories', 1);

$form->fields = [
    '_info'         => t("Informations")
    ,'lib'          => array_merge($form->gen_field['simple_html_i18n'], ['label' => t("Question")])
    ,'content'      => array_merge($form->gen_field['simple_html_i18n'], ['label' => t("Answer")])
    ,'id_treeview'  => array_merge($form->gen_field['select'], ['label' => t("Category")],
                        ['arrayValues' => $faqCategories]) 
    ,'_publication' => t("Publication")
    ,'url'          => array_merge($form->gen_field['lib'], ['label' => t("Url") . ' ' . t("(auto fill)")])
    ,'enabled'      => $form->gen_field['ouinon']
    ,'id_sites'     => array_merge($form->gen_field['select'], ['label' => t("Site")],
                        ['arrayValues'=>$gen->collector['sites']->getKv()],
                        ['class'=>'chosen-select'],
                        ['value' => defined('ID_SITE') ? ID_SITE : '']) 
    ,'updated_at'   => $form->gen_field['updated_at']
    ];
