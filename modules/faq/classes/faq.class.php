<?php
require_once(COMMON . 'urls.trait.php');

class Faq extends gen_faq {
    use urls;

    protected $_search_fields = ['lib', 'content'];
    
    public function set($primary_key_value = '' , $array_fields = '', $criteres = array()) {
        if (array_key_exists('url', $array_fields) && empty($array_fields['url'])) {
            $array_fields['url'] = strtolower(urlize2($array_fields['lib'])).'.html';
        }
        $id = parent::set($primary_key_value, $array_fields, $criteres);
        if (!empty($id)) {
            $this->buildUrl($id);
        }
        return $id;
    }
}