<?php
include(GABOX_BACK_ROOT.'/header.php');
$curr_obj='faq';

# Edit
if (isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])) {
    include('admin-fields.php');
    $form->lib     = t("Edit FAQ");
    $form->heading = t("FAQ post depends on the treeview for categories management");
    include(GABOX_GEN_ROOT.'gen_edit.php');
} else { # Listing
    $listing        = ['sql','table_join','join_conditions','fields','title'];
    $listing['sql'] = ['gen_'.$curr_obj.'.id_'.$curr_obj
        ,'gen_'.$curr_obj.'.lib'
        ,'gen_treeview.lib AS category_lib'
        ,'gen_'.$curr_obj.'.enabled'
        ,'gen_'.$curr_obj.'.hit'
        ,'gen_sites.lib AS site_lib'
    ];

    $listing['fields'] = ['lib'          => ['label' => t("Titre")]
                        , 'enabled'      => ['label' => t("Publié ?")
                            ,'type'         => 'switch'
                            ,'callback'     => 'getEnable'
                            ,'edit_in_place'=> 'true'
                        ]
                        , 'category_lib' => ['label' => t("Categore")]
                        , 'hit'          => ['label' => t("Nombre de vues")]
                        , 'site_lib'     => ['label' => t("Site")]
    ];
        
    $listing['title'] = t("FAQ list");
    include(GABOX_GEN_ROOT.'genlist.php');
}
include(GABOX_BACK_ROOT.'/footer.php');
