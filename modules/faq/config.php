<?php
$mod['name']        = 'FAQ';
$mod['desc']        = 'Ce module permet de gérer des FAQ et des catégories de FAQ';
$mod['fa-icon']     = 'question-circle';
$mod['version']     = '1.0';
$mod['parent-hook'] = 'faq-categories';

# FAQs
$mod['sql'][]      = "
CREATE TABLE IF NOT EXISTS `gen_faq` (
  `id_faq` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sites` mediumint(8) UNSIGNED NOT NULL,
  `lib` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `url` varchar(1024) DEFAULT '',
  `id_treeview` mediumint(8) UNSIGNED NOT NULL,
  `hit` int(11) UNSIGNED NOT NULL DEFAULT 0,
  FOREIGN KEY (id_sites) REFERENCES gen_sites(id_sites),
  FOREIGN KEY (id_treeview) REFERENCES gen_treeview(id_treeview)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
$mod['sql'][]      =  "INSERT INTO `gen_treeview` (`id_treeview`, `lib`, `url`, `description`, `file`, `meta_title`, `meta_desc`, `id_parent`, "
        . "`int_order`, `id_site`, `level`, `controller`, `href`, `id_locales`, `media`, `is_valid`, `hook`, `fullurl`, `is_on_home`, "
        . "`show_in_menu`, `show_in_filter`, `show_products`, `id_link_treeview`, `show_in_footer`, `show_in_search`, `show_in_coach`, "
        . "`show_in_guide`, `item_order`, `guide_media`, `id_treeview2`) VALUES "
        . "(NULL, 'FAQ categories', '', 'Root treeview for FAQ categories', '', '', '', '0', (SELECT MAX(`int_order`)+1 FROM gen_treeview t), NULL, '1', NULL, NULL, NULL, '', '1', "
        . "'" . $mod['parent-hook'] . "', NULL, '1', '1', '0', '0', NULL, '1', '1', '1', '0', '0', NULL, NULL);";
$mod['sql'][]      = "
CREATE TABLE IF NOT EXISTS `gen_faq_url` (
  `id_faq_url` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_locale` int(11) NOT NULL,
  `id_site` mediumint(8) UNSIGNED NOT NULL,
  `id_faq` int(11) UNSIGNED NOT NULL,
  `id_treeview` int(11) NOT NULL,
  `is_canonical` tinyint(1) DEFAULT '0',
  `url` varchar(1024) NOT NULL,
  FOREIGN KEY (id_site) REFERENCES gen_sites(id_sites),
  FOREIGN KEY (id_faq) REFERENCES gen_faq(id_faq),
  KEY `getCanonicalFront` (`id_locale`,`id_site`,`is_canonical`,`id_faq`),
  KEY `id_locale` (`id_locale`,`url`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
";

$mod['unsql'][]    = "DROP TABLE gen_faq_url;";
$mod['unsql'][]    = "DROP TABLE gen_faq;";

$mod['bo-hook'] = ['lib'  => 'FAQ',
                   'icon' => 'question-circle',
                   'links' => [['link' => '/modules/faq/admin.php',  'lib' => 'Liste']
                              ,['link' => '/modules/treeview/treeview.php?parent-hook=' . $mod['parent-hook'],  'lib' => 'Catégories']
                              ]
                  ];
