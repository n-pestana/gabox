<?php

$mod['name']       = t("Gabox Logs");
$mod['desc']       = t("Gestion des log Gabox");
$mod['fa-icon']    = 'fa-solid fa-message-exclamation';
$mod['version']    = '1.0';



$mod['bo-hook'] = ['lib'  => $mod['name'], 
                   'icon' => $mod['fa-icon'],
                  'link' => '/modules/log/admin.php',
                  'lib' => t("Log Gabox")
                ];

$mod['sql'][]      = "CREATE TABLE IF NOT EXISTS `gen_log` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) DEFAULT NULL,
  `level` int DEFAULT NULL,
  `message` longtext,
  `time` int UNSIGNED DEFAULT NULL,
  `id_admins` text,
  `action` text,
  `id_gabox` text,
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`),
  KEY `level` (`level`),
  KEY `time` (`time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";


if(isset($gen->collector['log'])&&method_exists($gen->collector['log'],'getCount')){
    $cnt = $gen->collector['log']->getCount();
    $class = ($cnt==0) ? 'badge-light-secondary' : 'badge-danger';
    $mod['bo-hook']['lib'] = $mod['bo-hook']['lib'].'&nbsp;<span class="badge '.$class.'">'.$gen->collector['log']->getCount().'</span>';
}

