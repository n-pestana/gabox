<?php
include(GABOX_BACK_ROOT.'header.php');
if(!defined('ID_LOC'))define('ID_LOC',4);
$curr_obj = 'log';


$listing        = ['sql','table_join','join_conditions','fields','title'];
$listing['sql'] = [
    'gen_'.$curr_obj.'.id'
    ,'gen_'.$curr_obj.'.channel'
    ,'gen_'.$curr_obj.'.level'
    ,'gen_'.$curr_obj.'.message'
    ,'gen_'.$curr_obj.'.time'
    ,'gen_'.$curr_obj.'.action'
];



$listing['fields'] = [
                   'id'     => ['label' => t('OD')]
                    ,'channel'     => ['label' => t('Canal')]
                    ,'level'     => ['label' => t('Niveau'),'callback'=>'getLevelView']
                    ,'message'     => ['label' => t('Message'),'advanced_callback'=>'FormatCode']
                    ,'time'     => ['label' => t('Date'),'callback'=>'dateTime']
                    ,'action'     => ['label' => t('Action')]
                    
];
$listing['fields']['edit']=false;
$listing['fields']['delete']=false;


if(isset($_SESSION['filter'][$curr_obj]) && $_SESSION['filter'][$curr_obj]){
    foreach($_SESSION['filter'][$curr_obj] as $k=>$v){
        $listing['conditions'][$k]=$v;
    }
}

$listing['title'] = t('Liste des log');
$_GET['orderby']='id';
    ?>
    <?php 
    include(GABOX_GEN_ROOT.'genlist.php');
include(GABOX_BACK_ROOT.'footer.php');
function dateTime($timestamp){
 return date("d/m/Y - H:i:s",$timestamp);
}

function FormatCode($array){
    $extra = ($array['values']['level'] != 400) ? 'style="color:green"' : '';
    return '<code '.$extra.' class="language-markup"> '.htmlentities($array['values']['message']).'</code>';
}
function getLevelView($str){
 switch($str){
    case '200':
        return '<span class="badge badge-success">'.t("Info").'</span>';
    break;

    case '400':
        return '<span class="badge badge-warning">'.t("Error").'</span>';
    break;

 }
}
?>
<style>
td.time{
width:185px;
}
td.message{
white-space:normal !important;
}
</style>
