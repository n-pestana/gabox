<?php
include(GABOX_BACK_ROOT.'header.php');

$curr_obj='admins';


# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    include('admin-fields.php');
    $form->lib= t("Edition d'administrateur");
    $form->heading= '';
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
# Listing
else{

    $listing['sql']= array(
              'gen_admins.id_'.$curr_obj
              ,'gen_admins.email'
              ,'gen_admins.phone'
              ,'gen_admins.firstname'
              ,'gen_admins.is_valid as is_valid'
            ,'gen_usertypes.lib as usertype'
    );

    $listing['fields']= array(
        'email' => array(
            'label'         => t("E-mail")
            ,'edit_in_place'=>'true'
        )
        ,'phone' => array(
            'label'         => t("Tél.")
            ,'edit_in_place'=>'true'
        )
        ,'firstname' => array(
            'label'         => t("Prénom")
                ,'edit_in_place' => true
        )
       ,'is_valid' => array(
            'label'         => t("Publié ?")
            ,'callback'=>'getEnable'
            ,'edit_in_place'=>'true'
            ,'type'=>'switch'
        )
        ,'usertype'=> array(
            'label' => t("Type")
        )
       
    );

    if(defined('ID_SITE')){ 
        $listing['table_join']      = array('gen_admins');
//        $listing['join_conditions'] = ' gen_admins.id_site='.ID_SITE;
    }
    $listing['table_join'] = array('gen_usertypes');
    $listing['join_conditions'] = 'gen_admins.usertype=gen_usertypes.id_usertypes';

    if($_SESSION['admins']['usertype']!=1){
 //       $listing['join_conditions'] .= ' and gen_admins.usertype !=1';
    }

    if(!empty($restrictions)) $listing['join_conditions']  .= ' and gen_admins.id_admins='.$_SESSION['admins']['id_admins'];

    if(defined('ID_SITE')){ 
  //    $listing['join_conditions'].= ' and gen_admins.id_sites='.ID_SITE;
    }

    $listing['title'] = t("Liste des administrateurs");
    include(GABOX_GEN_ROOT.'genlist.php');
}

include(GABOX_BACK_ROOT.'footer.php');
