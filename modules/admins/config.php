<?php
$mod['name']       = t("Administrateurs");
$mod['desc']       = t("Module de gestion des administrateurs");
$mod['fa-icon']    = 'fa-solid fa-user-unlock';
$mod['version']    = '1.0';


    $mod['bo-hook'] = ['lib'  => t("Administrateurs"),
                       'icon' => 'fa-solid fa-user-unlock'
                       ,'links' => [
                        ['link'=>'/modules/admins/admin.php?id_admins='.$_SESSION['admins']['id_admins'],'lib'=>t("Mon Compte")]
                        ,['link'=>'/modules/admins/admin.php','lib'=>t("Lister les administrateurs")]
                        ,['link'=>'/modules/admins/stats.php','lib'=>t("Activités")]
                        ,['link'=>'/modules/usertypes/admin.php','lib'=>t("Lister les groupes")]
                        ,['link'=>'/login.php?logout=1','lib'=>t("Me deconnecter")]
                        ]
                    ];

