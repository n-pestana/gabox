<?php
include(GABOX_BACK_ROOT.'header.php');

$curr_obj='backevents';


    $listing['sql']= array(
              'id_'.$curr_obj
              ,'created_at'
              ,'lib'
              ,'type'
    );

    $listing['fields']= array(
        'type' => array(
            'label'         => t("Type")
            ,'callback'         => "showType" 
        )
        ,'created_at' => array(
            'label'         => t("Date")
            ,'callback'         => "DateFr"
        )
        ,'lib' => array(
            'label'         => t("Evenement")
            ,'callback' => 'showLogin'
        )

      
    );

    $listing['fields']['delete']= false;
    $listing['fields']['edit']= false;

    function showLogin($t){
        $a = explode(':',$t);
        return '<b>'.$a[1].str_replace(' , , ','',$a[2]).'</b> - '.$a[0];
    }

    function showType($t){
        switch($t){
            case 'MSG':
            case 'WARNING':
                $out = '<span class="badge badge-success">'.$t.'</span>';
            break;
            default:
                $out = '<span class="badge badge-warning">'.$t.'</span>';
            
        }
        return $out;
    } 


    $listing['title'] = t("Activités des administrateurs");
    include(GABOX_GEN_ROOT.'genlist.php');


include(GABOX_BACK_ROOT.'footer.php');
