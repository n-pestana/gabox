<?php
$form->fields= array(
    '_cat1'    => t("Informations")
    ,'firstname'=>array_merge($form->gen_field['lib'],array('label'=>t("Prénom")))
    ,'lastname'=>array_merge($form->gen_field['lib'],array('label'=>t("Nom")))
    ,'email'=>array_merge($form->gen_field['lib'],array('label'=>t("E-mail")))
    ,'is_valid'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Activé ?")))
    ,'phone'=>array_merge($form->gen_field['lib'],array('label'=>t("Tél.")))
    ,'password'=>array_merge($form->gen_field['password'],array('label'=>t("Mot de passe")))
    ,'signature'=>array_merge($form->gen_field['html'],array('label'=>t("Signature")))
    ,'_cat20'=>t("Permissions")
    ,'usertype'=>array_merge($form->gen_field['select'],array('label'=>t("Niveau")),array('arrayValues'=>$gen->collector['usertypes']->getKv(
       ((isset($_SESSION['admins']['usertype']) && $_SESSION['admins']['usertype']!=1) ?     (array('>id_usertypes'=>1)) : '' )
        )))
//  ,'usertype_filtered'=>array_merge($form->gen_field['select'],array('label'=>("Admin Level")),array('arrayValues'=>$this->collector['usertypes']->getKv(array('>id_usertypes'=>1))),array('class'=>'chosen-select'))
    ,'id_locales'=>array_merge($form->gen_field['select'],array('label'=>t("Langue favorite")),array('arrayValues'=>$gen->collector['locales']->getKv(array('is_master'=>true))),array('placeholder'=>t("Selectionnez une langue")))
    ,'id_sites'=>array_merge($form->gen_field['select'],array('label'=>t("Bloqué sur un seul site ?")),array('arrayValues'=>$gen->collector['sites']->getKv()),array('placeholder'=>t("Tous les sites")))
    ,'updated_at'=>$form->gen_field['updated_at']
);
