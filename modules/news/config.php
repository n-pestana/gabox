<?php
$mod['name']       = 'Actualités';
$mod['desc']       = 'Ce module permet de gérer des actualités (lib, desc, media)';
$mod['fa-icon']    = 'newspaper';
$mod['version']    = '1.0';

$mod['sql'][]      = "
CREATE TABLE IF NOT EXISTS `gen_news` (
  `id_news` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sites` mediumint(8) UNSIGNED NOT NULL,
  `lib` varchar(255) NOT NULL,
  `content` mediumtext,
  `file` varchar(255) DEFAULT '',
  `date_lib` date DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `url` varchar(1024) DEFAULT '',
  `item_order` mediumint(9) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  FOREIGN KEY (id_sites) REFERENCES gen_sites(id_sites)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
";
$mod['sql'][]      = "
CREATE TABLE IF NOT EXISTS `gen_news_url` (
  `id_news_url` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_locale` int(11) NOT NULL,
  `id_site` mediumint(8) UNSIGNED NOT NULL,
  `id_news` int(11) UNSIGNED NOT NULL,
  `id_treeview` int(11) NOT NULL,
  `is_canonical` tinyint(1) DEFAULT '0',
  `url` varchar(1024) NOT NULL,
  FOREIGN KEY (id_site) REFERENCES gen_sites(id_sites),
  FOREIGN KEY (id_news) REFERENCES gen_news(id_news),
  KEY `getCanonicalFront` (`id_locale`,`id_site`,`is_canonical`,`id_news`),
  KEY `id_locale` (`id_locale`,`url`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
";

$mod['unsql'] = "DROP TABLE gen_news;";