<?php
require_once(COMMON . 'urls.trait.php');

class News extends gen_news {
    use urls;
    
    // Limit of characters after which content is cut for resume + [...]
    public $_limitResume = 135;
    // Total of news displayed on one page
    public $_elementsPerPage = 20;

    public function __construct() {
        // On n'utilise pas de champ treeview comme les news ne sont pas catégorisées, mais un hook correspondant à une entrée de treeview
        $this->setTreeviewField();
        $this->setTreeviewHook('news');
    }
    
    public function set($primary_key_value = '' , $array_fields = '', $criteres = array()) {
        if(array_key_exists('url', $array_fields) && empty($array_fields['url'])){
            $array_fields['url'] = strtolower(urlize2($array_fields['lib'])).'.html';
        }
       $id = parent::set($primary_key_value, $array_fields, $criteres);
       return $id;
    }
}