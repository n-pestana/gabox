<?php
$form->fields = [
    '_info'         => t("Informations")
    ,'lib'          => $form->gen_field['lib_i18n']
    ,'content'      => array_merge($form->gen_field['simple_html_i18n'], ['label' => t("Contenu")])
    ,'date_lib'     => array_merge($form->gen_field['calendar'], ['label' => t("Date format YYYY-MM-DD")])
    ,'file'         => array_merge($form->gen_field['file'], ['label' => t("Media")])
    ,'_publication' => t("Publication")
    ,'url'          => array_merge($form->gen_field['lib'], ['label' => t("Url") . ' ' . t("(auto fill)")])
    ,'enabled'      => $form->gen_field['ouinon']
    ,'id_sites'     => array_merge($form->gen_field['select'], ['label' => t("Site")],
                        ['arrayValues'=>$gen->collector['sites']->getKv()],
                        ['class'=>'chosen-select'],
                        ['value' => defined('ID_SITE') ? ID_SITE : '']) 
    ,'updated_at'   => $form->gen_field['updated_at']
    ];
