<?php
include(GABOX_BACK_ROOT.'/header.php');
$curr_obj = 'news';

# Edit
if (isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])) {
    include('admin-fields.php');
    $form->lib     = t("Edit news");
    $form->heading = '';
    include(GABOX_GEN_ROOT.'gen_edit.php');
} else { # Listing
    $listing        = ['sql','table_join','join_conditions','fields','title'];
    $listing['sql'] = ['gen_'.$curr_obj.'.id_'.$curr_obj
        ,'gen_'.$curr_obj.'.lib'
        ,'gen_'.$curr_obj.'.date_lib'
        ,'gen_'.$curr_obj.'.enabled'
        ,'gen_'.$curr_obj.'.url'
        ,'gen_sites.lib AS site_lib'
    ];

    $listing['fields'] = ['lib'      => ['label' => t("Title"), 'edit_in_place' => true]
                        , 'date_lib' => ['label' => t("Date"), 'edit_in_place' => true]
                        , 'enabled'  => ['label' => t("Publié ?")
                            ,'type'         => 'switch'
                            ,'callback'     => 'getEnable'
                            ,'edit_in_place'=> 'true'
                        ]
                        , 'site_lib' => ['label' => t("Site")]
    ];
        
    $listing['title'] = t("News list");
    include(GABOX_GEN_ROOT.'genlist.php');
}
include(GABOX_BACK_ROOT.'/footer.php');
