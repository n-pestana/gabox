#!/bin/bash

if [ $# -lt 1 ]; then
    echo "You must enter the name of your module"
else
    echo "Gabox module creator: [$1]"
    if [ -d "./$1" ] 
    then
        echo "Error: the module $1 already exists" 
    else
        echo "\nCreating ..."
        mkdir "./$1"
        config="./$1/config.php"
        admin="./$1/admin.php"

        echo "<?php" >> $config
        echo "\$mod['name']       = 'Add you title';" >> $config
        echo "\$mod['desc']       = 'Add your description';" >> $config
        echo "\$mod['fa-icon']    = 'comment';" >> $config
        echo "\$mod['version']    = '1.0';" >> $config
        echo "\$mod['sql'][]        =' " >> $config
        echo "CREATE TABLE \`gen_$1\` (" >> $config
        echo "  \`id_$1\` int(10) UNSIGNED NOT NULL," >> $config
        echo "  \`created_at\` datetime DEFAULT NULL," >> $config
        echo "  \`updated_at\` datetime DEFAULT NULL," >> $config
        echo "  \`id_site\` int(11) DEFAULT NULL," >> $config
        echo "  \`enabled\` tinyint(1) DEFAULT 0," >> $config
        echo "  \`lib\` varchar(1024) DEFAULT NULL" >> $config
        echo ") ENGINE=InnoDB DEFAULT CHARSET=utf8;';" >> $config
        echo "" >> $config
        echo "\$mod['sql'][] = 'ALTER TABLE \`gen_$1\` ADD UNIQUE(\`id_$1\`);';" >> $config
        echo "\$mod['sql'][] = 'ALTER TABLE \`gen_$1\` CHANGE \`id_$1\` \`id_$1\` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;';" >> $config
        echo "\$mod['unsql'] = ' DROP TABLE gen_$1;';" >> $config
        
        echo "\nDone !"
    fi
fi
    




