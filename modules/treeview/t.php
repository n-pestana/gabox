<?php
include(GABOX_BACK_ROOT.'header.php');
$curr_obj='treeview';


# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    $fields = array(
        '_cat1' => 'Informations'
        ,'id_'.$curr_obj
        ,'lib'
        ,'content'
        ,'is_valid'
        ,'id_faqcat'
        ,'updated_at'

    );
    $form->lib= 'FAQ post editing';
    $form->heading= 'FAQ post depends on the FAQ Categories module';
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
# Listing
else{
    $array = array(
          'id_'.$curr_obj
          ,'updated_at'
          ,'is_valid'
         ,'lib'
    );

    $fieldsOptions = array(
        'id_'.$curr_obj => array(
            'label'         => ("ID")
            ,'label'         =>'<input style="border:inset 1px;" name="allbox" type="checkbox" value="Check All" onclick="CheckAll(document.checkall);" />ID' 
            ,'callback' => 'myCheckbox'
        )
        ,'updated_at' => array(
            'label'         => ("Updated at")
        )
       ,'is_valid' => array(
            'label'         => ("Publié ?")
        ,'callback'=>'getEnable'
            ,'edit_in_place'=>'true'
            ,'type'=>'switch'
        )
     
        ,'lib' => array(
            'label'         => ("Name")
        ,'edit_in_place'=>true
        ) 

    );

    $extra_conf['title'] = 'FAQ post editing listing';
    include(GABOX_GEN_ROOT.'genlist.php');
}

include(GABOX_BACK_ROOT.'footer.php');
