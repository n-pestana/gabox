<?php
$mod['name']       = t("Menus & Catégories");
$mod['desc']       = t("Module de gestion des catégories");
$mod['fa-icon']    = 'fa-light fa-list-tree';
$mod['version']    = '1.0';
$mod['link']       = '/modules/treeview/treeview.php';

// Gestion des treeview disponibles pour un site
// Si le collector sites_treeview existe et si nous sommes en multisite
if(!isset($plateforme)) $plateforme = $gen->collector['plateforme']->getOneCached();
$rootElements = [];
if (isset($GLOBALS['gen']->collector['sites_treeview']) && defined('ID_SITE') && $plateforme['is_multisite'] == 1) {
    $results = $GLOBALS['gen']->collector['sites_treeview']->getCached(['id_sites' => ID_SITE]);
    $availableTreeviews = [];
    if (isset($results['id_treeview'])) {
        $availableTreeviews[] = $results['id_treeview'];
    } elseif (is_array($results) && count($results)>0) {
        foreach($results as $treeview) {
            $availableTreeviews[] = $treeview['id_treeview'];
        }
    }
    if (count($availableTreeviews) > 0) {
        $rootElements = $GLOBALS['gen']->collector['treeview']->get(['id_treeview' => $availableTreeviews], 'ORDER BY int_order');
    }
} else {
    $rootElements = $GLOBALS['gen']->collector['treeview']->get(['id_parent' => 0], 'ORDER BY int_order');
}
if (is_array($rootElements) && count($rootElements) > 0) {
    $mod['bo-hook'] = ['lib'  => t("Menu & Catégories"),
                       'icon' => 'tree'
                    ];
    $mod['bo-hook']['links'] = [];
    if (!defined('ID_SITE') || (!isset($plateforme['is_multisite']) || $plateforme['is_multisite'] != 1)) {
        $mod['bo-hook']['links'] = [['link' => '/modules/treeview/treeview.php',  'lib' => t("Tous")]];
    }
    if(isset($rootElements) && is_array($rootElements)){
        foreach ($rootElements as $rootElement) {
            $mod['bo-hook']['links'][] = ['link' => '/modules/treeview/treeview.php?parent-id=' . $rootElement['id_treeview'],  'lib' => $rootElement['lib']];
        }
    }
}
