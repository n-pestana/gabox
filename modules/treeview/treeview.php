<?php
$curr_obj='treeview';
if($gen->collector[$curr_obj] === null){
  die('The module ['.$curr_obj.'] looks to not be available');
}

$cnt = $gen->collector['treeview']->getCount();
if($cnt == 0){
    $gen->collector['treeview']->set(array('id_parent'=>0,'lib'=>'Main menu'));
    $gen->collector['treeview']->set(array('id_parent'=>0,'lib'=>'Footer menu'));
}

# regeneration des urls à la volée, trop lent si bcp de treeview et si multilingue: parametrable dans le bo-plateforme
$plateforme = $gen->collector['plateforme']->getFirst();
if(isset($plateforme['autoupdate_url']) && $plateforme['autoupdate_url'] ==1){
 include(GABOX_BACK_ROOT.'/gen_admin/ajax/buildurl.php');
}

$obj[$curr_obj]= ($gen->collector[$curr_obj]);
if(isset($_POST) && !empty($_POST)){
    $r = $gen->collector['form']->savePostedDataByForm($curr_obj,$curr_obj);
    $notify=$curr_obj;
}

include(GABOX_BACK_ROOT.'header.php');
//die(' a partir de tout treeview, possibilité de lier à un group de cat pour listing');

if(defined('ID_SITE')){ $join_conditions=' and id_site='.ID_SITE; }

?>
<?php $available_locales  = ($gen->collector['locales']->get('','is_master desc ,lib asc')); ?>
<?php $locale['id_locales'] = '0';?>
<div class="grid_12 col-md-12 pcoded-content card">
<h3><?=t("Menus & catégories")?></h3>

<div class="card-body">
<?php 

$restrictions = (isset($gen->collector['form']->restrictions) && !empty($gen->collector['form']->restrictions)) ? $gen->collector['form']->restrictions : '';
$conditions = [];
$parentHook = isset($_GET['parent-hook']) ? $_GET['parent-hook'] : null;
$parentId = isset($_GET['parent-id']) ? $_GET['parent-id'] : null;
if (!empty($parentId)) {
    $gen->collector['treeview']->setRetrieveOnlyValidChildren(false);
    $conditions['hook'] = [$parentId];
    $parentNode = $gen->collector['treeview']->getOne($parentId);
} elseif (!empty($parentHook)) {
    $gen->collector['treeview']->setRetrieveOnlyValidChildren(false);
    $conditions['hook'] = $parentHook;
    $filters = ['hook' => $parentHook];
    if (isset($gen->collector['sites_treeview']) && !empty($gen->collector['treeview']->getAffectedTreeviews())) {
        $filters = array_merge($filters, ['id_treeview' => $gen->collector['treeview']->getAffectedTreeviews()]);
    }
    $parentNode = $gen->collector['treeview']->get($filters);
    if (count($parentNode) > 1) {
        $errorMsg = t("Error : Multiple parent nodes found. Try to select a site.");
    } else {
        $parentNode = $parentNode[0];
    }
} elseif (defined('ID_SITE') && $plateforme['is_multisite'] == 1) {
    $errorMsg = t("Error : You must choose a category in the left menu.");
}
if (isset($errorMsg)) {
    echo $errorMsg;
} else {
    if(!isset($gen->collector['form']->restrictions) || empty($gen->collector['form']->restrictions)):?>
                <div class="col-sm-12">
                    <button  id="add_treeview" type="button" rel="<?=$locale['id_locales']?>" class="btn btn-primary"><i class="feather icon-plus-square "></i> <?=t("Ajouter un élément")?></button>
                    <button type="button" data-action="expand-all"  class="btn btn-primary" onclick=" $('.dd').nestable('expandAll');"><?=t("Tout déplier")?></button>
                    <button type="button" data-action="expand-all"  class="btn btn-primary" onclick=" $('.dd').nestable('collapseAll');"><?=t("Tout plier")?></button>
                </div>
                </div>
                <div class="row">
    <?php endif ?>

                <div class="col-sm-12">

                    <div class="cf nestable-lists" style="border:0px">
                <?php if(!empty($restrictions)):?>
                            <div class="dd" id="nestable-treeview-ro">
                <?php else:?>
                            <div class="dd" id="nestable-treeview">
                <?php endif ?>
                        <?php 
                        $gen->collector['treeview']->displayAdmin($restrictions, $conditions, !empty($parentHook) || !empty($parentId) ? $parentNode['level'] : 1); 
                        echo $gen->collector['treeview']->out 
                        ?>	
                            </div>
                    </div>
                    <textarea style="display:none" id="nestable-output"></textarea>
                    <input type="hidden" id="lastid" value="<?=$gen->collector['treeview']->GetLastInsertId()?>">
    <?php if (isset($parentNode)) { ?>
                    <input type="hidden" id="parentLevel" value="<?=$parentNode['level']?>">
                    <input type="hidden" id="parentOrder" value="<?=$parentNode['int_order']?>">
    <?php } ?>
                            </div>
<?php } ?>
<script>
// activate Nestable for list 2
    

</script>
<style>
p.edit_in_place {
    margin: 0px;
    padding: 0px;
    overflow: hidden;
    position: absolute;
    right: 6px;
    margin-top: -31px;
}
/**
 * Nestable
 */

.dd {
  position: relative;
  display: block;
  margin: 0;
  padding: 0;
  max-width: 600px;
  list-style: none;
  font-size: 13px;
  line-height: 20px;
}

.dd-list {
  display: block;
  position: relative;
  margin: 0;
  padding: 0;
  list-style: none;
}

.dd-list .dd-list {
  padding-left: 30px;
}

.dd-collapsed .dd-list {
  display: none;
}

.dd-item,
.dd-empty,
.dd-placeholder {
  display: block;
  position: relative;
  margin: 0;
  padding: 0;
  min-height: 20px;
  font-size: 13px;
  line-height: 20px;
}

.dd-handle {
  display: block;
  height: 30px;
  margin: 5px 0;
  padding: 5px 10px;
  color: #333;
  text-decoration: none;
  border: 1px solid #ccc;
  background: #fafafa;
  background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
  background: -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
  background: linear-gradient(top, #fafafa 0%, #eee 100%);
  -webkit-border-radius: 3px;
  border-radius: 3px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.dd-handle:hover {
  color: #2ea8e5;
  background: #fff;
}

.dd-item > button {
  display: block;
  position: relative;
  cursor: pointer;
  float: left;
  width: 25px;
  height: 20px;
  margin: 5px 0;
  padding: 0;
  text-indent: 100%;
  white-space: nowrap;
  overflow: hidden;
  border: 0;
  background: transparent;
  font-size: 12px;
  line-height: 1;
  text-align: center;
  font-weight: bold;
}

.dd-item > button:before {
  content: '+';
  display: block;
  position: absolute;
  width: 100%;
  text-align: center;
  text-indent: 0;
}

.dd-item > button[data-action="collapse"]:before {
  content: '-';
}

.dd-placeholder,
.dd-empty {
  margin: 5px 0;
  padding: 0;
  min-height: 30px;
  background: #f2fbff;
  border: 1px dashed #b6bcbf;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.dd-empty {
  border: 1px dashed #bbb;
  min-height: 100px;
  background-color: #e5e5e5;
  background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff), -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
  background-image: -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff), -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
  background-image: linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff), linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
  background-size: 60px 60px;
  background-position: 0 0, 30px 30px;
}

.dd-dragel {
  position: absolute;
  pointer-events: none;
  z-index: 9999;
}

.dd-dragel > .dd-item .dd-handle {
  margin-top: 0;
}

.dd-dragel .dd-handle {
  -webkit-box-shadow: 2px 4px 6px 0 rgba(0, 0, 0, .1);
  box-shadow: 2px 4px 6px 0 rgba(0, 0, 0, .1);
}
/**
 * Nestable Extras
 */

.nestable-lists {
  display: block;
  clear: both;
  padding: 30px 0;
  width: 100%;
  border: 0;
  border-top: 2px solid #ddd;
  border-bottom: 2px solid #ddd;
}

#nestable-menu {
  padding: 0;
  margin: 20px 0;
}

#nestable-output,
#nestable2-output {
  width: 100%;
  height: 7em;
  font-size: 0.75em;
  line-height: 1.333333em;
  font-family: Consolas, monospace;
  padding: 5px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

#nestable2 .dd-handle {
  color: #fff;
  border: 1px solid #999;
  background: #bbb;
  background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
  background: -moz-linear-gradient(top, #bbb 0%, #999 100%);
  background: linear-gradient(top, #bbb 0%, #999 100%);
}

#nestable2 .dd-handle:hover {
  background: #bbb;
}

#nestable2 .dd-item > button:before {
  color: #fff;
}

@media only screen and (min-width: 700px) {
  .dd {
    float: left;
    width: 100%;
  }
  .dd + .dd {
    margin-left: 2%;
  }
}

.dd-hover > .dd-handle {
  background: #2ea8e5 !important;
}
/**
 * Nestable Draggable Handles
 */

.dd3-content {
  display: block;
  height: 30px;
  margin: 5px 0;
  padding: 5px 10px 5px 40px;
  color: #333;
  text-decoration: none;
  font-weight: bold;
  border: 1px solid #ccc;
  background: #fafafa;
  background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
  background: -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
  background: linear-gradient(top, #fafafa 0%, #eee 100%);
  -webkit-border-radius: 3px;
  border-radius: 3px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.dd3-content:hover {
  color: #2ea8e5;
  background: #fff;
}

.dd-dragel > .dd3-item > .dd3-content {
  margin: 0;
}

.dd3-item > button {
  margin-left: 30px;
}

.dd3-handle {
  position: absolute;
  margin: 0;
  left: 0;
  top: 0;
  cursor: pointer;
  width: 30px;
  text-indent: 100%;
  white-space: nowrap;
  overflow: hidden;
  border: 1px solid #aaa;
  background: #ddd;
  background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
  background: -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
  background: linear-gradient(top, #ddd 0%, #bbb 100%);
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}

.dd3-handle:before {
  content: '≡';
  display: block;
  position: absolute;
  left: 0;
  top: 3px;
  width: 100%;
  text-align: center;
  text-indent: 0;
  color: #fff;
  font-size: 20px;
  font-weight: normal;
}

.dd3-handle:hover {
  background: #ddd;
}


.shortcuts-list li {
height: 105px;
width: 80px;
}
div.actions{
float: left;
    left: 100%;
    margin-left: -45px;
    position: relative;
    top: -30px;
    width: 50px;
}
a.edit{

 margin-right:100px;
}
a.editbutton{
    font-size: 12px;
    border-width: 1px;
    cursor: pointer;
    display: inline-block;
    line-height: 12px;
    position: absolute;
    right: 28px;
    font-weight:normal;
    top: 4px;
}
                
.editbutton:hover{
 box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
}
.treeviewDel{
    margin-right:30px;
    
}
.shortcuts-list li {
height: 105px;
width: 120px;
}

/**/
.dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd3-content:hover { color: #2ea8e5; background: #fff; }

.dd-dragel > .dd3-item > .dd3-content { margin: 0; }

.dd3-item > button { margin-left: 30px; }

.dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
    border: 1px solid #aaa;
    background: #ddd;
    background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:         linear-gradient(top, #ddd 0%, #bbb 100%);
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
.dd3-handle:hover { background: #ddd; }


</style>


<div id="gen_edit"> </div>
</div>
</div>
<script>


</script>
<?php include(GABOX_BACK_ROOT.'footer.php');
