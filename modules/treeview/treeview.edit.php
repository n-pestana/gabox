<?php 
include(GABOX_BACK_ROOT.'header.php');

$curr_obj='treeview';


if(defined('ID_SITE')){ 
  $table_join = array('gen_treeview');
  $join_conditions = ' gen_treeview.id_site='.ID_SITE;
}


# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    $form->fields = array(
      '_cat1'=>t("Informations")
      ,'lib'=>array_merge($form->gen_field['lib_i18n'],array('label'=>t("Libellé *"), 'pattern' => '^.+'))
      ,'description'=>array_merge($form->gen_field['lib_i18n'],array('label'=>t("Description"),'tooltip'=>t("Champ facultatif"), 'maxlength' => ''))
      ,'controller'=>array_merge(isset($form->gen_field['controllers']) ? $form->gen_field['controllers'] : [],array('label'=>t("Controller"),'tooltip'=>t("Les controllers se trouvent dans le repertoire controller de votre projets")))
      ,'url'=>array_merge($form->gen_field['lib_i18n'],array('label'=>t("Url"),'tooltip'=>t("Vous pouvez saisir une url personnalisée, ou laisser vide pour que Gabox la génère pour vous")))
      ,'get_parameters'=>array_merge($form->gen_field['lib_i18n'],array('label'=>t("Paramètres GET"),'tooltip'=>t("Vous pouvez saisir des paramètres GET supplémentaires par ex: ?lorem=ipsum&dolor=sit%20amet")))
      ,'fullurl'=>array_merge($form->gen_field['lib_i18n'],array('label'=>t("Url complète"),'readonly'=>true,'tooltip'=>t("Ceci est l'url générée en fonction de la hierarchie de votre élément")))
      ,'is_valid'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Activé ?")))

      ,'_cat4'=>t("SEO")
      ,'meta_title'=>array_merge($form->gen_field['lib_i18n'],array('label'=>t("Balise Meta Title")))
      ,'meta_desc'=>array_merge($form->gen_field['lib_i18n'],array('label'=>t("Balise Meta Description")))
      ,'seo_callback' => array_merge($form->gen_field['lib'], [
          'label' => ("Callback"),
          'tooltip' => ("Fonction de callback au format 'className::method' ou 'function'<br /> Permet de récupérer un tableau de variables qui sera utilisé pour remplacer les parties dynamiques des title et meta-description<br /> Exemple : Actualité {title} + callback qui retourne ['title' => 'titre actu']")
      ])

      ,'_cat6'=>t("Options")
      ,'hook'=>array_merge($form->gen_field['lib_i18n'],array('label'=>t("Hook"),'tooltip'=>t("Ceci est une étiquette textuelle pour pouvoir appeler cet éléments simplement pour le développement.<br />Vous pouvez saisir un nom personnalisé, ou le laisser vide pour que Gabox le génère pour vous")))
      ,'file'=>array_merge($form->gen_field['file_i18n'],array('label'=>t("Bandeau")))
      ,'href'=>array_merge($form->gen_field['lib_i18n'],array('label'=>t("Lien externe")))
      ,'class'=>array_merge($form->gen_field['lib'],array('label'=>t("Classe CSS")))

      ,'_cat60'=>t("Visibilité")
      ,'show_in_menu'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Montrer dans le menu ?")))
      ,'show_in_filter'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Montrer dans les filtres ?")))
      ,'show_in_search'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Montrer dans la recherche ?")))
      ,'id_link_treeview'=>array_merge($form->gen_field['select'],array('label'=>t("Lien vers un menu existant")),array('placeholder' => t("Merci de choisir un noed de destination"), 'arrayValues'=>$gen->collector['treeview']->getKandMore(array('lib', 'hook'))),array('class'=>'chosen-select'))
      ,'id_site'=>$form->gen_field['id_site']

      ,'_catavancee' => t("Configuration avancée")
      ,'url_dynamicpart' => array_merge($form->gen_field['lib'], [
          'label' => t("Partie dynamique de l'URL"),
          'tooltip' => t("La partie dynamique de l'URL contient des portions qui seront automatiquement matchées avec des variables.<br /> Mettre les variables entre accolades. Exemple : /{mavar}/{mavar2}<br /> Pour les portions facultatives, ajouter un ? aux séparateurs, par exemple : /{mavar}/?{mavar2}")
      ])
      ,'url_callback' => array_merge($form->gen_field['lib'], [
          'label' => t("Callbacks URL"),
          'tooltip' => t("Fonctions de callback au format 'className::method' ou 'function'<br /> Préciser une callback par élément de l'URL dynamique et les séparer par des virgules (laisser vide entre 2 virgules si pas de callback pour une variable)<br /> La callback recevra en paramètre la partie de l'URL (permet par exemple de trouver un id à partir de l'URL)<br /> Exemple: <br /> getIdForVar1<br /> <br /> MaClass::getIdForVar3")
      ])
    );


    if(isset($_GET['notreebuild']) || isset($_SESSION['notreebuild'])) {
        $gen->collector['treeview']->setBuildOnSave(false);
        $_SESSION['notreebuild'] = 1;
    }

    $form->lib= t("Edition d'élément d'arbre");
    $form->heading= '';
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
include(GABOX_BACK_ROOT.'footer.php');
