<?php
 
$treeviewCache = [];
$pagesData = ['' => t("- Sélectionnez une page parente -")];

$pagesList = $gen->collector['pagesbuilder']->aget([
    'conditions' => [ 'id_sites' => ID_SITE ],
    'translate' => true
]);

if($pagesList)foreach ($pagesList as $page) {

    if (! empty($page['id_treeview'])) {

        if(! isset($treeviewCache[$page['id_treeview']])) {   
            // get the treeview associated with the page
            $pageTreeview = $gen->collector['treeview']->getOne($page['id_treeview']);        
            // set the treeview name in the treeview cache
            $treeviewCache[$page['id_treeview']] = $pageTreeview['lib'];
        }

        $libCategory = $treeviewCache[$page['id_treeview']];

        $pagesData[$page['id_pagesbuilder']] = $page['lib'] . ' - ' . $treeviewCache[$page['id_treeview']];
    };
};


 $form->fields = array(
        '_cat1'         => t("Informations")
        ,'id_sites'     => $form->gen_field['id_sites']
        ,'lib'          => array_merge($form->gen_field['lib_i18n'],array('label'=>t("Libellé")))
        ,'url'          => array_merge($form->gen_field['lib_i18n'],array('label'=>t("Url"),'tooltip'=>t("Vous pouvez saisir une url personnalisée, ou laisser vide pour que Gabox la génère pour vous")))
        ,'type'         => array_merge($form->gen_field['select'],array('label'=>t("Type de contenu")),array('arrayValues'=>array('post'=>'post','page'=>'Page')))
        ,'picture'          => array_merge($form->gen_field['file_i18n'], ['label' => t("Vignette")])
        ,'id_treeview'  => array_merge($form->gen_field['select'], ['label' => t("Catégorie"), 'arrayValues' => $gen->collector['treeview']->getKV()])
        ,'id_parent'    => array_merge($form->gen_field['select'],['label' => t("Contenu parent"), 'arrayValues' => $pagesData ])
        ,'n_order'      => array_merge($form->gen_field['lib'], ['label' => t("Ordre d'affichage"), 'pattern' => "\d*"])
        ,'_SEO'         => 'SEO'
        ,'meta_title'   => array_merge($form->gen_field['lib'],array('label'=>t("Meta title")))
        ,'meta_desc'    => array_merge($form->gen_field['desc'],array('label'=>t("Meta desc")))
    );
