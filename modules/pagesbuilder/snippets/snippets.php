<?php
if (is_dir(GABOX_USR_DIR.'/etc/pagesbuilder') && file_exists(GABOX_USR_DIR.'/etc/pagesbuilder/snippets/snippets.php')) {
    include GABOX_USR_DIR . '/etc/pagesbuilder/snippets/snippets.php';
} else {
    include 'snippets_default.php';
}