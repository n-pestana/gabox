let id_pagesbuilder, id_pagesbuilder_draft, id_pagesbuilder_i18n, id_pagesbuilder_draft_i18n

$( document ).ready(function() {
    var aceeditor = setAceCodeEditor();
    // par defaut c'est le composer qui est activé
    var kceditor = setKCEditor();
    id_pagesbuilder  = $("#id_pagesbuilder").val();
    id_pagesbuilder_draft = $("#id_pagesbuilder_draft").val();
    id_pagesbuilder_i18n  = $("#id_pagesbuilder_i18n").val();
    id_pagesbuilder_draft_i18n = $("#id_pagesbuilder_draft_i18n").val();
    var current_tab= 'composer-tab';

    // Sauvegarde du brouillon au changement de langue
    $('#pb_locale_selector a[class!="active"]').on('click', function (){
        saveCurrentDraft($("#editor-tab").find('.active').attr('id'),aceeditor,kceditor);
    });

    $(".ajax-tab").click(function(e){
        e.preventDefault();

        var loadurl = $(this).attr('href');
        var targ = $(this).attr('data-target');

        $.ajaxSetup({async:false});

        // action sur le tab que l'on quitte
        saveCurrentDraft($("#editor-tab").find('.active').attr('id'),aceeditor,kceditor);
        var id= $(this).attr('id');

        $.get(loadurl, function(data) {
            $(targ).html(data)
            current_tab = id;
            // action sur le tab où l'on entre
            switch(id){
                case 'composer-tab':
                    kceditor = setKCEditor();
                break;

                case 'code-tab':
                    aceeditor = setAceCodeEditor();
                break;

                case 'preview-tab':
                break;
            }
        });

        $(this).tab('show')

    });

    $('#publishPage').click(function(){
        publishPage(current_tab,aceeditor,kceditor);
    })

    $('#back-button').click(function(){
        saveCurrentDraft(current_tab,aceeditor,kceditor);
    })

    $("#saveDraft").click(function(){
        saveCurrentDraft(current_tab,aceeditor,kceditor);
    })

});

function setAceCodeEditor(){
    let htmlEditor;
    ace.require("ace/ext/language_tools");
    let beautify = ace.require("ace/ext/beautify");

    let txt = document.createElement("textarea");
    txt.innerHTML = $("#editor").html();
    htmlEditor = ace.edit("editor");
    htmlEditor.getSession().setMode("ace/mode/html");
    htmlEditor.setTheme("ace/theme/merbivore");
    htmlEditor.setOptions({
        maxLines: 100,
        fontSize: 16,
        enableBasicAutocompletion: true,
        enableLiveAutocompletion: true
    });

    htmlEditor.setShowPrintMargin(false);
    htmlEditor.setHighlightActiveLine(false);
    htmlEditor.getSession().setValue(txt.value);
    beautify.beautify(htmlEditor.session);
    return htmlEditor;
}

function setKCEditor(){
        kceditor =  $('#defaultPage').keditor({
           language: 'fr'
        });

        return kceditor;
}

function saveCurrentDraft(current_tab,aceeditor,kceditor){
    $.ajaxSetup({async:false});
    let content;
    switch (current_tab) {
        case 'composer-tab':
            content = kceditor.getContent().replace(/(<LINK([^>]+)>)/gi, "");
            let beautify = ace.require("ace/ext/beautify");
            aceeditor.getSession().setValue(content.replace(new RegExp('&nbsp;', 'gi'), ' '));
            beautify.beautify(aceeditor.session);
        break;
    }
    content = aceeditor.getSession().getValue();
    let display_notify = false;
    $.post( "#", {
        id_pagesbuilder_draft: $("#id_pagesbuilder_draft").val(),
        id_pagesbuilder_draft_i18n: $("#id_pagesbuilder_draft_i18n").val(),
        html: content
    }).done(function(data) {
        if (data.length > 0) {
            let result = data.split('||');
            let selector = '#id_pagesbuilder_draft' + result[1];
            id_pagesbuilder_draft = result[0];
            if (id_pagesbuilder_draft.length && id_pagesbuilder_draft != $(selector).val()) {
                $(selector).val(id_pagesbuilder_draft);
                display_notify = true;
            }
        }
    });
    if (display_notify) {
        notify("Brouillon sauvegardé version N° " + id_pagesbuilder_draft);
    } else {
        notify("Pas de nouvelle modification, brouillon non sauvegardé");
    }
}

function EnablePublish(){
    $("#publishPage").removeAttr('disabled');
}

function DisablePublish(){
    $("#publishPage").attr('disabled','disabled');
}

function publishPage(current_tab,aceeditor,kceditor){
    saveCurrentDraft(current_tab,aceeditor,kceditor);
    $.post( "#", {
        id_pagesbuilder_draft: $("#id_pagesbuilder_draft").val(),
        id_pagesbuilder_draft_i18n: $("#id_pagesbuilder_draft_i18n").val(),
        action: 'publish'
    }).done(function( data ) {
        notify("Page publiée !");
    });

}
