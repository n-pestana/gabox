<?php
include(GABOX_BACK_ROOT.'header.php');
$curr_obj='pagesbuilder';

# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    include('admin-fields.php');
    $form->lib= t("Content editing");
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
# Listing
else{

    $listing        = ['sql','table_join','join_conditions','fields','title'];
    $listing['sql'] = [
        'id_'.$curr_obj,
        'n_order',
        'lib',
        'url',
        'is_published',
        'version',
        'id_admins',
        'type',
        'picture',
        'id_treeview',
        'updated_at',
        'id_'.$curr_obj.' as btn_edit',
        'id_locales',
        'id_sites',
    ];
    $listing['title'] = t("Comments list");

    $listing['fields'] = array(
        'version' => [
            'label' => t("Versions") . '<br />'
                . '<span class="badge badge-success">' . t("Publié") . '</span><br />'
                . '<span class="badge badge-warning">' . t("Brouillon plus récent") . '</span><br />'
                . '<span class="badge badge-danger">' . t("Non publié") . '</span><br />'
            ,
            'advanced_callback' => 'getVersions'
        ]

        ,'id_treeview' => array(
                'label'         => t("Catégorie")
               ,'callback'      => 'treeview::getLib' 
        ) 
        ,'n_order' => [
            'label' => t("Ordre"),
            'edit_in_place' => true
        ]
        ,'lib' => array(
            'label'         => t("Libellé")
            ,'advanced_callback'=>'getInfo'
        ) 
       ,'type' => array(
                'label'         => t("Type")
                ,'callback'     => "getTypeBadge"
        ) 
        ,'picture' => array(
            'label' => t("Vignette")
            ,'callback' => 'getMedia'
        )
        ,'id_admins' => array(
                'label'         => t("Utilisateur")
               ,'callback'      => 'admins::getLib' 
        ) 
        ,'updated_at' => array(
                'label'         => t("Dernière mise à jour")
                ,'callback'         => 'dateFr'
        )

        ,'is_published' => array(
            'label'         => t("Publié ?")
            ,'type'         => 'switch'
            ,'callback'     => 'getEnable'
            ,'edit_in_place'=> 'true'
        )
        , 'btn_edit' => ['label' => t("Contenu"), 'callback' => 'getEditButton', 'callbackFieldValue' => 'rowset']
        ,'edit'=>[ 'label'         => t("Propriétés") ,'href'         => '?hash='.HASH_KEY.'&id_'.$curr_obj.'=$id_'.$curr_obj ,'sortable'         => false] 


    );

    if (!empty($_GET['id_treeview'])) {
        $listing['conditions']['id_treeview'] = filter_input(INPUT_GET, 'id_treeview', FILTER_VALIDATE_INT);
    }
    $pagesTreeviews = $gen->collector['pagesbuilder']->getKAndMore(['id_treeview']);
    $categories = $gen->collector['treeview']->getKv(['id_treeview' => array_unique(array_values($pagesTreeviews))]);
    if ($categories) {
        $listing['search_extra'] = '<select class="form-control form-control-sm" name="id_treeview" onchange="this.closest(\'form\').submit();">
        <option value="">' . t("Toutes les catégories") . '</option>';
        foreach ($categories as $treeviewId => $label) {
            $selected = (isset($_GET['id_treeview']) && $treeviewId == $_GET['id_treeview']) ? ' selected' : '';
            $listing['search_extra'] .= '<option value="' . $treeviewId . '"' . $selected . '>' . $label . '</option>';
        }
        $listing['search_extra'] .= '</select>';
    }

    $extra_conf['title'] = 'Contenus';
    require_once(MODS . 'pagesbuilder/functions.php');
    include(GABOX_GEN_ROOT.'genlist.php');
}

include(GABOX_BACK_ROOT.'footer.php');
