         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" data-type="keditor-style" />
        <link rel="stylesheet" type="text/css" href="dist/css/keditor.css" data-type="keditor-style" />
        <link rel="stylesheet" type="text/css" href="dist/css/keditor-components.css" data-type="keditor-style" />
        <link rel="stylesheet" type="text/css" href="plugins/code-prettify/src/prettify.css" />
        <link rel="stylesheet" type="text/css" href="css/examples.css" />
        <link rel="stylesheet" type="text/css" href="css/gaboxbuilder.css?v=1.1" />
        <script src="assets/popper.min.js"></script>
        <script type="text/javascript" src="plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
        <script type="text/javascript" src="plugins/ckeditor-4.11.4/ckeditor.js"></script>
        <script type="text/javascript" src="plugins/formBuilder-2.5.3/form-builder.min.js"></script>
        <script type="text/javascript" src="plugins/formBuilder-2.5.3/form-render.min.js"></script>
        <script type="text/javascript" src="dist/js/keditor.js?v=1.1"></script>
        <script type="text/javascript" src="dist/js/keditor-components.js?v=1.1"></script>
        <script type="text/javascript" src="plugins/code-prettify/src/prettify.js"></script>
        <script type="text/javascript" src="plugins/js-beautify-1.7.5/js/lib/beautify.js"></script>
        <script type="text/javascript" src="plugins/js-beautify-1.7.5/js/lib/beautify-html.js"></script>

        <script  src="js/gaboxbuilder.js?<?=time()?>"></script>
        <script type="text/javascript" src="js/examples.js"></script>
        <input type="hidden" id="id_pagesbuilder" value="<?=$id_pagesbuilder?>"/>
        <input type="hidden" id="id_pagesbuilder_draft" value="<?=$id_pagesbuilder_draft?>"/>
        <input type="hidden" id="id_pagesbuilder_i18n" value="<?=$id_pagesbuilder_i18n?>"/>
        <input type="hidden" id="id_pagesbuilder_draft_i18n" value="<?=$id_pagesbuilder_draft_i18n?>"/>
        <div class="col-sm-12">
            <?php // TODO: use selected site instead of $_SERVER['id_site'] ?>
            <?php $locales = $gen->collector['sites']->getAvailabledLocalesForSite($_SERVER['id_site'], true); ?>
            <?php if (count($locales) > 1) { ?>
            <div class="row">
                <div class="col-md-12 text-right">
                    <ul class="nav nav-tabs customtab pro-customtab" id="pb_locale_selector">
                        <?php foreach ($locales as $locale) { ?>
                            <li class="nav-item"> <a class="nav-link<?=$locale['id_locales'] == $currentLocale ? ' active' : ''?>" href="<?=AddToMyRequestUrl($locale['id_locales'] != $masterPage['id_locales'] ? ['pb_locale' => $locale['id_locales']] : ['pb_locale' => NULL])?>"><span><?=$locale['lib_language']?></span></a> </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?php } ?>
            <div class="card">
                <div class="card-body">

                    <ul class="nav nav-tabs mb-3" id="editor-tab" role="tablist" >
                        <li class="nav-item ">
                            <a class="nav-link text-uppercase ajax-tab active" id="composer-tab" data-toggle="tab" data-target="#composer" href="composer.php?id_<?=$pageCollector?>=<?=${"id_$pageCollector"}?><?=$currentLocale != $masterPage['id_locales'] ? '&pb_locale=' . $currentLocale : ''?>" role="tab" aria-controls="composer" aria-selected="true">Composer</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase ajax-tab" id="code-tab" data-toggle="tab" data-target="#code" href="ajax/edit_code.php?id_<?=$pageCollector?>=<?=${"id_$pageCollector"}?><?=$currentLocale != $masterPage['id_locales'] ? '&pb_locale=' . $currentLocale : ''?>" role="tab" aria-controls="code" aria-selected="false">HTML Code</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase ajax-tab" id="preview-tab" data-toggle="tab" data-target="#preview"  href="ajax/preview.php?id_<?=$pageCollector?>=<?=${"id_$pageCollector"}?><?=$currentLocale != $masterPage['id_locales'] ? '&pb_locale=' . $currentLocale : ''?>" role="tab" aria-controls="preview" aria-selected="false">Preview</a>
                        </li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li>
                            <button id="publishPage" class="btn btn-primary noafter">Publier la page</button>
                        </li>

                        <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li>
                            <button id="saveDraft" class="btn btn-primary noafter">Sauvegarder le brouillon</button>
                        </li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>

                        <li>
                            <a href="admin.php" id="back-button" class="btn btn-primary noafter">Retour à la liste</a>
                        </li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li>
                            <a href="admin.php"  class="btn btn-warning noafter">Quitter sans sauvegarder</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="editor-tabContent">
                        <div class="tab-pane fade active show" id="composer" role="tabpanel" aria-labelledby="composer-tab">
                            <?php include('composer.php')?>
                        </div>

                        <div class="tab-pane" id="code" role="tabpanel" aria-labelledby="code-tab">
                            <div id="editor" class="col-md-12">
                            </div>
                        </div>

                        <div class="tab-pane " id="preview" role="tabpanel" aria-labelledby="preview-tab">
                            <div id="preview_content">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

