<div data-keditor="html" id="editor-content">
    <main role="main" class="landing wrapper defaultPage" id="defaultPage">
        <link rel="stylesheet" href="/assets/css/style.css?v=3">
        <?php
            $currentLocale = $gen->collector['pagesbuilder']->getEditingLocale();
            $pageCollector = $gen->collector['pagesbuilder']->getPageCollector($currentLocale);
            if (isset($_REQUEST['id_' . $pageCollector])){
                $draft = $gen->collector[$pageCollector]->getLastDraft((int)$_REQUEST['id_' . $pageCollector]);
            }
            echo $draft['content'];
        ?>
    </main>
</div>
