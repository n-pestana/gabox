<?php 
$site = $gen->collector['sites']->getOne(ID_SITE);
if ($conf['env'] === 'prod') {
    $front_url = $site[$conf['env'] . '_url'];
} else {
    $front_url = $site['internal_url'];
}
$currentLocale = $gen->collector['pagesbuilder']->getEditingLocale();
$pageCollector = $gen->collector['pagesbuilder']->getPageCollector($currentLocale);
?>

<iframe id="preview-frame" width="100%" height="100%" style="min-height:8000px" scrolling="no" 
    src="<?=$front_url?>/?id_<?=$pageCollector?>=<?=(int)$_GET['id_' . $pageCollector]?>">
</iframe>


