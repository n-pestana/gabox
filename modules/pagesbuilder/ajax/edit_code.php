<div id="editor" class="col-md-12">
<?php
$currentLocale = $gen->collector['pagesbuilder']->getEditingLocale();
$pageCollector = $gen->collector['pagesbuilder']->getPageCollector($currentLocale);
if(isset($_REQUEST['id_' . $pageCollector])){
    $draft = $gen->collector[$pageCollector]->getLastDraft((int)$_REQUEST['id_' . $pageCollector]);
    if(isset($draft['content'])){
      echo $draft['content'];
    }
}
?>
</div>
