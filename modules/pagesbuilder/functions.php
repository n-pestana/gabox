<?php
function getEditButton($id){
    return '<a href="edit.php?id_pagesbuilder='.(int)$id['id_pagesbuilder'].'" class="button btn btn-sm btn-outline-success edit"><i class="feather icon-edit"></i> '.t("Contenu").'<span></span></a>';

}
function getInfo($page){
    global $gen;
    $t = $gen->collector['treeview']->getOne($page['values']['id_treeview']);
    $site = $gen->collector['sites']->getOne(ID_SITE);
    if(empty($site['internal_url'])){
        $out = $page['values']['lib'];
        $out .= '<br /><span class="badge badge-danger">'.t("Url interne du site non définie").'</span>';
    }
    else{
        $out = '<a href="'.$site['internal_url'].$t['fullurl'].$page['values']['url'].'" target="_blank"><i class="fas fa-link"></i> '.$page['values']['lib'].'</a>';
    }
    return $out;

}
function getVersions($page){
    $val = $page['values'];
    global $gen;
    $out = '';

    $availableLocales = $gen->collector['sites']->getAvailabledIdLocalesForSite($val['id_sites']);
    if ($availableLocales) {
        $defaultlocale = $availableLocales[0];
    }
    // TODO: use selected site instead of $_SERVER['id_site']
    $locales = $gen->collector['sites']->getAvailabledLocalesForSite($_SERVER['id_site'], true);
    if (count($locales) > 1) {
        foreach ($locales as $locale) {
            $current_version = $last_draft_version = '';
            // Translated version
            if ($locale['id_locales'] != $val['id_locales'] && !is_null($val['id_locales'])) {
                $pagesbuilderI18n = $gen->collector['pagesbuilder_i18n']->getOne([
                    'id_pagesbuilder' => $val['id_pagesbuilder'],
                    'id_locales' => $locale['id_locales'],
                ]);
                if ($pagesbuilderI18n) {
                    $current_version = $pagesbuilderI18n['version'];
                    $last_draft = $gen->collector['pagesbuilder_i18n']->getLastDraft($pagesbuilderI18n['id_pagesbuilder_i18n']);
                    $last_draft_version = (isset($last_draft['id_pagesbuilder_draft_i18n'])) ? $last_draft['id_pagesbuilder_draft_i18n'] : 0;
                }
            } else {
                $current_version = $val['version'];
                $last_draft = $gen->collector['pagesbuilder']->getLastDraft($val['id_pagesbuilder']);
                $last_draft_version = (isset($last_draft['id_pagesbuilder_draft'])) ? $last_draft['id_pagesbuilder_draft'] : 0;
            }
            if (is_null($val['id_locales']) && $locale['id_locales'] != $defaultlocale) {
                $current_version = 0;
            }
            if ($current_version == $last_draft_version && $current_version != ''){
                $out .= '<span class="badge badge-success">'
                    . flagize($locale['country']) . ' ' . $current_version
                    . '</span>';
            } elseif ($current_version != '') {
                $out .= '<span class="badge badge-warning">'
                    . flagize($locale['country']) . ' ' . $last_draft_version
                    . '</span>';
            } else {
                $out .= '<span class="badge badge-danger">' . flagize($locale['country']) . '</span>';
            }
            $out .= '<br />';
        }
    } else {
        $current_version = $val['version'];
        $last_draft = $gen->collector['pagesbuilder']->getLastDraft($val['id_pagesbuilder']);
        $last_draft_version = (isset($last_draft['id_pagesbuilder_draft'])) ? $last_draft['id_pagesbuilder_draft'] : 0;

        if ($current_version == $last_draft_version && $current_version != ''){
            $out .= '<span class="badge badge-success">Version '.$current_version.'</span>';
        } elseif ($current_version != '') {
            $out .= '<span class="badge badge-warning">'.t("Version publiée").' : '.$current_version.'<br /><br />'.t("Version Brouillon").' : '.$last_draft_version.'</span>';
        } else {
            $out .='<span class="badge badge-danger">'.t("Aucune version publiée").'</span>';
            if ($last_draft_version != '') {
                $out .= '<br /><span class="badge badge-success">'.t("Version Brouillon").' : '.$last_draft_version.'</span>';
            }
        }
    }
    return $out ;
}

function getTypeBadge($type){
    if($type=='page'){
        return '<span class="badge badge-pill badge-primary">Page</span>';
    }
    if($type=='post'){
        return '<span class="badge badge-pill badge-secondary">Post</span>';
    }
}
