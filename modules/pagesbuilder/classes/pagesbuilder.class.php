<?php 
require_once(COMMON . 'urls.trait.php');
class PagesBuilder extends gen_pagesbuilder
{

    use urls;
    var $use_pages_v1_compatiblity_mode = false;

    public function set($primary_key_value = '' , $array_fields = '', $criteres = array()) 
    {
        if(is_array($array_fields) && array_key_exists('url',$array_fields) && $array_fields['url']===null ){
            $add_html = true;
            $array_fields['url'] = $this->makeUrl($array_fields, false, $add_html);
        }
        if (empty($array_fields['id_locales'])) {
            if (!isset($array_fields['id_sites'])) {
                $currentData = $this->getOne($primary_key_value);
                $array_fields['id_sites'] = $currentData['id_sites'];
            }
            $availableLocales = $this->collector['sites']->getAvailabledIdLocalesForSite($array_fields['id_sites']);
            if ($availableLocales) {
                $array_fields['id_locales'] = $availableLocales[0];
            }
        }
        $id = parent::set($primary_key_value, $array_fields, $criteres);

        if (!empty($id)) {
            $this->buildUrl($id);
        }
		return $id;
    }


    public function getLastDraft($id_pagesbuilder){
        return $this->collector['pagesbuilder_draft']->getOne(
            array(
                'id_pagesbuilder'=>$id_pagesbuilder
            )
            ,'order by id_pagesbuilder_draft desc'
        );
    }

    public function createDraftFromPage($id_pagesbuilder){

        $current =  $this->collector['pagesbuilder']->getOne(
            array(
                'id_pagesbuilder'=>$id_pagesbuilder
            )
        );
         
        return $this->collector['pagesbuilder_draft']->set('',array(
            'id_pagesbuilder'              =>  (int)$id_pagesbuilder
            ,'version'              =>  1
            ,'content'              => $current['content'] 
            ,'compiled_content'     => $current['compiled_content'] 
        ));
    }

    /**
     * Preprocess page properties before page can be used for display purposes
     * Time to get translated version of page too
     * @param array $page
     * @return array processed object
     */
    public function preprocess(array $page) {
        $pageTranslated = $this->collector['pagesbuilder_i18n']->getOneCached(['id_locales' => ID_LOC, 'id_pagesbuilder' => $page[$this->primary_key]]);
        if ($pageTranslated) {
            $page['content'] = $pageTranslated['content'];
        }

        // Photo component of keditor => replace data-idpage with an href around img tag
        preg_match_all('|<img[^>]*src="[^>]*"[^>]*data-idpage="(\d+)"[^>]*>|is', $page['content'], $matches, PREG_SET_ORDER);
        if (is_array($matches) && !empty($matches)) {
            foreach ($matches as $match) {
                $url = $this->getCanonicalURL($match[1]);
                if (!empty($url)) {
                    $temp = '<a href="' . $url . '">' . $match[0] . '</a>';
                    $page['content'] = str_replace($match[0], $temp, $page['content']);
                }
            }
        }
        // We take data-idpage put manually in source code and update href
        preg_match_all('|<a([^>]*)data-idpage="(\d+)"([^>]*)>|is', $page['content'], $matches, PREG_SET_ORDER);
        if (is_array($matches) && !empty($matches)) {
            foreach ($matches as $match) {
                $url = $this->getCanonicalURL($match[2]);
                if (!empty($url)) {
                    // We need to keep eventual other attributes, but remove existing href
                    $temp = '<a href="' . $url . '"'
                        . preg_replace('/href=".*"/', '', $match[1])
                        . preg_replace('/href=".*"/', '', $match[3])
                        . '>';
                    $page['content'] = str_replace($match[0], $temp, $page['content']);
                }
            }
        }
        // replace spaces with non breakable spaces before '?','!' and ':' chars.
        $page['content'] = preg_replace('/(\s)(?=[\?!:])/','&nbsp;', $page['content']);
        return $page;
    }

    public function getEditingLocale()
    {
        return isset($_GET['pb_locale'])
            ? filter_input(INPUT_GET, 'pb_locale', FILTER_VALIDATE_INT)
            : NULL;
    }

    public function getPageCollector(?int $locale)
    {
        return ($locale === NULL ? 'pagesbuilder' : 'pagesbuilder_i18n');
    }

    public function getDraftCollector(?int $locale)
    {
        return ($locale === NULL ? 'pagesbuilder_draft' : 'pagesbuilder_draft_i18n');
    }
}
