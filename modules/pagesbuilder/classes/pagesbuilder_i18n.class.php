<?php 
class pagesBuilder_i18n extends gen_pagesbuilder_i18n
{
    public function set($primary_key_value = '' , $array_fields = '', $criteres = [])
    {
        $id = parent::set($primary_key_value, $array_fields, $criteres);
        if (!empty($id)) {
            $this->buildUrl($id);
        }
		return $id;
    }

    public function getLastDraft($id_pagesbuilder_i18n){
        return $this->collector['pagesbuilder_draft_i18n']->getOne(
            ['id_pagesbuilder_i18n' => $id_pagesbuilder_i18n]
            ,'order by id_pagesbuilder_draft_i18n desc'
        );
    }

    public function createDraftFromPage($id_pagesbuilder_i18n){
        $current = $this->collector['pagesbuilder_i18n']->getOne([
            'id_pagesbuilder_i18n' => $id_pagesbuilder_i18n,
        ]);

        return $this->collector['pagesbuilder_draft_i18n']->set('', [
            'id_pagesbuilder_i18n' => $id_pagesbuilder_i18n,
            'content' => $current['content'],
        ]);
    }

    public function createFromMaster($id_pagesbuilder, $id_locales)
    {
        $master = $this->collector['pagesbuilder']->getOne([
            'id_pagesbuilder' => $id_pagesbuilder,
        ]);

        return $this->collector['pagesbuilder_i18n']->set('', [
            'id_pagesbuilder' => $id_pagesbuilder,
            'lib' => $master['lib'],
            'meta_desc' => $master['meta_desc'],
            'meta_title' => $master['meta_title'],
            'content' => $master['content'],
            'id_locales' => $id_locales,
        ]);
    }
}
