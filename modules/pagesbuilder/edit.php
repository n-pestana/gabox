<?php
header("Expires: on, 01 Jan 1970 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$is_ajax_request = 'xmlhttprequest' == strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' );
if(!$is_ajax_request){
  include(GABOX_BACK_ROOT.'header.php');
}

$currentLocale = $gen->collector['pagesbuilder']->getEditingLocale();
$pageCollector = $gen->collector['pagesbuilder']->getPageCollector($currentLocale);
$draftCollector = $gen->collector['pagesbuilder']->getDraftCollector($currentLocale);

# save
if (isset($_POST['id_' . $draftCollector]) && isset($_POST['html'])){
    $draft = $gen->collector[$draftCollector]->get((int)$_POST['id_' . $draftCollector]);
    if (isset($draft['id_' . $draftCollector]) && ($draft['content'] != $_POST['html'])) {
        $new_draft = $gen->collector[$draftCollector]->set('', [
            'content' => $_POST['html'],
            'id_' . $pageCollector => $draft['id_' . $pageCollector],
        ]);
        echo $new_draft . '||' . str_replace('pagesbuilder', '', $pageCollector);
    } else {
        echo $_POST['id_' . $draftCollector] . '||' . str_replace('pagesbuilder', '', $pageCollector);
    }
    die();
}

#publish
if (isset($_POST['id_' . $draftCollector]) && isset($_POST['action'])) {
    if($_POST['action'] == 'publish') {
       $tosave = $gen->collector[$draftCollector]->get($_POST['id_' . $draftCollector]);
       $gen->collector[$pageCollector]->set($tosave['id_' . $pageCollector], [
           'content' => $tosave['content'],
           'version' => $_POST['id_' . $draftCollector],
       ]);
       die();
    }
}

# get
if (isset($_GET['id_pagesbuilder'])) {
    $masterPage = $gen->collector['pagesbuilder']->getOne($_GET['id_pagesbuilder']);
    if ($masterPage && empty($masterPage['id_locales'])) {
        $availableLocales = $gen->collector['sites']->getAvailabledIdLocalesForSite($masterPage['id_sites']);
        if ($availableLocales) {
            $gen->collector['pagesbuilder']->set($_GET['id_pagesbuilder'], ['id_locales' => $availableLocales[0]]);
            $masterPage['id_locales'] = $availableLocales[0];
        }
    }
    if (empty($currentLocale)) {
        $currentLocale = $masterPage['id_locales'];
    }
    if ($pageCollector === 'pagesbuilder_i18n') {
        $page_i18n = $gen->collector[$pageCollector]->getOne([
            'id_pagesbuilder' => $_GET['id_pagesbuilder'],
            'id_locales' => $currentLocale,
        ]);
        if (!$page_i18n) {
            $_GET['id_pagesbuilder_i18n'] = $gen->collector[$pageCollector]->createFromMaster($_GET['id_pagesbuilder'], $currentLocale);
        } else {
            $_GET['id_pagesbuilder_i18n'] = $page_i18n['id_pagesbuilder_i18n'];
        }
    }
    $draft = $gen->collector[$pageCollector]->getLastDraft($_GET['id_' . $pageCollector]);
    if (empty($draft)) {
        $gen->collector[$pageCollector]->createDraftFromPage($_GET['id_' . $pageCollector]);
     }
    $draft = $gen->collector[$pageCollector]->getLastDraft($_GET['id_' . $pageCollector]);
    $id_pagesbuilder_draft = $draft['id_pagesbuilder_draft'] ?? null;
    $id_pagesbuilder       = $draft['id_pagesbuilder'] ?? null;
    $id_pagesbuilder_draft_i18n = $draft['id_pagesbuilder_draft_i18n'] ?? null;
    $id_pagesbuilder_i18n       = $draft['id_pagesbuilder_i18n'] ?? null;
}

if (!$is_ajax_request) {
  include('index.view.php');
  include(GABOX_BACK_ROOT.'footer.php');
} else {
    die();
}
