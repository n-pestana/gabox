<?php 
# ce controller n'est plus utilisé car déplacé dans gabox/front.php
if(isset($this->collector['pagesbuilder_draft']) && isset($_GET['id_pagesbuilder_draft'])){
   $temp = $this->collector['pagesbuilder_draft']->getOne((int)$_GET['id_pagesbuilder_draft']);
   $draft = $this->collector['pagesbuilder']->getLastDraft($temp['id_pagesbuilder']);
   echo $draft['content'] ;
}
