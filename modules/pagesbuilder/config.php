<?php
$mod['name']       = 'Gestion de contenu V2';
$mod['desc']       = "Outil WYSIWYG de création de page et de post avec bloc visuel et edition de code";
$mod['fa-icon']    = 'th-large';
$mod['version']    = '1.0';

$mod['sql'][] = " CREATE TABLE IF NOT EXISTS `gen_pagesbuilder` (
  `id_pagesbuilder` mediumint(8) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` enum('post','page') NOT NULL DEFAULT 'page',
  `picture` varchar(255) DEFAULT NULL,
  `id_admins` mediumint(11) DEFAULT NULL,
  `version` mediumint(9) DEFAULT NULL,
  `lib` varchar(1024) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `meta_desc` varchar(1024) DEFAULT NULL,
  `meta_title` varchar(1024) DEFAULT NULL,
  `id_templates` mediumint(8) unsigned DEFAULT NULL,
  `is_published` tinyint(1) DEFAULT NULL,
  `content` longtext,
  `compiled_content` longtext,
  `id_sites` mediumint(8) UNSIGNED NOT NULL,
  `media` varchar(255) DEFAULT NULL,
  `id_treeview` mediumint(8) UNSIGNED,
  `id_parent` mediumint(8) UNSIGNED,
  `id_locales` mediumint unsigned not null,
  `n_order` mediumint unsigned default 0 null,
  FOREIGN KEY (`id_treeview`) REFERENCES `gen_treeview` (`id_treeview`) ON DELETE CASCADE ON UPDATE NO ACTION,
  FOREIGN KEY (`id_parent`) REFERENCES `gen_pagesbuilder` (`id_pagesbuilder`) ON DELETE SET NULL ON UPDATE NO ACTION,
  FOREIGN KEY (id_locales) REFERENCES gen_locales (id_locales) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$mod['sql'][] = " CREATE TABLE IF NOT EXISTS `gen_pagesbuilder_draft` (
  `id_pagesbuilder_draft` mediumint(8) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `version` mediumint(9) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `content` longtext,
  `compiled_content` longtext,
  `id_sites` mediumint(8) unsigned DEFAULT NULL,
  `id_pagesbuilder` mediumint(8) unsigned DEFAULT NULL,
  KEY `id_pagesbuilder` (`id_pagesbuilder`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; ";
$mod['sql'][]      = " CREATE TABLE IF NOT EXISTS `gen_pagesbuilder_url` (
  `id_pagesbuilder_url` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_locale` int(11) NOT NULL,
  `id_site` mediumint(8) UNSIGNED NOT NULL,
  `id_pagesbuilder` int(11) UNSIGNED NOT NULL,
  `id_treeview` int(11) NOT NULL,
  `is_canonical` tinyint(1) DEFAULT '0',
  `url` varchar(1024) NOT NULL,
  KEY `getCanonicalFront` (`id_locale`,`id_site`,`is_canonical`,`id_pagesbuilder`),
  KEY `id_locale` (`id_locale`,`url`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
";
$mod['sql'][] = "CREATE TABLE `gen_pagesbuilder_data` (
  `id_pagesbuilder_data` mediumint(8) NOT NULL,
  `id_pagesbuilder` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `id_apps` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
$mod['sql'][] = "ALTER TABLE `gen_pagesbuilder_data`   ADD PRIMARY KEY (`id_pagesbuilder_data`),   ADD KEY `id_pagesbuilder` (`id_pagesbuilder`,`title`);";
$mod['sql'][] = "ALTER TABLE `gen_pagesbuilder_data`   MODIFY `id_pagesbuilder_data` mediumint(8) NOT NULL AUTO_INCREMENT; ";
$mod['sql'][] = "ALTER TABLE `gen_pagesbuilder_data`   ADD CONSTRAINT `pagesbuilder_data_fk` FOREIGN KEY (`id_pagesbuilder`) REFERENCES `gen_pagesbuilder` (`id_pagesbuilder`) ON DELETE CASCADE ON UPDATE CASCADE;";
$mod['sql'][] = "CREATE TABLE gen_pagesbuilder_i18n (
    id_pagesbuilder_i18n mediumint unsigned auto_increment primary key,
    version              mediumint(9) DEFAULT NULL,
    lib                  varchar(1024)      null,
    meta_desc            varchar(1024)      null,
    meta_title           varchar(1024)      null,
    is_published         tinyint(1)         null,
    content              longtext           null,
    created_at           datetime           null,
    updated_at           datetime           null,
    id_pagesbuilder      mediumint unsigned not null,
    id_locales           mediumint unsigned not null,
    id_treeview          mediumint unsigned null,
    constraint gen_pagesbuilder_i18n_gen_locales_id_locales_fk
        foreign key (id_locales) references gen_locales (id_locales),
    constraint gen_pagesbuilder_i18n_gen_pagesbuilder_id_pagesbuilder_fk
        foreign key (id_pagesbuilder) references gen_pagesbuilder (id_pagesbuilder)
);";
$mod['sql'][] = "CREATE TABLE gen_pagesbuilder_draft_i18n (
    id_pagesbuilder_draft_i18n mediumint unsigned auto_increment primary key,
    created_at                 datetime           null,
    updated_at                 datetime           null,
    content                    longtext           null,
    id_pagesbuilder_i18n       mediumint unsigned null,
    constraint gen_pagesbuilder_draft_i18n_id_pagesbuilder_i18n_fk
        foreign key (id_pagesbuilder_i18n) references gen_pagesbuilder_i18n (id_pagesbuilder_i18n)
);";
$mod['sql'][] = "ALTER TABLE gen_pagesbuilder_draft DROP COLUMN id_sites;";

$mod['unsql'][] = "DROP TABLE gen_pagesbuilder_draft;";
$mod['unsql'][] = "DROP TABLE gen_pagesbuilder_url;";
$mod['unsql'][] = "DROP TABLE gen_pagesbuilder_data;";
$mod['unsql'][] = "DROP TABLE gen_pagesbuilder;";
$mod['unsql'][] = "DROP TABLE gen_pagesbuilder_i18n;";
$mod['unsql'][] = "DROP TABLE gen_pagesbuilder_draft_i18n;";

$mod['bo-hook'] = [
    'lib' => 'Gabox Page Builder',
    'icon' => 'th-large',
    'links' => [
        ['link' => '/modules/pagesbuilder/admin.php', 'lib' => 'Lister les contenus']
    ]
];
