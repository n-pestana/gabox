<?php
$temp = array_values($this->collector['treeview']->getChildrenCached('blog'));
$this->vars['cat'] = $temp[0]['children'];
$root = $temp[0];
# si sur une page
if(strstr($this->vars['url'],'.html')){
    $rs = $this->collector['pagesbuilder_url']->getCached(
        array(
            'id_site'       => ID_SITE,
            'is_canonical'  => 1,
            'url'           => $this->vars['url']
        )
    );
    if($rs){
        $this->vars['page'] = $this->collector['pagesbuilder']->getOneCached($rs['id_pagesbuilder']);
    }
}

# listing catégorie ou index
else{
   $conditions = array(
            'id_sites'       => ID_SITE,
            'id_locales'    => ID_LOC,
            'is_published'  => 1, 
   );
   # si root    
   if($root['id_treeview'] != $this->vars['id_treeview']){
    $conditions['id_treeview'] = $this->vars['id_treeview'];
   }

   $this->vars['pages'] = $this->collector['pagesbuilder']->get(
       $conditions 
    ); 

}


include(VIEWS_DIR."page-builder-front.php");

