<br><br>
<div class='container blog' >
    <div class="row">
        <div class="col-md-3 col-xs-12 product-card blog-cat">
           <h2><?=t("Catégories")?></h2> 
            <ul>
                <?php foreach($this->vars['cat'] as $cat){?>
                <li><a href="<?=$cat['fullurl']?>" style="color:black"><?=$cat['lib']?></a></li>
                <?php }?>
            </ul>
        </div>
        <div class="col-md-8 col-xs-12 product-card blog-content">
            <?php if(isset($this->vars['page'])){?> 
                <div class="container">
                    <h1><?=$this->vars['page']['lib']?></h1>
                    <p><img src="<?=$this->vars['page']['picture']?>" alt="<?=$page['lib']?>"  loading="lazy" style="max-width:100%;" ></p>
                    <?=$this->vars['page']['content']?>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-right"><a href="<?=dirname($this->vars['url'])?>/" class="btn btn-primary"><?=t("Retour")?></a></div>
                    </div>
                </div>
            <?php }elseif(isset($this->vars['pages'])){?>
                <div class="news-section container">
                    <div class="row">
                <?php if(!empty($this->vars['pages'])){ ?>
                    <?php foreach($this->vars['pages'] as $page){?>
                        <?php $url = $this->collector['pagesbuilder']->getCanonicalUrl($page['id_pagesbuilder'])?>
                        <div class="col-md-4 col-xs-12 ">
                            <div class="card-item" style="">
                                <div class="img-wrap">
                                  <?php if(!empty($page['picture'])){ ?>
                                  <a href="<?=$url?>">
                                  <img src="<?=$page['picture']?>" alt="<?=$page['lib']?>"  loading="lazy">
                                  </a>
                                  <?php } ?>
                                </div>
                                <div class="content-block" style="border:0px;">
                                  <a href="<?=$url?>" class="card-title" style="font-size:14px;"><?=$page['lib']?></a>
                                  <div class="text-area text-center m-10">
                                    <a href="<?=$url?>" class="btn btn-primary" ><?=t("Lire la suite")?></a>
                                  </div>
                                </div>
                            </div>
                        </div>
                  <?php }?>
                  <?php }else{?>
                    <?=t("Aucune actualités ou aucun article dans cette catégorie pour le moment");?>
                  <?php }?>
                </div>
              </div>
            <?php }?>
        </div>
    </div>
</div>
