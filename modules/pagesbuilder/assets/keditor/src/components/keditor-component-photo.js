import $ from 'jquery';
import KEditor from 'keditor';

KEditor.components['photo'] = {
    init: function (contentArea, container, component, keditor) {
        let componentContent = component.children('.keditor-component-content');
        let img = componentContent.find('img');
        
        img.css('display', 'inline-block');
    },
    
    settingEnabled: true,
    
    settingTitle: 'Photo Settings',
    
    initSettingForm: function (form, keditor) {
        let self = this;
        let options = keditor.options;

        var pages_link = '';

        $.ajax({
            url: "snippets/snippets.php?get=pagesLink",
            async: false,
          }).done(function(dataPage) {
            $( this ).addClass( "done" );
            pages_link = dataPage;

            form.append(
                '<form class="form-horizontal">' +
                '   <div class="form-group">' +
                '       <div class="col-sm-12">' +
                '           <button type="button" class="btn btn-block btn-primary" id="photo-edit" onclick="">Change Photo</button>' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <label for="photo-align" class="col-sm-12">Align</label>' +
                '       <div class="col-sm-12">' +
                '           <select id="photo-align" class="form-control">' +
                '               <option value="left">Left</option>' +
                '               <option value="center">Center</option>' +
                '               <option value="right">Right</option>' +
                '           </select>' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <label for="photo-style" class="col-sm-12">Style</label>' +
                '       <div class="col-sm-12">' +
                '           <select id="photo-style" class="form-control">' +
                '               <option value="">None</option>' +
                '               <option value="img-rounded">Rounded</option>' +
                '               <option value="img-circle">Circle</option>' +
                '               <option value="img-thumbnail">Thumbnail</option>' +
                '           </select>' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <label for="photo-responsive" class="col-sm-12">Responsive</label>' +
                '       <div class="col-sm-12">' +
                '           <input type="checkbox" id="photo-responsive" style="z-index:9999!important;margin-left:3px !important;margin-bottom:5px !important"/>' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <label for="photo-alt" class="col-sm-12">Balise Alt</label>' +
                '       <div class="col-sm-12">' +
                '           <input type="text" id="photo-alt" class="form-control" />' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <label for="photo-width" class="col-sm-12">Width</label>' +
                '       <div class="col-sm-12">' +
                '           <input type="number" id="photo-width" class="form-control" />' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <label for="photo-height" class="col-sm-12">Height</label>' +
                '       <div class="col-sm-12">' +
                '           <input type="number" id="photo-height" class="form-control" />' +
                '           <input type="hidden" id="photo-ratio"" />' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <label for="photo-link" class="col-sm-12">Link (optional)</label>' +
                '       <div class="col-sm-12">' +
                            pages_link +
                '       </div>' +
                '   </div>' +
                '</form>'
            );
            
            $("#pages_list").chosen({disable_search_threshold: 10});
            let photoLink = form.find('#pages-list');
            photoLink.on('change', function () {
                keditor.getSettingComponent().find('img').attr('data-idpage', $('#pages-list').val());
            })

            let photoEdit = form.find('#photo-edit');
            let fileInput = photoEdit.next();
            let myfile ;
            photoEdit.on('click', function (e) {
                e.preventDefault();
                moxman.browse({
                        width:'1024'
                        ,no_host: true
                        ,oninsert: function(data) {
                            var FileURL = data.files[0].url.replace(/http:/g, 'https:');
                            GetFileObjectFromURL(FileURL, function (file) {
                                if (/image/.test(file.real_type) && /\.(jpe?g|png|gif|bmp|webp|tiff|svg)$/i.test(file.name)) {
                                    let reader = new FileReader();
                                    reader.addEventListener('load', function () {
                                        let img = keditor.getSettingComponent().find('img');
    
                                        img.attr('src', FileURL);
                                        var image = new Image();
                                        image.src = reader.result;
                                        image.onload = function() {
    
                                            let img = keditor.getSettingComponent().find('img');
    
                                            let inputWidth = form.find('#photo-width');
                                            let inputHeight = form.find('#photo-height');
                                            let inputRatio= form.find('#photo-ratio');
                                            let inputAlt= form.find('#photo-alt');
    
                                            inputWidth.val(image.width);
                                            inputHeight.val(image.height);
                                            inputRatio.val(image.width/image.height);

                                            img[0].setAttribute('width', image.width);
                                            img[0].setAttribute('height', image.height);
                                        };
                                    });
                                    reader.readAsDataURL(file);
                                } else {
                                    alert('Your selected file is not photo!');
                                }
    
                            });
                        }
    
                 });
            });

            let inputAlign = form.find('#photo-align');
            inputAlign.on('change', function () {
                let panel = keditor.getSettingComponent().find('.photo-panel');
                panel.css('text-align', this.value);
            });
            
            let inputResponsive = form.find('#photo-responsive');
            inputResponsive.on('click', function () {
                keditor.getSettingComponent().find('img')[this.checked ? 'addClass' : 'removeClass']('img-responsive');
            });
            
            let cbbStyle = form.find('#photo-style');
            cbbStyle.on('change', function () {
                let img = keditor.getSettingComponent().find('img');
                let val = this.value;
                
                img.removeClass('img-rounded img-circle img-thumbnail');
                if (val) {
                    img.addClass(val);
                }
            });
            
            let inputWidth = form.find('#photo-width');
            let inputHeight = form.find('#photo-height');
            let inputRatio= form.find('#photo-ratio');
            let inputAlt= form.find('#photo-alt');


            inputAlt.on('change', function () {
                let img = keditor.getSettingComponent().find('img');
                img[0].setAttribute('alt', inputAlt.val());
            });

            inputWidth.on('change', function () {
                let img = keditor.getSettingComponent().find('img');
                let newWidth = +this.value;
                let newHeight = Math.round(newWidth / inputRatio.val());
                
                if (newWidth <= 0) {
                    newWidth = self.width;
                    newHeight = self.height;
                    this.value = newWidth;
                }
                img[0].setAttribute('width', newWidth);
                img[0].setAttribute('height', newHeight);
                inputHeight.val(newHeight);
            });
            inputHeight.on('change', function () {
                let img = keditor.getSettingComponent().find('img');
                let newHeight = +this.value;
                let newWidth = Math.round(newHeight * inputRatio.val());
                
                if (newHeight <= 0) {
                    newWidth = self.width;
                    newHeight = self.height;
                    this.value = newHeight;
                }
                img[0].setAttribute('width', newWidth);
                img[0].setAttribute('height', newHeight);
                inputWidth.val(newWidth);
            });
        });
    },
    
    showSettingForm: function (form, component, keditor) {
        let self = this;
        let inputAlign = form.find('#photo-align');
        let inputResponsive = form.find('#photo-responsive');
        let inputWidth = form.find('#photo-width');
        let inputAlt= form.find('#photo-alt');
        let inputRatio= form.find('#photo-ratio');
        let inputHeight = form.find('#photo-height');
        let cbbStyle = form.find('#photo-style');
        
        let panel = component.find('.photo-panel');
        let img = panel.find('img');
        
        let algin = panel.css('text-align');
        if (algin !== 'right' || algin !== 'center') {
            algin = 'left';
        }
        
        if (img.hasClass('img-rounded')) {
            cbbStyle.val('img-rounded');
        } else if (img.hasClass('img-circle')) {
            cbbStyle.val('img-circle');
        } else if (img.hasClass('img-thumbnail')) {
            cbbStyle.val('img-thumbnail');
        } else {
            cbbStyle.val('');
        }
        
        inputAlign.val(algin);
        inputResponsive.prop('checked', img.hasClass('img-responsive'));
        inputWidth.val(img.width());
        inputHeight.val(img.height());
        inputAlt.val(img[0].getAttribute('alt'));
        inputRatio.val(img.width()/img.height());

        let embedItem = component.find('img');
        let src = embedItem.attr('data-idpage');
        $("#pages-list").val(null);
        if(src && src.length){
            $("#pages-list").val(src);
            $("#pages-list").trigger('chosen:updated');
        }
    }
};
var GetFileBlobUsingURL = function (url, convertBlob) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.responseType = "blob";
        var type = 'unknown';
        xhr.addEventListener('loadend', function() {
            var reader = new FileReader();
            reader.onloadend =  function(e){
                var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
                var header = '';
                for (var i = 0; i < arr.length; i++) {
                    header += arr[i].toString(16);
                }
                switch (header) {
                    case '89504e47':
                        type = 'image/png';
                        break;
                    case '47494638':
                        type = 'image/gif';
                        break;
                    case 'ffd8ffe0':
                    case 'ffd8ffe1':
                    case 'ffd8ffe2':
                        type = 'image/jpeg';
                        break;
                    case '52494646':
                        type = 'image/webp';
                        break;
                    case '49492a00':
                    case '4d4d002a':
                        type = 'image/tiff';
                        break;
                }
                if (type === 'unknown' && header.substring(0, 4) === '424D') {
                    type = 'image/bmp';
                }
                if (type === 'unknown' && e.target.result.toString().indexOf('<svg') !== -1) {
                    type = 'image/svg';
                }
                xhr.response.real_type = type;
                convertBlob(xhr.response);
            };
            reader.readAsArrayBuffer(xhr.response);
        });
        xhr.send();
};

var blobToFile = function (blob, name) {
        blob.lastModifiedDate = new Date();
        blob.name = name;
        return blob;
};

var GetFileObjectFromURL = function(filePathOrUrl, convertBlob) {
       GetFileBlobUsingURL(filePathOrUrl, function (blob) {
          convertBlob(blobToFile(blob, filePathOrUrl.substring(filePathOrUrl.lastIndexOf('/') + 1)));
       });
};

