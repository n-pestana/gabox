import KEditor from 'keditor';

KEditor.components['products'] = {
    init: function (contentArea, container, component, keditor) {
        let iframe = component.find('iframe');
        let wrapper = iframe.parent();
        keditor.initIframeCover(iframe, wrapper);
    },

    settingEnabled: true,
    settingTitle: 'Products Settings',

    initSettingForm: function (form, keditor) {

        var product_list ='';
        
        $.ajax({
          url: "snippets/snippets.php?get=produits",
          async: false,
        }).done(function(data) {
          $( this ).addClass( "done" );

            product_list = data;
            form.append(
                '<form class="form-horizontal">' +
                '   <div class="form-group">' +
                '       <label class="col-sm-12">Showned  products: </label>' +
                '       <div class="col-sm-12">' +product_list +
                '       </div> ' + 
                '   </div>' +

                '   <div class="form-group">' +
                '       <div class="col-sm-12">' +
                '           <button type="button" class="btn btn-block btn-primary btn-youtube-edit">Change Products</button>' +
                '       </div>' +
                '   </div>' +
                '</form>'
            );
            $('.keditor-sidebar').width(500);
            $("#produits_list").chosen({disable_search_threshold: 10});
            $('.chosen-container').width(500);
            let btnEdit = form.find('.btn-youtube-edit');
            btnEdit.on('click', function (e) {
                e.preventDefault();
                keditor.getSettingComponent().find('img').attr('alt', $('#produits_list').val().toString());

                notif({
                    msg:"Modification saved !",
                    type:"success",
                    timeout: 3000,
                    bgcolor:"#ffcd00",
                    color: "#000000"
                });

            });

        });
    },

    showSettingForm: function (form, component, keditor) {
        let embedItem = component.find('img');
        let src = embedItem.attr('alt');
        $("#produits_list").val(null);
        if(src.length){
            $("#produits_list").val(src.split(','));
            $("#produits_list").trigger('chosen:updated');
        }
    }
};
