import $ from 'jquery';
import KEditor from 'keditor';
import SETTING_CATEGORY from "../keditor/constants/settingCategory";

KEditor.components['quizz'] = {
    settingEnabled: true,
    settingTitle: 'Quizz settings',
    
    initSettingForm: function (form, keditor) {
        $.ajax({
            url: "snippets/snippets.php?get=quizzes",
            async: false,
        }).done(function(quizzes_list) {
            $(this).addClass( "done" );
            form.append(
                '<form class="form-horizontal">' +
                '   <div class="form-group">' +
                '       <label for="quizzes" class="col-sm-12">Quizz</label>' +
                '       <div class="col-sm-12">' +
                            quizzes_list +
                '       </div>' +
                '   </div>' +
                '</form>'
            );
            
            $("#quizzes-list").chosen({disable_search_threshold: 10});
            let quizzesSelect = form.find('#quizzes-list');
            quizzesSelect.on('change', function () {
                let builderElement = keditor.getSettingComponent().find('p');
                builderElement.attr('data-idquizzes', $('#quizzes-list').val());
                builderElement.html('Quizz : ' + $('#quizzes-list option:selected').html());
            })
        });
    },
    
    showSettingForm: function (form, component, keditor) {
        let embedItem = component.find('p');
        let src = embedItem.attr('data-idquizzes');
        $("#quizzes-list").val(null);
        if(src && src.length){
            $("#quizzes-list").val(src);
            $("#quizzes-list").trigger('chosen:updated');
        }
    }
};
