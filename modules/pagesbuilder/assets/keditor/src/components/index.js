import './keditor-component-audio.js';

import './keditor-component-form.js';

import './keditor-component-googlemap.js';

import './keditor-component-photo.js';

import './keditor-component-text.js';

import './keditor-component-video.js';

import './keditor-component-vimeo.js';

import './keditor-component-youtube.js';

import './keditor-component-products.js';

import './keditor-component-quizz.js';
