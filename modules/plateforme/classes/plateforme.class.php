<?php 
class Plateforme extends gen_plateforme {

	function getAvailabledLanguage($id_plateforme,$lang='') {
          $sql = "SELECT plateforme.id_plateforme, locales.id_locales,locales.locale,locales.id_country, locales.lib_language, locales.country, locales.language
                  FROM gen_plateforme plateforme
                  LEFT JOIN gen_plateforme_locales plateforme_locales ON plateforme.id_plateforme = plateforme_locales.id_plateforme
                  LEFT JOIN gen_locales locales ON plateforme_locales.id_locales = locales.id_locales
                  WHERE  plateforme_locales.id_plateforme =".$id_plateforme;


          if(!empty($lang)){
            $sql .= " and locales.language='".$lang."'"; 
          }


      	  $rs = $this->CachedQuery($sql);
          $temp =  $this->getRows($rs);

          if(!empty($lang)){
            if(isset($temp[0])){
                 return $temp[0];
            }
            else{
                return false;
            }
          }
          else{
             return $temp;
          }
	}

}
