<?php
$_GET['id_plateforme'] = $_REQUEST['id_plateforme'] = $_SERVER['id_plateforme'];
/*if(isset($_POST)&& !empty($_POST)){
    header("Location:  ".$_SERVER['PHP_SELF'].'?id_plateforme='.$_GET['id_plateforme']);
}*/
include(GABOX_BACK_ROOT.'header.php');

$curr_obj='plateforme';

if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    include('admin-fields.php');
    $form->lib= t("Paramétrage de la plateforme");
    ?>
    <div class="container-fluid">
        <div class="alert alert-warning">
            <p><?=t("Afin d'utiliser plusieurs langues dans l'interface d'administration, vous devez placer ces directives dans votre configuration Apache :")?> </p>
            <code>
                SetEnvIf Request_URI ^/?(..)/(.*)?$ lang=$1
                <br />RewriteRule ^/?(..)/(.*)$ /$2?lang=$1 [L,QSA]</code>
            <br /><br />
            <p><?=t("Vous pouvez également utiliser plusieurs plateformes dans le même environnement afin de proposer plusieurs points d'entrés à l'interface d'administration.<br />Cela vous permet d'avoir une URL et un skin différent. Pour cela ajoutez l'id de la plateforme dans la configuration Apache :")?> </p>
            <code>setEnv id_plateforme 1</code>
        </div>
    </div>
    <?php include(GABOX_GEN_ROOT.'gen_edit.php');

}
else{
    echo "list";
    $listing['nolist'] = true;
    include(GABOX_GEN_ROOT.'genlist.php');
}

include(GABOX_BACK_ROOT.'footer.php');


