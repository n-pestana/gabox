<?php
$form->fields = array(
  '_cat10' => t("Informations")
  ,'updated_at'=>$form->gen_field['updated_at']
  ,'lib'=>array_merge($form->gen_field['lib'],array('label'=>t("Nom de la plateforme")))
  ,'media'=>array_merge($form->gen_field['file'],array('label'=>t("Logo de la plateforme"),'tooltip'=>t("Le logo affiché sur la page de login et en haut du menu principale. ")))
  ,'favicon_code' =>array_merge($form->gen_field['desc'],array('label'=>t("Bloc de code à insérer danbs le header de l'administration"),'class'=>'html_code','tooltip'=>"Le contenu sera inséré dans la balise <head> de votre header.<br />Ce code n'est pas filtré et peut comporter des tags css/js/html/"))

    ,'is_multisite'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Multisite ? ")))
    ,'is_multilang'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Multilangue ? ")))

    ,'autoupdate_url'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Mettre à jour automatiquement les URLS ? "),'tooltip'=>"Lors de l'ajout ou de déplacement de catégories (treeview), les URLS complètes doivent être mise à jour. Dans le cas d'un site multilangue avec beaucoup de catégories, cette action peut pendre quelques secondes. Vous pour alors choisir si cette action est automatique ou manuelle."))
  ,'locale_list' =>array('label' => t("Langues disponibles pour le backoffice ?")
      ,'type' =>'relationel_list'
      ,'arrayValues'=>$gen->collector['locales']->getKv()
      ,'multiple'=>true
      ,'linked_object'=>'locales' 
      ,'class'=>'chosen-select '),

  '_2fa' => t("Backoffice double factor authentication"),
  '2fa_enabled' => array_merge($form->gen_field['ouinon'], ['label' => t("Enable double factor authentication")]),
  '2fa_issuer' => array_merge($form->gen_field['lib'], ['label' => t("Double factor authentication issuer")])

  ,'_cat20' => t("Skins")
  ,'.nolist'=>$form->gen_field['.nolist']
    ,'sidebarfix'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Menu fixe ? ")))
    ,'headerfix'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Entête fixe ?")))
    ,'fullwidth'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Pleine largeur ?")))
    ,'bgcolor_head'=>array_merge($form->gen_field['lib'],array('label'=>t("Couleur de fond de l'entête"),'tooltip'=>t("Valeur par défaut : #242e3e"),'class'=>'colorpicker-input'))
    ,'bgcolor_menu'=>array_merge($form->gen_field['lib'],array('label'=>t("Couleur de fond du menu"),'tooltip'=>t("Valeur par défaut : #242e3e"),'class'=>'colorpicker-input'))
    ,'menu_linkcolor'=>array_merge($form->gen_field['lib'],array('label'=>t("Couleur des liens du menu"),'tooltip'=>t("Valeur par défaut : #b5bdca"),'class'=>'colorpicker-input'))
    ,'menu_hovercolor'=>array_merge($form->gen_field['lib'],array('label'=>t("Couleur des liens surlignés du menu"),'tooltip'=>t("Valeur par défaut : #00a6b3"),'class'=>'colorpicker-input'))


);

