<?php
$mod['name']       = t("Plateforme");
$mod['desc']       = t("Paramétrage de la plateforme");
$mod['fa-icon']    = 'cogs';
$mod['version']    = '1.0';
$mod['link']       = '/modules/plateforme/admin.php?id_plateforme='.$_SERVER['id_plateforme'];
