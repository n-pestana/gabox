<?php
$mod['name']       = t("Sites");
$mod['desc']       = t("Module de gestion des sites");
$mod['fa-icon']    = 'fa-solid fa-browser';
$mod['version']    = '1.0';
if(defined('ID_SITE')){
    $mod['bo-hook'] = 
              array(
                'lib' => t("Site options"),
                'icon' => 'fa-solid fa-browser',
                'links' => array (
                    array('link' => '/modules/sites/admin.php',  'lib' => t("Lister les sites")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_info',  'lib' => t("Informations")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_url',  'lib' => t("Url")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_logo',  'lib' => t("Logos")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_coor',  'lib' => t("Coordonnées")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_map',  'lib' => t("Cartographie")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_rs',  'lib' => t("Réseaux sociaux")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_speed',  'lib' => t("Optimisation")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_i18n',  'lib' => t("Traductions")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_debug',  'lib' => t("Debug")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_gg',  'lib' => t("Google APIs")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_facebook',  'lib' => t("Facebook APIs")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_facebook_chat',  'lib' => t("Facebook Chat")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_mailchimp',  'lib' => t("Mailchimp APIs")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_smtp',  'lib' => t("SMTP & Mails")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_theme',  'lib' => t("Thèmes")),
                    array('link' => '/modules/sites/admin.php?id_sites='.ID_SITE.'&tab=_code',  'lib' => t("Code additionnel"))

                )
            );
}
else{
    $mod['bo-hook'] = 
              array(
                'lib' => t("Lister les sites "),
                'icon' => 'fa-solid fa-browser',
                'link' => '/modules/sites/admin.php'
            );
}
        

