<!-- Dans le footer -->
<?php if($this->vars['site']['fb_chat_page_id']!=''){ ?>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v3.3'
        });
      };

      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/fr_FR/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);

    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="fb-customerchat"
      attribution=setup_tool
      page_id="<?=$this->vars['site']['fb_chat_page_id']?>"
      theme_color="<?=$this->vars['site']['fb_chat_color']?>"
      logged_in_greeting="<?=$this->vars['site']['fb_chat_inmsg']?>"
      logged_out_greeting="<?=$this->vars['site']['fb_chat_outmsg']?>"
    >
    </div>
<?php } ?>
