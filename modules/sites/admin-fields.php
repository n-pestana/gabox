<?php
$form->fields= array(
      '_info' => t("Informations")
    ,'lib' => $form->gen_field['lib']
    
    );
if (isset($gen->collector['sites_treeview'])) {
    $plateforme = $gen->collector['plateforme']->getOneCached();
    if ($plateforme['is_multisite'] == 1) {
        $form->fields += [
               'treeview_list'  => ['label' => t("Menus et catégories disponibles")
              ,'type'           => 'relationel_list'
              ,'arrayValues'    => $gen->collector['treeview']->getKv(['id_parent' => 0], 'ORDER BY int_order')
              ,'multiple'       => true
              ,'linked_object'  => 'treeview' 
              ,'class'          => 'chosen-select']
            ];

    }
}
$form->fields += array(
    'is_published' => $form->gen_field['is_published']
    ,'id_country'=>array_merge($form->gen_field['select'],array('label'=>t("Pays")),array('arrayValues'=>$gen->collector['country']->getKv()))

    ,'_url' => t("Url")

    ,'prod_url'=>array_merge($form->gen_field['lib'],array('label'=>t("URL Production")))
    ,'internal_url'=>array_merge($form->gen_field['lib'],array('label'=>t("URL Interne")))

    ,'_logo' => t("Logos")

    ,'header_logo'=>array_merge($form->gen_field['file'],array('label'=>t("Logo Header")))
    ,'footer_logo'=>array_merge($form->gen_field['file'],array('label'=>t("Logo Footer")))

     ,'_coor' => t("Coordonnées")
    ,'form_contact_dest'=>array_merge($form->gen_field['desc'],array('label'=>t("Destinataire des contacts  (un par ligne)")))
    ,'main_phone_number'=>array_merge($form->gen_field['lib'],array('label'=>t("Primary Phone number")))
    ,'second_phone_number'=>array_merge($form->gen_field['lib'],array('label'=>t("Secondary Phone number")))
    ,'main_email'=>array_merge($form->gen_field['lib'],array('label'=>t("Primary e-mail")))
    ,'main_adress'=>array_merge($form->gen_field['desc'],array('label'=>t("Address")))
    ,'horaires' => array_merge($form->gen_field['desc'],array('label'=>t("Horaires")))


    ,'_map'=>t("Cartographie")
    ,'map_lat' => array_merge($form->gen_field['lat'],array('label'=>t("Map Lat")))
    ,'map_lng' => array_merge($form->gen_field['lat'],array('label'=>t("Map Long")))
    ,'map_zoom' => array_merge($form->gen_field['lat'],array('label'=>t("Map Zoom")))

      ,'_rs' => t("Réseaux sociaux")
    ,'sm_facebook'=>array_merge($form->gen_field['lib'],array('label'=>t("Facebook Link")))
    ,'sm_twitter_link'=>array_merge($form->gen_field['lib'],array('label'=>t("Twitter Link")))
    ,'sm_pinterest_link'=>array_merge($form->gen_field['lib'],array('label'=>t("Pinterest Link")))
    ,'sm_linkedin_link'=>array_merge($form->gen_field['lib'],array('label'=>t("Linkedin Link")))
    ,'sm_instagram_link'=>array_merge($form->gen_field['lib'],array('label'=>t("Instagram Link")))
    ,'sm_youtube_link'=>array_merge($form->gen_field['lib'],array('label'=>t("Youtube Link")))
    ,'sm_viadeo_link'=>array_merge($form->gen_field['lib'],array('label'=>t("Viadeo Link")))

      ,'_speed' => t("Vitesse")
    ,'use_auto_404'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Use Auto 404 ?")))
    ,'use_cache'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Use application cache ? ")))
    ,'use_apc'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Use APC serveur cache ? ")))
    ,'use_img_rewrite'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Use image name rewriting ?")))


      ,'_debug' => t("Debug")
    ,'debug_mode'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Main debug mode ? ")))
    ,'debug_sql'=>array_merge($form->gen_field['ouinon'],array('label'=>t("SQL debug mode ? ")))

      ,'_gg' => t("Google")
    ,'key_gg_ana'=>array_merge($form->gen_field['lib'],array('label'=>t("Google Analytics Key")))
    ,'key_gg_gwt'=>array_merge($form->gen_field['lib'],array('label'=>t("Google Webmaster Tools  Key")))
    ,'key_gg_gtm'=>array_merge($form->gen_field['lib'],array('label'=>t("Google GTM Key")))
    ,'key_gg_maps'=>array_merge($form->gen_field['lib'],array('label'=>t("Google Maps Key for FrontOffice (restriction by domain)")))
    ,'key_gg_maps_2'=>array_merge($form->gen_field['lib'],array('label'=>t("Google Maps Key for Batch (restriction by IP)")))
    ,'key_gg_captcha'=>array_merge($form->gen_field['lib'],array('label'=>t("Google Captcha Key")))
    ,'key_gg_captcha_url'=>array_merge($form->gen_field['lib'],array('label'=>t("Google Captcha URL")))
    ,'key_gg_captcha_secret'=>array_merge($form->gen_field['lib'],array('label'=>t("Google Captcha Secret")))
    ,'key_gg_captcha_sitekey'=>array_merge($form->gen_field['lib'],array('label'=>t("Google Captcha Site Key")))
    ,'key_gg_captcha_score'=>array_merge($form->gen_field['lib'],array('label'=>t("Google Captcha Score Treshold")))


    ,'_i18n' => t("Traduction")
    ,'is_i18n'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Utiliser plusieurs langues ?")))
    # edition d'une table relationnel avec relation 1-N, on est dans sites et on veut ajouter des locales. on cree une table relationnelles gen_sites_locales (voir le nommage des champs  dans la table)
    # le champ type le linked_object est la relation, la table relationnel est detectée automatiquement en fonction des nommages.
    ,'locale_list' =>array('label' => t("Langues disponibles ?")
          ,'type' =>'relationel_list'
          ,'arrayValues'=>$gen->collector['locales']->getKv()
          ,'multiple'=>true
          ,'linked_object'=>'locales' 
          ,'class'=>'chosen-select ')
      ,'key_deepl'=>array_merge($form->gen_field['lib'],array('label'=>t("DeepL Key")))
      ,'use_auto_translate'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Utiliser l'auto-traduction ?")))
      ,'use_i18n_master_slave'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Utiliser le blockage des traducteurs ?")))
      ,'use_i18n_all_fields'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Traduire tous les champs automatiquement ?"),'tooltip'=>t("Si activé, tous les champs type I18n seront traduits, sinon utilisez le tableau auto_translate_fields")))

       

      ,'_facebook' => t("API Facebook")
    ,'facebook_login'=>array_merge($form->gen_field['lib'],array('label'=>t("Facebook login")))
    ,'facebook_appid'=>array_merge($form->gen_field['lib'],array('label'=>t("Facebook Login APP ID")))

      ,'_facebook_chat' => t("Chat Facebook")
    ,'fb_chat_page_id'=>array_merge($form->gen_field['lib'],array('label'=>t("Facebook Page ID")))
    ,'fb_chat_color'=>array_merge($form->gen_field['lib'],array('label'=>t("Couleur")))
    ,'fb_chat_inmsg'=>array_merge($form->gen_field['lib'],array('label'=>t("Message de bienvenue")))
    ,'fb_chat_outmsg'=>array_merge($form->gen_field['lib'],array('label'=>t("Message d'au revoir")))

      ,'_smtp' => t("SMTP")
    ,'mailer_charset'=>array_merge($form->gen_field['lib'],array('label'=>t("SMTP Mailer Charset")))
    ,'mailer_host'=>array_merge($form->gen_field['lib'],array('label'=>t("SMTP Mailer Host")))
    ,'mailer_smtp_host'=>array_merge($form->gen_field['lib'],array('label'=>t("SMTP Mailer Host ")))
    ,'mailer_email_from' =>array_merge($form->gen_field['lib'],array('label'=>t("SMTP Mailer From")))
    ,'mailer_login'=>array_merge($form->gen_field['lib'],array('label'=>t("STMP AUTH Login")))
    ,'mailer_pass'=>array_merge($form->gen_field['lib'],array('label'=>t("SMTP AUTH Pass")))
    ,'mailer_port'=>array_merge($form->gen_field['lib'],array('label'=>t("SMTP Auth Port")))
    ,'mailer_sign'=>array_merge($form->gen_field['desc'],array('label'=>t("E-mail signature"),'class'=>'html_code','class'=>'html_code'))

      ,'_mailchimp' => t("API MailChimp")
    ,'mailchimp_api_url'=>array_merge($form->gen_field['lib'],array('label'=>t("MailChimp API URL")))
    ,'mailchimp_listid'=>array_merge($form->gen_field['lib'],array('label'=>t("MailChimd List ID")))
    ,'mailchimp_api_key'=>array_merge($form->gen_field['lib'],array('label'=>t("MailChimp API Key")))

      ,'_theme' => t("Thèmes")
    ,'www_themes' =>array_merge($form->gen_field['lib'],array('label'=>t("Theme")))
    ,'css_file' =>array_merge($form->gen_field['lib'],array('label'=>t("CSS file")))
      ,'_code' => t("Code additionnel")
    ,'head_code' =>array_merge($form->gen_field['desc'],array('label'=>t("Code dans le header"),'class'=>'html_code','tooltip'=>t("Le contenu sera inséré dans la balise <head> de votre header.<br />Ce code n'est pas filtré et peut comporter des tags css/js/html/")))
    ,'foot_code' =>array_merge($form->gen_field['desc'],array('label'=>t("Code dans le footer"),'class'=>'html_code','class'=>'html_code','tooltip'=>t("Le contenu sera inséré dans la balise <footer> de votre footer.<br />Ce code n'est pas filtré et peut comporter des tags css/js/html/")))
 );

