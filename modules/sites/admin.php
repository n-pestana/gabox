<?php
include(GABOX_BACK_ROOT.'header.php');
$curr_obj = 'sites';

# Edit
if((isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete']))){
    include('admin-fields.php');
    $form->lib      = t("Edition de site");
    $form->heading  = '';
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
# Listing
else{
    $listing        = array('sql','table_join','join_conditions','fields','title');
    $listing['sql'] = array(
          'gen_sites.id_'.$curr_obj
          ,'gen_sites.lib'
          ,'gen_sites.is_published'
          ,'group_concat(concat_ws(\'|\',gen_sites_locales.is_enabled,gen_locales.lib,gen_sites_locales.id_sites_locales) SEPARATOR ",")  as locale'
          ,'group_concat(gen_locales.locale SEPARATOR ",")  as flag'
    );

    $listing['table_join'] = array('gen_locales', 'gen_sites_locales');

    if (defined('ID_SITE') && !$gen->collector['admins']->isSuperAdmin()) {
        $listing['join_conditions'] = 'gen_sites.id_sites='.ID_SITE; 
    }
    else{
        $listing['join_conditions'] = '1';
    }

    $listing['join_conditions'] .= ' and gen_sites.id_sites = gen_sites_locales.id_sites AND gen_locales.id_locales = gen_sites_locales.id_locales group by gen_sites.id_'.$curr_obj.' ';

    $listing['fields']  = array(
        'lib' => array(
            'label'          => t("Nom du site")
            ,'edit_in_place' => true 
        )
        ,'is_published' => array(
            'label'         => t("Publié ?")
            ,'type'         => 'switch'
            ,'callback'     => 'getEnable'
            ,'edit_in_place'=> 'true'
        )
        ,'locale' => array(
            'label'              => t("Langage")
            ,'edit_in_place'     => false
            ,'type'              => 'string'
            ,'advanced_callback' => 'tagWithStatus'
        )

    );
    $listing['title'] = t("Liste des sites");
    include(GABOX_GEN_ROOT.'genlist.php');
}
include(GABOX_BACK_ROOT.'footer.php');

