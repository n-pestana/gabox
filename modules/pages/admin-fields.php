<?php
 $form->fields = array(
        '_cat1'         => t("Informations")
        ,'id_site'      =>$form->gen_field['id_site']
        ,'lib'          =>array_merge($form->gen_field['lib'],array('label'=>t("Libellé")))
        ,'url'=>array_merge($form->gen_field['lib'],array('label'=>t("Url"),'tooltip'=>t("Vous pouvez saisir une url personnalisée, ou laisser vide pour que Gabox la génère pour vous")))
        ,'type'         =>array_merge($form->gen_field['select'],array('label'=>t("Type de contenu")),array('arrayValues'=>array('post'=>'post','page'=>'Page'))) 
        ,'id_templates' =>array_merge($form->gen_field['select'],array('label'=>t("Template")),array('arrayValues'=>$gen->collector['templates']->getKV())) 
        ,'_SEO'         => 'SEO'
        ,'meta_title'   =>array_merge($form->gen_field['lib'],array('label'=>t("Meta title")))
        ,'meta_desc'    =>array_merge($form->gen_field['desc'],array('label'=>t("Meta desc")))
        ,'_content'        => 'Contenu'
        ,'media'=>array_merge($form->gen_field['file'],array('label'=>t("Image principale")))
        ,'content'=>array_merge($form->gen_field['simple_html_i18n'],array('label'=>t("Texte central")))
    );

