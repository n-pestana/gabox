<?php
include(GABOX_BACK_ROOT.'header.php');
$curr_obj='templates';

$table_join = array('gen_templates');

if(defined('ID_SITE')){ 
  $join_conditions = ' gen_templates.id_site='.ID_SITE;
}

 $form->action = 'templates.php'; 


# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){

 $form->fields = array(
        'id_site'      =>$form->gen_field['id_site']
        ,'lib'          =>array_merge($form->gen_field['lib'],array('label'=>t("Libellé")))
        ,'content'    =>array_merge($form->gen_field['desc'],array('class'=>'html_code','label'=>t("Contenu"),'tooltip'=>t("Vous pouvez utiliser des variables en fonction de la template choisie. <br />Par exemple pour la template principale, vous pouvez utiliser les variables suivantes : <br />{\$lib}<br />{\$media}<br />{\$content}")))
    );

    $form->lib= 'Template editing';
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
# Listing
else{
    $array = array(
          'id_'.$curr_obj
          ,'lib'
          ,'updated_at'
          ,'created_at'
    );

    $fieldsOptions = array(
        'lib' => array(
            'label'         => ("Libellé")
	        ,'edit_in_place'=>true
        ) 

        ,'created_at' => array(
                'label'         => ("Créé le")
        )
        ,'updated_at' => array(
                'label'         => ("Dernière mise à jour")
        )
    );

    $extra_conf['title'] = 'Templates';
    include(GABOX_GEN_ROOT.'genlist.php');
}

include(GABOX_BACK_ROOT.'footer.php');
