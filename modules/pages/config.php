<?php
$mod['name']       = 'Gestion de contenu';
$mod['desc']       = '';
$mod['fa-icon']    = 'fas fa-images';
$mod['version']    = '1.0';


$mod['sql'][] = "CREATE TABLE `gen_pages` (  `id_pages` mediumint(8) UNSIGNED NOT NULL,  `type` enum('post','page') NOT NULL DEFAULT 'page',  `id_admins` mediumint(11) DEFAULT NULL,  `version` mediumint(9) DEFAULT NULL,  `lib` varchar(1024) DEFAULT NULL,  `created_at` datetime DEFAULT NULL,  `updated_at` datetime DEFAULT NULL,  `url` varchar(256) DEFAULT NULL, `meta_desc` varchar(1024) DEFAULT NULL,  `meta_title` varchar(1024) DEFAULT NULL,  `id_templates` mediumint(8) UNSIGNED DEFAULT NULL,  `is_published` tinyint(1) DEFAULT NULL,  `content` longtext,  `id_site` mediumint(8) UNSIGNED DEFAULT NULL,  `media` varchar(255) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
$mod['sql'][] = "ALTER TABLE `gen_pages`  ADD PRIMARY KEY (`id_pages`);";
$mod['sql'][] = "ALTER TABLE `gen_pages`  MODIFY `id_pages` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;";

$mod['sql'][] = "CREATE TABLE `gen_pages_options` (  `id_pages_options` mediumint(8) UNSIGNED NOT NULL,  `content_preprend` text,  `content_append` text,  `css` text,  `created_at` datetime DEFAULT NULL,  `updated_at` datetime DEFAULT NULL,  `id_site` mediumint(8) UNSIGNED DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
$mod['sql'][] = "ALTER TABLE `gen_pages_options`  ADD PRIMARY KEY (`id_pages_options`);";
$mod['sql'][] = "ALTER TABLE `gen_pages_options`  MODIFY `id_pages_options` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;";

$mod['sql'][] = "CREATE TABLE IF NOT EXISTS `gen_templates` (  `id_templates` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,   `lib` varchar(1024) DEFAULT NULL,  `created_at` datetime DEFAULT NULL,  `updated_at` datetime DEFAULT NULL, `content` longtext NOT NULL,  `thumb` varchar(255) DEFAULT NULL,  `type` varchar(50) DEFAULT 'template',  `is_valid` tinyint(1) DEFAULT NULL,  `display_type` varchar(255) DEFAULT NULL,  `tpl_version` tinyint(4) DEFAULT NULL, `id_site` mediumint(8) UNSIGNED DEFAULT NULL , `configurable_fields` text,  PRIMARY KEY (`id_templates`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";


$mod['unsql'][] = "DROP TABLE gen_pages";
$mod['unsql'][] = "DROP TABLE gen_pages_options";
$mod['unsql'][] = "DROP TABLE gen_templates";

if(defined('ID_SITE')){
    $mod['bo-hook'] = 
          array(
            'lib' => 'Gestion de contenu',
            'icon' => 'th-large',
            'links' => array (
                array('link' => '/modules/pages/admin.php',  'lib' => 'Lister les contenus'),
                array('link' => '/modules/pages/templates.php?'.((defined('ID_SITE')) ? 'is_site='.ID_SITE : ''),  'lib' => 'Templates'),
                array('link' => '/modules/pages/options.php?id_site='.((defined('ID_SITE')) ? 'is_site='.ID_SITE : '').'&id_pages_options=1&nolist=1',  'lib' => 'Options')
            )
        );
}

