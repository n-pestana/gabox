<?php
include(GABOX_BACK_ROOT.'header.php');
$curr_obj='pages';

$table_join = array('gen_pages');

if(defined('ID_SITE')){ 
  $join_conditions = ' gen_pages.id_site='.ID_SITE;
}


# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    include('admin-fields.php');
    $form->lib= 'Content editing';
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
# Listing
else{
    $array = array(
          'id_'.$curr_obj
          ,'lib'
          ,'url'
          ,'is_published'
          ,'version'
          ,'id_admins'
          ,'id_templates'
          ,'type'
          ,'updated_at'
          ,'created_at'
    );

    $fieldsOptions = array(
        'lib' => array(
            'label'         => ("Libellé")
	        ,'edit_in_place'=>true
        ) 
        ,'url' => array(
            'label'         => ("Url")
        ) 
       ,'type' => array(
                'label'         => ("Type")
        ) 
        ,'version' => array(
                'label'         => ("Version")
        ) 
        ,'id_admins' => array(
                'label'         => ("Utilisateur")
               ,'callback'      => 'admins::getLib' 
        ) 


        ,'created_at' => array(
                'label'         => ("Créé le")
        )
        ,'updated_at' => array(
                'label'         => ("Dernière mise à jour")
        )
        ,'created_at' => array(
                'label'         => ("Type")
        )
        ,'is_published' => array(
            'label'         => t("Publié ?")
            ,'type'         => 'switch'
            ,'callback'     => 'getEnable'
            ,'edit_in_place'=> 'true'
        )


    );

    $extra_conf['title'] = 'Contenus';
    include(GABOX_GEN_ROOT.'genlist.php');
}

include(GABOX_BACK_ROOT.'footer.php');
