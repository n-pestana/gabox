<?php
$_GET['id_pages_options'] = 1;
if(isset($_POST)&& !empty($_POST)){
    header("Location:  ".$_SERVER['PHP_SELF'].'?id_pages_options='.$_GET['id_pages_options']);
}

include(GABOX_BACK_ROOT.'header.php');
$curr_obj = 'pages_options';

# Edit
if((isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete']))){
    $form->fields = array(
        'id_site'      =>$form->gen_field['id_site']

        ,'content_preprend'    =>array_merge($form->gen_field['desc'],array('label'=>t("Contenu avant la page")))
        ,'content_append'    =>array_merge($form->gen_field['desc'],array('label'=>t("Contenu après la page")))
        ,'css'    =>array_merge($form->gen_field['desc'],array('label'=>t("Contenut CSS")))

    );

    $form->lib      = t("Gestion de contenus - Options");
    $form->heading  = '';
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
else{
    echo "list";
    $listing['nolist'] = true;
    include(GABOX_GEN_ROOT.'genlist.php');
}


include(GABOX_BACK_ROOT.'footer.php');

