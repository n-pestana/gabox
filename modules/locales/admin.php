<?php
include(GABOX_BACK_ROOT.'header.php');
$curr_obj='locales';

# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    include('admin-fields.php');
    $form->lib      = t("Locales listing");
    $form->heading  = t("Locales are predefined regarding ISO-3166");
    include(GABOX_GEN_ROOT.'gen_edit.php');
}
# Listing
else{

    $listing['sql']= array(
              'id_'.$curr_obj
              ,'locale'
              ,'language'
              ,'country'
              ,'lib_fr'
              ,'lib_en'
              ,'lib'
              ,'lib_language'
              ,'is_master'
    );

    $listing['fields']= array(
        'lib_en'=> array(
            'label' => t("Name")
            ,'edit_in_place' => true 
        )
        ,'locale' => array(
            'label'         => t("Name")
            ,'edit_in_place' => true 
        )
        ,'lib' => array(
            'label'         => t("Language")
            ,'edit_in_place' => true 
        )
        ,'country' => array(
            'label'         => t("Country")
            ,'callback' => 'flagize'
        )
        ,'is_master' => array(
            'label'         => t("Is master ? ")
            ,'callback'=>'getEnable'
            ,'edit_in_place'=>'true'
            ,'type'=>'switch'
        )
    );
    $listing['title'] = t("Locales listing");
    include(GABOX_GEN_ROOT.'genlist.php');
}

include(GABOX_BACK_ROOT.'footer.php');
