<?php
$form->fields= array(
        '_cat1' => t("Informations")
        ,'locale'=>array_merge($form->gen_field['lib'],array('label'=>t("Locale (ex : en_GB)"),'maxlength'=>'6'))
        ,'language'=>array_merge($form->gen_field['lib'],array('label'=>t("Code de la langue (ex: en)"),'size'=>'6'))
        ,'country'=>array_merge($form->gen_field['lib'],array('label'=>t("Code du pays (ex: GB)"),'size'=>'6'))
        ,'iso_translator'=>array_merge($form->gen_field['lib'],array('label'=>t("Code langue pour traducteur automatique"),'size'=>'6'))
        ,'is_master'=>array_merge($form->gen_field['ouinon'],array('label'=>t("Est maitresse")))
        ,'_cat2'=> t("Libellés")
        ,'lib_fr'=>array_merge($form->gen_field['lib'],array('label'=>t("Nom en français"),'size'=>'6'))
        ,'lib_en'=>array_merge($form->gen_field['lib'],array('label'=>t("Nom dans la langue"),'size'=>'6'))
        ,'lib'=>array_merge($form->gen_field['lib'],array('label'=>t("Nom internationnal"),'size'=>'6'))
        ,'lib_language'=>array_merge($form->gen_field['lib'],array('label'=>t("Nom traduit"),'size'=>'6'))
        ,'id_country'=>array_merge($form->gen_field['select'],array('label'=>t("Pays")),array('arrayValues'=>$gen->collector['country']->getKv()),array('class'=>'chosen-select'))
    );
