<?php
$mod['name']       = 'Locales';
$mod['desc']       = 'Module de gestion des locales';
$mod['fa-icon']    = 'fa-duotone fa-earth-americas';
$mod['version']    = '1.0';
$mod['link']       = '/modules/locales/admin.php';
