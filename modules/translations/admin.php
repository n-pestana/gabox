<?php
if(isset($_GET['translate-now'])){
    if(isset($_GET['id'])){
        $gen->collector['translations']->TranslateNow((int)$_GET['id'],true);
    }
    else{
        $gen->collector['translations']->TranslateNow();
    }
}

include(GABOX_BACK_ROOT.'/header.php');
$curr_obj='translations';

# Edit
if (isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])) {
    include('admin-fields.php');
    $form->lib     = t("Edition de surcharge textuelle traduction");

    include(GABOX_GEN_ROOT.'gen_edit.php');
        
} else { # Listing
//    $listing        = ['sql','table_join','join_conditions','fields','title'];
    $listing['sql'] = [
         'gen_'.$curr_obj.'.id_'.$curr_obj
        ,'gen_'.$curr_obj.'.lib'
        , 'gen_'.$curr_obj.'.id_'.$curr_obj .' as id2'
        , 'gen_'.$curr_obj.'.id_'.$curr_obj .' as id3'
        ,'gen_'.$curr_obj.'.is_published'
    ];

    $listing['fields'] = [
                        'lib' => ['label' => t("Source"), 'edit_in_place' => true]
                        , 'id2'  => ['label' => t("Surcharge textuelle"),'callback'=>'getTranslations']
                        , 'is_published' => ['label' => t("Visible ?")
                            ,'type'         => 'switch'
                            ,'callback'     => 'getEnable'
                            ,'edit_in_place'=> 'true'
                        ]
                        , 'id3'  => ['label' => t("Traduire !"),'callback'=>'TranslateButton']
    ];


    $listing['title'] = t("Listing des surcharges textuelles traductions");
    $listing['heading'] = t("Le frontoffice et le backoffive peuvent être textuellement surchargées ou traduites via ce module, il s'agit des parties non administrables en général")
                    .'<br />'
                    .t("Vous pouvez activer la découverte et l'enregistrement des chaines de caractère à traduire en cliquand sur ce bouton:")
                    .'<br /><br /><center>';
    
if(defined('ID_SITE')){
    $site = $gen->collector['sites']->get(ID_SITE);
    if($site){
        $listing['heading'] .= '<a target="_blank" href="'.$site['prod_url'].'?auto-add-translations=true" class="btn btn-primary">'.t("Activer la découverte des traductions en prod").'<br>'.$site['prod_url'].'</a>';
        $listing['heading'] .= '&nbsp;<a target="_blank" href="'.$site['internal_url'].'?auto-add-translations=true" class="btn btn-primary">'.t("Activer la découverte des traductions en preprod").'<br>'.$site['prod_url'].'</a>';
    }
    else{
        $listing['heading']="<span class='alert alert-danger'>".t("Aucune information trouvé sur votre site ID").ID_SITE.t("Cela peut se produire si le ID_SITE défini dans votre configuration Apache est différent de celui entré en base").'</span>';
    }
}
if(!isset($site['key_deepl']) ||  !$site['key_deepl']){
    
    $listing['heading'] .= "<br><br ><div class='alert alert-warning'><p>".t("Afin d'utiliser la traduction automatique, vous devez entrer votre clé DeepL disponible dans")."&nbsp; <a href='".URL_LOC."/modules/sites/admin.php?id_sites=".ID_SITE."&tab=_i18n'>".t("Options de langue")."</a></p></div>";

}
else{
    $cnt = $gen->collector['translations']->getCount();
    $listing['heading'] .= '&nbsp;'.'<a class="btn btn-primary" href="/modules/translations/admin.php?translate-now=true">'.t("Lancer la traduction automatique"). '<br> ('.$cnt.')</a></center>';
}

    include(GABOX_GEN_ROOT.'genlist.php');
}
include(GABOX_BACK_ROOT.'/footer.php');

function getTranslations($id){
    global $gen;
    $out = '';
    $temp = $gen->collector['i18n']->get(array('id_element'=>$id,'module'=>'translations'));
    if(is_array($temp))foreach($temp as $k=>$v){
        if(empty($v['lib'])) continue;
        $locale = $gen->collector['locales']->getCached($v['id_locales']);
        $flag = '<img width="16" height="11" title="'.$locale['lib'].'" alt="'.$locale['lib'].'" src="/assets/images/icons/flags/'.strtolower($locale['country']).'.png" >';
        $out .= $flag.' '.$v['lib']."<br />";
    }
    return $out;
    
    
}

function TranslateButton($id){
    return '<a class="btn btn-primary" href="/modules/translations/admin.php?translate-now=true&id='.$id.'">'.t("Traduire !").'</a>';

}
