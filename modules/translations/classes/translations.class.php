<?php class Translations extends  gen_translations{
	var $id_locales= '';
    
    protected $_currentLocale;

    function __construct(){
        if(defined('ID_LOC')){
           $this->setCurrentLocale(ID_LOC);
        }
    }

    
    function TranslateNow($id_translations = '', $force = false){
        # force = true will replace all existing translations

        $t = $this->get(
            ($id_translations) 
                ? array('id_translations'=>$id_translations) 
                : array()
        );


        $available_locales=array();

        $temp = $this->collector['sites']->getAvailabledLocalesForSite(ID_SITE); 

        foreach($temp as $k=>$v){
            $available_locales[$v['id_locales']] = $this->collector['locales']->getOneCached($v['id_locales']);
        }

        if(is_array($available_locales)) foreach ($available_locales as $locale){

            if(is_array($t)) foreach($t as $translation){

                $exist = $this->collector['i18n']->getOne(
                    array(
                         'module'       => 'translations'
                        ,'field_name'   => 'translations'
                        ,'id_element'   => $translation['id_translations']
                        ,'id_locales'   => $locale['id_locales']
                    )
                );

                if($force || (!$exist || !isset($exist['lib']) || empty($exist['lib']))){
                    $id = (isset($exist['id_i18n'])) ? $exist['id_i18n'] : '';
                    $a = array(
                       'module'         => 'translations'
                      ,'field_name'     => 'translations'
                      ,'id_element'     => $translation['id_translations']
                      ,'id_locales'     => $locale['id_locales']
                      ,'lib'            => $this->auto_translate($translation['lib'], '', $locale['iso_translator'])
                      ,'is_published'   => 1
                    );
                    $this->collector['i18n']->set($id,$a);
                }

            }

        }

    }

	function translate($source, $part='' , $autoTranslate=false) {


        # testing mode, if source is not replaced by '-', add t() 
		if(isset($_GET['hide-translations'])) return '-'; 
        
        $exist= $this->getOne(array(
            'lib'   => $source
        ));

        # setting back or front part by default
        if(empty($part)){
           $part = (isset($_SERVER['back']) && $_SERVER['back']==1) ? 'back' : 'front';
        }

        # not found in gen_translations : add it !
        if(!$exist || empty($exist)) {
            $id = $this->set(array(
                'lib'   => $source
                ,'part' => $part
            ));
            $exist = $this->getOne($id);
        }

        if($this->_currentLocale !== null) {
            $trad = $this->getOneI18n(array('gen_translations.lib'=> $source), '', '', $this->_currentLocale);
        } else {
            $trad = $this->getOneI18n(array('gen_translations.lib'=> $source));
        }


        # translation exist and is filled
        if(isset($trad['translations']) && !empty($trad['translations']) && $trad['is_published']==1){
            return $trad['translations'];
        }

        # the translation is disabled, the content must be hidden 
        elseif(isset($trad['translations']) && !empty($trad['translations']) && $trad['is_published']==0){
            return '';
        }
        # no i18 translation found or i18n empty
        elseif(!isset($trad['translations'])  || ( isset($trad['lib'])&&$trad['lib']=='' )){

            if(defined('ID_SITE')){
                if(isset($this->collector['sites'])){
                    $site = $this->collector['sites']->get(ID_SITE);
                }
            }
            if($this->_currentLocale !== null && isset($site['use_auto_translate']) && $site['use_auto_translate']==1 && isset($site['key_deepl']) &&  $site['key_deepl'] ) {

                $current_i18n = array(
                   'module'         => 'translations'
                  ,'field_name'     => 'translations'
                  ,'id_element'     => $trad['id_translations']
                  ,'id_locales'     => $this->_currentLocale 
                  ,'is_published'   => 1
                );

                $i18n_exist = $this->collector['i18n']->getOne( $current_i18n );

                # i18n not found, created it
                if(!$i18n_exist || !isset($i18n_exist['id_i18n'])) {
                    $id = $this->collector['i18n']->set('',$current_i18n);
                    $i18n_exist['id_i18n'] = $id;
                }

                $locale = $this->collector['locales']->getOneCached($this->_currentLocale);

                if(!$locale){
                    die('Unable to find locale during auto translation process');
                }

                $new_lib = $this->auto_translate($source, '', $locale['iso_translator']);
                $this->collector['i18n']->set($i18n_exist['id_i18n'], array('lib'=>$new_lib));
                return $new_lib;
            }
            # no autotranslate
            else{
               return (isset($trad['lib'])) ? $trad['lib'] : $source;
            }
    
        } elseif(isset($trad['lib'])) {
            die('no trad found');
            return $trad['lib'];
        }
		return $source;
	}

    public function getCurrentLocale() 
    {
        return $this->_currentLocale;
    }
    public function setCurrentLocale($locale) 
    {
        $this->_currentLocale = (int) $locale;
    }

}
