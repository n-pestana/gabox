<?php
$form->fields = [
    '_info'        => t("Informations")
    ,'lib'         => array_merge($form->gen_field['desc'], ['label' => t("Source")])
    ,'translations'   => array_merge($form->gen_field['lib_i18n'], ['label' => t("Surcharge textuelle"), 'maxlength' => ''])
    ,'is_published'   => array_merge($form->gen_field['ouinon'], ['label' => t("Visible ?")])
    ,'updated_at'   => $form->gen_field['updated_at']
    ];
