<?php
$mod['name']        = t("Surcharge textuelle");
$mod['desc']        = t("Ce module permet de gérer les surcharges textuelles et eventuelles traductions dans le code appelé par t(), par exemple <?=t('Lorem Ipsum')?>");
$mod['fa-icon']     = 'fa-solid fa-language';
$mod['version']     = '1.1';

# integrated module
/*$mod['sql'][]      = "
CREATE TABLE IF NOT EXISTS `gen_translations` (
  `id_translations` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `lib` mediumtext NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `level` tinyint(1) NOT NULL DEFAULT '1',
  `translations` mediumtext,
  `part` varchar(1024) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
        */
//$mod['unsql'][]    = "DROP TABLE gen_translations;";


