<?php
include(GABOX_BACK_ROOT.'/header.php');
$curr_obj='contents';

# Edit
if (isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])) {
    include('admin-fields.php');
    $form->lib     = t("Edit content");
    $form->heading = '';
    
    if(!empty($_GET['id_'.$curr_obj])) {
        $contentData = $gen->collector[$curr_obj]->getOne($_GET['id_'.$curr_obj]);
        $templateData = $gen->collector['contents_templates']->getOne($contentData['id_contents_templates']);
        $contentFieldsCount = $templateData['content_count'];
        $textFieldsCount = $templateData['text_count'];
        $mediaFieldsCount = $templateData['media_count'];
        for ($i=$contentFieldsCount+1; $i<=3; $i++) {
            unset($form->fields["content$i"]);
        }
        for ($i=$textFieldsCount+1; $i<=7; $i++) {
            unset($form->fields["text$i"]);
        }
        for ($i=$mediaFieldsCount+1; $i<=5; $i++) {
            unset($form->fields["media$i"]);
        }
    }
    
    include(GABOX_GEN_ROOT.'gen_edit.php');
} else { # Listing
    $listing        = ['sql','table_join','join_conditions','fields','title'];
    $listing['sql'] = ['gen_'.$curr_obj.'.id_'.$curr_obj
        ,'gen_'.$curr_obj.'.lib'
        ,'gen_'.$curr_obj.'.enabled'
        ,'gen_contents_templates.lib AS tpl_lib'
        ,'gen_contents_templates.media'
        ,'gen_contents_templates.type'
        ,'gen_sites.lib AS site_lib'
    ];
    
    $listing['table_join'] = ['gen_contents_templates'];
    $listing['join_conditions'] = 'gen_contents_templates.id_contents_templates = gen_'.$curr_obj.'.id_contents_templates';

    $listing['fields'] = ['lib'      => ['label' => t("Title"), 'edit_in_place' => true]
                        , 'tpl_lib'  => ['label' => t("Template")]
                        , 'media'    => ['label' => t("Preview"), 'callback' => 'getMedia']
                        , 'type'     => ['label' => t("Type de template")]
                        , 'enabled'  => ['label' => t("Publié ?")
                            ,'type'         => 'switch'
                            ,'callback'     => 'getEnable'
                            ,'edit_in_place'=> 'true'
                        ]
                        , 'site_lib' => ['label' => t("Site")]
    ];
        
    $listing['title'] = t("Contents list");
    include(GABOX_GEN_ROOT.'genlist.php');
}
include(GABOX_BACK_ROOT.'/footer.php');
