<?php
$form->fields = [
    '_info'                  => t("Informations")
    ,'lib'                   => array_merge($form->gen_field['lib_i18n'], ['label' => t("Title")])
    ,'hook'                  => array_merge($form->gen_field['lib'], ['label' => t("Hook")])
    ,'id_contents_templates' => array_merge($form->gen_field['select'] , ['label' => t("Template")], 
                        ['arrayValues' => $gen->collector['contents_templates']->getKV()])
    ,'display_bo_menu'       => array_merge($form->gen_field['ouinon'], ['label' => t("Display in BO menu")])
    ,'_content'              => 'Contenu'
    ];
for ($i=1; $i<=3; $i++) {
    $form->fields["content$i"] = array_merge($form->gen_field['simple_html_i18n'], ['label' => t("Contenu") . " #$i"]);
}
for ($i=1; $i<=7; $i++) {
    $form->fields["text$i"] = array_merge($form->gen_field['lib_i18n'], ['label' => t("Texte") . " #$i"]);
}
for ($i=1; $i<=5; $i++) {
    $form->fields["media$i"] = array_merge($form->gen_field['file'], ['label' => t("Image") . " #$i"]);
}
$form->fields += [
    '_publication'  => t("Publication")
    ,'url'          => array_merge($form->gen_field['lib_i18n'], ['label' => t("Url") . ' ' . t("(auto fill)")])
    ,'enabled'      => $form->gen_field['ouinon']
    ,'id_sites'     => array_merge($form->gen_field['select'], ['label' => t("Site")],
                        ['arrayValues'=>$gen->collector['sites']->getKv()],
                        ['class'=>'chosen-select'],
                        ['value' => defined('ID_SITE') ? ID_SITE : '']) 
    ,'updated_at'   => $form->gen_field['updated_at']
    ];
