<?php
class Contents extends gen_contents {
    public function set($primary_key_value = '' , $array_fields = '', $criteres = array()) {
        if(array_key_exists('url', $array_fields) && empty($array_fields['url'])){
            $array_fields['url'] = strtolower(urlize2($array_fields['lib']));
        }
       $id = parent::set($primary_key_value, $array_fields, $criteres);
       return $id;
    }
}