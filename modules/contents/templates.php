<?php
include(GABOX_BACK_ROOT.'header.php');
$curr_obj = 'contents_templates';

$form->action = 'templates.php'; 

# Edit
if(isset($_GET['id_'.$curr_obj]) && !isset($_GET['delete'])){
    $form->fields = [
        '_info'          => t("Informations")
        ,'lib'           => array_merge($form->gen_field['lib'], ['label' => t("Title")])
        ,'name'          => array_merge($form->gen_field['lib'], ['label' => t("Name")])
        ,'media'         => array_merge($form->gen_field['file'], ['label' => t("Preview")])
        ,'type'          => array_merge($form->gen_field['select'], ['label' => t("Type de contenu")], 
                            ['arrayValues' => ['block' => t("Bloc"), 'page' => t("Page")]])
        ,'content_count' => array_merge($form->gen_field['select'], ['label' => t("Content fields count")], ['arrayValues' => [0,1,2,3]])
        ,'text_count'    => array_merge($form->gen_field['select'], ['label' => t("Text fields count")], ['arrayValues' => [0,1,2,3,4,5,6,7]])
        ,'media_count' => array_merge($form->gen_field['select'], ['label' => t("Media fields count")], ['arrayValues' => [0,1,2,3,4,5]])
        ,'_publication'  => t("Publication")
        ,'enabled'       => $form->gen_field['ouinon']
        ,'id_sites'      => array_merge($form->gen_field['select'], ['label' => t("Site")],
                            ['arrayValues' => $gen->collector['sites']->getKv()],
                            ['class' => 'chosen-select'],
                            ['value' => defined('ID_SITE') ? ID_SITE : '']) 
        ,'updated_at'    => $form->gen_field['updated_at']
        ];
    $form->lib= 'Edit template';
    include(GABOX_GEN_ROOT.'gen_edit.php');
} else { # Listing
    $listing        = ['sql','table_join','join_conditions','fields','title'];
    $listing['sql'] = ['gen_'.$curr_obj.'.id_'.$curr_obj
        ,'gen_'.$curr_obj.'.lib'
        ,'gen_'.$curr_obj.'.media'
        ,'gen_'.$curr_obj.'.type'
        ,'gen_'.$curr_obj.'.enabled'
        ,'gen_sites.lib AS site_lib'
    ];

    $listing['fields'] = ['lib'      => ['edit_in_place' => true]
                        , 'media'    => ['label' => t("Preview"), 'callback'=>'getMedia']
                        , 'type'     => ['label' => t("Type")]
                        , 'enabled'  => ['label' => t("Publié ?")
                            ,'type'         => 'switch'
                            ,'callback'     => 'getEnable'
                            ,'edit_in_place'=> 'true'
                        ]
                        , 'site_lib' => ['label' => t("Site")]
    ];

    $listing['title'] = 'Templates list';
    include(GABOX_GEN_ROOT.'genlist.php');
}
include(GABOX_BACK_ROOT.'footer.php');
