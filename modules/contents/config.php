<?php
$mod['name']       = 'Gestion de contenu';
$mod['desc']       = 'Gestion des contenus des blocs et des pages du site';
$mod['fa-icon']    = 'th-large';
$mod['version']    = '1.0';

$mod['sql'][] = "
CREATE TABLE IF NOT EXISTS `gen_contents_templates` (
  `id_contents_templates` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sites` mediumint(8) UNSIGNED NOT NULL,
  `type` enum('page','block') NOT NULL,
  `lib` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_count` tinyint(1) DEFAULT '1',
  `text_count` tinyint(1) DEFAULT '0',
  `media_count` tinyint(1) DEFAULT '0',
  `enabled` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `media` varchar(1024) DEFAULT '',
  FOREIGN KEY (id_sites) REFERENCES gen_sites(id_sites)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";

$mod['sql'][] = "
CREATE TABLE IF NOT EXISTS `gen_contents` (
  `id_contents` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sites` mediumint(8) UNSIGNED NOT NULL,
  `lib` varchar(255) NOT NULL,
  `hook` varchar(255) NOT NULL,
  `content1` longtext DEFAULT NULL,
  `content2` longtext DEFAULT NULL,
  `content3` longtext DEFAULT NULL,
  `text1` text DEFAULT NULL,
  `text2` text DEFAULT NULL,
  `text3` text DEFAULT NULL,
  `text4` text DEFAULT NULL,
  `text5` text DEFAULT NULL,
  `text6` text DEFAULT NULL,
  `text7` text DEFAULT NULL,
  `media1` varchar(255) DEFAULT NULL,
  `media2` varchar(255) DEFAULT NULL,
  `media3` varchar(255) DEFAULT NULL,
  `media4` varchar(255) DEFAULT NULL,
  `media5` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `display_bo_menu` tinyint(1) DEFAULT '0',
  `id_contents_templates` mediumint(8) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  FOREIGN KEY (id_sites) REFERENCES gen_sites(id_sites),
  FOREIGN KEY (id_contents_templates) REFERENCES gen_contents_templates(id_contents_templates)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";

$mod['unsql'][] = "DROP TABLE gen_contents;";
$mod['unsql'][] = "DROP TABLE gen_contents_templates;";

$mod['bo-hook'] = [
        'lib' => 'Gestion de contenu',
        'icon' => 'th-large',
        'links' => [
            ['link' => '/modules/contents/admin.php', 'lib' => 'Lister les contenus'],
            ['link' => '/modules/contents/templates.php', 'lib' => 'Templates']
        ]
    ];

if (isset($gen->collector['contents']) && $gen->collector['contents']->get(['display_bo_menu' => 1])) {
    foreach ($gen->collector['contents']->get(['display_bo_menu' => 1]) as $link) {
        $mod['bo-hook']['links'][] = ['lib' => $link['lib'], 'link' => '/modules/contents/admin.php?hash=' . HASH_KEY . '&id_contents=' . $link['id_contents']];
    }
}
