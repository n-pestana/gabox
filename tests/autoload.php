<?php
error_reporting(E_ALL);
$_SERVER['lang'] = 'fr';
$_SERVER['back'] = 0;
$_SERVER['front'] = 1;
$_SERVER['env'] = 'preprod';
$_SERVER['prod'] = 0;
$_SERVER['phpunit'] = 1;

if (defined('PROJECT_NAME')) {
    $_SERVER['usr'] = PROJECT_NAME;
} else {
    //TODO: tests Gabox ?
}
if (defined('PROJECT_ID')) {
    $_SERVER['id_site'] = PROJECT_ID;
}

if (defined('BACK')) {
    if (BACK == "1") {
        $_SERVER['back'] = 1;
        $_SERVER['front'] = 0;
    }
}
if (defined('ENVIRONMENT')) {
    $_SERVER['env'] = ENVIRONMENT;
}
if (defined('PROD')) {
    $_SERVER['prod'] = PROD;
}

if (!empty(getenv('DOCUMENT_ROOT'))) {
    $_SERVER['DOCUMENT_ROOT'] = getenv('DOCUMENT_ROOT');
} else {
    $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../../usr/' . $_SERVER['usr'] . '/www/');
}

const DISPLAY_OUTPUT = 'no';
include(dirname(__FILE__) . '/../autoprepend.php');

if (is_dir(GABOX_USR_DIR . '/tests/autoload')) {
    spl_autoload_register(function ($class) {
        if (file_exists(GABOX_USR_DIR . '/tests/autoload/' . $class . '.php')) {
            require_once GABOX_USR_DIR . '/tests/autoload/' . $class . '.php';
        }
    });
}
