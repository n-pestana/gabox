# Installation
## Composer
Une fois le projet installé, afin d'installer les dépendances gérées par composer, lancer les commandes suivantes :
- `composer install`
- `composer dump-autoload`

## Tips
### tester si un module est activé : 
if($this->collector['modules']->IsEnabled('testimonials')){ 

### dans une classe genclass on peut accéder aux vars du front ainsi : 
$this->collector['displayer']->vars[]

### ne pas utiliser de liens en dur, utiliser le treeview et : 
$this->collector['treeview']->LinkByHook('contact')
