<?php
use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use Psecio\SecureDotenv\Parser;
use Symfony\Component\Dotenv\Dotenv;

// Place les variables dans le scope global, sinon elles ne sont pas visibles au niveau de l'autoload
// cf. https://stackoverflow.com/questions/15676353/laravel-4-why-does-my-class-autoload-but-global-variables-are-not-available
global $conf, $gen;

# for dynamic images
if(isset($_GET['l']) && isset($_GET['p'])){
    $temp = md5($_GET['l'].$_GET['p']);
    $res = apc_fetch($temp);
    if($res){
      header("Cache-Control: private, max-age=10800, pre-check=10800");
      header("Pragma: private");
      header("Content-Type: image/jpeg");
      die($res);
    }
}
# starting chrono
$_SERVER['start'] = microtime(true);
define('TIME_START', microtime(true));

define('IP_STAFF','80.119.202.145');

$conf = $obj = array();

foreach(array('lang','usr','env','country','front','prod') as $k=>$v){
    $conf[$v]=isset($_SERVER[$v]) ? $_SERVER[$v] : '';
}
if(isset($_SERVER['id_site'])) {
    define('ID_SITE',$_SERVER['id_site']);
}

if (substr($_SERVER['DOCUMENT_ROOT'], -1) != '/') {
    $_SERVER['DOCUMENT_ROOT'] .= '/';
}

#usr batch mode: no param  (current usr project and curent env)
if(isset($argv) && isset($argv[0]) && !isset($argv[1]) && !defined('GABOX_USR_DIR')){
    define('GABOX_USR_DIR',realpath('../'));
    define('USR',realpath('../'));
    define('ROOT',realpath('../../../'));
}

if(!defined('GABOX_USR_DIR')) {
    if(isset($_SERVER['front']) && $_SERVER['front']==1){
        $conf['usrdir']=realpath($_SERVER['DOCUMENT_ROOT'].'../');
        $conf['root']=realpath($_SERVER['DOCUMENT_ROOT'].'../../../');
    }
    else{
        $conf['usrdir']=realpath($_SERVER['DOCUMENT_ROOT'].'../../')."/usr/".$conf['usr'];
        $conf['root']=realpath($_SERVER['DOCUMENT_ROOT'].'../../');
    }

    # mode batch multi parametre sans sans passage de USR et ENV en parametre
    if($conf['root'] == '/'){
      define('GABOX_USR_DIR',realpath('../'));
      define('USR',realpath('../'));
      define('ROOT',realpath('../../../'));
    }
    else{
      define('GABOX_USR_DIR',$conf['usrdir']);
      define('USR',$conf['usr']);
      define('ROOT',$conf['root']);
    }
}



define('INC',ROOT.'/gabox/');
define('LIBS',ROOT.'/gabox/libs/');
define('COMMON',ROOT.'/gabox/common/');
define('ADMIN',ROOT.'/gabox/admin-common/');
define('MODS',ROOT.'/gabox/modules/');
define('USR_MODS', GABOX_USR_DIR.'/modules/');
if (file_exists(INC . 'vendor/autoload.php')) {
    require_once INC . 'vendor/autoload.php';
}
if (file_exists(GABOX_USR_DIR . '/vendor/autoload.php')) {
    require_once GABOX_USR_DIR . '/vendor/autoload.php';
}
// Récupération des paramètres du fichier .env
$env_file = GABOX_USR_DIR . '/.env';
if (isset($_SERVER['env']) && file_exists(GABOX_USR_DIR.'/.env.' . $_SERVER['env'])) {
    $env_file = GABOX_USR_DIR.'/.env.' . $_SERVER['env'];
}
if (file_exists($env_file)) {
    $dotenv = new Dotenv();
    $dotenv->load($env_file);
}

if (!function_exists('getCryptedEnv')) {
    function getCryptedEnv($name) {
        $env_file = GABOX_USR_DIR . '/.env';
        if (isset($_SERVER['env']) && file_exists(GABOX_USR_DIR.'/.env.' . $_SERVER['env'])) {
            $env_file = GABOX_USR_DIR.'/.env.' . $_SERVER['env'];
        }
        $key = Crypto::decrypt(getenv('key'), Key::loadFromAsciiSafeString($_ENV['K']));
        $d = new Parser($key, $env_file);
        return is_array($d->getContent(strtoupper($name))) ? null : $d->getContent(strtoupper($name));
    }
}


if(!isset($_SERVER['SERVER_NAME'])) $_SERVER['SERVER_NAME']='';

define('HASH_KEY',md5($_SERVER['SERVER_NAME'].$conf['usr']));

require(COMMON.'db.php');
require(COMMON.'functions.php');
require(COMMON.'genclass.class.php');

require(COMMON.'myForm.class.php');

define('LIB_PATH',INC.'');
define('VAR_PATH',ROOT.'/usr/'.$conf['usr'].'/var/');
$gen = new GenClass($obj['db']);
if (isset($argv) || (isset($_SERVER['phpunit']) && $_SERVER['phpunit'] === 1)) {
    $gen->header=false;
    $gen->footer=false;
}

$buildPwd = isset($_SERVER['buildPwd']) ? $_SERVER['buildPwd'] : '@bde24rs38d!';

# test si install cli
if(file_exists(GABOX_USR_DIR.'/classes/auto/'.$gen->getClassAutoPath().'include.php')){
    require(GABOX_USR_DIR.'/classes/auto/'.$gen->getClassAutoPath().'include.php');
}
elseif(file_exists(GABOX_USR_DIR.'/classes/auto/include.php')){
    require(GABOX_USR_DIR.'/classes/auto/include.php');
}

if(empty($objects)||$objects[0] == ''){
   $gen->buildClass();
   $gen->collector=$gen->loadClassFromDb();
   header("Location: /login.php?build=" . $buildPwd);
   die();
}
if(isset($_SERVER['back']) && $_SERVER['back'] == 1) {
    if(isset($_GET['build'])) {
        include(INC.'/build.php');
        header("Location: /");
        die();
    }
}
if (isset($_SERVER['phpunit']) && $_SERVER['phpunit'] === 1) {
    include(INC.'/build.php');
}

# autocreate class from database schema, write module in file and database
# dev only
if(isset($_SERVER['env']) && ($_SERVER['env'] == 'dev')){
    $gen->buildClass();
}

$gen->collector = $gen->loadClassFromDb();

foreach (array_keys($gen->collector) as $module_k=>$module_name){
    $gen->collector[$module_name]->collector=&$gen->collector;
}

# if collector is still empty... something get wrong
if(empty($gen->collector)) {
    $error      = "<h1>Fatal Error : Empty collector</h1>";
    $perm_error = !is_writable(GABOX_USR_DIR.'/classes/auto/');
    $error      .= '<p>It look Gabox cannot write into the auto class Folder :</p>';
    if($perm_error){
        $error      .= '<br /><code>'.GABOX_USR_DIR.'/classes/auto/</code>';
        $error      .= '<p>Thanks to change permissions on this folder</p>';
    }
    $error      .= "<p>Try to recreate collector class : </p><p><a href='?build=".$buildPwd."'>Rebuild</a></p>";
    die($error);
}

if(isset($_SERVER['id_site'])){

    $site = $gen->collector['sites']->getOneCached((int)$_SERVER['id_site']);
    if(isset($site['use_cache']) && $site['use_cache'] == 1){
        
        define('USE_CACHE',true);
    }
    else{
        define('USE_CACHE',false);
    }

    if(isset($site['use_img_rewrite']) && $site['use_img_rewrite'] == 1){
        define('USE_IMG_REWRITE',true);
    }
    else{
        define('USE_IMG_REWRITE',false);
    }

    if(isset($site['use_auto_translate']) && $site['use_auto_translate'] == 1){
        define('AUTO_TRANSLATE',true);
    }
    else{
        define('AUTO_TRANSLATE',false);
    }

    if(isset($site['use_i18n_all_fields']) && $site['use_i18n_all_fields'] == 1){
        define('AUTO_TRANSLATE_ALL',true);
    }
    else{
        define('AUTO_TRANSLATE_ALL',false);
    }


    if(isset($site['use_i18n_master_slave']) && $site['use_i18n_master_slave'] == 1){
        define('USE_I18N_MASTER_SLAVE',true);
    }
    else{
        define('USE_I18N_MASTER_SLAVE',false);
    }

    if(isset($site['debug_mode']) && $site['debug_mode'] == 1) {
        error_reporting(E_ALL);
    }
    else{
         error_reporting(E_ERROR);
    }

    if(isset($site['debug_sql']) && $site['debug_sql'] == 1){
        $gen->db->debug=true;
    }
}

#setting versionnig for each mod
$modules = $gen->collector['modules']->getCached();
if(!empty($modules)){
    foreach($modules as $module){
        if(isset($module['use_versioning']) && isset($module['lib'])){
            if(isset($gen->collector[$module['lib']])) $gen->collector[$module['lib']]->versionning = (boolean)$module['use_versioning']; 
        }
        elseif(isset($module['lib']) && isset($gen->collector[$module['lib']])){
            $gen->collector[$module['lib']]->versionning = false; 
        }
    }
}

if(defined('ID_SITE') && file_exists(GABOX_USR_DIR.'/etc/config.'.ID_SITE.'.php')){
    include(GABOX_USR_DIR.'/etc/config.'.ID_SITE.'.php');
}
elseif(file_exists(GABOX_USR_DIR.'/etc/config.php')) {
    include(GABOX_USR_DIR.'/etc/config.php');
}

if(defined('GENCLASS_RAW_RESULTS')) {
    foreach($gen->collector as &$module) {
        if($module instanceof GenClass) $module->setReturnRawResults(GENCLASS_RAW_RESULTS);
    }
}

if(isset($genCryptedColumns) && is_array($genCryptedColumns) && count($genCryptedColumns) > 0) {
    if(is_file(dirname(__FILE__) . '/common/cryptSessionHandler.class.php')) {
        require_once(dirname(__FILE__) . '/common/cryptSessionHandler.class.php');
        $sessionHandler = new cryptSessionHandler();
        session_set_save_handler($sessionHandler, true);
        ini_set('session.gc_maxlifetime', 36000); 
        ini_set('session.cookie_lifetime', 36000);
        ini_set('session.cache_expire', 36000);
        session_start();
    }
    foreach($genCryptedColumns as $cryptCollectorName => $cryptColumns) {
        if(array_key_exists($cryptCollectorName, $gen->collector)) {
            $gen->collector[$cryptCollectorName]->setCryptedColumns($cryptColumns);
        }
    }
}

define('AJAX_PASSPHRASE',md5($_SERVER['SERVER_NAME']));

if(isset($_SERVER['front']) && $_SERVER['front']==1) {
    define('SIMPLE_LANGUAGE',!$site['is_i18n']);
    include(INC.'front.php');
}

elseif(isset($_SERVER['back']) && $_SERVER['back'] == 1) {
    include(INC.'back.php');
}
