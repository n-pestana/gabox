# usage : .sh PATH 
#   .sh test/ 

find $1 | egrep '(png|jpg|jpeg)$' | grep -v '.blur.' | grep -v '.thumb.' |  while read j;
do

   echo "\n$j" 
    # optimisation selon l'extension
    if echo "$j" | grep -qi ".jpg"; then
       echo "$j" | xargs jpegoptim --strip-all
       ext='jpg'
    fi

    if echo "$j" | grep -qi ".jpeg"; then
        echo "$j" | xargs jpegoptim --strip-all
        ext='jpeg'
    fi

    if echo "$j" | grep -qi ".png"; then
        echo "$j" | xargs optipng -quiet  
        ext='png'
    fi 


   blur=$(echo "$j" | sed "s/$ext/blur.$ext/")
   thumb=$(echo "$j" | sed "s/$ext/thumb.$ext/")

   convert -resize 450\> "$j" "$thumb"

   webpfile="${j%.*}.webp"
  /usr/local/bin/cwebp -quiet "$j"  -q 90 -o "$webpfile" 

   thumbwebpfile="${thumb%.*}.webp"
  /usr/local/bin/cwebp -quiet "$thumb"  -q 90 -o "$thumbwebpfile" 

done;
echo "\nFin Convert"




