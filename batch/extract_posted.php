<?php 
#usage :  php extract_posted.php [USR[/ID_SITE(optionnel)]] [FORM_NAME] [ENV] [DEST]
#ex:
# php extract_posted.php gabowebv4 contact prod
# php extract_posted.php usrname|3 contact prod email
# if [ENV] not set , default is prod 
error_reporting(E_ALL);
$_SERVER['lang']='fr';
$_SERVER['back']='1';
$usr = explode('/', $argv[1]);
$_SERVER['usr']=$usr[0];
if(count($usr) > 1) {
    define('ID_SITE', $usr[1]);
    $_SERVER['is_site'] = $usr[1];
}
$_SERVER['DOCUMENT_ROOT']=realpath('.');

if(isset($argv[3])) {
    $_SERVER['env']=$argv[3];
}
else{
     $_SERVER['env']='prod';
}
include('../autoprepend.php');

 use PHPMailer\PHPMailer\PHPMailer;
 use PHPMailer\PHPMailer\SMTP;
 use PHPMailer\PHPMailer\Exception;

$gen->header=false;
$gen->footer=false;

$debug = false; 

$path = ROOT.'/usr/'.$argv[1].'/upload/'; 

$has_file = array(
    'recrutement'=>array('cv','motivation')
);
$forms =$argv[2]; // = array('contact', 'demande');

$filters = array('sent'=>'0','form_name'=>$forms);
if (defined('ID_SITE')) {
    $filters['id_site'] = ID_SITE;
}
$rs = $gen->collector['posted']->get($filters);

$siteOptionsById = array();

if(is_array($rs) && !empty($rs)) {
    foreach($rs as $k=>$v) {
        if($debug==true)echo "\n\n--".$v['id_posted'] ."\n";
        $site= $gen->collector['sites']->getOne($v['id_site']);

        if(isset($argv[4])){
            $contacts = explode(",",$argv[4]);
        }
        else{
            $contacts = explode("\n",$site['form_contact_dest']);
        }

        $is_main_site = false;
        $locale = $gen->collector['locales']->getOne($v['id_loc']);
        // Clean des pièces jointes précédemment attachées au mailer/
        $mailer = new PHPMailer;
        $mailer->ClearAttachments();
        $mailer->ClearAllRecipients();
        $mailer->Encoding = 'base64';
        $mailer->CharSet = ($site['mailer_charset']) ? $site['mailer_charset'] : 'UTF-8';
        $mailer->Host = ($site['mailer_smtp_host']) ? $site['mailer_smtp_host'] : '127.0.0.1';
        if($site['mailer_smtp_host'] == '127.0.0.1'){
            $mailer->IsSMTP();
        }
        if(!$site['mailer_login']){
            $mailer->SMTPAuth = false;
        }
        if($site['mailer_email_from']){
            $mailer->SetFrom($site['mailer_email_from']);
        }


        $hasfile=false;
        $post = $gen->collector['posted_data']->get(array('id_posted'=>$v['id_posted']));
        $body = '';
        if(empty($post))continue;

        $files = array();
        if(is_dir($path.$v['form_name'].'/') && isset($has_file[$v['form_name']])){
            foreach($has_file[$v['form_name']] as $file){
                $attached = glob($path . $v['form_name'] . '/' . $file . '/' . $file . '_' . $v['id_posted'].'.*');
                if(!empty($attached)) $files[$file]=current($attached);
            }
        }
        foreach ($files as $name=>$to_attach){
            $e = explode('.',$to_attach);
            $mailer->AddAttachment($to_attach,$name.'-'.$v['id_posted'].'.'.end($e));
        }

        $country_id='';
        $pdv = '';
        foreach($post as $name=>$value){
            if($value['post_name']=='consent_contact' || $value['post_name']=='g-recaptcha-response') continue;
            switch($value['post_name']){
                case 'country':
                    $body .= "<br />".(strtoupper($value['post_name']))." : ".$gen->collector['country']->getLib($value['post_value']);  
                    $country_id = $value['post_value'];
                    break; 
                case 'pdv':
                    $pdv = $gen->collector['pdv']->getOne(array('id_pdv'=>$value['post_value']));
                    $body .="<br />".(strtoupper($value['post_name']))." : ".($pdv['lib'] . " - " . $pdv['town']);
                    break;
                case 'pays':
                    $country = $gen->collector['country']->getOneI18n(array('id_country'=>$value['post_value']));
                    $body .="<br />".(strtoupper($value['post_name']))." : ".($country['lib']);
                    $contactCountry = $gen->collector['contact_country']->getOne(array('id_country'=>$value['post_value']));
                    break;
                default:
                    $body .= "<br />".(strtoupper($value['post_name']))." : ".($value['post_value']);
                    break;
            }
        }


        foreach($contacts as $contact){
            $mailer->AddAddress($contact);
        }

        $mailer->Subject = 'Contact du site Internet '.$site['lib'].' '.$argv[1].' - '.$argv[2];
        if($debug==true) echo "\n\t--".$mailer->Subject;

        $mailer->IsHtml();
        $mailer->Body = $body;

        if($debug==false){ 
            if($mailer->Send()) {
                $gen->collector['posted']->set($v['id_posted'],array('sent'=>1));
            }
        }

        if($debug==true)echo "\n\n";    
    }
}   
die();
