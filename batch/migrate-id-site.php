<?php 
#usage :  php migrate-id-site.php [USR[/ID_SITE(optionnel)]] [FORM_NAME] [ENV] [DEST]
#ex:

# update all id_site fields in all gen_* tables

error_reporting(E_ALL);
$_SERVER['lang']='fr';
$_SERVER['back']='1';
$usr = explode('/', $argv[1]);
$_SERVER['usr']=$usr[0];
if(count($usr) > 1) {
    define('ID_SITE', $usr[1]);
}
$_SERVER['DOCUMENT_ROOT']=realpath('.');

if(isset($argv[3])) {
    $_SERVER['env']=$argv[3];
}
else{
     $_SERVER['env']='prod';
}
$id_site_src = 1;
include('../autoprepend.php');

$gen->header=false;
$gen->footer=false;
$mods = $gen->collector['modules']->get();
//$gen->collector['sites']->db->debug=true;
 
foreach($mods as $k=>$v){
    if(strstr($v['columns_name'],'"id_site"')){
        $sql = 'update gen_'.$v['lib'].' set id_site = '.ID_SITE.' where id_site = '.$id_site_src;
        $gen->collector['sites']->db->Execute("SET FOREIGN_KEY_CHECKS=0;");
        ($gen->collector['sites']->db->Execute($sql));
        $gen->collector['sites']->db->Execute("SET FOREIGN_KEY_CHECKS=1;");
        echo "\n".$sql;

    }
    if(strstr($v['columns_name'],'"id_sites"')){
        $sql = 'update gen_'.$v['lib'].' set id_sites = '.ID_SITE.' where id_sites = '.$id_site_src;
        $gen->collector['sites']->db->Execute("SET FOREIGN_KEY_CHECKS=0;");
        ($gen->collector['sites']->db->Execute($sql));
        $gen->collector['sites']->db->Execute("SET FOREIGN_KEY_CHECKS=1;");
        echo "\n".$sql;
    }
}

die();
