<?php 
#usage :  php extract_posted.php [USR] [FORM_NAME] [ENV]
#ex:
# php extract_posted.php gabowebv4 contact prod
# if [ENV] not set , default is prod 
error_reporting(E_ALL);
$_SERVER['lang']='fr';
$_SERVER['back']='1';
if(!isset($argv[1]) ||empty($argv[1])){
die('usage :script_name.php  [USR]  [ENV]');
}
$_SERVER['usr']=$argv[1];
$_SERVER['DOCUMENT_ROOT']=realpath('.');

if(isset($argv[2])) {
    $_SERVER['env']=$argv[2];
}
else{
     $_SERVER['env']='prod';
}
include('../autoprepend.php');

$gen->header=false;
$gen->footer=false;

$cmd = "find ../../usr/".$_SERVER['usr']." -iname '*.php' \
    | xargs grep -sir 'name=\"formname\"'  \
    | sed -e 's/.*value=\"\([^\"]*\)\".*/ \* \* \* \* \* cd ".str_replace('/','\/',$_SERVER['DOCUMENT_ROOT']).";  php extract_posted.php ".$_SERVER['usr']." \\1 ". $_SERVER['env'] ." /g' \
    | sort -u";
echo system($cmd)."\n";
die();
