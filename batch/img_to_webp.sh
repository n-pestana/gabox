# install https://storage.googleapis.com/downloads.webmproject.org/releases/webp/index.html
echo "\nDébut Convert Img to Webp"
for j in `find ../../usr/*  -regextype posix-extended -regex '.*\.(jpg|png|jpeg)'`
do
    webpfile="${j%.*}.webp"
    if [ -f $webpfile ]
    then
        echo   "$webpfile [SKIP]"
      #  chown www-data:www-data "$webpfile"
    else
        echo  "$webpfile [CREA]"
        cwebp "$j"  -q 90 -o "$webpfile"
        chown www-data:www-data "$webpfile"
    fi
done;
echo "\nFin Convert"


