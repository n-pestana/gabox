<!DOCTYPE html>
<html lang="en">
<head>

	<title>GaboX <?=(defined('plateform_name')) ? plateform_name : '' ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="" />
	<meta name="keywords" content="">
	<meta name="author" content="" />
	<!-- Favicon icon -->
	<link rel="icon" href="/assets/images/favicon.png" type="image/x-icon">

	<!-- vendor css -->
	<link rel="stylesheet" href="/assets/css/style.css">


</head>

<!-- [ auth-signin ] start -->
<div class="auth-wrapper">
	<div class="auth-content text-center">
		<img src="/assets/images/logo-gabox.png" alt="" class="img-fluid mb-3">
		<div class="card">
			<div class="row align-items-center">
				<div class="col-md-12">
					<div class="card-body">

		                            <form class="form" id="password-recovery" method="post" action="login.php">
						<h4 class="mb-3 f-w-400">Admin <?=(defined('plateform_name')) ? plateform_name : '' ?></h4>
						<div class="input-group mb-3">
							<input type="email" class="form-control" id="recovery-mail" name="recovery-mail" placeholder="<?=t("Adresse e-mail")?>" value="">
						</div>
					        <button class="btn btn-block btn-primary mb-4 rounded-pill" type="submit"><?=t("M'envoyer un nouveau mot de passe")?></button>
                                            </form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<svg width="100%" height="250px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave" d="" />
	</svg>
	<svg width="100%" height="250px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave-two" d="" />
	</svg>
	<svg width="100%" height="250px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave-three" d="" />
	</svg>
</div>
<!-- [ auth-signin ] end -->

<!-- Required Js -->
<script src="assets/js/vendor-all.min.js"></script>
<script src="assets/js/plugins/bootstrap.min.js"></script>
<script src="assets/js/waves.min.js"></script>
<script src="assets/js/pages/TweenMax.min.js"></script>
<script src="assets/js/pages/jquery.wavify.js"></script>
<script>
	$('#feel-the-wave').wavify({
		height: 100,
		bones: 3,
		amplitude: 90,
		color: 'rgba(22, 163, 221, 4)',
		speed: .25
	});
	$('#feel-the-wave-two').wavify({
		height: 70,
		bones: 5,
		amplitude: 60,
		color: 'rgba(82, 110, 181, .3)',
		speed: .35
	});
	$('#feel-the-wave-three').wavify({
		height: 50,
		bones: 4,
		amplitude: 50,
		color: 'rgba(216, 35, 145, .5)',
		speed: .45
	});
</script>
</body>
</html>
