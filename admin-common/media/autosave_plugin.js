tinymce.PluginManager.add('jsave', function(editor) {
    $("#"+editor.getParam('id')).keyup(function() {
        saveContent($(this));
    });

    $("#"+editor.getParam('id')).blur(function() {
      saveContent($(this));
    });
}); 
