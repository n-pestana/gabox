<?php 
if(isset($_SESSION['notreebuild'])) {
    $gen->collector['treeview']->setBuildOnSave(false);
}

if(isset($_POST['action']) && $_POST['action'] == 'publish'){
	$obj = json_decode($_POST['val'],true);
	//$gen->collector['treeview']->id_locales = $_POST['id_locales'];
//	$gen->collector['treeview']->publishMenu($gen->collector['treeview_published'],$obj);
}
elseif(isset($_POST['action']) && $_POST['action'] == 'cancel'){
	//$gen->collector['treeview']->id_locales = $_POST['id_locales'];
	$obj = json_decode($_POST['val'],true);
//	$gen->collector['treeview']->cancelMenu($gen->collector['treeview_published'],$obj);
}
elseif(isset($_POST['action']) && $_POST['action'] == 'addElement'){
	//$gen->collector['treeview']->id_locales = $_POST['id_locales'];
	//$obj = json_decode($_POST['val'],true);
	echo $gen->collector['treeview']->addElement();
}
elseif(isset($_POST['action']) && $_POST['action'] == 'delete'){
    $gen->collector['treeview']->setRetrieveOnlyValidChildren(false);
	$result = $gen->collector['treeview']->deleteFull($_POST['id']);
    $gen->collector['treeview']->setRetrieveOnlyValidChildren(true);
}
else{
	$obj = json_decode($_POST['val'],true);
        if (isset($_POST['parentOrder'])) {
            $parentOrder = (int) $_POST['parentOrder'];
            $parentLevel = (int) $_POST['parentLevel'];
            $gen->collector['treeview']->saveTreeview($obj, '', $parentOrder, $parentLevel);
        } else {
            $gen->collector['treeview']->saveTreeview($obj);
        }
    $gen->collector['treeview']->BuildAllUrl();


}


