<?php 
$gen->collector['treeview']->BuildAllUrl(array());


if(isset($gen->collector['produits'])){
    $produits = $gen->collector['produits']->get(array('is_valid'=>1,'status'=>'ok'));
    foreach($produits as $k=>$v){
        $gen->collector['produits']->BuildUrl($v['id_produits']);
    }
}



if(isset($gen->collector['caches'])) $gen->collector['caches']->truncate();


$cur = rglob(GABOX_USR_DIR."/var/cache/db/*.cache");
if(defined('GABOX_USR_DIR')){
    system("find ".GABOX_USR_DIR."/var/cache/db/ -iname '*.cache' | xargs rm");
}


$classesByTrait = $gen->getClassesByTrait();
if(array_key_exists('urls', $classesByTrait)) {
    foreach($classesByTrait['urls'] as $className) {
        $gen->collector[$className]->BuildUrl();
    }
}

function rglob($pattern, $flags = 0){
  $files = glob($pattern, $flags);
       
  foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir){
    $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
  }
       
  return $files;
}

