<?php
header('Content-type: application/json');
$query = '*'. $_GET['q'] . '*';
$collector = $_GET['collector'];
$data = array();
if(isset($gen->collector[$collector])) {
    $col = $gen->collector[$collector];
    $searchFields = $col->getAutocompleteSearchFields();
    $sql = $gen->collector[$collector]->getSearch($searchFields, $query);
    $rs = $gen->collector[$collector]->Query($sql);
    $rows = $gen->collector[$collector]->getRows($rs);
    foreach($rows as $i => $row) {
        $lib = array();
        foreach($searchFields as $field) {
            $lib[] = $row[$field];
        }
        $data[$i]['value'] = $row['id_' . $collector];
        $data[$i]['lib'] = implode($col->getAutocompleteResultSeparator(), $lib);
        $data[$i]['row'] = $row;
    }
}
die(json_encode($data));
