<?php 
if(!isset($_GET['action']) || !isset($_GET['id']) ||  !isset($_GET['hash'])){
 die('bad request');
}

$request = explode('@',$_GET['id']);
$curr_obj= $request[0];
$field   = $request[1];
$id      = (int) $request[2];
$type    = $request[3];
$action  = (string) $_GET['action'];

if(!is_string($curr_obj) || !is_string($field) || !is_int($id) || !is_string($type)){
 die('bad request');
}
if(securitize($id) != $_GET['hash']) die('ajax execute security error');


switch ($action){

  case 'reverse':
	$new_state = $gen->collector[$curr_obj]->reverseBoolean($id, $field);
	$msg = $curr_obj."(".$id.") a été ";
	$msg .= ($new_state == 1) ? 'activé' : 'désactivé' ;
        $msg .= " by ".$_SESSION['admins']['email'];
	echo $msg;	
  	break;

   case 'reverseCountry':
    $newvalue= (string) $_GET['newvalue'];
	$gen->collector['i18n']->SetStatusForElementByIdSite($id,ID_SITE,$newvalue,$curr_obj);
	$msg = $curr_obj."(".$id.") has been ";
	$msg .= (($newvalue== 1) ? 'enabled' : 'disabled') ." for all available language";
        $msg .= " by ".$_SESSION['admins']['email'];
	echo $msg;	
    break;

   case 'reverseCountryPurchase':
        $newvalue= (string) $_GET['newvalue'];
	$gen->collector['i18n']->SetPurchaseForElementByIdSite($id,ID_SITE,$newvalue,$curr_obj);
	$msg = $curr_obj."(".$id.") has been ";
	$msg .= (($newvalue== 1) ? 'enabled' : 'disabled') ." for all available language";
        $msg .= " by ".$_SESSION['admins']['email'];
	echo $msg;	
  	break;


    
}
