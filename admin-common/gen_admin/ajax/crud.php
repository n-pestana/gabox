<?php
if(
    isset($_POST['method']) && $_POST['method'] == 'deleteList' 
 && isset($_POST['ajax']) 
 && isset($_POST['ids']) && $_POST['ids'] != '') {
    $ids = explode(',', $_POST['ids']);
    $collector = $gen->collector[$_POST['collector']];
    $results = array();
    $errorOccured = false;
    foreach($ids as $id) {
        $result = $collector->del($id);
        if(!$result) {
            $errorOccured = true;
        }
        $results[$id] = $result;
    }
    die(json_encode(array('results' => $results, 'error_occured' => $errorOccured)));
}

if(
    isset($_POST['method']) && $_POST['method'] == 'getList' 
 && isset($_POST['ajax']) 
 && isset($_POST['name']) && $_POST['name'] != '') {
    $fullName = $_POST['name'];
    $name = preg_replace('/.*\[(\w+)\]\.*/', '$1', $fullName);
    $isList = strstr($name, '_list[]');
    $collectorName = str_replace(array('id_', '[]', '_list'), '', $name);
    $collector = $isList ? $gen->getSiteCollector($collectorName) : $gen->collector[$collectorName];
    if(strstr($collectorName, 'contracts')) {
        $list = $collector->getKandMore(array('num_contract', 'num', 'address'), array('enabled' => 1), 'num_contract');
    } else {
        $list = $collector->getKv(array(), $collector->getLibField());
    }
    $res = array();
    foreach($list as $k => $v) {
        $res[(string) $k] = $v;
    }
    $defaultValue = method_exists($collector, 'getListDefaultValue') ? $collector->getListDefaultValue() : '';
    header('Content-type: application/json; charset=utf-8');
    die(json_encode(array('data' => $res, 'defaultValue' => $defaultValue)));
}
