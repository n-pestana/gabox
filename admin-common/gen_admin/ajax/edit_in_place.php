<?php
//session_start();
ob_get_clean();

if(isset($_POST['value']) && isset($_POST['id'])){

	$request = explode('@',$_POST['id']);
	$curr_obj= $request[0];
	$field   = $request[1];
	$id      = (int) $request[2];
	$type    = $request[3];
	$value   =  $_POST['value'];


	if(!is_string($curr_obj) || !is_string($field) || !is_int($id) || !is_string($type)){
	 die('bad request');
	}
	if(securitize($id) != $_GET['hash']) die('ajax execute security error');

	if($value=='-') $value='';

	switch ($type){
	  case 'string':
		$gen->collector[$curr_obj]->set($id, array($field=>$value));
		echo $value;	
		break;
	  case 'textarea':
		$gen->collector[$curr_obj]->set($id, array($field=>$value));
		echo $value;	
		break;

	  case 'enum':
		$gen->collector[$curr_obj]->set($id, array($field=>$value));
		echo $gen->collector[$curr_obj]->{$field}[$value]['color'];
		break;
	  case 'default':
	    
	}
}
elseif(isset($_GET['id'])){
	$request = explode('@',$_GET['id']);
	$curr_obj= $request[0];
	$field   = $request[1];
	$id      = (int) $request[2];
	$type    = $request[3];
	die(json_encode( $gen->collector[$curr_obj]->getEnum($field)));
}
else die('bad request');
