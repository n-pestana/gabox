<?php

include(dirname(__FILE__) . '/../check-log.php');

$obj[$curr_obj] = $gen->collector[$curr_obj];
$pkVal = isset($_REQUEST[$obj[$curr_obj]->primary_key]) ? $_REQUEST[$obj[$curr_obj]->primary_key] : '';

if(isset($_REQUEST[$obj[$curr_obj]->table_name]) && false === $obj[$curr_obj]->isFormValid($_REQUEST[$obj[$curr_obj]->table_name], $pkVal)) {
    die(json_encode(array('is_valid' => 0, 'errors' => $obj[$curr_obj]->getFormErrors())));
}
if(isset($_GET['id_'.$curr_obj])&&!empty($_GET['id_'.$curr_obj])){
  $temp = $obj[$curr_obj]->getOne((int)$_GET['id_'.$curr_obj]);
}
?>
<?php if(!isset($_GET['ajax'])){?> 
<div class="container-fluid">
    <div class="row">
        <div id="myform_container" class="col-sm-12">
            <div class="card">
                <div class="card-body">
<?php } ?>
                    <?php 
                    if(isset($self_callback)) $form->self_callback=$self_callback;
                    if(!isset($fields)) $fields = '';
                    ?>
                    <?php  
                    if(isset($view_only)&&($view_only==true)) $form->view_only=true;

                     if(empty($form->lib)){
                        $form->lib = (!isset($_GET['id_'.$curr_obj])) ? 'Adding "'.$curr_obj.'"' : 'Modifying "'.$curr_obj.'"'  ;
                     }
                     echo $form->DisplayForm($curr_obj, $curr_obj);
                    ?>

<?php if(!isset($_GET['ajax'])){?> 
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$('#myform_container :input:text:visible:not(input[class*=filter]):first').focus();
</script>
<?php } ?>
