<?php
include('../header.php');
$curr_obj='locales';

# Edit
if(isset($_GET['id_'.$curr_obj])){
    $fields = array(
        '_cat1' => t("Informations")
        ,'id_'.$curr_obj
        ,'locale'
        ,'language'
        ,'country'
        ,'id_country'
        ,'is_master'
        ,'_cat3'=> t("Ma Catégorie")
        ,'lib_fr'
        ,'lib_en'
        ,'lib'
        ,'lib_language'
        ,'gender_list'
    );
    $form->lib= t("Locales listing");
    $form->heading= t("Locales are predefined regarding ISO-3166'");
    include('gen_edit.php');
}
# Listing
else{

    $array = array(
              'id_'.$curr_obj
              ,'locale'
              ,'language'
              ,'country'
              ,'lib_fr'
              ,'lib_en'
              ,'lib'
              ,'lib_language'
              ,'is_master'
    );

    $fieldsOptions = array(
        'lib_en'=> array(
            'label' => t("Nom")
            ,'edit_in_place' => true 
        )
        ,'locale' => array(
            'label'         => t("Nom")
            ,'edit_in_place' => true 
        )
        ,'lib' => array(
            'label'         => t("Langue")
            ,'edit_in_place' => true 
        )
        ,'country' => array(
            'label'         => t("Pays")
            ,'callback' => 'flagize'
        )
        ,'is_master' => array(
            'label'         => t("Est maitresse ? ")
            ,'callback'=>'getEnable'
            ,'edit_in_place'=>'true'
            ,'type'=>'switch'
        )
    );
    $extra_conf['title'] = t("Liste des locales");
    include('genlist.php');
}

include('../footer.php');
