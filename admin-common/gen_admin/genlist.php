<?php 
include(dirname(__FILE__) . '/../check-log.php');
if($gen->collector[$curr_obj] === null){
  die('The module ['.$curr_obj.'] looks to not be available');
}
$obj[$curr_obj]= ($gen->collector[$curr_obj]);
if(isset($_POST) && !empty($_POST) && empty($_POST['bulk_action'])){
    $r = $gen->collector['form']->savePostedDataByForm($curr_obj,$curr_obj);
    $notify=$curr_obj;
}
if(isset($_REQUEST['.nolist'])){
   include(MODS.'/'.$curr_obj.'/config.php'); 
   if(isset($mod['link'])){
        header("Location: ".$mod['link']);
   }
   else{
        header("Location: /modules/".$curr_obj."/admin.php");
   }
}
 
if(isset($_GET['limit'])) $_SESSION['limit'] = $_GET['limit'];

if(isset($_GET['delete']) 
    && isset($_GET['id_'.$curr_obj])){
    $obj[$curr_obj]->del($_GET['id_'.$curr_obj]);
}
elseif(isset($_GET['action']) && $_GET['action']!= 'getRow'  && is_array($_GET['val'] )){

  switch($_GET['action']){
    case 'del':
      foreach($_GET['val'] as $id){
        $obj[$curr_obj]->del($id);
      }
      break;
    default:
      foreach($_GET['val'] as $id){
      }
      break;
  }
}

$hash = '';

if(!isset($fieldsOptions)){
     $extra_conf = $listing;
     $fieldsOptions = $listing['fields'];
}
if(!isset($fieldsOptions['id_'.$curr_obj])){

    $temp = array_merge( array('id_'.$curr_obj => array(
            'label'         => "&snbp;".t("ID")
            ,'label'         =>'<input style="border:inset 1px;" name="allbox" type="checkbox" value="Check All" onclick="CheckAll(document.checkall);" />&nbsp;'.t("ID") 
            ,'callback' => 'myCheckbox'
        )),$fieldsOptions);
    $fieldsOptions = $temp;
}
$obj[$curr_obj]->SetUsedFields( 
        (isset($listing['sql'])) 
            ? $listing['sql'] 
            : array_keys($fieldsOptions)
);  
if (!isset($listing['table_join'])) {
    $listing['table_join'] = ['gen_'.$curr_obj];
} else {
    $listing['table_join'] = array_unique(array_merge(['gen_' . $curr_obj], $listing['table_join']));
}
#XXX
if(isset($listing['sql'])){
    if ($curr_obj != 'sites' && !empty(preg_grep('/^gen_sites\..*/', $listing['sql']))) {
        $listing['table_join'] = array_merge( ['gen_sites'],(array) $listing['table_join']);
        if (!isset($listing['join_conditions'])) {
            $listing['join_conditions'] = 'gen_sites.id_sites = gen_'.$curr_obj.'.id_sites';
        } else {
            $listing['join_conditions'] .= ' AND gen_sites.id_sites = gen_'.$curr_obj.'.id_sites';
        }
        if(defined('ID_SITE')){ 
            $listing['join_conditions'] .= ' AND gen_'.$curr_obj.'.id_sites='.ID_SITE; 
        }
        if(isset($listing['inline_conditions'])){
            $listing['join_conditions'] = $listing['inline_conditions']; 
        }
    }
}

if(isset($listing['sql'])){
    if (!empty(preg_grep('/^gen_treeview\..*/', $listing['sql']))) {
        $listing['table_join'] = array_merge((array) $listing['table_join'], ['gen_treeview']);
        if (!isset($listing['join_conditions'])) {
            $listing['join_conditions'] = 'gen_treeview.id_treeview = gen_'.$curr_obj.'.id_treeview';
        } else {
            $listing['join_conditions'] .= ' AND gen_treeview.id_treeview = gen_'.$curr_obj.'.id_treeview';
        }
    }
}

$obj[$curr_obj]->simulation = true;
if(isset($listing['conditions'])) $conditions = $listing['conditions'];
if(isset($listing['table_join'])) $table_join= $listing['table_join'];
if(isset($listing['join_conditions'])) $join_conditions= $listing['join_conditions'];
if(isset($listing['group_by'])) $group_by= $listing['group_by'];

# requete de recherche {{{
if(!isset($listing['inline_sql'])){
    $search = (isset($_GET['search'])) ? urldecode($_GET['search']) : '';
    $scope = array_keys($gen->collector[$curr_obj]->array_fields);

    if(isset($_GET['scope']) && in_array($_GET['scope'], array_keys($gen->collector[$curr_obj]->array_fields))){
      $scope = array($_GET['scope']);
    }
    if(!isset($conditions)) $conditions = array();
    if(!isset($join_conditions)) $join_conditions = '';
    if(!isset($table_join)) $table_join= '';

    if(!empty($table_join) ||!defined('ID_LOC') || isset($_GET['scope'])){
        $sql = $obj[$curr_obj]->getSearch(
           $scope
          ,$search 
          ,((isset($_GET['orderby'])) ? urldecode($_GET['orderby']) : 'id_'.$curr_obj).' '.((isset($_GET['sens'])) ? urldecode($_GET['sens']) : 'desc')
          ,''
          ,''
          ,$table_join
          ,$join_conditions 
          ,$conditions 
        );
          if(isset($group_by)) $sql= str_replace('order by',$group_by.' order by', $sql);
    }else{
      if(!isset($conditions)) $conditions = array();
      $sql =  $obj[$curr_obj]->getI18n(
          $conditions
          ,'order by ' .((isset($_GET['orderby'])) ? urldecode($_GET['orderby']) : 'id_'.$curr_obj).' '.((isset($_GET['sens'])) ? urldecode($_GET['sens']) : 'asc')
          ,$search
      );
    }
}else{
 $sql = $listing['inline_sql'];
}
if(isset($_SERVER['env']) && ($_SERVER['env'] != 'prod') && isset($_GET['show_sql'])){
    echo $sql;

}

if(isset($_GET['dl'])){
    $gen->collector[$curr_obj]->downloadCSVBySql($sql);
}

if(isset($_GET['action']) &&  $_GET['action'] == 'getRow'){
    $extra_conf['getrow']=true;
}


/*if(!isset($fieldsOptions['id_'.$curr_obj])){
      $fieldsOptions= array('id_'.$curr_obj=>array(
            'label'         => ("ID")
            ,'sortable'         => false 
        ))+$fieldsOptions;
}
*/


if(!isset($form->action)){
    if(file_exists( $curr_obj.'.edit.php')){
        $action = $curr_obj.'.edit.php';
    }
    else {
        $action = 'admin.php';
    }
}
else{
    $action = $form->action;
}
if(!isset($fieldsOptions['id_'.$curr_obj])){
    $id_field = array('id_'.$curr_obj=>array( 'label'=> t("ID") ,'sortable'=> true));
    $fieldsOptions = array_merge_recursive($id_field, $fieldsOptions );
}
if(!isset($fieldsOptions['edit'])){
        $fieldsOptions['edit'] = array(
            'label'         => '<i class="fa-regular fa-pen-to-square"></i>&nbsp;'.t("Editer")
            ,'href'         => $action.'?hash='.HASH_KEY.'&id_'.$curr_obj.'=$id_'.$curr_obj
            ,'sortable'     => false 
        );
}

if(!isset($fieldsOptions['delete'])){
        $fieldsOptions['delete'] = array(
            'label'         => '<i class="fa-solid fa-trash-can"></i>&nbsp;'.t("Effacer")
            ,'href'         => $action.'?delete=1&id_'.$curr_obj.'=$id_'.$curr_obj.'&hash='.HASH_KEY.''
            ,'sortable'     => false 
        );
}

foreach($fieldsOptions as $option_name=>$option_value){
    if($option_value === false) unset($fieldsOptions[$option_name]);
}


if(isset($gen->collector['myform']->restrictions) && !empty($gen->collector['form']->restrictions)){

  foreach($fieldsOptions as $k=>$v):
    if(isset($fieldsOptions[$k]['type']) && $fieldsOptions[$k]['type'] == 'switch' ) $v['edit_in_place'] = true;
    else{
      $v['edit_in_place'] = false;
    }
    $fieldsOptions[$k] = $v;
  endforeach ;
}
if($curr_obj=='produits' && (!isset($forceDeleteBtn) || (isset($forceDeleteBtn) && $forceDeleteBtn === false))) {
}
# on reactive tout pour les callback
$obj[$curr_obj]->simulation = false;
$sql = preg_replace("|(.*) where (.*) AS NAME |isu","$1 where $2 ",$sql);
if(!isset($count_only) ||! $count_only){
    $obj[$curr_obj]->RollBackUsedFields();
    if(is_null($extra_conf)) $extra_conf = array();
    $params = array_merge($extra_conf,array('class'=>$curr_obj,'more_info' => true, 'excludeFromDisplay' => NULL,'limited'=>false));
    $out =  toHTML( $sql ,$fieldsOptions ,(isset($_SESSION['limit'])) ? $_SESSION['limit'] : 50 ,((isset($_GET['cp']) ? $_GET['cp']: '1' )),$params); 

    if(isset($_GET['action']) &&  $_GET['action'] == 'getRow'){
        die($out);
    }

?>



<div class="grid_12 col-md-12 pcoded-content card">
    <input type="hidden" id="delete_confirm_title" value="<?=t("Confirmation demandée")?>" />
    <input type="hidden" id="delete_confirm_content" value="<?=t("Souhaitez vous vraiment supprimer cet élément ?")?>" />
    <input type="hidden" id="delete_confirm_ok" value="<?=t("Suppression confirmée")?>" />
    <input type="hidden" id="delete_confirm_cancel" value="<?=t("Suppression annulée")?>" />

  <?php 
    # si pas de données :
    if($out === false){?>
     <div class="p-4 m-4 alert alert-light" role="alert">
        <h2><?=t("Aucun enregistrement trouvé !")?> </h2><p><br><br> <?=t("Vous pouvez en ajouter  en cliquant ici")?> :
            <a class="btn btn-outline-primary" href="<?=$action?>?id_<?=$curr_obj?>=">
            <i class="feather icon-plus-square"></i>&nbsp;<?=t("Ajouter une nouvel élément")?></a>
        </p>
     </div>     

   <?php }   
    else{
        echo $out;
    }   
    ?>

    <?php $filters= (isset($_GET['filters'])) ? $_GET['filters'] : '';
    if(!empty($filters)){
      $filters = @unserialize($filters);
      if(empty($current_obj))$current_obj=$curr_obj ;
      $filters = (is_array($filters)) ? $filters : array();
      $sql = $obj[$current_obj]->get(
        $filters
       ,((isset($_GET['orderby'])) ? urldecode($_GET['orderby']) : 'id').' '.((isset($_GET['sens'])) ? urldecode($_GET['sens']) : 'asc')
      );
    }
    ?>
</div>

<?php if(isset($notify)){?>
   <input type="hidden" class="notify-hidden" value="<?=$notify?>"/> 
<?php } 

}?>
