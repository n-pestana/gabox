<?php
 $gen->collector['modules']->disableCache();
$errors = array();
if (isset($_GET['mod'])) {
    // Usr mods first to be taken into account
    if(file_exists(USR_MODS.$_GET['mod'].'/config.php')) {
        $modDir = USR_MODS .$_GET['mod'];
    } else {
        $modDir = MODS .$_GET['mod'];
    }
}
if(isset($_GET['install']) && isset($_GET['mod'])){
    if(!file_exists($modDir.'/config.php')){
        die('mod not found:'.$modDir.'/config.php');
    }
    include($modDir.'/config.php');

    if(is_array($mod['sql'])){
        $success=true;
        foreach($mod['sql'] as $k=>$v){
            $success = $gen->collector['modules']->Query($v);
            if($success === false){
                $errors[] = $gen->collector['modules']->db->errorMsg()."<i>".$v."</i>";
            }
        }
    }
    else{
        $success = $gen->collector['modules']->Query($mod['sql']);
    }
    if($success !== false){
        include(INC.'/build.php');
        
        //Activation du module : insertion dans gen_module
        $success = $gen->collector['modules']->set(crc32($_GET['mod']),array('lib'=>$_GET['mod'],'columns_name'=>json_encode($gen->collector['modules']->db->metaColumnNames('gen_'.$_GET['mod']))));
        if($success === false) {
            $errors[] = $gen->collector['modules']->db->errorMsg()."<i>".$v."</i>";
        } else {
            $success = true;
        }
   }
}
elseif(isset($_GET['uninstall']) && isset($_GET['mod'])){
    if(!file_exists($modDir.'/config.php')){
        die('mod not found:'.$modDir.'/config.php');
    }

    include($modDir.'/config.php');
    if(is_array($mod['unsql'])){
        $success = true;
        foreach($mod['unsql'] as $k=>$v){
            if ($gen->collector['modules']->Query($v) === false) {
                $success = false;
            }
        }
    }
    else{
        $success= $gen->collector['modules']->Query($mod['unsql']);
    }
    if ($success !== false) {
        $gen->buildClass();
    }
    //if($success!==false){
        $temp = $gen->collector['modules']->getOne(array('lib'=>$_GET['mod']));
        $success= $gen->collector['modules']->del($temp['id_modules']);
        unset($gen->collector[$_GET['mod']]);
    //}

}
elseif(isset($_GET['disable']) && isset($_GET['mod'])){
    $temp = $gen->collector['modules']->getOne(array('lib'=>$_GET['mod']));
    $success= $gen->collector['modules']->set($temp['id_modules'],array('is_enabled'=>0));
}
elseif(isset($_GET['enable']) && isset($_GET['mod'])){
    $temp = $gen->collector['modules']->getOne(array('lib'=>$_GET['mod']));
    $success= $gen->collector['modules']->set($temp['id_modules'],array('is_enabled'=>1));
}

// Pour éviter la collision sur la variable $mod des fichiers de conf
// TODO : voir pour cleaner
$tempmod = isset($mod) ? $mod : null;
include(GABOX_BACK_ROOT.'header.php');
$mod = isset($tempmod) ? $tempmod : null;

if(isset($success) && $success === false){?>

<div class="grid_12 col-md-12 pcoded-content card">
    <h4>Install <?=$mod['name']?> Version <?=$mod['version']?></h4>;
    <div class="alert alert-danger" role="alert">
        <p>Erreur durant l'installation du module  ! </p>
        <pre><?=(implode("<br />",$errors))?></pre>
        <p>Vous pouvez tentez de désinstaller ou réinstaller à nouveau ce module : <br />
            <a href="?uninstall=1&mod=<?=$_GET['mod']?>" class="btn btn-warning">Désinstaller le module <?=$_GET['mod']?></a>
            <a href="?install=1&mod=<?=$_GET['mod']?>" class="btn btn-info">Réinstaller le module <?=$_GET['mod']?></a>
        </p>
    </div>
</div>

<?php }elseif((isset($success) && $success === true)){ ?>

    <div class="grid_12 col-md-12 pcoded-content card">
        <div class="row">
        <?php if(isset($_GET['uninstall'])){?>
            <div class="alert alert-warning success" role="alert"> <p>Module [<?=$_GET['mod']?>] <?=t("désinstallé avec succès !")?></p></div>
        <?php } else{ ?>
            <div class="alert alert-success" role="alert"> <p>Module [<?=$_GET['mod']?>]  <?=t("installé avec succès !")?></p></div>
        <?php }?> 
        </div>
    </div>
<?php } ?>


<div class="grid_12 col-md-12 pcoded-content card">
    <div class="row">
        <h3><?=t("Gestion des modules")?></h3>
    </div>
    <div class="row">
        <table class="table-hover mods-table table-striped table-bordered nowrap dataTable">
            <thead>
                <tr role="row">
                    <th scope="col"><?=t("ID")?></th>
                    <th scope="col"><?=t("Libéllé")?></th>
                    <th scope="col"><?=t("Description")?></th>
                    <th scope="col"><?=t("Dépendances")?></th>
                    <th scope="col"><?=t("Version")?></th>
                    <th scope="col"><?=t("Utilisation")?></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </head>
            <tbody>
                <?php 
                $mods = array_unique(glob('{'.MODS.','.USR_MODS.'}/*',GLOB_ONLYDIR|GLOB_BRACE));
                foreach($mods as $k=>$v){
                    // Si le module usr est installé via lien symbolique, on ne prend en compte le module qu'une fois
                    // TODO: retirer ce test une fois que tous les projets seront passés sur la directive AliasMatch
                    if(strpos($v, USR_MODS) !== false && file_exists(str_replace(USR_MODS, MODS, $v))) {
                        continue;
                    }
                    $mod = $temp = array();
                    $action1 = $action2 = $temp='';
                    $id = basename($v);
                    $error = false;
                    $config = $v.'/config.php';
                ?>
                <tr>
                <?php 
                   if(!file_exists($config)) {?>
                    <td><?=$id?></td>
                    <td align="center">-</td><td align="center">-</td><td align="center">-</td><td align="center">-</td><td align="center">-</td><td><center><div class="alert-danger"><?=t("Fichier de configuration<br /> introuvable")?>  </div></center></td>
                   <?php }
                   else{
                       $inc = include($config); 
                       $temp = $gen->collector['modules']->getOne(array('lib'=>$id));
                       $action1 = (isset($temp['is_enabled']) && $temp['is_enabled']==1) ? 'disable': 'enable' ;
                       $action2 = ($temp!==false) ? 'uninstall': 'install' ;
                      ?>
                      <td><?=$id?></td>
                      <td><i class="fa fa-<?=$mod['fa-icon']?>"></i>&nbsp;<b><?=$mod['name']?></b></td>
                      <td><?=$mod['desc']?></td>
                      <td><?=(isset($mod['depends'])) ? implode(',',$mod['depends']) : ''?></td>
                      <td align="center"><?=$mod['version']?></td>
                      <td><center><?=(isset($gen->collector[$id]) && method_exists($gen->collector[$id],'getCount')) ? $gen->collector[$id]->getCount().' '.t("élément(s)") : '-' ?> </center></td>
                      <?php if(isset($mod['installable']) && $mod['installable'] === false){?>
                        <td></td>
                        <td align="center"><span class="badge badge-light"><?=t("Module non installable")?></span></td>
                      <?php }elseif(!isset($mod['sql'])){ ?>
                        <td></td>
                        <td align="center"><span class="badge badge-light"><?=t("Module intégré")?></span></td>
                      <?php }else{?>
                        <td align="center">
                                <?php if($temp !== false){?>
                                    <?=($temp['is_enabled']==1) ? '<!--<div class="alert-success">Actuellement activé</div>-->': '<!--<div class="alert-warning">Actuellement désactivé</div>-->'?><!--<br />--><a href="?<?=$action1?>=1&mod=<?=$id?>"<?=($temp['is_enabled']==1) ? ' class="btn btn-warning"> '.t("Désactiver maintenant") : ' class="btn btn-info"> '.t("Activer maintenant")?></a>
                                <?php } ?>
                        </td>
                        <td align="center">
                            <?php if(isset($temp['is_enabled']) && $temp['is_enabled']==1){ ?>
                            <?php } else { ?>
                                <?=($temp!==false) ? '<!--<div class="alert-success">Actuellement installé</div>-->': '<!--<div class="alert-warning">Actuellement désinstallé</div>-->'?><!--<br />--><a href="?<?=$action2?>=1&mod=<?=$id?>" <?=($temp!==false) ? 'class="btn btn-danger">'.t("Déinstaller maintenant") : 'class="btn btn-info">'.t("Installer maintenant")?></a>
                            <?php } ?>
                        </td>
                      <?php } ?>
                  <?php } ?>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php include(GABOX_BACK_ROOT.'footer.php');?>
