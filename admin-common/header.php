<?php
@session_start();

if (empty($_SESSION)) {
    header("Location: /login.php");
    die();
}
if (!isset($_SESSION['admins'])) {
    header("Location: /login.php");
    die();
}
if (!is_array($_SESSION['admins']) && empty($_SESSION['admins'])) {
    header("Location: /login.php");
}
if (!isset($_GET['action']) && !isset($_GET['dl'])) {
    if (isset($_SERVER['id_plateforme']) && $_SERVER['id_plateforme'] != '') {
        $plateforme = $gen->collector['plateforme']->getOne((int)$_SERVER['id_plateforme']);
    } else {
        $plateforme = $gen->collector['plateforme']->getFirst();
    }

    # getting admin info for auto refresh
    if(!isset($_SESSION['admins']['id_admins'])) header("Location: /login.php");
    $_SESSION['admins']=$gen->collector['admins']->get($_SESSION['admins']['id_admins']);
    $_SESSION['admins_type']=$gen->collector['usertypes']->get( $_SESSION['admins']['usertype']);
    if(defined('ID_SITE')){
        $site=$gen->collector['sites']->getOne(ID_SITE);
    }
    $restrictions = array();
    if(!is_array($_SESSION['admins']) || empty($_SESSION['admins'])) header("Location: /login.php");
    if($_SESSION['admins']['id_locales']!=0 && $_SESSION['admins']['id_sites'] != 0){
      if(!defined('ID_LOC'))define('ID_LOC',$_SESSION['admins']['id_locales']);
      if(!defined('ID_SITE'))define('ID_SITE',$_SESSION['admins']['id_sites']);
      $restrictions= array(
        'locales' => $gen->collector['sites']->getAvailabledIdLocalesForSite(ID_SITE, $only_enabled = false)
        ,'pref_loc' => $_SESSION['admins']['id_locales']
        ,'id_site' => $_SESSION['admins']['id_sites']
        ,'usertype' => $gen->collector['usertypes']->get( $_SESSION['admins']['usertype'])
      );
    }

    if(!isset($_SESION['limit'])) $_SESSION['limit'] = 20;
    $_SESSION['is_logged']=true;

    $form->id_locales_only=(isset($_SESSION['admins']['id_locales'])) ? $_SESSION['admins']['id_locales'] : '';
    $form->restrictions = $restrictions;
    if(($_SERVER['REQUEST_URI'] == '/') && isset($form->restrictions['usertype']) && isset($form->restrictions['usertype']['bootstrap_url']) && $form->restrictions['usertype']['bootstrap_url']!=''){
        ob_get_clean();
        header("Location: ". $form->restrictions['usertype']['bootstrap_url']);
        die();
    }

    $structureMenuBo = array(
            'plateforme'=>array()
            ,'modules'=>array(
                    'link' => '/mods.php','lib' => t("Gestion des modules"),'icon'=>'puzzle-piece'
            )

                ,'sites'=>array()
                ,'media'=>array(
                    'lib' => t("Médiathèque")
                    ,'icon'=> 'fa-solid fa-photo-film-music'
                    ,'link' => '/modules/media/admin.php'   
                )
                ,'treeview'=>array()
                ,'locales'=>array(
                    'lib' => t("Locales")
                    ,'icon'=> 'fa-light fa-language'
                    ,'link' => '/modules/locales/admin.php'
                )
                ,'log'=>array(
                    'lib' => t("Gabox Log")
                    ,'icon'=> 'fa-solid fa-message-exclamation'
                    ,'link' => '/modules/log/admin.php'
                ),
        );
    $headerShortcuts = [];

    foreach (['modules', 'log', 'locales', 'media'] as $module) {
        if (!$gen->collector['admins']->can('edit', $module)) {
            unset($structureMenuBo[$module]);
        }
    }

    $modules = $gen->collector['modules']->get(array('is_enabled'=>1));

    # media has not table
    $modules[] = array('lib'=>'media');

    include(ADMIN.'/common-fields.php');

    foreach($modules as $module){
        if (!$gen->collector['admins']->can('edit', $module['lib'])) {
            continue;
        }
        $mod = array();
        $moduleAlias = '';
        $f = USR_MODS.$module['lib'].'/config.php';
        if(!file_exists($f)) {
            $f = MODS . $module['lib'].'/config.php';
        } elseif (!is_link(MODS . $module['lib'])) {
            // Si c'est un module utilisateur qui ne fonctionne pas sur l'ancien système des symlinks, on ajoute
            // un subpath qui sera pris en compte dans l'URL via la directive AliasMatch du vhost.
            // AliasMatch "^/modules/usr/([^/]*)/(.*)$" "/app/usr/$1/modules/$2"
            // Cela permet ainsi depuis une URL admin d'accéder aux modules usr sans lien symbolique.
            $moduleAlias = 'usr/' . $conf['usr'] . '/';
        }
        if (!file_exists($f)) {
            continue;
        }
        include($f);
        if(isset($mod['bo-hook'])) {
            $structureMenuBo[$module['lib']] = $mod['bo-hook'];
        } else {
            $menu = array(
                'lib'   => $mod['name']
                ,'icon' => $mod['fa-icon']
                ,'link' => (isset($mod['link'])) ?  $mod['link'] : '/modules/'.$module['lib'].'/admin.php'
            );
            $structureMenuBo[$module['lib']] = $menu;
        }

        // Liens supplémentaires définis dans le menu
        if (isset($mod['extra_links']) && is_array($mod['extra_links'])) {
            $counter = 0;
            foreach ($mod['extra_links'] as $menu) {
                $structureMenuBo[$module['lib'] . '_' . ++$counter] = $menu;
            }
        }

        // Raccourcis s'affichant à droite du header
        if (!empty($mod['bo-header-shortcuts']) && is_array($mod['bo-header-shortcuts'])) {
            foreach ($mod['bo-header-shortcuts'] as $url => $shortcut) {
                if ($gen->collector['admins']->can('edit', $module['lib'])) {
                    if(!is_array($shortcut)){
                        $headerShortcuts[$url] = URL_LOC.$shortcut; 
                    }
                }
            }
        }

        // Pour tous les liens, on ajoute si nécessaire l'alias défini ci-dessus.
        // Les modules usr doivent donc définir leurs liens en /modules/nom_module/...
        if (isset($structureMenuBo[$module['lib']]['links'])) {
            foreach ($structureMenuBo[$module['lib']]['links'] as &$link) {
                if (isset($link['link'])) {
                    $link['link'] = str_replace('/modules/', '/modules/' . $moduleAlias, $link['link']);
                }
                if (isset($link['links'])) {
                    array_walk_recursive($link['links'], function (&$value) {
		                $value = str_replace('/modules/', '/modules/', $value);
	                });
                }
            }
        } elseif (!empty($structureMenuBo[$module['lib']]['link'])) {
            $structureMenuBo[$module['lib']]['link'] = str_replace(
                '/modules/',
                '/modules/' . $moduleAlias,
                $structureMenuBo[$module['lib']]['link']
            );
        }
    }

    // Tri alphabétique du menu
    $arrayLibsMenu = [];
    foreach ($structureMenuBo as $key => $menu) {
       if(isset($menu['lib'])){
            $arrayLibsMenu[$key] = $menu['lib'];
        }
    }
    asort($arrayLibsMenu, SORT_STRING);
    $structureMenuBoTmp = $structureMenuBo;
    $structureMenuBo = [];
    foreach ($arrayLibsMenu as $key => $menu) {
        $structureMenuBo[$key] = $structureMenuBoTmp[$key];
    }

    if( !isset($_GET['ajax'])) { ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title><?=(isset($plateforme['lib']) && !empty($plateforme['lib'])) ? $plateforme['lib'] : 'GaboX';?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="" />
        <meta name="keywords" content="">
        <meta name="author" content="" />
        <!--favicon-->
        <?php if($plateforme['favicon_code'] != '') { ?>
            <?php echo $plateforme['favicon_code'] ?>
        <?php }else{ ?>
            <link rel="icon" href="/assets/images/favicon.png" type="image/x-icon">
        <?php }?>
        <!-- end favicon -->
        <link rel="stylesheet" href="/assets/css/style.css?v=3">
        <link rel="stylesheet" href="/assets/css/plugins/PNotifyBrightTheme.css">
        <link href="/assets/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/assets/plugins/vendors/chosen/chosen.min.css">
        <link rel="stylesheet" href="/assets/css/merge.css?v=3">
        <script src="/assets/js/vendor-all.min.js"></script>
        <link rel="stylesheet" href="/assets/css/plugins/lightbox.min.css">
        <link rel="stylesheet" href="/assets/plugins/vendors/iconpicker/fontawesome-iconpicker.min.css">
        <link rel="stylesheet" href="/assets/css/plugins/jquery.minicolors.css">
        <link rel="stylesheet" href="https://kit.fontawesome.com/546ffdf64d.css" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">


    <?php
    // Inclusion des fichiers css des modules actifs si existants
    $modules = $gen->collector['modules']->get(['is_enabled'=>1]);
    foreach ($modules as $module) {
        $cssFiles = [];
        foreach (glob('{'.MODS.','.USR_MODS.'}' . $module['lib'] . '/css/*.css',GLOB_BRACE) as $cssFile) {
            $fileName = str_replace([INC, GABOX_USR_DIR], '', $cssFile);
            if (!is_link(MODS . $module['lib'])) {
                $cssFiles[$module['lib'] . '/' . basename($cssFile)] = str_replace('/modules/', '/modules/usr/' . $conf['usr'] . '/', $fileName);
            } else {
                $cssFiles[$module['lib'] . '/' . basename($cssFile)] = $fileName;
            }
        }
        foreach ($cssFiles as $cssFile) { ?>
            <link rel="stylesheet" href="<?=$cssFile?>">
        <?php } ?>
    <?php } ?>
    </head>
    <body>
        <div class="loader-g">
        <header class="navbar pcoded-header navbar-expand-lg navbar-light <?=(isset($plateforme['headerfix']) && $plateforme['headerfix']==1) ? 'headerpos-fixed'  : ''?> header-light">
            <div class="subnavbar content-main  <?=(isset($plateforme['fullwidth']) && $plateforme['fullwidth']==1) ? 'container-fluid'  : 'container'?>">
                <div class="m-header">
                <?php $usertype = $gen->collector['usertypes']->get( $_SESSION['admins']['usertype']);?>
                    <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
                <?php if(isset($usertype['bootstrap_url'])){ ?>
                    <a href="<?=URL_LOC?><?=$usertype['bootstrap_url']?>" class="b-brand">
                <?php }else{?>
                    <a href="/<?=URL_LOC?>" class="b-brand">
                <?php }?>
                        <img src="<?=(isset($plateforme['media']) && !empty($plateforme['media'])) ? $plateforme['media'] : '/assets/images/gabox-logo-w.png'?>" alt="" class="logo" style="max-width:150px;max-height:68px;">
                    </a>
                    <a href="#!" class="mob-toggler">
                        <i class="feather icon-more-vertical"></i>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <div class="metanav">
                        <?php foreach ($headerShortcuts as $urlShortcut => $headerShortcut) { ?>
                            <a href="<?=$urlShortcut?>" class="btn btn-sm <?=$headerShortcut['class'] ?? ''?>"><?=!empty($headerShortcut['fa-icon']) ? '<i class="fas ' . $headerShortcut['fa-icon'] . '"></i> ' : ''?><?=$headerShortcut['lib']?></a>
                        <?php } ?>
                        <a href="#" onClick="openModal('cache') " id="clearcacheBtn" class="btn btn-sm btn-primary "><i class="fas fa-eraser"></i> <?=t("Vider le cache")?></a>
                        <?php if(!isset($plateforme['autoupdate_url']) || $plateforme['autoupdate_url'] !=1){?>
                            <a href="#" onClick="openModal('url') " id="buildurlBtn" class=" btn btn-sm btn-primary "><i class="fas fa-link"></i> <?=t("Reconstruire les URLS")?></a>
                        <?php }?>
                        <?php if(defined('ID_SITE')) { ?>
                            <?php if (!empty($site['internal_url'])) { ?>
                                <a href="<?=$site['internal_url']?>" target="_blank" class=" btn btn-sm btn-primary " title="Go to your website"><i class="feather icon-link"></i><?=t("Voir le site preprod")?></a>
                            <?php } ?>
                            <?php if (!empty($site['prod_url'])) { ?>
                                <a href="<?=$site['prod_url']?>" target="_blank" class="btn btn-sm btn-primary" title="Go to your website"><i class="feather icon-link"></i><?=t("Voir le site prod")?></a>
                            <?php } ?>
                        <?php } ?>
                        <a href="<?=URL_LOC?>/login.php?logout=1" class="btn btn-sm btn-primary " title="Logout"><i class="feather icon-log-out"></i> <?=t("Se deconnecter")?></a>
                    </div>
                </div>
            </div>
        </header>
        <div class="content-main <?=(isset($plateforme['fullwidth']) && $plateforme['fullwidth']==1) ? 'container-fluid'  : 'container'?>">
            <nav class="pcoded-navbar menupos-fixed menu-<?=(!empty($plateforme['bgcolor_menu']) && hexdec(str_replace('#', '', $plateforme['bgcolor_menu'])) < hexdec('888888')) ? 'dark' : 'light'?>">
                <div class="navbar-wrapper content-main container">
                    <div class="navbar-content scroll-div">
                    <?php if(isset($plateforme['is_multisite']) && $plateforme['is_multisite'] ==1){?>
                    <ul class="navbar-nav m-4">
                        <li>
                         <?php if(empty($restrictions)):?>
                            <form name="change_site" method="post" id="change_site"m-4>
                                <select name="change_id_site" id="change_id_site" class="form-control" onchange="this.form.submit()">
                                    <option value=""><?=t("Tous les sites")?></option>
                                    <?php foreach( $gen->collector['sites']->get(array(),'lib asc') as $site):?>
                                  <?php if(defined('ID_SITE')):?>
                                    <?php $selected = ($site['id_sites'] == ID_SITE) ? 'selected = "selected"' : '' ?>
                                  <?php endif ?>
                                    <?php $color= ($site['is_published'] == 1) ? 'green' : 'red' ?>
                                    <option <?=(isset($selected)) ? $selected : '' ?> style="color:<?=$color?>" value="<?=$site['id_sites']?>"><?=$site['lib']?></option>
                                    <?php endforeach ?>
                                </select>
                            </form>
                          <?php endif ?>
                      </li>
                    </ul>
                    <?php } ?>
                        <ul class="nav pcoded-inner-navbar ">
                            <?php if(isset($usertype['bootstrap_url'])){ ?>
                                    <li class="nav-item"><a href="<?=URL_LOC?><?=$usertype['bootstrap_url']?>">
                                        <span class="pcoded-micon"><i class="fa-regular fa-gauge"></i></span> <span class="pcoded-mtext"><?=t("Tableau de bord")?></span></a>
                                    </li>
                            <?php }?>
                            <?php foreach($structureMenuBo as $k=>$level1){
                                   if($level1===false)continue;
                                   if(!is_array($level1)){?>
                                        <li class="nav-item pcoded-menu-caption">
                                            <label><?=$level1?></label>
                                        </li>
                                   <?php }elseif(isset($level1['links'])){ ?>
                                        <li class="nav-item pcoded-hasmenu">
                                            <a href="#" class="nav-link "><span class="pcoded-micon"><i class="fas fa-<?=$level1['icon']?>"></i></span><span class="pcoded-mtext"><?=$level1['lib']?></span></a>
                                            <ul class="pcoded-submenu">
                                                <?php foreach($level1['links'] as $k2=>$level2){?>
                                                    <?php if(isset($level2['links']) && is_array($level2['links']) && !empty($level2['links'])){ ?>
                                                            <li class="nav-item pcoded-hasmenu">
                                                                <a href="#"><i class="fas fa-<?=$level2['icon']?>"></i> <span class="pcoded-mtext"><?=$level2['lib']?></span></a>
                                                                <ul class="pcoded-submenu">
                                                                    <?php foreach($level2['links'] as $k3=>$level3){?>
                                                                        <li><a href="<?=URL_LOC?><?=$level3['link']?>" ><i class="fas fa-<?=$level3['icon']?>"></i> <?=$level3['lib']?></a></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </li>
                                                    <?php } else { ?>
                                                        <li><a href="<?=URL_LOC?><?=$level2['link']?>" ><?=$level2['lib']?></a></li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                   <?php }else{ ?>
                                        <li class="nav-item">
                                            <a href="<?=URL_LOC?><?=$level1['link']?>" class="nav-link "><span class="pcoded-micon"><i class="fas fa-<?=$level1['icon']?>"></i></span><span class="pcoded-mtext"><?=$level1['lib']?></span></a>
                                        </li>
                                   <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- LoadTime : <?= number_format(microtime(true)-$_SERVER['start'], 4).'s'?> -->
            <div class="pcoded-main-container">
                <div class="pcoded-content">
                    <div class="row">
<?php
    }
}

if(isset($restrictions['usertype'])){
    # restriction par simple fichier, aucun droit d'executer un autre fichier php
    if($restrictions['usertype']['bootstrap_url']!='' && is_file('.'.$restrictions['usertype']['bootstrap_url'])){
    }

    # restriction par simple, fichier, aucun droit de descendre dans le repertoire parent
    elseif($restrictions['usertype']['bootstrap_url']!='' && is_dir($_SERVER['DOCUMENT_ROOT'].$restrictions['usertype']['bootstrap_url'])){
        # XXX ajouter la gestion de n niveau de repertoire
        if(current(explode('/',preg_replace('|^/|isu','',$_SERVER['PHP_SELF']))) != str_replace('/','',$restrictions['usertype']['bootstrap_url'])) die('permission denied [DIR]');
    }
}

if (!isset($_GET['dl']) && !isset($_GET['ajax'])) { ?>
    <style>
   .pcoded-header .input-group .input-group-text:hover:before, .pcoded-header a:hover:before, .pcoded-header dropdown-toggle:hover:before {
        content: "";
        width: 0px;
        height: 0px;
        background: rgba(36, 46, 62, 0.1);
        top: calc(35px - 21px);
        left: -13px;
        border: none;
        border-radius: 50%;
    }
    .chosen-container { width: 100% !important; }
    .pcoded-navbar, .navbar {
        z-index: 200;
       background-color:<?=(isset($plateforme['bgcolor_head']) && !empty($plateforme['bgcolor_head'])) ? 'background-color:'.$plateforme['bgcolor_head'].'!important' : '';?>
    }

    .subnavbar{
       <?=(isset($plateforme['bgcolor_head']) && !empty($plateforme['bgcolor_head'])) ? 'background-color:'.$plateforme['bgcolor_head'].'!important' : '';?>
    }
    .table-bordered th{
            background-color: #e2e5e8!important;
    color: #444444 !important;
/*       background-color:<?=(isset($plateforme['menu_hovercolor']) && !empty($plateforme['menu_hovercolor'])) ? $plateforme['menu_hovercolor'].'!important' : '';?>;
       color: <?=(isset($plateforme['menu_linkcolor']) && !empty($plateforme['menu_linkcolor'])) ? ''.$plateforme['menu_linkcolor'].'!important' : '';?>*/
    }


    .pcoded-navbar{
       border-right:1px solid  <?=(isset($plateforme['menu_hovercolor']) && !empty($plateforme['menu_hovercolor'])) ? $plateforme['menu_hovercolor'].'!important' : '';?>
    }
    .navbar {
        border-bottom: 1px solid <?=(isset($plateforme['menu_hovercolor']) && !empty($plateforme['menu_hovercolor'])) ? $plateforme['menu_hovercolor'].'!important' : '';?>
    }

    #moxman-modal-block{
        z-index: -65536;
    }
    #moxman-modal-block.moxman-in{
    opacity:0;
    }
    .pcoded-navbar .pcoded-inner-navbar li > a{
       <?=(isset($plateforme['menu_linkcolor']) && !empty($plateforme['menu_linkcolor'])) ? 'color:'.$plateforme['menu_linkcolor'].'!important' : '';?>;
        margin:0px !important;
    }
    .pcoded-navbar .pcoded-inner-navbar li.active > a, .pcoded-navbar .pcoded-inner-navbar li:focus > a, .pcoded-navbar .pcoded-inner-navbar li:hover > a{


    }
    .pcoded-navbar .pcoded-inner-navbar li > a:hover {
        <?=(isset($plateforme['menu_hovercolor']) && !empty($plateforme['menu_hovercolor'])) ? 'color:'.$plateforme['menu_hovercolor'].'!important' : '';?>
    }
    .navbar-content{
       <?=(isset($plateforme['bgcolor_menu']) && !empty($plateforme['bgcolor_menu'])) ? 'background-color:'.$plateforme['bgcolor_menu'].'!important' : '';?>
    }
    .m-header, .pcoded-header {
       <?=(isset($plateforme['bgcolor_head']) && !empty($plateforme['bgcolor_head'])) ? 'background-color:'.$plateforme['bgcolor_head'].'!important' : '';?>

    }
    .pcoded-header .input-group .input-group-text, .pcoded-header a, .pcoded-header dropdown-toggle{
        font-size: 12px;
    }
    .thumbnails_container {
    float: left; ;
    }

    .pcoded-header[class*="header-"] .mobile-menu span {
        background-color: darkcyan!important;
    }

    .pcoded-header[class*="header-"] .mobile-menu span:after, .pcoded-header[class*="header-"] .mobile-menu span:before {
        background-color: gray !important;
    }

    .pcoded-header[class*="header-"] .mobile-menu span:after, .pcoded-header[class*="header-"] .mobile-menu span:before {
        background-color: gray!important;
    }
    </style>
<?php }

if (!empty($_SESSION['notifications'])) { ?>
    <script>
        $(document).ready(function () {
<?php
    foreach ($_SESSION['notifications'] as $type => $notification) { ?>
            notify("<?=$notification?>", "<?=$type?>");
<?php
    } ?>
        });
    </script>
<?php
    unset($_SESSION['notifications']);
}
