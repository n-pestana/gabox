<?php @session_start();
if(defined('CHECK_LOG_HASH') && isset($_GET['checkLogHash']) && $_GET['checkLogHash'] == CHECK_LOG_HASH) {
    header('Content-type: application/json');
    $isLogged = false;
    if(isset($_SESSION['admins']['id_admins'])) $isLogged = true;
    echo json_encode(array('logged' => ($isLogged ? 1 : 0), 'id' => $_SESSION['admins']['id_admins']));
    die;
}
if(empty($_SESSION)){ header("Location: /login.php"); die();}
if(!isset($_SESSION['admins'])){ob_get_clean();header("Location: /login.php");die();}
if(!is_array($_SESSION['admins']) && !isset($_SESSION['admins'])){header("Location: /login.php");die();}
if(!isset($_SESSION['admins']['id_admins'])) { header("Location: /login.php");die();}
?>
