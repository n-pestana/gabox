<?php
global $form, $gen;

$form->setPerms(array('pdv','press','news','pages','homepages','sites','backmsg','sites_options','admins','diag_r_incontournables','diag_r_soin','pages404','clients','orders','delivrymode','promocodes','advantages','adresses','pushs','comments','services','company','blogs','sportifs','zones','sports','pcat','pcatgrp','catgrp','treeview', 'testimonials','mailtype','faq','news_categories','forces','partners', 'history_line', 'commitment', 'key_numbers', 'advertising', 'solutions', 'about', 'team', 'homepage_corpo', 'worth', 'mutuals', 'services', 'contact_country','smallcarrousel','locales','plateforme','pse_nos_services','pse_homepage','contents','pages_options','templates'));
$form->auto_translate_fields = array(
    'news'=>array('author','lib','content','url')
    ,'treeview'=>array('lib', 'description', 'meta_title', 'meta_desc', 'presentation' ,'lib')
    ,'produits'=>array('presentation')
   ,'translations'=>array('lib','translations')
);
 
$form->gen_field['id'] =  array('label' => t("ID") ,'type' => 'text' ,'pattern' => '^.+' ,'size' => '4' ,'maxlength' => '32' 
                    ,'errorMsg' => t("Merci d'entrer un ID") ,'value' => '','style'=>'width:60px');
$form->gen_field['lib'] = array( 'label' => t("Libellé") ,'type' => 'text' ,'pattern' => '' ,'size' => '80' ,'maxlength' => '255' 
                    ,'errorMsg' => t("Merci d'entrer un libellé") ,'value' => '');
$form->gen_field['lib_i18n'] = array( 'label' => t("Libellé") ,'type' => 'i18n' ,'pattern' => '' ,'size' => '80' ,'maxlength' => '255' 
                    ,'errorMsg' => t("Merci d'entrer un libellé") ,'value' => '');
$form->gen_field['info'] = array( 'label' => t("Label") ,'type' => 'info');
$form->gen_field['submit'] = array( 'type'=> 'submit' ,'value'=> ("Enregistrer") ,'class' => 'btsubmit btn btn-primary'); 
$form->gen_field['button'] = array( 'type'=> 'submit' ,'value'=> ("Enregistrer") ); 
$form->gen_field['ouinon']=array( 'label' => t("Publié ?") ,'type' =>  'select' , 'style'=>'width:100px;'
                                ,'arrayValues' => array('1'=>t("OUI"),'0'=>t("NON")));
$form->gen_field['is_published']=array( 'label' => t("Publié ?") ,'type' =>  'select' 
                                , 'arrayValues' => array('1'=>t("OUI"),'0'=>t("NON")));
$form->gen_field['select']=array( 'label' => t("Est ce un menu déroulant ?") ,'type' =>  'select' );
$form->gen_field['select_i18n']=array( 'label' => t("Est ce un menu déroulant ?") ,'type' =>  'select_i18n' );
$form->gen_field['desc']= array( 'type' => 'textarea' ,'label'=> t("Description") ,'rows' => '2' 
                            ,'toolTip'=> t("this description will be showed in the frontoffice") ,'class'=>'full-width', 'pattern'=> '' ,'errorMsg' => _(""));
$form->gen_field['meta_des']= array( 'type' => 'textarea' ,'label'=> t("Description for SEO") ,'rows' => '2' 
                            ,'toolTip'=> t("this description will be showed in the frontoffice") ,'class'=>'full-width', 'pattern'=> '^.+' ,'errorMsg' => _(""));
$form->gen_field['meta_title'] = array( 'label' => t("Titre pour SEO") ,'type' => 'text' ,'pattern' => '^.+' ,'size' => '30' ,'maxlength' => '255' 
                            ,'errorMsg' => t("Merci d'enter un libellé pour le SEO") ,'value' => '');
$form->gen_field['html']= array('type' => 'html','label'=>t("Contenu HTML")); 
$form->gen_field['password'] = array( 'label' => t("Mot de passe") ,'type' => 'password' ,'pattern' => '^.+' ,'size' => '30' ,'maxlength' => '255' 
                                    ,'errorMsg' => t("Merci d'enter un mot de passe") ,'value' => '');
$form->gen_field['updated_at'] =array('type'=>'hidden','value'=>'');
$form->gen_field['.nolist'] =array('type'=>'hidden','value'=>'1');
$form->gen_field['id_sites'] =array('type'=>'hidden', 'value' => defined('ID_SITE') ? ID_SITE : '');
$form->gen_field['id_site'] = $form->gen_field['id_sites'];
$form->gen_field['autocomplete'] = array( 'label' => t("Autocomplete") ,'type' => 'autocomplete' ,'pattern' => '^.+' ,'size' => '30' 
                                ,'maxlength' => '255' ,'errorMsg' => t("Please enter the value") ,'value' => '');
$form->gen_field['simple_html_i18n']=array('type'=>'simple_html_i18n','label'=>t("I18n Field"));
$form->gen_field['simple_html']=array('type'=>'simple_html','label'=>t("Contenu HTML"));
$form->gen_field['file_i18n']=array('type'=>'file_i18n','label'=>t("Contenu HTML"));
$form->gen_field['file'] = array( 'label' => t("Fichier") ,'type' => 'file');
$form->gen_field['calendar'] = array( 'label' => t("Date : mm/dd/YYYY") ,'type' => 'calendar','format'=>'format-y-m-d','callback'=>'DbDatetoDateFr');
$form->gen_field['html_i18n']=array('type'=>'html_i18n','label'=>t("Contenu HTML"));
$form->gen_field['ouinon_i18n']=array( 'label' => t("Published ?") ,'type' =>  'select_i18n' 
                                , 'arrayValues' => array('1'=>t("OUI"),'0'=>t("NON")));
$form->gen_field['generated'] = array('type' => 'generated', 'label' => "Lib", 'value' => t("Contenu HTML"));
$form->gen_field['sep']=array('type'=>'tab_separator','label'=>t("Principal"));
$form->gen_field['files'] = array( 'label' => t("Fichiers(s)") ,'type' => 'files');
$form->gen_field['lat'] = array( 'label' => t("Lattitude") ,'type' => 'text' ,'pattern' => '' ,'maxlength' => '255' 
                            ,'errorMsg' => t("Merci d'entrer une lattitude"),'style'=>'width:100px');
$form->gen_field['cat'] = array( 'label' => t("Catégorie") ,'type' => 'text' ,'pattern' => '' ,'maxlength' => '255' ,'value' => ''
                                ,'tooltip'=>t("Entrez le nom de la catégorie dans laquelle vous souhaitez classer cet élément "));
if(defined('ID_SITE')){
    $form->gen_field['controllers']=array( 'label' => t("Controller") ,'type' =>  'select' 
                                    ,'arrayValues' => array_merge(array(''=>t("Aucun controller")),$gen->collector['sites']->getControllers(ID_SITE)));
}
