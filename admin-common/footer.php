<?php if(!isset($_GET['ajax'])){ ?>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="/assets/js/jquery.modal.js"></script>
        <script src="/assets/js/autocomplete.js"></script>
        <script src="/assets/js/fancybox/jquery.fancybox.pack.js?201511101547"></script>
        <script src="/assets/js/plugins/bootstrap.min.js"></script>
        <script src="/assets/js/pcoded.min.js"></script>
        <script src="/assets/js/plugins/jquery.dataTables.min.js"></script>
        <script src="/assets/js/plugins/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/js/pages/data-basic-custom.js"></script>
        <script src="/assets/js/libs/jquery.jeditable.mini.js?201511101547"></script>
        <script src="/assets/js/libs/jquery.nestable.js?201511101547"></script>
        <script src="/assets/js/libs/tinymce/tinymce.min.js?201511101547"></script>
        <script src="/media/js/moxman.loader.min.js?201511101547"></script>
        <script src="/assets/js/plugins/PNotify.js"></script>
        <script src="/assets/js/plugins/PNotifyButtons.js"></script>
        <script src="/assets/js/plugins/PNotifyCallbacks.js"></script>
        <script src="/assets/js/plugins/PNotifyDesktop.js"></script>
        <script src="/assets/js/plugins/PNotifyConfirm.js"></script>
        <script src="/assets/js/pages/notify-event.js"></script>
        <script src="/assets/js/fancybox/jquery.fancybox.pack.js?201511101547"></script>
        <script src="/assets/plugins/vendors/chosen/chosen.jquery.min.js"></script>
        <script src="/assets/plugins/vendors/chosen/chosen.order.jquery.min.js"></script>
        <script src="/assets/plugins/vendors/raphael/raphael-min.js"></script>
        <script src="/assets/plugins/vendors/morrisjs/morris.js"></script>
        <script src="/assets/plugins/vendors/ace/src-min/ace.js"></script>
        <script src="/assets/plugins/vendors/ace/src-min/ext-beautify.js"></script>
        <script src="/assets/plugins/vendors/ace/src-min/ext-language_tools.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/apexcharts@3.37.0"></script>
        <script src="/assets/js/common.js"></script>
        <script src="/assets/js/main.js?v=3"></script>
        <script src="/assets/js/test.js?v=3"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-migrate@3.4.1/dist/jquery-migrate.min.js"></script>

        <script src="/assets/plugins/vendors/iconpicker/iconpicker.js"></script>

        <script src="/assets/js/plugins/jquery.minicolors.min.js"></script>
        <script src="/assets/js/plugins/dropzone-amd-module.min.js"></script>
        <script src="/assets/js/plugins/bootstrap-notify.min.js"></script>
        <?php
        if(isset($pageJsLibs)) {
            foreach($pageJsLibs as $lib) { ?>
            <script type="text/javascript" src="<?= $lib;?>"></script>
            <?php }
        }

        $modules = $gen->collector['modules']->get(['is_enabled'=>1]);
        foreach($modules as $module) {
            $jsFiles = [];
            foreach (glob('{'.MODS.','.USR_MODS.'}' . $module['lib'] . '/js/*.js',GLOB_BRACE) as $jsFile) {
                $fileName = str_replace([INC, GABOX_USR_DIR], '', $jsFile);
                if (!is_link(MODS . $module['lib'])) {
                    $jsFiles[$module['lib'] . '/' . basename($jsFile)] = str_replace('/modules/', '/modules/usr/' . $conf['usr'] . '/', $fileName);
                } else {
                     $jsFiles[$module['lib'] . '/' . basename($jsFile)] = $fileName;
                }
            }
            foreach ($jsFiles as $jsFile) { ?>
                <script src="<?=$jsFile?>"></script>
            <?php } ?>
        <?php } ?>
        <!-- http://html.phoenixcoded.net/mintone/bootstrap/default/index.html#!  -->
        <style>
        </style>
        <script src="/assets/js/plugins/lightbox.min.js"></script>

         <script src="https://kit.fontawesome.com/546ffdf64d.js" crossorigin="anonymous"></script>
        <script>
            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true
            })
        </script>
    </body>
    </html>
<?php } ?>

<?php if(count($structureMenuBo)<6){ ?>
<script>
$(".nav-item").addClass("active open pcoded-trigger");
</script>
<?php } ?>
