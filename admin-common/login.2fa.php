<?php

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use OTPHP\TOTP;

header('X-Frame-Options: SAMEORIGIN');

if(!isset($_SERVER['GEOIP_CITY'])) $_SERVER['GEOIP_CITY'] = '';
if(!isset($_SERVER['GEOIP_REGION_NAME'])) $_SERVER['GEOIP_REGION_NAME'] = '';
if(!isset($_SERVER['GEOIP_COUNTRY_NAME'])) $_SERVER['GEOIP_COUNTRY_NAME'] = '';
$info = ' from IP : '.getRealUserIp().',<b> '.$_SERVER['GEOIP_CITY'].', '.$_SERVER['GEOIP_REGION_NAME'].', '.$_SERVER['GEOIP_COUNTRY_NAME'].'</b>';
if(isset($_GET['logout']) && isset($_SESSION['admins']) && isset($_SESSION['admins']['email'])){
    $gen->collector['backevents']->set('',array('type'=>'WARNING','lib'=>'Logout : '.$_SESSION['admins']['email'].$info));
}

if(isset($_GET['logout'])){
    $_SESSION=array();
    session_destroy();
    $_COOKIE=array();
    setcookie ('autolog', '', time() + 35000 );
    header("Location: /login.php");
    die();
}

$error = false;
$msg   = false;

if(!isset($_SESSION['prelogged_admins']['email'])) {
    header("Location: /login.php");
    die();
}

if (isset($_SERVER['id_plateforme']) && $_SERVER['id_plateforme'] != '') {
    $plateforme = $gen->collector['plateforme']->getOne((int)$_SERVER['id_plateforme']);
} else {
    $plateforme = $gen->collector['plateforme']->getOne();
}
$email = $_SESSION['prelogged_admins']['email'];
$loggedAdmin = $gen->collector['admins']->getOne(array('id_admins' => (int) $_SESSION['prelogged_admins']['id_admins'], 'email' => $email));

// Two factor authentication
$env = isset($_SERVER['env']) ? strtoupper($_SERVER['env']) : 'PROD';
if (!empty($loggedAdmin['2fa_key'])
) {
    $secretKey = $loggedAdmin['2fa_key'];
    $otp = TOTP::create($secretKey);
} else {
    $otp = TOTP::create();
}
$otp->setIssuer((!empty($plateforme['2fa_issuer']) ? $plateforme['2fa_issuer'] : 'GABOX') . '_' . $env);
$otp->setLabel(str_replace(':', '', $email));
$secretKey = $otp->getSecret();
if ($loggedAdmin['2fa_key_enabled'] == 0) {
    $writer = new PngWriter();
    $qrCode = QrCode::create($otp->getProvisioningUri())
        ->setEncoding(new Encoding('UTF-8'))
        ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
        ->setSize(200)
        ->setMargin(0)
        ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
        ->setForegroundColor(new Color(0, 0, 0))
        ->setBackgroundColor(new Color(255, 255, 255));
    $qrResult = $writer->write($qrCode);
}

if (isset($_POST['b'])) {
    $valid = false;
    $redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : 'index.php';
    
    // Check fields
    if (!isset($_POST['qrcode']) or strlen($_POST['qrcode']) == 0) {
        $error = t('Please enter your code');
    } else {
        if ($otp->verify($_POST['qrcode']) === false) {
            $error = "Your authentication code isn't valid. Please try again with another one";
        } else {
            $gen->collector['admins']->set((int) $loggedAdmin['id_admins'], ['2fa_key_enabled' => 1]);
            $valid = $gen->collector['admins']->getOne(array('id_admins' => (int) $_SESSION['prelogged_admins']['id_admins'], 'email' => $email));
            $adminType = $loggedAdmin['usertype'];
            $bootstrapUrl = $gen->collector['usertypes']->getOne((int) $adminType);
            if(is_array($bootstrapUrl) && isset($bootstrapUrl['bootstrap_url'])) {
                $redirect = $bootstrapUrl['bootstrap_url'];
            }
        }
    }
    $ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) and strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    if ($valid) {
        if(array_key_exists('last_login', $gen->collector['admins']->array_fields)) {
            $now = new Datetime();
            $now->setTimeZone(new DateTimeZone('Europe/Paris'));
            $gen->collector['admins']->setLogOrigin('SYSTEM');
            $gen->collector['admins']->set((int) $_SESSION['prelogged_admins']['id_admins'], array('last_login' => $now->format('Y-m-d H:i:s')));
            $gen->collector['admins']->setLogOrigin('OPERATOR');
        }
        $gen->collector['backevents']->set('',array('type'=>'MSG','lib'=>'Login second step: '.$_SESSION['prelogged_admins']['email'] . $info));
        @session_start();
        $_SESSION['admins'] = $valid;

        if ($ajax) {
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: '.date('r', time()+(86400*365)));
            header('Content-type: application/json');

            echo json_encode(array(
                'valid' => true,
                'redirect' => $redirect
            ));
            exit();
        } else {
            header('Location: '.$redirect);
            exit();
        }
    } else {
        if ($ajax) {
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: '.date('r', time()+(86400*365)));
            header('Content-type: application/json');

            echo json_encode(array(
                'valid' => false,
                'error' => $error
            ));
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title><?=(isset($plateforme['lib']) && !empty($plateforme['lib'])) ? $plateforme['lib'] : 'GaboX';?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="" />
	<meta name="keywords" content="">
	<meta name="author" content="" />
	<!-- Favicon icon -->
	<link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
	<!-- vendor css -->
	<link rel="stylesheet" href="assets/css/style.css">
</head>

<!-- [ auth-signin ] start -->
<div class="auth-wrapper" style="background-color:<?=(isset($plateforme['bgcolor_menu']) && !empty($plateforme['bgcolor_menu']))  ? $plateforme['bgcolor_menu'] : '#242e3e'?>">
	<div class="auth-content text-center">
		<img src="<?=(isset($plateforme['media']) && !empty($plateforme['media'])) ? $plateforme['media'] : '/assets/images/gabox-logo-w.png'?>" alt="" class="img-fluid mb-3">
		<div class="card">
			<div class="row align-items-center">
				<div class="col-md-12">
					<div class="card-body">
                        <form class="form with-margin" name="login-form" id="login-form" method="post" action="">
                            <input type="hidden" name="b" id="b" value="2fa-log">
                            <h4 class="mb-3 f-w-400">Admin <?=(defined('plateform_name')) ? plateform_name : '' ?></h4>
                            <?php if ($error){?>
                                <div class="alert alert-danger" role="alert"><?php echo htmlspecialchars($error); ?></div>
                            <?php }elseif($msg){ ?>	
                                <div class="alert alert-success" role="alert"> <?php echo htmlspecialchars($msg); ?></div>
                            <?php } ?>
                            <div class="error-ajax" id="error-ajax"></div>
                            <?php
                            if (isset($_REQUEST['redirect'])):?>
                                <input type="hidden" name="redirect" id="redirect" value="<?php echo htmlspecialchars($_REQUEST['redirect']); ?>">
                            <?php endif ?>

                            <div class="input-group">
                                <label style="margin-right: 15px;">E-mail</label>
                                <strong><?= $email;?></strong>
                            </div>
                            <div class="input-group mb-3"></div>
                            <?php if (isset($qrResult)) { ?>
                            <div class="qr-code">
                                <label><?=t("If you haven't synchronized your <strong>Google Authenticator App</strong> yet, please <strong>scan the following QR Code</strong> to pair your mobile and get your code")?></label>
                            </div>
                            <p class="inline-small-label captcha-block">
                                <img src="<?=$qrResult->getDataUri()?>" alt="Two factor authentication image" />
                            </p>
                            <?php
                                if (empty($loggedAdmin['2fa_key'])) {
                                    $gen->collector['admins']->set((int) $loggedAdmin['id_admins'], ['2fa_key' => $secretKey]);
                                }
                            } else { ?>
                            <p class="qr-code"><?=t("Please use <strong>Google Authenticator App</strong> to get a valid login code.<br/>This code will be refreshed every 30 seconds")?></p>
                            <?php } ?>
                            <p class="input-group mb-3">
                                <input type="text" name="qrcode" id="qrcode" class="form-control" value="" required placeholder="Code">
                            </p>
                            <button class="btn btn-block btn-primary mb-4 rounded-pill" type="submit">Login</button>
                            <p class="mb-2 text-muted">Forgot password? <a href="password-reminder.php" class="f-w-400">Reset</a></p>
                        </form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<svg width="100%" height="250px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave" d="" />
	</svg>
	<svg width="100%" height="250px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave-two" d="" />
	</svg>
	<svg width="100%" height="250px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave-three" d="" />
	</svg>
</div>
<!-- [ auth-signin ] end -->

<!-- Required Js -->
<script src="assets/js/vendor-all.min.js"></script>
<script src="assets/js/plugins/bootstrap.min.js"></script>
<script src="assets/js/waves.min.js"></script>
<script src="assets/js/pages/TweenMax.min.js"></script>
<script src="assets/js/pages/jquery.wavify.js"></script>
<script>
	$('#feel-the-wave').wavify({
		height: 100,
		bones: 3,
		amplitude: 90,
		color: 'rgba(22, 163, 221, 4)',
		speed: .25
	});
	$('#feel-the-wave-two').wavify({
		height: 70,
		bones: 5,
		amplitude: 60,
		color: 'rgba(82, 110, 181, .3)',
		speed: .35
	});
	$('#feel-the-wave-three').wavify({
		height: 50,
		bones: 4,
		amplitude: 50,
		color: 'rgba(216, 35, 145, .5)',
		speed: .45
	});
</script>

		<form class="form" id="password-recovery" method="post" action="#">
				<fieldset class="grey-bg no-margin collapse">
					<legend><a href="#">Lost password?</a></legend>
					<p class="input-with-button">
						<label for="recovery-mail">Enter your e-mail address</label>
						<input type="text" name="recovery-mail" id="recovery-mail" value="">
                        <button type="submit" class="float-right">Envoyer</button>
					</p>
				</fieldset>
			</form>
		</div>
        
        </div>
        <br />
	
	<script type="text/javascript">
    $(document).ready(function() {
        $('#login-form').submit(function(event) {
            event.preventDefault();
            var code = $('#qrcode').val();
            if (!code || code.length == 0) {
                $('#error-ajax').html('<div class="alert alert-success" role="alert">Please enter your code</div>');
            } else {
                var target = $(this).attr('action');
                if (!target || target == '')
                {
                    target = document.location.href.match(/^([^#]+)/)[1];
                }

                var data = {
                    b: $('#b').val(),
                    qrcode: code
                };
                var redirect = $('#redirect');
                if (redirect.length > 0) {
                    data.redirect = redirect.val();
                }

                var sendTimer = new Date().getTime();
                $.ajax({
                    url: target,
                    dataType: 'json',
                    type: 'POST',
                    data: data,
                    success: function(data, textStatus, XMLHttpRequest) {
                        if (data.valid) {
                            var receiveTimer = new Date().getTime();
                            if (receiveTimer-sendTimer < 1000) {
                                setTimeout(function() {
                                    document.location.href = data.redirect;
                                }, 1000-(receiveTimer-sendTimer));
                            } else {
                                document.location.href = data.redirect;
                            }
                        } else {
                            $('#error-ajax').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        $('#error-ajax').html('<div class="alert alert-warning">Error while contacting server, please try again</div>');
                    }
                });
                $('#error-ajax').html('<div class="alert alert-success" role="alert">Please wait, checking code...</div>');
            }
        });
    });
	</script>
</body>
</html>
