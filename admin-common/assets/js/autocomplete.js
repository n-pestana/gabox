var genAutocomplete = {
    fieldId: null,
    init: function() {
        if($('.gen_autocomplete').length) {
            $('.gen_autocomplete').each(function() {
                genAutocomplete.fieldId = jQuery(this).attr('id');
                if(jQuery(this).siblings('.twitter-typeahead').length == 0 && jQuery(this).parent().is('.twitter-typeahead') === false) {
                    var url = $(this).attr('data-url');
                    var qryParam = url.indexOf('?') == -1 ? '?' : '&';
                    var provider = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('lib'),
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        remote: {
                            url: url + qryParam + 'q=%QUERY',
                            wildcard: '%QUERY'
                        }
                    });

                    $(this).typeahead(null, {
                        name: $(this).attr('id'),
                        highlight: true,
                        limit: 50,
                        display: 'lib',
                        source: provider
                    });

                    $(this).bind('typeahead:select', function(ev, suggestion) {
                        var hiddenField = genAutocomplete.fieldId.replace('ac_', '');
                        if(jQuery('#' + hiddenField).length) {
                            jQuery('#' + hiddenField).val(suggestion.value);
                        }
                        var cp = jQuery('#' + hiddenField).parents('form:first').find('#cp');
                        if(jQuery(cp).length) {
                            jQuery(cp).val(suggestion.row.code_postal);
                        }
                    });

                    $(this).on('blur', function() {
                        if(jQuery(this).val() == '') {
                            var hiddenField = genAutocomplete.fieldId.replace('ac_', '');
                            if(jQuery('#' + hiddenField).length) {
                                jQuery('#' + hiddenField).val('');
                            }
                        }
                    });
                }
            });
        }
    }
};
