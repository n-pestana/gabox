 
    function changeTextarea(){
    var img = '';
    $( "textarea.html" ).each(function() {
      img =$('#bg_'+this.id).val();
      if (typeof  img!= 'undefined' ){
        tinymce.init({
            relative_urls: false,
            indentation : '5px',
            selector: ".html",
            theme: "modern",
            height: 411, 
            force_p_newlines : false,
            forced_root_block : "p", 
            width:'100%',
            scroll: 'false',
            plugin_preview_width : "100%",
            plugin_preview_height : "411",
            style_formats: [
            {title: "Caps", items: [
                {title: 'None', inline: 'span', styles: { 'text-transform': 'none'}}
                ,{title: 'Uppercase', inline: 'span', styles: { 'text-transform': 'uppercase'}}
              ]}
            ,{title: "Size", items: [
                {title: 'Bigest', inline: 'span', styles: { 'font-size': '34px','line-height':'40px', 'font-family':  'Helvetica,Arial,Tahoma,sans-serif', 'text-transform': 'uppercase', 'font-weight': ' normal','margin-left':'3px'}}
                ,{title: 'Big', inline: 'span', styles: { 'font-size': '28px','line-height':'36px', 'font-family':  'Helvetica,Arial,Tahoma,sans-serif', 'text-transform': 'uppercase', 'font-weight': ' normal','margin-left':'3px'}}
               ,{title: 'Normal', inline: 'span', styles: {'font-size': '24px','line-height':'32px', 'font-family':  'Helvetica,Arial,Tahoma,sans-serif', 'text-transform': 'uppercase', 'font-weight': ' normal','margin-left':'3px'}}
               ,{title: 'Small', inline: 'span', styles: { 'font-size': '22px','line-height':'30px', 'font-family':  'Helvetica,Arial,Tahoma,sans-serif', 'text-transform': 'uppercase', 'font-weight': ' normal','margin-left':'3px'}}
               ,{title: 'Smaller', inline: 'span', styles: { 'font-size': '18px','line-height':'26px', 'font-family':  'Helvetica,Arial,Tahoma,sans-serif', 'text-transform': 'uppercase', 'font-weight': ' normal','margin-left':'3px'}}
               ,{title: 'Smallest', inline: 'span', styles: { 'font-size': '16px','line-height':'24px', 'font-family':  'Helvetica,Arial,Tahoma,sans-serif', 'text-transform': 'uppercase', 'font-weight': ' normal'}}
              ]}

            ,{title: "Titles", items: [
                 {title: 'H1', block: 'h1'}
                ,{title: 'H2', block: 'h2'}
                ,{title: 'H3', block: 'h3'}
                ,{title: 'H4', block: 'h4'}
                ,{title: 'H5', block: 'h5'}
           ]}

          ],

             plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code ",
                "insertdatetime media nonbreaking table contextmenu directionality",
                "emoticons template paste textcolor layer moxiemanager"
            ],
            toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code",
            image_advtab: true,
            templates: [
                {title: 'Full Link', content: '<a href="#" style="overflow:hidden;width:100%;height:100%;position:absolute;" class="back-over">&nbsp;</a>'}
            ]
        });
      }

    });
}

    changeTextarea();
    if(genAutocomplete != undefined) {
        genAutocomplete.init();
    }
    var timeout = $('.modal-content').length ? 300 : 1;
    setTimeout('initEditPage();', timeout);

    // si page avec traductions
    if($('.hasi18nfields').length){
        $('.i18nchild').hide();
        $('.myform').find('.edition-button-bar').prepend('<button class=" btn btn-primary btni18n">'+$("#i18n_show_translation").val()+'</button>');
        $('.btni18n').bind( "click", function() {
            $('.i18nchild').toggle(function(){
                if($(this).css('display')=='none'){
                    $('.btni18n').html($("#i18n_show_translation").val());
                }
                else{
                    $('.btni18n').html($("#i18n_hide_translation").val());
                }

            });
            return false;
        });
    }

function initEditPage() {
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

/*     $(".myform").submit(function(){
       $.ajax({type:"POST", data: $(this).serialize(), url: $(this).attr('action'),
            success: function(data){
                try {
                    var jsonObj = $.parseJSON(data);
                    $('.myform .err-desc').remove();
                    $('.myform .error').removeClass('error');
                    if(jsonObj.is_valid == 0) {
                        var errs = jsonObj.errors;
                        for(k in errs) {
                            var sels = new Array('.myform #' + k, '.myform .gen_' + k);
                            for(i in sels) {
                                if($(sels[i]).length) {
                                    $(sels[i]).addClass('error');
                                    $(sels[i]).parent().after('<div class="err-desc">' + errs[k] + '</div>');
                                }
                            }
                        }
                    }
                } catch(err) {
                    $(".myform").html(data);
                }
            },
            error: function() {
                $(".myform").html('Une erreur est survenue.');
            }
        });
        return false;
    });

*/
}

$("label").hover(function(){
  $(this).css( 'cursor', 'pointer' );
});
  $("legend").css("cursor","pointer").click(function(){
        var legend = $(this);
        var value = $(this).children("span").html();
        if(value=="[-]")
            value="[+]";
        else
            value="[-]";
       $(this).siblings().slideToggle("slow", function() { legend.children("span").html(value); } );
    });

