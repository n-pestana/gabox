function CheckAll(){
    $('.check').prop('checked', true);
}

//Event listener for iframe resizing (front editable)
window.addEventListener( "message",
  function (e) {
        //if(e.origin !== 'B'){ return; } 
        // TODO: Check origin within ajax to ensure security
        if(e.data.indexOf('resize:') != -1 && e.data.length < 15) {
            var s = e.data.split(':');
            var size = s[1];
            //$(".fancybox-wrap").css({ top: top, left: left});
            jQuery('.fancybox-outer, .fancybox-skin, .fancybox-inner').animate({width: size}, 350, function () { 
                var top = ($(window).height() / 2) - ($(".fancybox-skin").outerHeight() / 2);
                var left = ($(window).width() / 2) - ($(".fancybox-skin").outerWidth() / 2);
                jQuery('.fancybox-wrap').animate({left: left, top: top}, 200);
            });
        }
  },
false);

$( document ).ready(function() {

    $('.gaboxTooltip').each(function(){
          $(this).hover(function(){
            console.log('clic');
            $(this).next('p').show();
          },function(){
            $(this).next('p').hide();
        })
      });



    /* 
        gestion des champs type html_code
    */



    if($('.iconpicker-input').length){ 
        $('.iconpicker-input').iconpicker();
    }

    if($('.colorpicker-input').length){ 
        $('.colorpicker-input').minicolors({
             control:  'hue'
            ,theme: 'bootstrap'
            ,format:'hex'
            
        });
    }

    if($('.html_code').length){ 

        $('.html_code').each(function () {

            var textarea = $(this);

            var mode = textarea.data('editor');

            var editDiv = $('<div>', {
                position: 'absolute',
                width: textarea.width(),
                height: textarea.height(),
                'class': textarea.attr('class')
            }).insertBefore(textarea);

            textarea.css('visibility', 'hidden');

             var editor=ace.edit(editDiv[0], {
                theme: "ace/theme/tomorrow_night_eighties",
                mode: "ace/mode/html",
                maxLines: 30,
                wrap: true,
                printMargin:false,
                fontSize: 14,
                showInvisibles: false,
                enableBasicAutocompletion: true
            });

            editor.renderer.setShowGutter(false);
            editor.getSession().setValue(textarea.val());

            textarea.closest('form').submit(function () {
                textarea.val(editor.getSession().getValue());
            })

        });

        $(".ace_editor").height( 500 );
    }

  var config = {
      /*'.chosen-select'           : {},
    */
      '.chosen-select-deselect'  : {allow_single_deselect: true}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    $(".chosen-select").chosen();
    // Gestion du champ permettant de conserver l'ordre des éléments liés
    $(".chosen-select").on('change', function() {
        if (typeof ChosenOrder.getSelectionOrder === 'function') {
            var orderField = $(this).attr('name').replace('][]', '_order]');
            if ($('input[name="' + orderField + '"]').length) {
                $('input[name="' + orderField + '"]').val($(this).getSelectionOrder());
            }
        }
    });
    if ($(".chosen-select").length) {
        $(".chosen-select").each(function() {
            if (typeof ChosenOrder.getSelectionOrder === 'function') {
                var orderField = $(this).attr('name').replace('][]', '_order]');
                if ($('input[name="' + orderField + '"]').length) {
                    $(this).setSelectionOrder($('input[name="' + orderField + '"]').val().split(','));
                }
            }
        });
        $(".chosen-select").parents('form').on('submit', function() {
            $(".chosen-select").trigger('change');
        })
    }
        
var navManager = {
    currentModal: null,
    currentFancyLink: null
};


    $("section ul").each(function() {
        var elem = $(this);
        if (elem.children().length == 0) {
          elem.parent().remove();
        }
    });


// $(".fancybox").fancybox();
    $('.notify-hidden').each(function(){
            notify('"'+$(this).val()+'" mis à jour !');
    });

    $('.toggle').each(function(){
          $(this).click(function(){
            $(this).parent().next('ul').toggle();
          })
      });

 $('.edit_in_place').each(function() {
	if($(this).hasClass('edit_in_place string')){
	    var hash =($(this).attr('rel'));
	    $(this).editable('/gen_admin/ajax/edit_in_place.php?hash='+hash, {
		 indicator : 'Saving...',
		 tooltip   : '--',
		 placeholder: '-',
                 callback : function(value, settings) {
                    notify("Mise à jour effectuée !");
                }
                
	     });

	}
	if($(this).hasClass('edit_in_place textarea')){
	    var hash =($(this).attr('rel'));
	    $(this).editable('/gen_admin/ajax/edit_in_place.php?hash='+hash, {
		 indicator : 'Saving...',
		 tooltip   : '--',
		 type      : "textarea",
		 placeholder: '-',
		height: "150px",
		  submit    : 'OK',
	     });
	}

	if($(this).hasClass('edit_in_place enum')){
		var tmp = $(this);
		$(this).change(function(){
		       $.ajax({
			 type: "POST",
			 url: "/gen_admin/ajax/edit_in_place.php?hash="+($(this).attr('rel')),
			 data: "id="+($(this).attr('id'))+"&value="+$(this).val(),
			 success:function(msg){
                            notify("Mise à jour effectuée !");
			    $(tmp).parent().parent().css("background-color",msg);
			    $(tmp).parent().css("background-color",msg);
			 }
		       });
		});

	}


  });


  $(".eip-switch").click( function (){
      $(".wait").show();
      $("body").css("cursor", "wait");
      $(".wait").show();
      var hash =($(this).parent('p').attr('rel'));
      var action='reverse';
      var id=($(this).parent('p').attr('id'));
     // var tr_id = ($(this).parent('p').parent('td').parent('tr').attr('id'));
       $.ajax({
         type: "GET",
         url: "/gen_admin/ajax/action.php",
         data: "action="+action+"&hash="+hash+"&id="+id,
         success: function(msg){
    	 notify(msg);
        $(".wait").hide();
          $("body").css("cursor", "");
         //reloadRow(tr_id.replace('tr_', ''));
         }
       });
       return true;
    }); 

$(".eip-switch-countrypurchase").click( function (){
      $(".wait").show();
      $("body").css("cursor", "wait");
      var hash =($(this).parent('p').attr('rel'));
      var action='reverseCountryPurchase';
      var id=($(this).parent('p').attr('id'));
      var newvalue=(  $(this).parent('p').find('input').attr("checked")  ) ? 0 : 1 ;

       $.ajax({
         type: "GET",
         url: "/gen_admin/ajax/action.php",
         data: "action="+action+"&hash="+hash+"&id="+id+"&newvalue="+newvalue,
         success: function(msg){
	 notify(msg);
        $(".wait").hide();
          $("body").css("cursor", "");
         //reloadRow(tr_id.replace('tr_', ''));
         }
       });
       return true;
    }); 



  $(".eip-switch-country").click( function (){
      $(".wait").show();
      $("body").css("cursor", "wait");
      var hash =($(this).parent('p').attr('rel'));
      var action='reverseCountry';
      var id=($(this).parent('p').attr('id'));
      var newvalue=(  $(this).parent('p').find('input').attr("checked")  ) ? 0 : 1 ;

     // var tr_id = ($(this).parent('p').parent('td').parent('tr').attr('id'));
       $.ajax({
         type: "GET",
         url: "/gen_admin/ajax/action.php",
         data: "action="+action+"&hash="+hash+"&id="+id+"&newvalue="+newvalue,
         success: function(msg){
             notify(msg);
             $("body").css("cursor", "");
             //reloadRow(tr_id.replace('tr_', ''));
         }
       });
       return true;
    }); 

  
   $(".eip-multi-switch").click( function (){

      $(".wait").show();
      $("body").css("cursor", "wait");
      var hash =($(this).parent().find('p').attr('rel'));
      var action='reverse';
      var id=($(this).parent().find('p').attr('id'));

		if ($(this).parent().is('.green-keyword')) {
			$(this).parent().removeClass('green-keyword');
			$(this).parent().addClass('purple-keyword');
		
		} else {
			$(this).parent().addClass('green-keyword');
			$(this).parent().removeClass('purple-keyword');

		}

	$.ajax({
         type: "GET",
         url: "/gen_admin/ajax/action.php",
         data: "action="+action+"&hash="+hash+"&id="+id,
         success: function(msg){
	 	notify(msg);
         	$("body").css("cursor", "");
                $('.wait').hide();
         }
       });
       return true;
    }); 
  
    $("#addUrl").click( function (){
      var url=($("#url").val());
      var hash =($(this).attr('rel'));
      var action='create_project';
      var id='';
       $.ajax({
         type: "GET",
         url: "/gen_admin/ajax/action.php",
         data: "action="+action+"&param="+url+"&hash="+hash+"&id="+id,
         success: function(msg){
            location.reload();
         }
       });
       return false;
    }); 

$(".enable-language").click(function (){
 if(confirm("You are going to change the language of the website, are you sure ?")){
	alert('ok');
 } 
 else{
  return false;

 }
});

$(".azeip-switch").click(function (){
 if(confirm("This modification will ta change now, are you sure ?")){
  
 } 
 else{
  return false;

 }
});


$(" a.edit_content, td.fancylink a").click( function (){
    $("body").css("cursor", "wait");
    var action=($(this).attr('href'));
    navManager.currentFancyLink = $(this);
    var elem = this;
    $.ajax({
      type: "GET",
      cache : false,
      url:action,
      data  : $(this).serializeArray(),
      success: function(data) {
        $.fancybox({
            'height': $(elem).parent().hasClass('small') ? '70%' : '99%',
            'content':data,
            'width':$(elem).parent().hasClass('small') ? '30%' : '99%',
            'scrolling' : 'auto',
            'autoSize' : false,
             'keys' : {
                close  : null
            },
            afterShow: function() {
                bindPopinElements();
            }

          });

         $("body").css("cursor", "auto");
         $(".wait").hide();
      }
    });
    return false;
});

    $("#addElement").click( function (){
    
      $("body").css("cursor", "wait");
      var action=($(this).attr('rel'));
       $.ajax({
         type: "GET",
         url:action,
         success: function(data){
             $.fancybox({
                'height': '90%',
                'content':data,
                'width':'90%',
                'scrolling' : 'auto',
                'autoSize' : false,
                'keys' : {
                    close  : null
                },
                afterShow: function() {
                    bindPopinElements();
                }
              });

             //$("#gen_edit").html(msg);

         $("body").css("cursor", "auto");
         }
       });
       return false;
    });

	if ($("#nestable-treeview").length>0) {
		var updateOutput = function(e)
		{

			var list   = e.length ? e : $(e.target),
			output = list.data('output');
			if (window.JSON) {
			output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                        
                                var extraParams = '';
                                if ($('input#parentOrder').length) {
                                    extraParams = '&parentLevel=' + $('input#parentLevel').val() + '&parentOrder=' + $('input#parentOrder').val();
                                }
                                
				jQuery.ajax({
				   type: "POST",
				   url: "/gen_admin/ajax/ajax.treeview.php",
				   data: "val="+window.JSON.stringify(list.nestable('serialize')) + extraParams,
					contentType: "application/x-www-form-urlencoded;charset=UTF-8",
					dataType: "html",
					error:function (xhr, ajaxOptions, thrownError){
				alert(xhr.status);
				alert(thrownError);
				} , 
				   success: function(msg){
						notify("Menu mis à jour !");
					//alert('Un email de confirmation vous a été envoyé !');
				  }
				});
				
			} else {
			output.val('JSON browser support required for this demo.');
			}
		};

		// activate Nestable for list 1


		$('#nestable-treeview').nestable({
			group: 1
            ,maxDepth: 10
		})
		.on('change', updateOutput);

		$('#nestable-treeview').nestable('expandAll');
		updateOutput($('#nestable-treeview').data('output', $('#nestable-output')));

		$('#nestable-treeview-menu').on('click', function(e)
		{
			var target = $(e.target),
			action = target.data('action');
			if (action === 'expand-all') {
			$('.dd').nestable('expandAll');
			}
			if (action === 'collapse-all') {
			$('.dd').nestable('collapseAll');
			}
		});

	}
	if ($("#nestable-treeview-ro").length>0) {
		var updateOutput = function(e)
		{
			var list   = e.length ? e : $(e.target),
			output = list.data('output');
			if (window.JSON) {
			//output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));

			} else {
			output.val('JSON browser support required for this demo.');
			}
		};

		// activate Nestable for list 1


		$('#nestable-treeview-ro').nestable({
			group: 1
            ,maxDepth: 10
		})
		.on('change', updateOutput);

		$('#nestable-treeview-ro').nestable('collapseAll');
		updateOutput($('#nestable-treeview-ro').data('output', $('#nestable-output')));

		$('#nestable-treeview-menu').on('click', function(e)
		{
			var target = $(e.target),
			action = target.data('action');
			if (action === 'expand-all') {
			$('.dd').nestable('expandAll');
			}
			if (action === 'collapse-all') {
			$('.dd').nestable('collapseAll');
			}
		});

	}

	$('#save_menu').click(function(e) {
			var list   = e.length ? e : $(e.target),
            	output = list.data('output');

			jQuery.ajax({
		       	   type: "POST",
		       	   url: "ajax.menu.php",
		       	   data: "action=publish&val="+window.JSON.stringify(list.nestable('serialize')),
				contentType: "application/x-www-form-urlencoded;charset=UTF-8",
				dataType: "html",
				error:function (xhr, ajaxOptions, thrownError){
                    	//alert(xhr.status);
                    	//alert(thrownError);
                	} , 
		       	   success: function(msg){
					notify('Menu saved');
		      	  }
		    	});
	});

	$('#save_treeview').click(function(e) {
			var list   = e.length ? e : $(e.target),
	            	output = list.data('output');

			jQuery.ajax({
		       	   type: "POST",
		       	   url: "/gen_admin/ajax/ajax.treeview.php",
		       	   data: "action=publish&val="+window.JSON.stringify(list.nestable('serialize')),
				contentType: "application/x-www-form-urlencoded;charset=UTF-8",
				dataType: "html",
				error:function (xhr, ajaxOptions, thrownError){
                    	//alert(xhr.status);
                    	//alert(thrownError);
                	} , 
		       	   success: function(msg){
					notify('Treeview saved');
		      	  }
		    	});
	});

	$('#cancel_menu').click(function(e) {
			//openModal();
			var list   = e.length ? e : $(e.target),
            	output = list.data('output');

			jQuery.ajax({
		       	   type: "POST",
		       	   url: "ajax.menu.php",
		       	   data: "action=cancel&val="+window.JSON.stringify(list.nestable('serialize')),
				contentType: "application/x-www-form-urlencoded;charset=UTF-8",
				dataType: "html",
				error:function (xhr, ajaxOptions, thrownError){
                    	alert(xhr.status);
                    	alert(thrownError);
                	} , 
		       	   success: function(msg){
					notify('Menu saved');
					location.reload();
		      	  }
		    	});
			
	});

	$('#cancel_treeview').click(function(e) {
			//openModal();
			var list   = e.length ? e : $(e.target),
            	output = list.data('output');

                        var extraParams = '';
                        if ($('input#parentOrder').length) {
                            extraParams = '&parentLevel=' + $('input#parentLevel').val() + '&parentOrder=' + $('input#parentOrder').val();
                        }

			jQuery.ajax({
		       	   type: "POST",
		       	   url: "/gen_admin/ajax/ajax.treeview.php",
		       	   data: "action=cancel&val="+window.JSON.stringify(list.nestable('serialize')) + extraParams,
				contentType: "application/x-www-form-urlencoded;charset=UTF-8",
				dataType: "html",
				error:function (xhr, ajaxOptions, thrownError){
                    	alert(xhr.status);
                    	alert(thrownError);
                	} , 
		       	   success: function(msg){
					notify('Menu saved');
					location.reload();
		      	  }
		    	});
			
	});




    	$('#add_menu').click(function(e) {
			jQuery.ajax({
		       	   type: "POST",
		       	   url: "/gen_admin/ajax/ajax.menu.php",
		       	   data: "action=addElement",
				contentType: "application/x-www-form-urlencoded;charset=UTF-8",
				dataType: "html",
				error:function (xhr, ajaxOptions, thrownError){
                    	alert(xhr.status);
                    	alert(thrownError);
                	} , 
		       	   success: function(msg){
					$("#nestable").append(msg);	
					$("#nestable li").last().applyTemplateSetup();
					$("#nestable li").last().find('a').bind({
						click: function() {
						$("body").css("cursor", "wait");
						var action=($(this).attr('href'));
						$.ajax({
						type: "GET",
						url:action,
							success: function(msg){
								$("#gen_edit").html(msg);
								$("body").css("cursor", "auto");
							}
						});
						return false;
					}});

					$("html, body").animate({ scrollTop: $(document).height() }, 1000);
					notify('Menu element added');

		      	  }
		    	});

    });	
    if( $('#lastid').length > 0){
            var nestablecount = $('#lastid').val();
    }
    $('#add_treeview').click(function(e) {
        const regex = /id_treeview=[0-9]+/gi;
        var href = '';
        if($('#nestable-treeview li a').first().length > 0){
            href = $('#nestable-treeview li a').first().attr('href').replace(regex, 'id_treeview=' + (++nestablecount));
        }
        var selector = '#nestable-treeview ol:first';
        if (href.indexOf('parent-hook') !== -1 || href.indexOf('parent-id') !== -1) {
            selector += ' ol:first';
            // Pas encore de sous-éléments
            if ($(selector).length === 0) {
                $('#nestable-treeview ol:first li').append('<ol class="dd-list"></ol>');
            }
        }
        //XXX un rel en dur ?
        $(selector).prepend('<li class="dd-item" data-id="' + nestablecount + '"><div class="dd-handle"><span rel="05da13029de18ded053cdda90d0020f8" class="bind-'+nestablecount+' edit_in_place string" id="treeview@lib@'+nestablecount+'@string" title="--">New ' + nestablecount + '</span></div><a href="' + href +'" class=" edit editbutton btn btn-primary btn-sm"><span class="smaller">edit</span></a><a href="#" id="'+nestablecount+'" class="treeviewDel editbutton btn btn-danger btn-sm"><span class="smaller">delete</span></a></li>');
        updateOutput($('#nestable-treeview'));
    });	

    $(document).on('click', 'td.delete .delete', function(){
        var button = $(this);

        PNotify.modules.Confirm.defaults.buttons[0].text = 'Supprimer';
        PNotify.modules.Confirm.defaults.buttons[1].text = "Annuler";

/**/
        var notice = PNotify.notice({
          //title: 'Confirmation demandée',
          //text: 'Souhaitez vous vraiment supprimer cet élément ?',
          title: $("#delete_confirm_title").val(),
          text: $("#delete_confirm_content").val(),
          icon: 'fas fa-question-circle',
          hide: false,
          modules: {
            Confirm: {
              confirm: true
            },
            Buttons: {
              closer: false,
              sticker: false
            },
            History: {
              history: false
            }
          }
        });
        notice.on('pnotify.confirm', function() {
            button.parent().parent().animate({opacity: '0'}, 150, function(){
              button.parent().parent().animate({height: '0px'}, 150, function(){
                button.parent().parent().remove();
              });
            });
            $.ajax({
                 type: "GET",
                 url: button.attr('href'),
                 success:function(msg) {
                    notify($("#delete_confirm_ok").val());
                 }
             });
            return true;
        });
        notice.on('pnotify.cancel', function() {
            notify($("#delete_confirm_cancel").val());
            return false;
        });
        return false;
    });

    $(document).on('click', '#nestable-treeview a.treeviewDel', function(){
        var el = $(this).parent('.dd-item');
        var itemName = el.find('.dd-handle:first').text();
        if(confirm("Are you sure you want to delete this item (" + itemName + ") and all its children ?\n\nThis action can't be reverted")) {
            $.ajax({
                 type: "POST",
                 data : {action: 'delete', id: $(this).attr('id')},
                 url: "/gen_admin/ajax/ajax.treeview.php",
                 success:function(msg) {
                    notify("Suppression effectuée !");
                    el.remove();
                 }
             });
        }
        return false;
    });


function initDatePicker(){
    $('.datepicker').datepick({
        alignment: 'bottom',
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'yyyy-mm-dd',
        renderer: {
            picker: '<div class="datepick block-border clearfix form"><div class="mini-calendar clearfix">' +
                    '{months}</div></div>',
            monthRow: '{months}',
            month: '<div class="calendar-controls" style="white-space: nowrap">' +
                        '{monthHeader:M yyyy}' +
                    '</div>' +
                    '<table cellspacing="0">' +
                        '<thead>{weekHeader}</thead>' +
                        '<tbody>{weeks}</tbody></table>',
            weekHeader: '<tr>{days}</tr>',
            dayHeader: '<th>{day}</th>',
            week: '<tr>{days}</tr>',
            day: '<td>{day}</td>',
            monthSelector: '.month',
            daySelector: 'td',
            rtlClass: 'rtl',
            multiClass: 'multi',
            defaultClass: 'default',
            selectedClass: 'selected',
            highlightedClass: 'highlight',
            todayClass: 'today',
            otherMonthClass: 'other-month',
            weekendClass: 'week-end',
            commandClass: 'calendar',
            commandLinkClass: 'button',
            disabledClass: 'unavailable'
        }
    });
}

function bindPopinElements() {
//    initDatePicker();
    uploadManager.init();
    $('.content-table .button.edit').click(function() { return editPopin(this); });
    $('.content-table .button.delete').click(function() {
        if(confirm('Are you sur you want to delete this ?')) {
            var elem = $(this);
            $.ajax({
                type: "POST",
                url: $(this).attr('href'),
                success: function(data) {
                    if(data.result) {
                        $(elem).parent().parent().remove();
                        console.log('removed');
                    } else {
                        alert('An error occured while deleting, please try again');
                    }
                }
            });
        }
        return false;
    });
}

function editPopin(elem) {
    try {
        navManager.currentModal = $.modal({
            url: $(elem).attr('href'),
            useIframe: false,
            width: 1050,
            height: 600,
            complete: function(responseText, textStatus, XMLHttpRequest) {
                $('.modal-content form #block-submit button').click(function() {
                    $('.wait').show();
                    var popinForm = $(this).parents('form:first');
                    var params = $(popinForm).serialize();
                    $.ajax({
                        type: "POST",
                        data: params,
                        url: $(popinForm).attr('action'),
                        success: function(data) {
                            try {
                                var jsonObj = jQuery.parseJSON(data);
                                $('#modal .myform .err-desc').remove();
                                $('#modal .myform .error').removeClass('error');
                                if(jsonObj.is_valid == 0) {
                                    var errs = jsonObj.errors;
                                    for(k in errs) {
                                        if($('#modal .myform #' + k).length) {
                                            $('#modal .myform #' + k).addClass('error');
                                            $('#modal .myform #' + k).parent().after('<div class="err-desc">' + errs[k] + '</div>');
                                        }
                                    }
                                } else {
                                    navManager.currentModal.closeModal();
                                    $(navManager.currentFancyLink).trigger('click');
                                }
                            } catch(err) {
                                navManager.currentModal.closeModal();
                                $(navManager.currentFancyLink).trigger('click');
                            }
                        }
                    });
                    return false;
                });
            }
        });
    } catch(err) {
        alert(err);
    }
    return false;
}




  function reloadRow(id){
    $.ajax({
         type: "GET",
         url: "#",
         data: "action=getRow&id="+id,
         success: function(msg){
          var row_id='tr_'+id;
          $('#'+row_id).replaceWith(msg);
          $("body").css("cursor", "");
	  // templating
	  $("#tr_"+id).applyTemplateSetup();
         }
    }); 
     
  };

});
function openModal(type) {
                    $('.wait').show();
    switch(type){
        case 'cache':
            tmp = $('#clearcacheBtn').html();
            $('#clearcacheBtn').html('Processing..');
            $.ajax({ type: "get", url:'/gen_admin/ajax/img_cache.php', success: function(msg){
                notify(msg);
                $('#clearcacheBtn').html(tmp);
                //$("#smesg").html("<ul class='message ' style='overflow:scroll;max-height:300px'>"+msg+"<script>$('.wait').hide()</script></ul>")
            }});
        break;
        case 'url':
           tmp = $('#buildurlBtn').html();
           $('#buildurlBtn').html('Processing..');
           $.modal({
				content: ''+
						  '<ul class="simple-list with-icon">'+
						  '    <li>This operation can take few minutes, please wait during this operation !</li>'+
						  '</ul><br /><br /><center><div id="smesg"><img src="/assets/images/load.gif" /></div></center><br /><br />',
				title: 'Build URLs',
				maxwidth: 900,
				maxheight: 900,
				buttons: { 'Close': function(win) { win.closeModal(); }}
            });

            $.ajax({ type: "get", url:'/gen_admin/ajax/buildurl.php', success: function(msg){ 
                notify(msg);
                $('#buildurlBtn').html(tmp);
                //$("#smesg").html("<ul class='message ' style='overflow:scroll;max-height:300px'>"+msg+"<script>$('.wait').hide()</script></ul>")
            }});
			break;
        case 'makeurl':
            var mod=$.modal({
                content: '<br /><br />'+
                          '<ul class="simple-list with-icon">'+
                          '    <li>This operation can take few minutes, please wait during this operation !</li>'+
                          '</ul><br /><br /><center><div id="smesg"><img src="/assets/images/load.gif" /></div></center><br /><br /><script>$(".wait").hide()</script>',
                title: 'Building URLS',
                maxwidth: 900,
                buttons: { 'Close': function(win) { win.closeModal(); }}

            });

            $.ajax({ type: "get", url:'/gen_admin/ajax/makeurl.php', success: function(msg){ $("#smesg").html("<ul class='message success'><li>URL have been rewrited !</li></ul>")}});
            break;
    }
}

var uploadManager = {
    init: function() {
        if(!$('.fileupload input[type="file"]').length && !$('.fileupload input[class="fupload_readonly"]').length) return false;
        var baseparam = '?up=1';
        if(jQuery('#pkey_name').length && jQuery('#' + jQuery('#pkey_name').val()).length) {
            baseparam += '&pk=' + jQuery('#pkey_name').val() + '&' + jQuery('#pkey_name').val() + '=' + jQuery('#' + jQuery('#pkey_name').val()).val();
        }
        $('.fileupload').each(function() {
            var param = baseparam + '&field=' + jQuery(this).attr('id') + '&directory=' + jQuery(this).find('#directory_fileupload').val();
            $(this).fileupload({
                acceptFileTypes:/(\.|\/)(gif|jpe?g|png|pdf|docx?|xlsx?|pptx?|avi|mp4|mov)$/i,
                url: '/gen_admin/ajax/multi_file_upload.php' + param,
                autoUpload: true,
                fileInput: jQuery('.fileupload .fileupload-buttonbar input:file')
            });
            // Load existing files:
            $(this).addClass('fileupload-processing');
            $.ajax({
                url: $(this).fileupload('option', 'url'),
                dataType: 'json',
                context: $(this)[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), {result: result});
                if($('.fileupload input[class="fupload_readonly"]').length) {
                    $('.fileupload .template-download .btn.delete').hide();
                    $('.fileupload .template-download .toggle').hide();
                }
            });
        });
    }
}

$(".fancybox").fancybox();
$(".ifancybox").fancybox({
    'width'				: '95%',
    'height'			: '95%',
    'autoScale'     	: false,
    'transitionIn'		: 'none',
    'transitionOut'		: 'none',
    'type'				: 'iframe'
});
$(".ifancybox2").fancybox({
    'width'				: '95%',
    'height'			: '95%',
    'autoScale'     	: false,
    'transitionIn'		: 'none',
    'transitionOut'		: 'none',
    'type'				: 'iframe'
});
$('a').click(function() {
    $('.wait').show();
});

$('.massActionBt').click(function(){
    $('.wait').show();

        var data = { 'check[]' : []};
        $(".check:checked").each(function() {
          data['check[]'].push($(this).val());
        });
         $.ajax({
                     type: "POST",
                     url: "/gen_admin/ajax/massaffect.php?id_admins="+$('#massAction').val(),
                     data: data,
                     success:function(msg){
                        location.reload();
                     }
            });


});
$('.emailSendButton').click(function(){
    var pop= $.fancybox({
        width: "90%",
        height: "90%",
        autoSize: false,
        href: "/gen_admin/ajax/massmail.php?id_produits="+$(this).attr('id')+"&id_mailtype="+$(this).parent().find('select').val(),
        type: 'iframe',
        afterClose  : function() {
            window.location.reload();
        }

    });

    return false;
});

function notify(msg,type='success'){
        PNotify.defaults.delay=1500;
         if (typeof window.stackBottomRight === 'undefined') {
              window.stackBottomRight = { 'dir1': 'up', 'dir2': 'left', 'firstpos1': 25, 'firstpos2': 25 };
            }
            var opts = {
              text: msg, 
              stack: window.stackBottomRight
            };
        switch (type) {
            case 'error':
            opts.type = "error";
            break;

            case 'info':
            opts.type = "info";
            break;

            case 'success':
            opts.type = "info";
            break;
        }
       new PNotify.alert(opts);


}
