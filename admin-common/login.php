<?php
$login_bg=false;
if(!isset($_SERVER['GEOIP_CITY'])) $_SERVER['GEOIP_CITY'] = '';
if(!isset($_SERVER['GEOIP_REGION_NAME'])) $_SERVER['GEOIP_REGION_NAME'] = '';
if(!isset($_SERVER['GEOIP_COUNTRY_NAME'])) $_SERVER['GEOIP_COUNTRY_NAME'] = '';
$info = ' from IP : '.$_SERVER['REMOTE_ADDR'].',<b>';
if(isset($_GET['logout']) && isset($_SESSION['admins']) && isset($_SESSION['admins']['email'])){
    $gen->collector['backevents']->set('',array('type'=>'MSG','lib'=>'Logout : '.$_SESSION['admins']['email'].$info));
}

if(isset($_GET['logout'])){
    $_SESSION=array();
    session_destroy();
    $_COOKIE=array();
    setcookie ('autolog', '', time() + 35000 );
    header("Location: ".URL_LOC."/login.php");
}
if (isset($_SERVER['id_plateforme']) && $_SERVER['id_plateforme'] != '') {
    $plateforme = $gen->collector['plateforme']->getOne((int)$_SERVER['id_plateforme']);
} else {
    $plateforme = $gen->collector['plateforme']->getOne();
}
$use2FA = (isset($plateforme['2fa_enabled']) &&  $plateforme['2fa_enabled'] == 1);

if(isset($_COOKIE['autolog']) && !empty($_COOKIE['autolog'])){
    $auto = unserialize($_COOKIE['autolog']);
    if(!isset($auto['login']) || empty($auto['login'])){
        header("Location: /login.php?logout");
        die();
    }
    $temp= $gen->collector['admins']->get(array('email'=>$auto['login'],'is_valid'=>true));
    if(isset($temp[0]) && md5($temp[0]['password'].HASH) == $auto['hash']){
        session_start();
        $_SESSION['admins'] = $temp[0];
        if($use2FA) {
            header("Location: /login.2fa.php");
        } else {
            header("Location: /");
        }
        die();
    }
}
$error = false;
$msg   = false;
if(isset($_POST['recovery-mail'])){
    $gen->collector['admins']->passwordForget(array('email'=>$_POST['recovery-mail']));
    $msg = t("Un nouveau mot de passe vous a été envoyé si l'adresse e-mail saisie est associée à un compte"); 
}

if (isset($_POST['login']) && isset($_POST['pass'])) {
    $valid = false;
    $redirect = $use2FA ? '/login.2fa.php' : (isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : 'index.php');

    if (!isset($_POST['login']) or strlen($_POST['login']) == 0) {
        $error =t("Merci d'entrer votre login").'<script>get_captcha();</script>';
    } elseif (!isset($_POST['pass']) or strlen($_POST['pass']) == 0) {
        $error = t("Merci d'entrer votre mot de passe").'<script>get_captcha();</script>';
    } else {
        $valid = $gen->collector['admins']->get(array('email'=>$_POST['login'],'password'=>$gen->collector['admins']->encodePassword($_POST['pass'], $_POST['login']),'is_valid'=>true));
        if (!$valid) {
            $error = t("Mauvais utilisateur ou mot de passe").'<script>get_captcha();</script>';
            $gen->collector['backevents']->set('',array('type'=>'ERROR','lib'=>'Login Error : '.$_POST['login'].$info));
        } elseif($valid && $redirect == 'index.php') {
            $adminType = $valid[0]['usertype'];
            $bootstrapUrl = $gen->collector['usertypes']->getOne((int) $adminType);
            if(is_array($bootstrapUrl) && isset($bootstrapUrl['bootstrap_url'])) {
                $redirect = $bootstrapUrl['bootstrap_url'];
            }
        }
    }

    $ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) and strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

    if ($valid) {
        $logMsg = $use2FA ? 'Login first step : ' : 'Se connecter: ';
        $gen->collector['backevents']->set('',array('type'=>'MSG','lib'=>$logMsg.$_POST['login'].$info));
        @session_start();
        if($use2FA) {
            $_SESSION['prelogged_admins'] = array('email' => $valid[0]['email'], 'id_admins' => $valid[0]['id_admins']);
        } else {
            $_SESSION['admins'] = $valid[0];
        }
        if ($ajax) {
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: '.date('r', time()+(86400*365)));
            header('Content-type: application/json');
            echo json_encode(array(
                'valid' => true,
                'redirect' => $redirect
            ));
            exit();
        }
        else {
            header('Location: '.$redirect);
            exit();
        }
    }
    else {
        if ($ajax) {
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: '.date('r', time()+(86400*365)));
            header('Content-type: application/json');

            echo json_encode(array(
                'valid' => false,
                'error' => $error
            ));
            exit();
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>

	<title><?=(isset($plateforme['lib']) && !empty($plateforme['lib'])) ? $plateforme['lib'] : 'GaboX';?></title>
	<meta charset="utf-8">
    <meta name="google-site-verification" content="<?=$plateforme['content']?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="" />
	<meta name="keywords" content="">
	<meta name="author" content="" />
    <meta name="robots" content="noindex">
	<!-- Favicon icon -->
    <?php if(isset($plateforme['favicon_code']) &&  $plateforme['favicon_code']!= '') { ?>
        <?php echo $plateforme['favicon_code'] ?>
    <?php }else{ ?>
    	<link rel="icon" href="/assets/images/favicon.png" type="image/x-icon">
    <?php }?>

	<!-- vendor css -->
	<link rel="stylesheet" href="/assets/css/style.css">
</head>

<!-- [ auth-signin ] start -->
<div class="auth-wrapper" style="background-color:<?=(isset($plateforme['bgcolor_menu']) && !empty($plateforme['bgcolor_menu']))  ? $plateforme['bgcolor_menu'] : $login_bg?>">
	<div class="auth-content text-center">
		<img src="<?=(isset($plateforme['media']) && !empty($plateforme['media'])) ? $plateforme['media'] : '/assets/images/gabox-logo-w.png'?>" alt="" class="img-fluid mb-3" style="max-height:70px;">
		<div <?php if(strtoupper($plateforme['bgcolor_menu'])!='#FFFFFF') { ?> class="card" <?php } ?>>
			<div class="row align-items-center">
				<div class="col-md-12">
					<div class="card-body">
			                    <form class="form" name="login-form" id="login-form" method="post" action="login.php">
                                <h5 class="mb-3 f-w-400">Admin <?=(isset($plateforme['lib'])) ? $plateforme['lib'] : '' ?></h5>
                                                <?php if ($error){?>
                                                        <div class="alert alert-danger" role="alert"><?php echo htmlspecialchars($error); ?></div>
                                                <?php }elseif($msg){ ?>	
                                                        <div class="alert alert-success" role="alert"> <?php echo htmlspecialchars($msg); ?></div>
                                                <?php } ?>
                                                <div class="error-ajax" id="error-ajax"></div>
                                                <?php
                                                if (isset($_REQUEST['redirect'])):?>
                                                    <input type="hidden" name="redirect" id="redirect" value="<?php echo htmlspecialchars($_REQUEST['redirect']); ?>">
                                                <?php endif ?>

						<div class="input-group mb-3">
							<input type="email" class="form-control" id="login" name="login" placeholder="<?=t("Adresse e-mail")?>" value="<?php if (isset($_POST['login'])) { echo htmlspecialchars($_POST['login']); } ?>">
						</div>
						<div class="input-group mb-4">
							<input type="password" class="form-control" id="pass" name="pass" placeholder="<?=t("Mot de passse")?>">
						</div>
                        <?php $languages = $gen->collector['plateforme']->getAvailabledLanguage($_SERVER['id_plateforme']); ?>
                        <?php if(!empty($languages) && $languages){ ?>
						<div class="input-group mb-4">
                            <select name="lang" id="lang" class="form-control">
                                <?php foreach($languages as $lang){      
                                    $selected = (isset($_GET['lang']) && $_GET['lang'] == $lang['language']) ? 'selected="selected"' :'' ;
                                    if(!isset($_GET['lang']) && $lang['language']=='fr'){
                                        $selected = 'selected="selected"';
                                    }
                                ?>

                                <option value="<?=$lang['language']?>" <?=$selected?>><?=$lang['lib_language']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        
                        <?php }?>

						<button class="btn btn-block btn-primary mb-4 rounded-pill" type="submit"><?=t("Se connecter")?></button>
						<p class="mb-2 text-muted"><a href="password-reminder.php" class="f-w-400"><?=t("Mot de passe oublié ?")?></a></p>
                                            </form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<svg width="100%" height="250px" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave" d="" />
	</svg>
	<svg width="100%" height="250px" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave-two" d="" />
	</svg>
	<svg width="100%" height="250px" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave-three" d="" />
	</svg>
</div>
<!-- [ auth-signin ] end -->

<!-- Required Js -->
<script src="assets/js/vendor-all.min.js"></script>
<script src="assets/js/plugins/bootstrap.min.js"></script>
<script src="assets/js/waves.min.js"></script>
<script src="assets/js/pages/TweenMax.min.js"></script>
<script src="assets/js/pages/jquery.wavify.js"></script>
<script>

	$('#feel-the-wave').wavify({
		height: 100,
		bones: 6,
		amplitude: 90,
		color: 'rgba(0, 166, 179, 0.9)',
		speed: .15
	});
	$('#feel-the-wave-two').wavify({
		height: 70,
		bones: 5,
		amplitude: 60,
		color: 'rgba(0, 166, 179, 0.6)',
		speed: .15
	});
	$('#feel-the-wave-three').wavify({
		height: 50,
		bones: 4,
		amplitude: 50,
		color: 'rgba(82, 110, 181, .3)',
		speed: .15
	});
</script>


		<form class="form" id="password-recovery" method="post" action="#">
				<fieldset class="grey-bg no-margin collapse">
					<legend><a href="#"><?=t("Mot de passe oublié ?")?></a></legend>
					<p class="input-with-button">
						<label for="recovery-mail"><?=t("Entrez votre adresse E-mail")?></label>
						<input type="text" name="recovery-mail" id="recovery-mail" value="">
                                                <button type="submit" class="float-right"><?=t("Envoyer")?></button>
					</p>
				</fieldset>
			</form>
		</div>
        
        </div>
        <br />
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#login-form').submit(function(event) {
				event.preventDefault();
				var login = $('#login').val();
				var pass = $('#pass').val();
				if (!login || login.length == 0) {
                    $('#error-ajax').html('<div class="alert alert-danger" role="alert"><?=addslashes(t("Merci d'entrer votre email"))?></div>');
                    return false;
				} else if (!pass || pass.length == 0) {
                    $('#error-ajax').html('<div class="alert alert-danger" role="alert"><?=addslashes(t("Merci d'entrer votre mot de passe"))?></div>');
				} else {
					var target = $(this).attr('action');
                
                    if($('#lang').length){
                        if(target.indexOf($("#lang").val()+'/')==-1){
                            target = '/'+$("#lang").val()+'/'+target; 
                        }
                    }

					if (!target || target == '') {
						target = document.location.href.match(/^([^#]+)/)[1];
					}
					
					var data = {
						login: login,
						pass: pass,
					};
					var redirect = $('#redirect');
					if (redirect.length > 0) {
						data.redirect = redirect.val();
					}
					var sendTimer = new Date().getTime();
					
					$.ajax({
						url: target,
						dataType: 'json',
						type: 'POST',
						data: data,
						success: function(data, textStatus, XMLHttpRequest) {
							if (data.valid) {
								var receiveTimer = new Date().getTime();
								if (receiveTimer-sendTimer < 1000) {
									setTimeout(function() {
                                        if($('#lang').length && (data.redirect.indexOf($("#lang").val()+'/')==-1)){
									    	document.location.href =  '/'+$("#lang").val()+data.redirect;
                                        }
                                        else{
										    document.location.href = data.redirect;
                                        }
										
									}, 1000-(receiveTimer-sendTimer));
								}
								else {
                                        if($('#lang').length && (data.redirect.indexOf($("#lang").val()+'/')==-1)){
									    	document.location.href =  '/'+$("#lang").val()+data.redirect;
                                        }
                                        else{
										    document.location.href = data.redirect;
                                        }

								}
							}
							else {
                                $('#error-ajax').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');
                                return false;
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
						}
					});
                    $('#error-ajax').html('<div class="alert alert-success" role="alert"><?=t("Vérification en cours")?></div>');
				}
			});
		});
	</script>
</body>
</html>
