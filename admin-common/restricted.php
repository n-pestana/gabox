<?php
        
        $structureMenuBo = array(
                array(
                        'lib' => 'Gestion de contenu',
                        'grid' => 9,
                        'links' => array (
                                array('icon' => 'treeview',     'link' => 'treeview.php',       'lib' => 'Menus & categories'),
                                array('icon' => 'media',        'link' => 'javascript:;',       'lib' => 'Media',              'click' => "moxman.browse({view: 'list', fields: 'url'});"),
                                array('icon' => 'store',        'link' => 'pdv.php',            'lib' => 'Points of sales'),
                                array('icon' => 'news',         'link' => 'news.php',           'lib' => 'News'),
                                array('icon' => 'error',        'link' => 'pages404.php',       'lib' => '404 Pages'),
                                array('icon' => 'testimonials', 'link' => 'testimonials.php',   'lib' => 'Testimonials'),
                                array('icon' => 'mail',         'link' => 'worth.php',          'lib' => 'Mail'),
                                array('icon' => 'mailtype',     'link' => 'mailtype.php',       'lib' => 'Mailtype'),
                                array('icon' => 'page',         'link' => 'pages.php',          'lib' => 'Page')
                        )
                ),
                array(
                        'lib' => 'Utilisateur',
                        'grid' => 3,
                        'links' => array (
                                array('icon' => 'users',        'link' => 'admins.php',         'lib' => 'Users'),
                                array('icon' => 'access-event', 'link' => 'backevents.php',     'lib' => 'Access events'),
                        )
                )
        )

?>

<div class="container_12">
    <?php 
        foreach ($structureMenuBo as $section) {

            $myHtml = '<section class="grid_' . $section['grid'] . '"><div class="old-block-border"><div class="block-content"><h1>' . $section['lib'] . '</h1><ul class="shortcuts-list">';
            
            foreach ($section['links'] as $item) {
                if (isset($item['click']) && !empty($item['click'])):
                    $myHtml .= '<li><a href="' . $item['link'] . '" onclick="' . $item['click'] . '" ><i class="icon-' . $item['icon'] . '"></i><span class="lib">' . $item['lib'] . '</span></a></li>';
                else :
                    $myHtml .= '<li><a href="/gen_admin/' . $item['link'] . '"><i class="icon-' . $item['icon'] . '"></i><span class="lib">' . $item['lib'] . '</span></a></li>';
                endif;
            }

            $myHtml .= '</ul></div></div></section>';
            echo ($myHtml);
        }
    ?>
</div>
