<?php 
$hide_pass_form=false;
if(isset($_POST['password']) && isset($_POST['password_conf'])){
 $errors ='';
 if(empty($_POST['password'])||empty($_POST['password_conf']))$errors="Votre mot de passe ne doit pas être vide";
 elseif($_POST['password']!=$_POST['password_conf'])$errors="Le mot de passe et la confirmation du mot de passe ne correspondent pas";
 elseif(checkPassword($_POST['password'])!==true)$errors="Votre nouveau mot de passe doit comporter des chiffres, des lettres et doit avoir au minimum 8 caractères.";
 else{
	$gen->collector[$_SESSION['type']]->set($_SESSION[$_SESSION['type']]['id_'.$_SESSION['type']],array('pass'=>md5($_POST['password'].HASH),'has_change_pass'=>'1'));
	echo '<ul class=" message success"> <li>Votre mot de passe a été changé avec succès</li> </ul>';
	include ('panel.'.$_SESSION['type'].'.php');
	$hide_pass_form=true;
 }
}
?>
<?php if(!$hide_pass_form):?>
<div id="myform_container">
<section class="grid_6">
<div class="block-border">
<?php if(!empty($errors)): ?>
	<ul class=" message error ">
		<li><?=$errors?></li>
	</ul>
<?php endif ?>
<form id="password_change" name="pass" class="block-content form " enctype="multipart/form-data" method="post" action="#">
<h3>Ceci est votre première connexion. Afin de sécurisé votre accès, merci d'entrez un mot de passe personnel.</h3>
<br />
<p>Votre nouveau mot de passe doit comporter des chiffres, des lettres et doit avoir au minimum 8 caractères. </p>
<br /><br />
<p id="line_lib" class="colx1-right">
<label class="required">Mot de passe</label>
<input id="password" class=" full-width" type="password" style="" errormsg="Please enter the label" pattern="^.+" onclick="" maxlength="255" size="30" value="" name="password">
<br class="clear">
</p>

<p id="line_lib" class="colx1-right">
<label class="required">Confirmation de mot de passe</label>
<input id="password_conf" class=" full-width" type="password" style="" errormsg="Please enter the label" pattern="^.+" onclick="" maxlength="255" size="30" value="" name="password_conf">
<br class="clear">
</p>

<p id="line_submit" class="colx1-right">
<button  type="submit">Valider</button>
<br class="clear">
</p>
</form>
</div>
</section>
</div>
<?php endif ?>
