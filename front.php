<?php

if(!isset($_SERVER['autoDectectLanguage']))$_SERVER['autoDectectLanguage']=1;
if($_SERVER['SCRIPT_URL']=='/google-site-verification.php') {
    echo(file_get_contents(GOOGLE_VERIFICATION));
    die();
}

mb_internal_encoding("UTF-8");
session_start();

require('common/Displayer.class.php');
require('common/Cookies.class.php');

$gen->collector['displayer'] = $display = new Displayer();
$GLOBALS['gen'] = $gen;

if(!defined('ID_SITE')){
    define('ID_SITE',1);
    $site = $gen->collector['sites']->getOneCached(ID_SITE);
}

if(isset($site['internal_url']) &&  stristr($site['internal_url'],$_SERVER['SERVER_NAME'])){
   $_SERVER['prod']=0;
}
if(!isset($_SERVER['prod'])){
    $_SERVER['prod'] = 1;
}

if(isset($_SERVER['prod']) && $_SERVER['prod']==1 && $site['is_published'] == 0){
  echo '<!-- prod = 1 et is_published=0 -->';
  echo $display->fetch($conf['usrdir'].'/www/soon.php');
  die();
}


if(!defined('VIEWS_DIR'))define('VIEWS_DIR',$conf['usrdir'].'/www/views/');
if(!defined('CONTROLLER_DIR'))define('CONTROLLER_DIR',$conf['usrdir'].'/www/controller/');

if(!defined('VIEWS_BLOCKS_DIR'))define('VIEWS_BLOCKS_DIR',$conf['usrdir'].'/www/views/blocks/');
if(!defined('CONTROLLER_BLOCKS_DIR'))define('CONTROLLER_BLOCKS_DIR',$conf['usrdir'].'/www/controller/blocks/');

$display->vars['cookies']=new Cookies($_SERVER['SERVER_NAME']);
$display->cookies=$display->vars['cookies'];
$cookie =  $display->vars['cookies']->LoadConfiguration();



if(isset($_SERVER['private']) &&  $_SERVER['private']==1){
    $_SERVER['prod']=0;
}

$languages  = $gen->collector['sites']->getAvailabledLanguageForCountry($_SERVER['id_site'],$_SERVER['prod']);
if(empty($languages) && empty($_SERVER['lang'])){

  echo '<!--empty language ';
    print_r($languages);
    echo "lang:".($_SERVER['lang']);
    echo '-->';
  echo $display->fetch($conf['usrdir'].'/www/soon.php');
  die('no language defined');
}
elseif(empty($languages)){
 $cookie['current_lang_'.$_SERVER['id_site']] = $_SERVER['lang'];
}
else{
  $available_language= array();
  $preg_language = array();
  foreach($languages as $k=>$v){
    $available_language[] = current(explode('_',$v['locale'])); 
    $preg_language[] = '('.strtolower(current(explode('_',$v['locale']))).')';
  }
}

$userCollectorName = isset($_GET['c']) && isset($gen->collector[$_GET['c']]) ? $_GET['c'] : (defined('LOGIN_COLLECTOR') ? LOGIN_COLLECTOR : 'clients'); 
if(isset($_GET['logout']) && $gen->collector[$userCollectorName]->hasIdentity()) {
    # droping previous basket non-client
    if(isset($cookie['id_visitors']) && isset($cookie['id_sites'])){
    }
    $gen->collector[$userCollectorName]->clearSession();
}

$plateform = $gen->collector['plateforme']->getFirstCached();

#################### GESTION DES LOCALES LANG_COUNTRY #########################
if(isset($_GET['lang']) && $_SERVER['autoDectectLanguage']==1 ){
    # XXX tester si plateforme multilangue 
 if(in_array($_GET['lang'], $available_language)){
   $cookie['current_lang_'.$_SERVER['id_site']] = $_GET['lang'];
  }else{
    $cookie['current_lang_'.$_SERVER['id_site']] = $available_language[0];
  }
    if(isset($plateform['is_multilang']) && $plateform['is_multilang'] == 1){
         header('Location: /'.$cookie['current_lang_'.$_SERVER['id_site']]).'/' . (!empty($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING'] : '');
         die();
    }
}
elseif(isset($preg_language) && preg_match('@'.implode('|',$preg_language).'@isu',$_SERVER['SCRIPT_URL'],$preg)){
  $_SERVER['SCRIPT_URL'] = str_replace('//','/',str_replace('/'.$preg[0].'/','/',$_SERVER['SCRIPT_URL']));
  $_SERVER['SCRIPT_NAME'] = str_replace('//','/',str_replace('/'.$preg[0].'/','/',$_SERVER['SCRIPT_NAME']));
  $cookie['current_lang_'.$_SERVER['id_site']] = strtolower(current(explode('/',$preg[0])));
}

elseif($_SERVER['autoDectectLanguage']==1 ){
    
  # si pas cookie ni session, on detect la langue
  //if(!isset($cookie['current_lang']) || empty($cookie['current_lang'])) {
    # XXX add ajax detection to not dot that

    # tester si plateform multilangue 
    if(isset($plateform['is_multilang']) && $plateform['is_multilang'] == 1){
            $detected = DetectLocale();
            foreach( $detected as $locale){
              $lang=current(explode('_',$locale));
              if(in_array($lang, $available_language)){
                $cookie['current_lang_'.$_SERVER['id_site']] = $lang;
                if($_SERVER['SCRIPT_URL'] == '/') {
                    header("Status: 301 Moved Permanently", false, 301);
                    header('Location: /'.$cookie['current_lang_'.$_SERVER['id_site']].'/' . (!empty($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING'] : ''));
                    die();
                }
                break;
              }
            }
        }
}
if(empty($cookie['current_lang_'.$_SERVER['id_site']])){
  $cookie['current_lang_'.$_SERVER['id_site']]= $available_language[0]; //en
}

###les locales sont maintenant definis
$display->vars['cookies']->setLocale($cookie['current_lang_'.$_SERVER['id_site']]);
# par default : la premiere langue 
$master_locale=$gen->collector['locales']->getOneCached(array('is_master'=>1,'language'=>$cookie['current_lang_'.$_SERVER['id_site']]));
$locale=$gen->collector['sites']->getAvailabledLocalesBySiteAndLang($_SERVER['id_site'],$cookie['current_lang_'.$_SERVER['id_site']],$_SERVER['prod']);

define('ID_COUNTRY',$site['id_country']);
define('ID_LOC',$locale['id_locales']);
if(empty($master_locale['id_locales'])) $master_locale['id_locales']='0';
define('ID_LOC_MASTER',$master_locale['id_locales']);

$gen->collector['i18n']->id_locales_master =  ID_LOC_MASTER;
$gen->collector['i18n']->id_locales = ID_LOC;
$loc = $gen->collector['locales']->getCached(ID_LOC);
define('LOC',$loc['locale']);

# set the local the translations module
if(isset($gen->collector['translations'])){
    $gen->collector['translations']->setCurrentLocale(ID_LOC);
}

$display->conf['sites']=$site;

/* Gestion du theme enfant */
$display->www_themes= (isset($display->conf['sites']['www_themes'])) ? $display->conf['sites']['www_themes'] : null ;
/**/

# XXX passer par les sites options
define('DATE_FORMAT','jj/mm/aaaa');

$logVisitors = (!defined('LOG_VISITORS') || (defined('LOG_VISITORS') && LOG_VISITORS === TRUE));
if($logVisitors && isset($_SERVER['GEOIP_LONGITUDE'])) {
 
    $visitors=array(
        'remote_addr' => getRealUserIp()
        ,'user_agent' => $_SERVER['HTTP_USER_AGENT']
        ,'continent_code' => (isset($_SERVER['GEOIP_CONTINENT_CODE'])) ? $_SERVER['GEOIP_CONTINENT_CODE'] : ''
        ,'continent_code' => (isset($_SERVER['GEOIP_CONTINENT_CODE'])) ? $_SERVER['GEOIP_CONTINENT_CODE'] :''
        ,'country_code' => (isset($_SERVER['GEOIP_COUNTRY_CODE'])) ? $_SERVER['GEOIP_COUNTRY_CODE'] :''
        ,'country_name' => (isset($_SERVER['GEOIP_COUNTRY_NAME'])) ? $_SERVER['GEOIP_COUNTRY_NAME'] : ''
        ,'latitude' => (isset($_SERVER['GEOIP_LATITUDE'])) ? $_SERVER['GEOIP_LATITUDE'] : 0
        ,'longitude' => (isset($_SERVER['GEOIP_LONGITUDE'])) ? $_SERVER['GEOIP_LONGITUDE'] : 0
        ,'id_sites' => $_SERVER['id_site']
    );

    $temp=$display->vars['cookies']->getCookie();

    if(isset($temp) && isset($temp['id_visitors']) && !empty($temp['id_visitors'])){ 
        $visitors['id_visitors']=$temp['id_visitors'];
        $visitors['updated_at']='';
        $gen->collector['visitors']->set($visitors['id_visitors'],$visitors);

        if(isset($temp['filters'])) {
            $visitors['filters'] = $temp['filters'];
        }
        //error_log('RECOGNIZED VISITOR:'); 
    } else {
        $visitors['id_visitors']=$gen->collector['visitors']->set('',$visitors);
        //error_log('NEW VISITOR:'); 
    }

    define('ID_VISITOR',$visitors['id_visitors']);
    $cookie_set =$display->vars['cookies']->setCookie($visitors);
}

if(!defined('IS_BOT')){
    define('IS_BOT', false);
}

if($logVisitors && array_key_exists('visited_pages',$gen->collector)) {
    # si un visiteur alors on logue les requests
    if(!empty($visitors['id_visitors'])){
        $display->vars['id_visitors'] = $visitors['id_visitors'];
        if(!strstr($_SERVER['REQUEST_URI'],'img.php'))
            $gen->collector['visited_pages']->set('',array('id_visitors'=>$visitors['id_visitors'],'url'=>$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']));
    }
}

$lang = current(explode('_',$cookie['current_lang_'.$_SERVER['id_site']]));
$temp = explode('_',$cookie['current_lang_'.$_SERVER['id_site']]);
$pays= end($temp);
define('COUNTRY',$pays);

if(isset($plateform['is_multilang']) && $plateform['is_multilang'] == 1){
    define('URL_LOC','/'.strtolower(str_replace('_','/',$cookie['current_lang_'.$_SERVER['id_site']])));
} else {
    define('URL_LOC','');
}

// 301 redirections management if the module is enabled
if (isset($gen->collector['redirections'])) {
    $redirection = $gen->collector['redirections']->getOne([
        'origin' => [$_SERVER['REQUEST_URI'], substr($_SERVER['REQUEST_URI'], strlen(URL_LOC))],
        'enabled' => 1,
        'id_sites' => ID_SITE,
    ]);
    if ($redirection !== false && !empty($redirection['destination'])) {
        header("HTTP/1.1 301 Moved Permanently",true,301);
        header("Location: " . $redirection['destination']);
        die;
    }
}

    #GESTION DES POST {{{
    $useFormToken = defined('USE_FORM_TOKEN') && USE_FORM_TOKEN ===true;
    if(isset($_POST) && !empty($_POST) && isset($_POST['formname']) && !empty($_POST['formname']) && (false === $useFormToken || ($useFormToken && $_SESSION['ftoken'] == $_POST['ftoken']))) {
        if(isset($_POST['first_name']) && !empty($_POST['first_name'])) {
            // Honey pot
        } else {

            unset($_POST['first_name']);
            $post = $fields = array();
            $post['name']= filter_var($_POST['formname'], FILTER_SANITIZE_STRING);
            unset($_POST['formname']);
            foreach($_POST as $k=>$v){
                $fields[$k] = $v;//filter_var($v, FILTER_SANITIZE_STRING);
            }
            $post['ip'] = getRealUserIp(); 
            $post['agent'] = $_SERVER['HTTP_USER_AGENT'];
            $post['localisation'] = (isset($_SERVER['GEOIP_COUNTRY_NAME'])) ? $_SERVER['GEOIP_COUNTRY_NAME'] : '';
            if(isset($_SERVER['GEOIP_CITY'])) $post['localisation'].=', '.$_SERVER['GEOIP_CITY'];
            $isValid = true;
            if(method_exists($gen->collector['posted_data'], 'isValid')) {
                $isValid = $gen->collector['posted_data']->isValid($_POST);
            }
            if($isValid) {
                $inserted_id = $gen->collector['posted_data']->setPost($post,$fields,ID_SITE);

                if($inserted_id>0 && $inserted_id!=false) {
                    # pas fun de le define
                    define('FORM_RESULT_'.strtoupper($post['name']),true);
                    define('FORM_SAVED_ID', $inserted_id);
                } else {
                    define('FORM_RESULT_'.strtoupper($post['name']),false);
                }

                if(isset($_FILES) && !empty($_FILES)) {
                    $path = realpath($_SERVER['DOCUMENT_ROOT'].'../').'/upload/';
                    @mkdir($path.'/'.$post['name'].'/');
                    // Si POST d'un champ _o_name, on va essayer de récupérer les champs d'upload configurés dans la classe qui porte ce nom pour avoir les extensions valides
                    if(isset($_POST['_o_name'])) {
                        if(is_file(GABOX_USR_DIR . '/classes/' . $_POST['_o_name'] . '.class.php')) {
                            require_once(GABOX_USR_DIR . '/classes/' . $_POST['_o_name'] . '.class.php');
                            $oClass = ucfirst($_POST['_o_name']);
                            $cls = $oClass::getInstance();
                            if(method_exists($cls, 'getUploadedFields')) {
                                $extAcceptByFields = $cls->getUploadedFields();
                            }
                        }
                    }
                    $ext_accept = array('pdf','PDF','JPG','JPEG','jpeg','jpg','doc','DOC','DOCX','docx','od','odd','xls','xlsx','bmp','gif','GIF','png','PNG');
                    foreach($_FILES as $field=>$file) {
                        if(empty($file['name']))continue;
                        $path = realpath($_SERVER['DOCUMENT_ROOT'].'../').'/upload/';
                        @mkdir($path.'/'.$post['name'].'/'.$field);
                        $path = $path.'/'.$post['name'].'/'.$field.'/';
                        $refExtentions = isset($extAcceptByFields[$field]['ext']) ? $extAcceptByFields[$field]['ext'] : $ext_accept;
                        $partsNameFile = explode('.',$file['name']);
                        $extention = end($partsNameFile);

                        if ( !in_array($extention, $refExtentions) ) {
                            define('HAS_ERROR_'.strtoupper($field), $field.': Upload format file not allowed');
                        }
                        else{
                            if ( ! move_uploaded_file($file['tmp_name'], $path.$field.'_'.$inserted_id.'.'.$extention)) {
                                define('HAS_ERROR_'.strtoupper($field), $field.': Unable to upload');
                            } else {
                                define('FILE_PATH_'.strtoupper($field), $path.$field.'_'.$inserted_id.'.'.$extention);
                            }
                        }
                    }
                }
            } else {
                define('FORM_RESULT_'.strtoupper($post['name']), false);
            }
        }
    }
# }}}fin gestion des posts



# get page 
$display->collector = $gen->collector;

if($gen->collector['treeview']->use_site_id ===false && ($temp=$gen->collector['treeview_url']->getOneCached( array('id_site'=>ID_SITE,'id_locale'=>ID_LOC  ,'url' =>$_SERVER['SCRIPT_URL'])))!==false){ 
    if(!isset($temp['lib'])) {
        if(isset($site['lib'])){
            $temp['lib'] = $site['lib'];
        }
        else{
            $temp['lib'] = 'GaboX';
        }
    }
    $controller = $gen->collector['treeview']->getController($temp['id_treeview']);
    $current_treeview =$temp['id_treeview'];

    $display->vars['meta']['title']=(!empty($temp['meta_title'])) ? $temp['meta_title'] : $temp['lib']; 
    $display->vars['meta']['desc']=(!empty($temp['meta_desc'])) ? $temp['meta_desc'] : $temp['lib']; 

    $display->setVar('controller', $controller);
    $display->setVar('id_treeview',$temp['id_treeview']);
    $display->setVar('treeview',$gen->collector['treeview']->getOneI18n(array('id_treeview'=>$temp['id_treeview'])));
}

if ($display->www_themes && (file_exists(GABOX_USR_DIR.'/'.$display->www_themes.'/common.php'))) {
    include (GABOX_USR_DIR.'/'.$display->www_themes.'/common.php');
} elseif (file_exists(GABOX_USR_DIR.'/www/common.php')) {
    include(GABOX_USR_DIR.'/www/common.php');
}

$requestedUrl = $_SERVER['REQUEST_URI'];
if (URL_LOC !== '' && str_starts_with($_SERVER['REQUEST_URI'], URL_LOC)) {
    $requestedUrl = strtok(ltrim(ltrim($_SERVER['REQUEST_URI'], '/'), ltrim(URL_LOC, '/')), '?');
}
if ($_SERVER['SERVER_NAME'] === '/'
    || $_SERVER['SERVER_NAME'] === ''
    || $requestedUrl === '/'
) {
    $requestedUrl = '/index.php';
}

define('HIDE_DISABLED_SITE',true);

$temp = preg_replace(array('|^/|','|/$|','|//|'),'',$_SERVER['SCRIPT_URL']);
$request_token= (!empty($temp)) ? explode('/',$temp) : '';

if(!empty($request_token)){
    $country = $request_token[0];
    if(isset($request_token[1])) $lang = $request_token[1];
}

$page = '';
$checkValidated = true;
$checkSaveChanges = isset($gen->collector['pages']) && method_exists($gen->collector['pages'], 'checkSaveChanges') && $gen->collector['pages']->checkSaveChanges();
// Page preview
$clean_url = explode('?',$_SERVER['REQUEST_URI']);
$clean_url = strtolower($clean_url[0]);
if($clean_url[0] == '/')$clean_url=preg_replace('/\//','',$clean_url);

if(isset($_GET['hash']) && isset($_GET['pagepkey']) && $_GET['hash'] == md5(HASH) && $_GET['pagepkey'] == md5(PAGE_PREVIEW_KEY)) {
    $pagei18 = $gen->collector['i18n']->getOne(array('id_locales'=>ID_LOC,'module'=>'pages','field_name'=>'url','id_element'=>(int) $_GET['pid']));
    $checkValidated = false;
} else {
    if($site['is_i18n'] ===1){
        $pagei18 = $gen->collector['i18n']->getOne(array('id_locales'=>ID_LOC,'is_published'=>1,'module'=>'pages','field_name'=>'url','lib'=>$clean_url));


        if(empty($pagei18)) {
            $params = array('id_site' => ID_SITE, 'is_published' => 1, 'url' => $clean_url );
            if($checkSaveChanges && $checkValidated) {
                $params[$gen->collector['pages']->getValidationField()] = '1';
            }
            $page = $gen->collector['pages']->getOne($params);
            if($page) {
                $elements = $gen->collector['pages']->getPagesElements($page['id_pages'],ID_LOC);
                $testi18 = $gen->collector['i18n']->getOne(array('id_locales'=>ID_LOC,'is_published'=>1,'module'=>'pages','field_name'=>'url','id_element'=>$page['id_pages']));
                if($testi18) {
                    $display->vars['meta']['page_canonical'] = $testi18['lib'];
                }
            }
        }
    }  elseif (isset($gen->collector['pages'])&& method_exists($gen->collector['pages'], 'getOne')) {
        $page = $gen->collector['pages']->getOne(array('id_site'=>ID_SITE,'is_published'=>1,'url'=>$clean_url));
    }
}

if(!empty($pagei18)) {
    $pageParams = array('id_pages'=>$pagei18['id_element'],'id_site'=>ID_SITE);
    if($checkSaveChanges && $checkValidated) {
        $pageParams[$gen->collector['pages']->getValidationField()] = '1';
    }
        
    $page = $gen->collector['pages']->getOneI18n($pageParams);
    if($page){
        $elements = $gen->collector['pages']->getPagesElements($page['id_pages'],ID_LOC);
    }
}

// Si on match une url dynamique
if (!is_null($matchedTreeview = $gen->collector['treeview']->matchDynamicUrl()) && file_exists(CONTROLLER_DIR . $matchedTreeview['controller'])) {
    foreach (['title', 'desc'] as $field) {
        if (!empty($matchedTreeview['meta_' . $field])) {
            $display->vars['meta'][$field] = $matchedTreeview['meta_' . $field];
        }
    }
    // Variables définies en fonction de la correspondance à la partie dynamique de l'URL
    $display->vars['matchedVars'] = $matchedTreeview['matchedVars'];

    $display->content = $display->fetch(CONTROLLER_DIR . $matchedTreeview['controller']);
} elseif(!empty($page) && ($checkValidated === false || $page['is_published']==1)) {

    $pages_options = $gen->collector['pages_options']->getOne(array('id_site'=>ID_SITE));
    $templates_tag = array('{$lib}'=>$page['lib'],'{$media}'=>$page['media'],'{$content}'=>$page['content']);
    # si c'est par template:
    if($page['id_templates']) {
        $tpl = $gen->collector['templates']->getOneCached($page['id_templates']);
        $display->content = str_replace(array_keys($templates_tag), array_values($templates_tag),$tpl['content']);
        $display->content = $gen->collector['pages']->CallBackModule($display->content);
    } else {
        $display->content = $page['content'];
    }
    define('IS_CMS',true);
    $display->vars['meta']['title']=(!empty($page['meta_title'])) ? $page['meta_title'] : $page['lib'];
    $display->vars['meta']['desc']=(!empty($page['meta_desc'])) ? $page['meta_desc'] : $page['lib'];
    $display->vars['meta']['canonical']=str_replace(URL_LOC, '', strtolower(current(explode("?",$_SERVER['REQUEST_URI']))));

    if(isset($display->vars['gtm'])&&isset($page['gtm_lib'])){
        $display->vars['gtm']['page'] =  $page['gtm_lib'];
    }

    // Gestion des hreflang
    if(!defined('IS_MULTIBRAND') || (defined('IS_MULTIBRAND') && IS_MULTIBRAND === true))
        $languages  = $gen->collector['sites']->getAvailabledLanguageForCountry($_SERVER['id_site'],$_SERVER['prod']);
    else
        $languages  = $gen->collector['sites']->getAvailabledLanguageForCountry('',$_SERVER['prod']);
    $in = array();
    $loc = array();
    foreach($languages as $lang){
        if($lang['id_locales'] == ID_LOC) continue;
        $loc[$lang['id_locales']]=$lang;
        $in[]=$lang['id_locales'];
    }
    $pageUrls = $gen->collector['i18n']->get(array('module' => 'pages', 'id_element' => $page['id_pages'], 'id_locales' => $in, 'field_name' => 'url', '!=lib' => ''));
    if(is_array($pageUrls)) {
        $out = '';
        foreach($pageUrls as $k=>$v) {
            if(!isset($v['id_locales']))continue;
            if(!defined('IS_MULTIBRAND') || (defined('IS_MULTIBRAND') && IS_MULTIBRAND === true))
                $out .= "\n".'<link rel="alternate" href="/'.$loc[$v['id_locales']]['language'].$v['lib'].'" hreflang="'.strtolower(str_replace('_','-',$loc[$v['id_locales']]['locale'])).'" />';
            else {
                $siteLoc = $gen->collector['sites_locales']->getOneCached(array('id_locales' => $v['id_locales']));
                $temp = $gen->collector['sites']->getOneCached($siteLoc['id_sites']);
                if($temp['is_published'] != 1) continue;
                $out .= "\n".'<link rel="alternate" href="'.$temp['prod_url'].'/'.$loc[$v['id_locales']]['language'].str_replace(URL_LOC, '', $v['lib']).'" hreflang="'.strtolower(str_replace('_','-',$loc[$v['id_locales']]['locale'])).'" />';
            }
        }
        $display->vars['meta']['hreflang'] = $out;
    }

}
# si on view un draft du module pagesbuilder à partir du bo #XXX
elseif(isset($gen->collector['pagesbuilder']) && isset($_GET['id_pagesbuilder'])){
   $draft = $gen->collector['pagesbuilder']->getLastDraft((int)$_GET['id_pagesbuilder']);
   $display->content = $draft['content'] ;
} elseif (isset($gen->collector['pagesbuilder_i18n']) && isset($_GET['id_pagesbuilder_i18n'])) {
   $draft = $gen->collector['pagesbuilder_i18n']->getLastDraft((int)$_GET['id_pagesbuilder_i18n']);
   $display->content = $draft['content'] ;
}
# si controller existant, si fichier physique
elseif(

	(file_exists($conf['usrdir'].'/www/controller'.$requestedUrl) &&  is_file($conf['usrdir'].'/www/controller'.$requestedUrl))
	||(is_link($conf['usrdir'].'/www/controller'.$requestedUrl) )
	){

  $display->content = $display->fetch($conf['usrdir'].'/www/controller'.$requestedUrl);
}

# si page existante, si fichier physique
elseif(
	(file_exists($conf['usrdir'].'/www'.$requestedUrl) &&  is_file($conf['usrdir'].'/www'.$requestedUrl))
	||(is_link($conf['usrdir'].'/www'.$requestedUrl) )
	){

  $display->content = $display->fetch($conf['usrdir'].'/www'.$requestedUrl);
}
elseif(
	(file_exists($conf['usrdir'].'/www'.$requestedUrl.'/index.php') &&  is_file($conf['usrdir'].'/www'.$requestedUrl.'/index.php'))
	||(is_link($conf['usrdir'].'/www'.$requestedUrl.'/index.php') )
	){

        $display->content = $display->fetch($conf['usrdir'].'/www'.$requestedUrl.'/index.php');
}
# test si produit
elseif(array_key_exists('produits_url', $gen->collector) && $gen->collector['treeview']->use_site_id ===false && ($temp=$gen->collector['produits_url']->getOne( array( 'id_locale'=>ID_LOC  ,'url' => $_SERVER['SCRIPT_URL'])))!==false){ 
   $controller = 'controller/'.str_replace('listing_','', $gen->collector['treeview']->getController($temp['id_treeview']));
    $display->setVar('id_produits',$temp['id_produits']);
    $display->setVar('controller', $controller );
    $display->setVar('id_treeview', $temp['id_treeview']);
    $display->setVar('url', $temp['url']);
    $display->content = $display->fetch($conf['usrdir'].'/www/'.$controller);
}


# pareil qu'au dessus, nouveau raccourci si on est en mode héritage (pour une grappe de site)
# test si catégorie
elseif($gen->collector['treeview']->use_site_id ===false && ($temp=$gen->collector['treeview_url']->getOneCached( array('id_site'=>ID_SITE,'id_locale'=>ID_LOC  ,'url' =>$_SERVER['SCRIPT_URL'])))!==false){

   //print_r($gen->collector['treeview']->get($temp['id_treeview']));
    //die();
    $controller = $gen->collector['treeview']->getController($temp['id_treeview']);
    $current_treeview =$temp['id_treeview'];
    $display->setVar('id_treeview',$temp['id_treeview']);

    /* hreflang automatiques basés sur le treeview */
    if(is_array($languages) && count($languages)>1) {
        foreach($languages as $lang) {
            $treeviewUrls = $gen->collector['treeview_url']->get(array('id_treeview' => $temp['id_treeview'], 'id_site' => ID_SITE, 'id_locale' => $lang['id_locales']));
            foreach($treeviewUrls as $k=>$v) {
                if(!array_key_exists('meta',$display->vars)) $display->vars['meta'] = array();
                if(!array_key_exists('hreflang',$display->vars['meta'])) $display->vars['meta']['hreflang'] = '';
                $display->vars['meta']['hreflang'] .= "\n".'<link rel="alternate" href="/' . $lang['language'] . $v['url'].'" hreflang="'.strtolower(str_replace('_','-',$lang['locale'])).'" />';
            }
        }
    }

    /* pour le cas d'un controller avec un ou plusieurs paramètres get */
    $paramsController = strstr($controller, '?');
    if($paramsController) {
        $paramsController = substr($paramsController, 1);
        $controller = strstr($controller, '?', true);
        $params = explode('&', $paramsController);
        foreach($params as $p) {
            $cont = explode('=',$p);
            $_GET[$cont[0]] = $cont[1];
        }
    }

    $display->setVar('controller', $controller);

/*    if($display->www_themes && (file_exists(GABOX_USR_DIR.'/'.$display->www_themes.'/common.php'))) {
        include (GABOX_USR_DIR.'/'.$display->www_themes.'/common.php');
    }
    elseif(file_exists(GABOX_USR_DIR.'/www/common.php')) {
        include(GABOX_USR_DIR.'/www/common.php');
    }*/

    if(file_exists($conf['usrdir'].'/www/controller/'.$controller)){
        $display->content = $display->fetch($conf['usrdir'].'/www/controller/'.$controller);
    }
    else{
        $display->content = $display->fetch($conf['usrdir'].'/www/'.$controller);
    }
} else {
    $classesByTrait = $gen->getClassesByTrait();
    $loaded = false;
    if(array_key_exists('urls', $classesByTrait)) {

        foreach($classesByTrait['urls'] as $className) {
            if(array_key_exists($className . '_url', $gen->collector) && $gen->collector['treeview']->use_site_id === false && ($temp=$gen->collector[$className . '_url']->getOne(array( 'id_locale'=>ID_LOC  ,'url' => $_SERVER['SCRIPT_URL'])))!==false) {
                $controller = str_replace('listing_', '', $gen->collector['treeview']->getController($temp['id_treeview']));
                $automaticFilteringEnabled = $gen->collector[$className]->isAutomaticFilteringEnabled();
                if ($automaticFilteringEnabled) {
                    $gen->collector[$className]->setAutomaticFilteringEnabled(false);
                }
                $object = $gen->collector[$className]->getOneI18n($temp[$gen->collector[$className]->primary_key]);
                $gen->collector[$className]->setAutomaticFilteringEnabled($automaticFilteringEnabled);
                if ((!isset($object['is_published']) && !isset($object['enabled']))
                    || (isset($object['is_published']) && $object['is_published'] == 1)
                    || (isset($object['enabled']) && $object['enabled'] == 1)
                ) {
                    if (method_exists($className, 'preprocess')) {
                        $object = $gen->collector[$className]->preprocess($object);
                    }
                    $display->setVar($className, $object);
                    // Si pas de controller défini dans le treeview, on regarde si un controller du type collector_type.php ou collector.php existe
                    if (empty($controller)) {
                        if (in_array('type', array_keys($gen->collector[$className]->array_fields))) {
                            $controller = $className . '_' . $object['type'] . '.php';
                        }
                        if (!file_exists(GABOX_USR_DIR . '/www/controller/' . $controller)) {
                            $controller = $className . '.php';
                        }
                        if (!file_exists(GABOX_USR_DIR . '/www/controller/' . $controller)) {
                            $controller = NULL;
                        }
                    }
                } else {
                    // If a controller is defined in the treeview but the object is unpublished, reset the controller
                    $controller = NULL;
                }
                if (!empty($controller)) {
                    $treeviewField = $gen->collector[$className]->getTreeviewField();
                    $display->setVar($gen->collector[$className]->primary_key, $temp[$gen->collector[$className]->primary_key]);
                    $display->setVar('controller', $controller);
                    if (!empty($treeviewField)) $display->setVar($treeviewField, $temp['id_treeview']);
                    $display->setVar('url', $temp['url']);
                    if (file_exists($conf['usrdir'] . '/www/controller/' . $controller)) {
                        $display->content = $display->fetch($conf['usrdir'] . '/www/controller/' . $controller);
                    } else {
                        $display->content = $display->fetch($conf['usrdir'] . '/www/' . $controller);
                    }
                    $loaded = true;
                }
            }
            # sinon est ce que cette url existe dans l'historique des url ?
            elseif(array_key_exists($className . '_url', $gen->collector) 
                && $gen->collector['treeview']->use_site_id === false 
                && isset($gen->collector[$className . '_url_history'])
                && ($temp=$gen->collector[$className . '_url_history']->getOne(array( 'id_locale'=>ID_LOC  ,'url' => $_SERVER['SCRIPT_URL'])))!==false) {
                # trouvé dans l'historique, on redige vers l'element trouvé si il existe toujours
                $exist  = $gen->collector[$className.'_url']->getOne(array(
                    'id_locale'=>$temp['id_locale']
                   ,'is_canonical'=> 1 
                   ,$gen->collector[$className]->primary_key => $temp[$gen->collector[$className]->primary_key]
                ));
                if(is_array($exist) && isset($exist['url'])){
                    header("HTTP/1.1 301 Moved Permanently",true,301);	
                    header("Location: ".$exist['url']);
                    die();
                }
            }
        }
    }

    // On recherche les classes qui implémentent l'interface URLToAction et on lance leur méthode launchCallableByURL
    foreach (
        array_filter(
            get_declared_classes(),
            function ($className) {
                return in_array('Gabox\Interfaces\URLToAction', class_implements($className));
            }
        ) as $className
    ) {
        if (array_key_exists($className, $gen->collector)) {
            $gen->collector[$className]->launchCallableByURL($_SERVER['SCRIPT_URL']);
        }
    }

    if($loaded === false) {
        if($gen->collector['treeview']->use_site_id ===false) {
            $temp = $gen->collector['treeview_url']->getOneCached( array( 'id_site'=>ID_SITE,'id_locale'=>ID_LOC  ,'url' =>dirname($_SERVER['SCRIPT_URL'])));
        }
        $cond = array('id_site'=>ID_SITE,'id_locale'=>ID_LOC  ,'url' =>$_SERVER['SCRIPT_URL']);
        
        if(isset($temp['url']) && $temp['url']!='/') {
            $controller = $gen->collector['treeview']->getController($temp['id_treeview']);
            $display->setVar('id_treeview',$temp['id_treeview']);
            $display->setVar('controller', $controller);
            if(file_exists($conf['usrdir'].'/www/controller/'.$controller)){
                $display->content = $display->fetch($conf['usrdir'].'/www/controller/'.$controller);
            }
            else{
                $display->content = $display->fetch($conf['usrdir'].'/www/'.$controller);
            }

        } elseif(isset($gen->collector['produits'])) {
            // gestion des erreur 404, les admins peuvent créer des 301 
            $gen->collector['produits']->use_treeview = true;

            $el = $gen->collector['produits']->getElementByUrl($_SERVER['SCRIPT_URL']);

            if(!empty($el) && isset($el[0]['idp'])) {

                if($gen->collector['treeview']->use_site_id ===false) {
                    $elIsValid = $gen->collector['i18n']->getStatusByID_SITE(ID_SITE, $el[0]['idp']);
                }
                else{
                    $elIsValid = $gen->collector['i18n']->getStatusByID_SITE(ID_SITE, $el[0]['idp']);
                }
                if(current($elIsValid) == 0) {
                    $el = null;
                }

            }
            if(!empty($el)){
                if($gen->collector['produits']->use_treeview == true){
                    # si c'est un produit
                    if( (isset($el['id_produits'])) ) {
                    #XXX specifique LBC ???
                        if($el['status'] == 'ok' ) {
                            $temp = $gen->collector['produits_url']->getOneCached(
                                    array(
                                        'id_locale'=>ID_LOC
                                        ,'id_site'=>ID_SITE
                                        ,'url' => $_SERVER['SCRIPT_URL']
                                        )
                                    );
                            $treeview = $gen->collector['treeview']->getOneCached($temp['id_treeview']);	
                            $controller = 'produit.php';
                        } else {
                            header('Location: ' . URL_LOC . "/");
                            die;
                        }
                    }
                    # si c'est une catégorie
                    else{
                        $treeview = $el;
                        $controller = $gen->collector['treeview']->getController($treeview['id_treeview']);
                    }

                    $display->setVar('produits',(isset($el['id_produits'])) ? $el : false);
                    # retocompat
                    $display->setVar('id_produits',(isset($el['id_produits'])) ? $el['id_produits'] : false);
                    $display->setVar('treeview',$treeview);

                    $display->setVar('controller', $controller );
                    $display->content = $display->fetch($conf['usrdir'].'/www/'.$controller);
                }
                else{
                    $cur_gamme=$el[0];
                    $id_menus = (empty($cur_gamme['ml4'])) ?
                        (empty($cur_gamme['ml3'])) ?
                        (empty($cur_gamme['ml2'])) ?
                        (empty($cur_gamme['ml1'])) ? ''

                        : $cur_gamme['ml1']
                        : $cur_gamme['ml2']
                        : $cur_gamme['ml3']
                        : '';
                    $menu = $gen->collector['menus_published']->getCached(array('id_menus_published'=>$id_menus));
                    $menu = $menu[0];
                    $display->setVar('menu',$menu);
                    $display->setVar('gamme',$el);

                    if(!empty($menu['controller']) && (!isset($el[0]['idp']) || (isset($el[0]['idp']) && empty($el[0]['idp'])))) {
                        $display->setVar('controller', $menu['controller']);
                        $display->content = $display->fetch($conf['usrdir'].'/www/'.$menu['controller']);
                    }
                    elseif(isset($el[0]['idp']) && !empty($el[0]['idp'])){
                        $display->setVar('id_produits',$el[0]['idp']);
                        $display->content = $display->fetch($conf['usrdir'].'/www/produit.php');
                    }
                    else{
                        $display->content = $display->fetch($conf['usrdir'].'/www/gammev2.php');
                    }
                }

            }
            else{
                $p404 = $gen->collector['pages404']->getOne(array('lib'=>$_SERVER['REQUEST_URI'],'id_site'=>ID_SITE));
                if(!empty($p404) && isset($p404['destination']) && !empty($p404['destination'])){
                    switch($p404['http_code']):

                    case '301':
                        header("HTTP/1.1 301 Moved Permanently",true,301);	
                        header("Location: ".$p404['destination']);
                        break;

                    case '302':
                        header("HTTP/1.1 302 Found",true,302); 
                        header("Location: ".$p404['destination']);
                        die();

                    case '404':
                        header("HTTP/1.0 404 Not Found",true,404); 
                        $content = $display->fetch($conf['usrdir'].'/www/404.php'); // Fetch content before header/footer for $this->vars use
                        die($display->fetch($conf['usrdir'].'/www/header.php').$content.$display->fetch($conf['usrdir'].'/www/footer.php'));

                    endswitch;
                } else {
                    if($site['use_auto_404'] == 1){
                        $is_page = true;
                        if($is_page)    {
                            $temp =preg_split('/[\/]/',str_replace('.html','',$_SERVER['REQUEST_URI']));
                            //$sql = "SELECT url,id_produits_url,id_produits,url, MATCH (url) AGAINST ( '".str_replace('-',' ',end($temp))."' IN NATURAL LANGUAGE MODE ) AS score FROM gen_produits_url where id_locale in (".ID_LOC.",".ID_LOC_MASTER.") having score > 1  order by score desc limit 1";
                            $sql = "SELECT url,id_search_info,element_id,url, MATCH (url) AGAINST ( '".str_replace('-',' ',end($temp))."' IN NATURAL LANGUAGE MODE ) AS score FROM gen_search_info where id_locale in (".ID_LOC.",".ID_LOC_MASTER.") having score > 1 order by score desc limit 1";

                            $temp = $gen->collector['pages404']->Query($sql);
                            $url = empty($gen->fetchField($temp, 'url')) ? false : $gen->fetchField($temp, 'url');
                        }
                        else{
                            $temp = str_replace(URL_LOC,'',$_SERVER['REQUEST_URI']);
                            $temp =preg_split('/[\/]/',str_replace('.html','',$temp));
                            $sql = "SELECT url,id_treeview_url,id_treeview,url, MATCH (url) AGAINST ( '".implode(' ',$temp)."' IN NATURAL LANGUAGE MODE ) AS score FROM gen_treeview_url where id_locale in (".ID_LOC.",".ID_LOC_MASTER.") having score > 1  order by score desc limit 1";
                            $temp = $gen->collector['pages404']->Query($sql);
                            $url = empty($gen->fetchField($temp, 'url')) ? false : $gen->fetchField($temp, 'url');
                        } 
                        if($url){
                            header("HTTP/1.1 301 Moved Permanently",true,301);
                            header("Location: ".URL_LOC.$url);
                            die();
                        }
                        else{
                            header("HTTP/1.0 404 Not Found",true,404); 
                            $content = $display->fetch($conf['usrdir'].'/www/404.php'); // Fetch content before header/footer for $this->vars use
                            die($display->fetch($conf['usrdir'].'/www/header.php').$content.$display->fetch($conf['usrdir'].'/www/footer.php'));
                        }
                    }
                    else{
                        $current = $gen->collector['pages404']->getOne(array('lib'=>$_SERVER['REQUEST_URI'],'id_site'=>ID_SITE));
                        if($current){
                            $gen->collector['pages404']->set($current['id_pages404'],array('lib'=>$_SERVER['REQUEST_URI'],'id_site'=>ID_SITE,'http_code'=>'404','status'=>'NEW'));
                        }
                        else{
                            $gen->collector['pages404']->set(array('lib'=>$_SERVER['REQUEST_URI'],'id_site'=>ID_SITE,'http_code'=>'404','status'=>'NEW'));
                        }
                        header("HTTP/1.0 404 Not Found",true,404); 
                        $content = $display->fetch($conf['usrdir'].'/www/404.php'); // Fetch content before header/footer for $this->vars use
                        die($display->fetch($conf['usrdir'].'/www/header.php').$content.$display->fetch($conf['usrdir'].'/www/footer.php'));
                        $display->content ='<div id="main"><h1><div class="container"><div class="boxes info-boxes"><div class="info-row"><h2>'.("Page not found !")."</h2></div></div></div></h1></div>";
                    }
                }

            }
        } elseif (!isset($_SERVER['phpunit']) || $_SERVER['phpunit'] !== 1) {
            // On ne passe pas ici pour les tests unitaires !
            $current = $gen->collector['pages404']->getOne(array('lib'=>$_SERVER['REQUEST_URI'],'id_site'=>ID_SITE));
                        if($current){
                            $gen->collector['pages404']->set($current['id_pages404'],array('lib'=>$_SERVER['REQUEST_URI'],'id_site'=>ID_SITE,'http_code'=>'404','status'=>'NEW', 'destination' => ''));
                        }
                        else{
                            $gen->collector['pages404']->set(array('lib'=>$_SERVER['REQUEST_URI'],'id_site'=>ID_SITE,'http_code'=>'404','status'=>'NEW', 'destination' => ''));
                        }
                        header("HTTP/1.0 404 Not Found",true,404); 
                        $content = $display->fetch($conf['usrdir'].'/www/404.php'); // Fetch content before header/footer for $this->vars use
                        die($display->fetch($conf['usrdir'].'/www/header.php').$content.$display->fetch($conf['usrdir'].'/www/footer.php'));

        }
    }
	
}

if(isset($display->vars['id_treeview']) && (int) $display->vars['id_treeview'] != 0 && ! array_key_exists('meta', $display->vars)) {
    $treeview = $gen->collector['treeview']->getOneI18n((int) $display->vars['id_treeview']);
    if(is_array($treeview) && count($treeview) > 0) {
        $display->setVar('meta', array('title' => ((!empty($treeview['meta_title'])) ? $treeview['meta_title'] : $treeview['lib']), 'desc' => ((!empty($treeview['meta_desc'])) ? $treeview['meta_desc'] : $treeview['description'])));
    }
}

if(!strstr($_SERVER['SCRIPT_URL'],'static') && (!isset($_SERVER['phpunit']) || $_SERVER['phpunit'] !== 1)){
    if($display->header!==false && empty($display->header)) $display->header = $display->fetch($conf['usrdir'].'/www/header.php');
    if($display->footer!==false && empty($display->footer)) $display->footer = $display->fetch($conf['usrdir'].'/www/footer.php');
}

if(defined('FRONT_APPEND_FILES')) {
    $frontFiles = explode('|', FRONT_APPEND_FILES);
    foreach($frontFiles as $file) {
        $display->content .= $display->fetch($conf['usrdir'].'/www' . $file);
    }
}

# callback function
if(file_exists($conf['usrdir'].'/etc/content_callback.php') &&  is_file($conf['usrdir'].'/etc/content_callback.php')){
	include($conf['usrdir'].'/etc/content_callback.php');
	$display->setCallBackFunction('content_callback');
}

// DISPLAY_OUTPUT sert à désactiver l'output pour l'autoload phpunit
if (!defined('DISPLAY_OUTPUT') || (defined('DISPLAY_OUTPUT') && DISPLAY_OUTPUT !== 'no')) {
    $display->display();
}

// On ne stoppe pas l'exécution pour les tests unitaires !
if (!isset($_SERVER['phpunit']) || $_SERVER['phpunit'] !== 1) {
    exit(1);
}

