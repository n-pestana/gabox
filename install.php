<?php $file = GABOX_USR_DIR.'/etc/db-config.php'; ?> 
<?php if(isset($_POST) && !empty($_POST)){
file_put_contents($file,"<?php
\$host       = '".((empty($_POST['host'])) ? 'localhost' : $_POST['host'])."';
\$username   = '".$_POST['username']."';
\$password   = '".$_POST['password']."';
\$dbname     = '".$_POST['dbname']."';
\$charset    = 'utf8';
\$type       = 'mysqli';
define('PWD_HASH','".$_POST['pwd_hash']."');
");
#    echo "<br />Information de connexion enregistrée dans :";
#    echo "<pre>".$file."</pre>";
    $temp['host']   = ((empty($_POST['host'])) ? 'localhost' : $_POST['host']); 
    $temp['username']   = $_POST['username'];
    $temp['password']   = $_POST['password'];
    $temp['dbname']     = (isset($_SERVER['DB_NAME'])) ? $_SERVER['DB_NAME'] : $_POST['dbname'];
    $temp['charset']    = 'utf8';
    $temp['type']    = 'mysqli';
    unset($host,$username,$password,$dbname,$charset,$type);
    require_once(LIBS.'adodb5.20.9/adodb.inc.php');
    require_once(LIBS.'adodb5.20.9/adodb-pager.inc.php');
    $obj = array();
    $obj['db'] = ADONewConnection( (empty($temp['type'])) ? 'mysqli' : $temp['type'] );
    $test = @$obj['db']->Connect( $temp['host'] , $temp['username'] , $temp['password'] , $temp['dbname'],$temp['charset']);
}
if(!isset($test) || $test===false){?>
<!DOCTYPE html>
<html lang="en">
<head>

    <title><?=(isset($plateforme['lib']) && !empty($plateforme['lib'])) ? $plateforme['lib'] : 'GaboX';?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="" />
    <!-- Favicon icon -->
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <!-- vendor css -->
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

<div class="auth-wrapper" style="background-color:<?=(isset($plateforme['bgcolor_menu']) && !empty($plateforme['bgcolor_menu']))  ? $plateforme['bgcolor_menu'] : '#242e3e'?>">
	<div class="auth-content text-center">
		<img src="<?=(isset($plateforme['media']) && !empty($plateforme['media'])) ? $plateforme['media'] : '/assets/images/gabox-logo-w.png'?>" alt="" class="img-fluid mb-3">
		<div class="card">
			<div class="row align-items-center">
				<div class="col-md-12">
					<div class="card-body">
			            <form class="form" name="config-form" id="config-form" method="post" action="#">
						    <h1 class="h4 mb-4 f-w-400">Gabox Installation</h1>

                            
                            <?php if(isset($test) && $test===false){?>
                                <div style="alert alert-danger" role="alert">Error de connexion de test à la base de données : <?=  $obj['db']->_errorMsg?></div>
                            <?php } ?>

                            <fieldset class="input-group mb-4">
                                <h2 class="h5">Data Base</h2>

                                <div class="input-group mb-3">
                                    <input type="text" name="host" value="<?=$_POST['host'] ?? '' ?>" placeholder="host (localhost by default)" class="form-control" />
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-database"></i></span>
                                    </div>
                                    <input type="text" name="dbname" value="<?=$_POST['dbname'] ?? '' ?>" placeholder="BDD name (compulsory)" class="form-control" required />
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-users-cog"></i></span>
                                    </div>
                                    <input type="text" name="username" value="<?=$_POST['username'] ?? '' ?>" placeholder="username (compulsory)" class="form-control" required />
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-shield"></i></span>
                                    </div>
                                    <input type="password" name="password" value="" placeholder="Password (compulsory)" class="form-control" required />
                                </div>

                                <div class="input-group mb-3">
                                    <input type="text" name="pwd_hash" value="<?=$_POST['pwd_hash'] ?? '' ?>" placeholder="Hash key for password encryption" class="form-control" />
                                </div>
                            </fieldset>

                            <fieldset class="input-group mb-4">
                                <h2 class="h5">Project</h2>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-project-diagram"></i></span>
                                    </div>
                                    <input type="text" name="project-name" value="<?=$_POST['project-name'] ?? '' ?>" placeholder="Project Directory (compulsory)" class="form-control" required />
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">@</span>
                                    </div>
                                    <input type="text" name="bo-user" value="<?=$_POST['bo-user'] ?? '' ?>" placeholder="Admin E-mail (compulsory)" class="form-control" required />
                                </div>

                                <div class="input-group mb-3">
                                    <input type="password" name="bo-password" value="" placeholder="Admin password (generated if null)" class="form-control" />
                                </div>
                            </fieldset>
                            <fieldset class="custom-control custom-switch mb-4">
                                <input type="checkbox" class="custom-control-input" name="demo" id="demo">
                                <label class="custom-control-label" for="demo">Install demo website</label>
                            </fieldset>
                            <input type="hidden" name="GaboXInstall" value="1"/>
						    <button class="btn btn-block btn-primary mb-4 rounded-pill" type="submit">Go</button>
                        </form>
					</div>
				</div>
			</div>
		</div>
		
	</div>

	<svg width="100%" height="250px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave" d="" />
	</svg>
	<svg width="100%" height="250px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave-two" d="" />
	</svg>
	<svg width="100%" height="250px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave bg-wave">
		<title>Wave</title>
		<defs></defs>
		<path id="feel-the-wave-three" d="" />
	</svg>
</div>


<!-- Required Js -->
<script src="assets/js/vendor-all.min.js"></script>
<script src="assets/js/plugins/bootstrap.min.js"></script>
<script src="assets/js/waves.min.js"></script>
<script src="assets/js/pages/TweenMax.min.js"></script>
<script src="assets/js/pages/jquery.wavify.js"></script>
<script>
	$('#feel-the-wave').wavify({
		height: 100,
		bones: 3,
		amplitude: 90,
		color: 'rgba(22, 163, 221, 4)',
		speed: .25
	});
	$('#feel-the-wave-two').wavify({
		height: 70,
		bones: 5,
		amplitude: 60,
		color: 'rgba(82, 110, 181, .3)',
		speed: .35
	});
	$('#feel-the-wave-three').wavify({
		height: 50,
		bones: 4,
		amplitude: 50,
		color: 'rgba(216, 35, 145, .5)',
		speed: .45
	});
</script>
<body>
</html>
<?php }elseif(isset($test)&&$test===true){
        $sqls = explode("\n",file_get_contents(INC.'sql/start.sql'));
        foreach($sqls as $sql){
            if(trim($sql)=='')continue;
            $obj['db']->Query($sql);
        }
    $url ='/?build='.(( isset($_SERVER['buildPwd'])) ? $_SERVER['buildPwd'] : '@bde24rs38d!');
    header("Location: ".$url);
    die();
} 

