<?php
    $gen->collector=$gen->buildClass();
    $gen->collector=$gen->loadClassFromDb();


    $obj['db']->Execute("DELETE FROM gen_modules;");

    $sql_versionning_struct = false;
    if(file_exists(INC.'common/sql_versionning.php')){
       include(INC.'common/sql_versionning.php'); 
       $sql_versionning_struct = true;
    }
    if(file_exists(GABOX_USR_DIR.'/etc/sql_versionning.php')){
       include(GABOX_USR_DIR.'/etc/sql_versionning.php'); 
       $sql_versionning_struct = true;
    }

    foreach (array_keys($gen->collector) as $module_k=>$module_name){
        if(isset($sql_versionning_struct) && ($sql_versionning_struct == true)){
            if(isset($sql_versionning[$module_name]))
                foreach($sql_versionning[$module_name] as $field_name=>$field_struc){
                    if( !in_array($field_name,$gen->collector[$module_name]->db->metaColumnNames('gen_'.$module_name))){
                        #$gen->collector[$module_name]->db->Execute($field_struc); 
                        $obj['db']->Execute($field_struc);
                    }
                }
        }
        if($module_name =='form')continue;
        if($module_name =='mailer')continue;
        $gen->collector['modules']->set(crc32($module_name),array('lib'=>$module_name,'columns_name'=>json_encode($gen->collector['modules']->db->metaColumnNames('gen_'.$module_name)), 'is_enabled' => 1));
        if(is_object($gen->collector)){
            $gen->collector[$module_name]->collector=$gen->collector;
        }
    }
    if(isset($sql_versionning['create'])) {
        foreach( $sql_versionning['create'] as $k=>$v) {
            $begin = strpos($v, 'gen_');
            $obj['db']->Execute($v);

            if($begin !== false && stripos($v, 'CREATE TABLE') !== false) {
                $m = trim(str_replace(array('`', 'gen_'), '', substr($v, $begin, (strpos($v, ' (') - $begin))));
                if (!in_array($m, array_keys($gen->collector))) {
                    $gen->collector['modules']->set(crc32($m), array('lib' => $m, 'columns_name' => json_encode($gen->collector['modules']->db->metaColumnNames('gen_' . $m))));
                }
            }
        }
    }
    $gen->collector=$gen->buildClass();
    $gen->collector=$gen->loadClassFromDb();
