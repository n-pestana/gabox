<?php
if (!isset($_SESSION)) {
    ini_set('session.gc_maxlifetime', 36000);
    ini_set('session.cookie_lifetime', 36000);
    ini_set('session.cache_expire', 36000);
    session_start();
}

$gen->RunSQLVersioning();
 ?>
<?php $restrictions = array();?>
<?php error_reporting(E_ALL);?>
<?php if($_SERVER['PHP_SELF'] != '/login.php'): 

# n-pestana ajout pour retrocompat cassé .... 
if(isset($_POST['change_id_site']) && $_POST['change_id_site']==0){

	unset($_SESSION['change_id_site']);
	unset($_SESSION['current_site']);

	unset($_POST['change_id_site']);
	unset($_POST['current_site']);

}
# fin n-pestana

// Fix pour ne pas que la page de login soit cassée si on passe par l'index directement
$whiteList = array(
    '/css/mini.php'
    ,'/js/mini.php'
);
if(isset($_SESSION['admins']) && !isset($_SESSION['admins']['id_admins']) && (count($_SESSION['admins']) >1) && !in_array($_SERVER['SCRIPT_NAME'], $whiteList)){
    header("Location: /login.php?logout=1");
    die();
}

// Set log events if defined in config.php
if(defined('LOG_BO_ACTIONS') && LOG_BO_ACTIONS === true) {
    foreach(array_keys($gen->collector) as $module_k => $module_name) {
        if($gen->collector[$module_name] instanceof GenClass) {
            $gen->collector[$module_name]->setLogEvents(true);
        }
    }
}

if(isset($_SESSION['admins']) && isset($_SESSION['admins']['id_admins'])){
    $_SESSION['admins']=$gen->collector['admins']->getOne($_SESSION['admins']['id_admins']);
    if(isset($_SESSION['current_site']) && isset($gen->collector['admins_sites'])) {
        $as = $gen->collector['admins_sites']->getOne(array('id_admins' => $_SESSION['admins']['id_admins'], 'id_sites' => $_SESSION['current_site']));
        $userType = $gen->getSiteCollector('usertypes')->getOne((int) $as['id_usertypes']);
        $_SESSION['admins']['usertype'] = $userType;
    }

    if(isset($_SESSION['admins']['id_locales']) && $_SESSION['admins']['id_locales']!=0 && $_SESSION['admins']['id_sites'] != 0) {
        //define('ID_LOC',$_SESSION['admins']['id_locales']);
        //define('ID_SITE',$_SESSION['admins']['id_sites']);
        $idLoc = $_SESSION['admins']['id_locales'];
        $idSite = $_SESSION['admins']['id_sites'];
        $restrictions = array(
          'locales' => $gen->collector['sites']->getAvailabledIdLocalesForSite($idSite, $only_enabled = false)
          ,'pref_loc' => $_SESSION['admins']['id_locales']
          ,'id_sites' => $_SESSION['admins']['id_sites']
        );
    }
}
if(isset($_SESSION['current_site']) && !isset($_POST['change_id_site'])) {
    //define('CURRENT_SITE', $_SESSION['current_site']);
    $currentSite = $_SESSION['current_site'];
    $site = $gen->collector['sites']->getOne($currentSite);
    //if(!defined('ID_SITE')) define('ID_SITE',$site['id_sites']);
    //define('CODE_SITE', $site['internal_code']);
    $idSite = $site['id_sites'];

//    $codeSite = $site['internal_code']; # 
    # n-pestana : correction
     $codeSite = (isset($site['internal_code'])) ? $site['internal_code'] : '';

}
if(isset($_POST['change_id_site']) && $_POST['change_id_site'] == '') {
    unset($_SESSION['change_id_site']);
    unset($_POST['change_id_site']);
    
}
if(isset($_POST['change_id_site']) ||( isset($_SESSION['change_id_site']) && $_SESSION['change_id_site']!='')){
    if(!isset($_SESSION['admins'])) {  
        header("Location: /login.php?logout=1");
        die();
    } 
    if(!empty($_SESSION['admins']['id_sites']) && $_POST['change_id_site'] != $_SESSION['admins']['id_sites']) {
        header("Location: /");
        die();
    }
    if(isset($_POST['change_id_site']) && !empty($_SESSION['admins']['id_site']) && $_POST['change_id_site'] != $_SESSION['admins']['id_site']) {
        unset($_SESSION['change_id_site']);
        unset($_POST['change_id_site']);
    }
    if(isset($_POST['change_id_site'])) {
        $_SESSION['change_id_site'] = $_POST['change_id_site'];
        $_SESSION['current_site'] = $_POST['change_id_site'];
    }
    //if(!defined('ID_SITE')) define('ID_SITE', $_SESSION['change_id_site']);
    $idSite = $_SESSION['change_id_site'];
    $restrictions = array(
         'locales' => $gen->collector['sites']->getAvailabledIdLocalesForSite($idSite, $only_enabled = false)
        ,'pref_loc' => $_SESSION['admins']['id_locales']
        ,'id_sites' => $_SESSION['admins']['id_sites']
    );
    //define('ID_LOC',$_SESSION['admins']['id_locales']);
    $idLoc = $_SESSION['admins']['id_locales'];
}

if(isset($idLoc)) { define('ID_LOC', $idLoc); }
if(isset($idSite)) {
    if(!defined('ID_SITE')) define('ID_SITE', $idSite);
    if(!isset($codeSite)) {
        $site = $gen->collector['sites']->getOne(ID_SITE);
	if(isset($site['internal_code'])) $codeSite = $site['internal_code'];
    }
}
if(isset($codeSite)) { define('CODE_SITE', $codeSite); }
if(isset($currentSite)) { define('CURRENT_SITE', $currentSite); }

?>
<?php $_SESSION['is_logged']=true?>
<?php 
endif ;
if(is_file(GABOX_USR_DIR . '/classes/myForm'.((isset($_SERVER['myFormVersion'])) ? $_SERVER['myFormVersion'] : '').'.class.php')) {
    include_once(GABOX_USR_DIR . '/classes/myForm'.((isset($_SERVER['myFormVersion'])) ? $_SERVER['myFormVersion'] : '').'.class.php');
} elseif(is_file(INC.'myForm'.((isset($_SERVER['myFormVersion'])) ? $_SERVER['myFormVersion'] : '').'.class.php')) {
    include_once(INC.'myForm'.((isset($_SERVER['myFormVersion'])) ? $_SERVER['myFormVersion'] : '').'.class.php');
}
$form = new myFormV1();
$form->id_locales_only = (isset($_SESSION['admins']['id_locales'])) ? $_SESSION['admins']['id_locales'] : false;
$form->conf_file = defined('CODE_SITE') && is_file(GABOX_USR_DIR.'/etc/' . CODE_SITE . '.form_fields.php') ? GABOX_USR_DIR.'/etc/' . CODE_SITE . '.form_fields.php' : GABOX_USR_DIR.'/etc/form_fields.php';
$form->form_options['class'] = 'block-content form';
$form->restrictions = $restrictions; 
$form->collector = &$gen->collector;
$gen->collector['form'] = $form;

$gen->versionning = true;
if(isset($self_callback)) $form->self_callback=$self_callback;
if(isset($_SESSION['admins']) && isset($_SESSION['admins']['id_admins'])) $gen->collector['admins']->id=$_SESSION['admins']['id_admins'];

define('MUST_CHOOSE_SITE','<div class="txt-center"><h2>You MUST choose a website for editing free content by selecting the drop down menu on the top right corner</h2><br /><br /><img src="/assets/images/B4xxrW4.png" /></div>');
define('ID_LOC_MASTER',4); # if not defined : default is French

define('GABOX_BACK_ROOT',$_SERVER['DOCUMENT_ROOT']);
define('GABOX_GEN_ROOT',$_SERVER['DOCUMENT_ROOT'].'/gen_admin/');
define('AJAX_ROOT',$_SERVER['DOCUMENT_ROOT'].'/gen_admin/ajax/');

$gen->collector['treeview']->getAffectedTreeviews();

# si monoplateforme, pour spécifier la plateforme, ajouter dans la conf apache : SetEnv id_plateforme N
if(!isset($_SERVER['id_plateforme'])){
    $temp = $gen->collector['plateforme']->getFirstCached();
    $_SERVER['id_plateforme'] = $temp['id_plateforme'];
}

# si lang rewrite dans apache sous la forme : RewriteRule ^/?(..)/(.*)$ /$2?lang=$1 [L,QSA] 
if(isset($_GET['lang'])){
    $language = $gen->collector['plateforme']->getAvailabledLanguage($_SERVER['id_plateforme'],$_GET['lang']);
    if(isset($language['id_locales'])){
        define('ID_COUNTRY',$language['id_country']);
        define('ID_LOC',$language['id_locales']);
        define('URL_LOC','/'.$language['language']);
        $gen->collector['i18n']->id_locales_master =  ID_LOC_MASTER;
        $gen->collector['i18n']->id_locales = ID_LOC;
        $gen->collector['translations']->setCurrentLocale(ID_LOC);
    }
}

if(!defined('URL_LOC')){
    define('URL_LOC','');
}

// Gestion des droits sur les modules
if(isset($_SERVER['REQUEST_URI'])){
    if (strpos($_SERVER['REQUEST_URI'], '/modules/') !== false
        && preg_match('#.*/modules/(?:usr/.*?/)?(.*?)/.*#i', $_SERVER['REQUEST_URI'], $matches)
        && isset($_SESSION['admins']['id_admins'])
    ) {
        if (!$gen->collector['admins']->can('edit', $matches[1])) {
            $_SESSION['notifications'] = [
                'error' => t("Vous n'avez pas le droit d'accéder à cette page"),
            ];
            $bootstrapUrl = $gen->collector['usertypes']->getOne((int) $_SESSION['admins']['usertype']);
            if (is_array($bootstrapUrl) && isset($bootstrapUrl['bootstrap_url'])
                && strpos($bootstrapUrl['bootstrap_url'], '/modules/') !== false
                && preg_match('#.*/modules/(?:usr/.*?/)?(.*?)/.*#i', $bootstrapUrl['bootstrap_url'], $matches)
                && $gen->collector['admins']->can('edit', $matches[1])
            ) {
                header("Location: " . $bootstrapUrl['bootstrap_url']);
                die;
            }
            header("Location: /");
            die;
        }
    }
}

if(file_exists(GABOX_USR_DIR.'/admin/common.php')) {
   include (GABOX_USR_DIR.'/admin/common.php');
}
