<?php
# TODO : mettre en place la validation des checkbox


class myFormV1  {

    public array $gen_field;
  var $id_locales_only=0;
  var $view_only = false;
  var $restrictions = array();
  var $id          	= '';
  var $etc_path     	= 'etc/';
  var $fields_ref   	= '';
  var $fields           = '';
  var $langue       	= 'fr';
  var $form_options 	= array(
      'name'     => ''
      ,'action'   => '#'
      ,'method'   => 'post'
      ,'enctype'  => 'multipart/form-data'
      ,'class'=>'block-content form'
  );
  var $default_values_locale = '';

  var $exclude    	= array();
  var $addAjaxSpan	= FALSE;
  var $from_groupe 	= FALSE;
  var $checkNoProfil= FALSE;

  var $obj;
  var $collector;
  var $conf_file;
  var $self_callback = false;
  var $onlyFields = FALSE;
  # XXX rendre administrable
  var $module_can_be_fully_edited_by_country_admin = array();
  var $auto_translate_fields=array();
  var $lib= '';
  var $action= '';
  var $heading= '';
  var $save_only = false;
  var $field_struc = array(
    'type'
    ,'id'
    ,'label'
    ,'pattern'
    ,'errorMsg'
    ,'maxlength'
    ,'size'
    ,'value'
    ,'arrayValues'
    ,'cols'
    ,'rows'
    ,'onclick'
    ,'onchange'
    ,'onsubmit'
    ,'class'
    ,'style'
    ,'optionStyle'
    ,'placeholder'
    ,'tooltip'
  );
  var $current_id = '';
  /**
   * Check if linked collectors exists for "relationel_list" type
   *
   * @access protected
   * @var boolean
   */
  protected $_checkLinkedCollector = true;

  public function getPerms() {
      return $this->module_can_be_fully_edited_by_country_admin;
  }
  
  public function setPerms($array){
      $this->module_can_be_fully_edited_by_country_admin = $array;
  }

  public function getConfig($formName) 
  {
      // KM: Réactivé pour l'utilisation de fonctionnalités en BO type translator
      require($this->conf_file);
      $ref = $$formName; 
      if(!is_array($ref)) $this->error(_("Get Config : no form reference found:".$formName));
      return $ref;
  }

  function get($id, array $values, $form_name, $selectFields = null, $idToEdit = array(),$legend = ''){
    include(ADMIN.'/common-fields.php');

    if($this->fields != ''){
        $ref = $this->fields;
    }
    else{
        include($this->conf_file);
        $ref = $$form_name; 
    }

    if(!is_array($ref))  {
        $this->error(_(" Get : no form reference found:".$ref));
    }
    $excludeTypesCheck = array ('button' , 'submit');
        $head = $foot = $out = '';
        $forced_id_site = '';
        
        if( ! $this->onlyFields ) {
          $head.='<form 
            action   ="'.str_replace('.edit.php','.php',$this->form_options['action']).'"
            method   ="'.$this->form_options['method'].'"
            id       ="'.$this->form_options['id'].'"
            enctype  ="'.$this->form_options['enctype'].'"
            class    ="'.$this->form_options['class'].(defined('BO_FORM_CLASS') ? ' ' . BO_FORM_CLASS : '') .'"
            >';

        }
        $head.='<div class="form-group ">';
        if(!empty($this->lib)){
          $head.='<h3>'.$this->lib.'</h3>';
        }
        else{ # retrocompat
            if(!empty($legend)){
              $head.='<h1>'.$legend.'</h1>';
            }
        }

        if(!empty($this->heading)){
          $head.='<h5>'.$this->heading.'</h5>';
        }

        
        /* mise en tab*/ 
        $tabs = array();
        $meta_nav = '<div class="text-right edition-button-bar col-md-12">';
            
        if(!isset($_GET['ajax'])){
            $meta_nav .= '<a href="'.str_replace('.edit.php','.php',$this->form_options['action']).'" class="btn btn-primary"><i class="fa-solid fa-backward" aria-hidden="true"></i> '.t("Retour").'</a>';
        }
        $meta_nav .= '<button type="submit" onclick="" class=" btn btn-primary"><i class="feather mr-2 icon-check-circle"></i> '.t("Enregistrer").'</button></div>';


        if( ! $this->onlyFields ) {
            $out = $meta_nav;
            if(isset($_GET['ajax'])){
                # horizontal tabs
                $out .= '<ul  class="nav nav-tabs mb-3" id="tabEdit" role="tablist">';
            }
            else{
                # vertical tabs
                $out .= '<ul  class="nav nav-tabs mb-3" id="tabEdit2" role="tablist">';
                $out .= '<ul  class="nav flex-column nav-pills " style="width:220px" id="tabEdit" role="tablist" aria-orientation="vertical">';
            }


            $i=0;
            foreach($ref as $f_id=>$f_lib){
                # si c'est un label pour tab
                if(isset($f_id[0]) && $f_id[0]=='_'){
                    $tabs[] = $f_lib;
                    $out .= '<li class="nav-item"><a class="nav-link '.(($i==0 && !isset($_GET['tab']) ? 'active' : ((isset($_GET['tab']) && $_GET['tab'] == $f_id) ? 'active' : ''))).' text-uppercase" id="'.$f_id.'-tab" data-toggle="tab" href="#'.$f_id.'" role="tab" aria-controls="'.$f_id.'" aria-selected="true">'.$f_lib.'</a></li>';
                    $i++;
                }
            }
            
            $out .= '</ul>';

            $out .= '<div class="tab-content col-md-'.(!isset($_GET['ajax']) ? '9' : '12').'" id="tabEditContent" style="min-height:400px !important">';
        }

        $i = 0; 

        foreach($ref as $f_id=>$field_option){


            if( ! $this->onlyFields ) {
    
                # gestion des tab
                if(isset($f_id[0]) && $f_id[0]=='_' && $i==0){
                    $out .= '<div class="tab-pane fade show '.(($i==0 && !isset($_GET['tab']) ? 'active' : ((isset($_GET['tab']) && $_GET['tab'] == $f_id) ? 'active' : ''))).' " id="'.$f_id.'"  role="tabpanel" aria-labelledby="'.$f_id.'-tab">';
                }
                if(isset($f_id[0]) && $f_id[0]=='_' && $i>0){
                    $out .= '</div><div class="tab-pane fade show '.(($i==0 && !isset($_GET['tab']) ? 'active' : ((isset($_GET['tab']) && $_GET['tab'] == $f_id) ? 'active' : ''))).'" id="'.$f_id.'" role="tabpanel" aria-labelledby="'.$f_id.'-tab">';
                }
            }
            if(!is_array($field_option)) continue ;
          //  $f_id = trim($f_id);
           // if(!isset($ref[$f_id])) continue;
            if( isset($field_option['disabled']) && ($field_option['disabled'] === true)) continue ;
            # initialisation pour eviter de faire des isset
            foreach($this->field_struc as $k_struc){
               //if(!array_key_exists ($k_struc,$field_option)) $field_option[$k_struc] = '';
               if(!isset($field_option[$k_struc])) $field_option[$k_struc] = '';
            }
            if(!isset($values[$f_id])) $values[$f_id] = '';

            # on test si multilangue pour le libele du champ:
            $field_option['label']    = (is_array($field_option['label'])) ? $field_option['label'][$this->langue] : $field_option['label'];
            $field_option['value']    = (is_array($field_option['value'])) ? $field_option['value'][$this->langue] : $field_option['value'];
            $field_option['errorMsg'] = (is_array($field_option['errorMsg'])) ? $field_option['errorMsg'][$this->langue] : $field_option['errorMsg'];
            $value = '';
            
            # si le module n'est pas full administrable pour un site slave et qu'il faut les droits master 
            // Première condition = si je suis admin multisite, j'ai les droits
            if($_SESSION['admins']['usertype'] != 1 && !(defined('ID_SITE') && !in_array($form_name,$this->module_can_be_fully_edited_by_country_admin)  && (!strstr($field_option['type'],'i18n')) && (!strstr($field_option['type'],'submit')) && (!strstr($field_option['type'],'hidden')) && $field_option['type'] != 'generated')) {
//                die('modification not allowed');
 //               continue;
            } 
            elseif(defined('ID_SITE') && empty($forced_id_site)){
              $forced_id_site .= ID_SITE;
            }
            if (isset($field_option['kv']) && $field_option['kv'] && strpos($values[$f_id], '||') !== false) {
                $values[$f_id] = str_replace('||', ', ', $values[$f_id]);
                $ref[$f_id]['type'] = 'info';
            }
    
            if( $field_option['type'] == 'key_value_store') { 
                    $out .= ''; 
            }
            else{
                $out .= '<div class="block-line m-4 form-group row num'.$i.' '.$field_option['type'].'" id="block-'.$f_id.'" >';
            }

            $hasPattern = isset($field_option['pattern']) && $field_option['pattern'] != '';

            $class = $hasPattern ? 'class="required formlabel col-md-3"' : 'class="formlabel col-md-3"';

            if(!empty($field_option['tooltip'])){
                $tooltip = '&nbsp;<a href="#" class="btn btn-sm  btn-light btn-tooltip-old gaboxTooltip"><i class="fa fa-circle-info"></i></a><p class="tooltip">'.htmlentities(str_replace(array('<br>','<br/>','<br />'),"\n",$field_option['tooltip'])).'</p>';
               $field_option['label']= ''.$field_option['label'].$tooltip.'';
            }
            $label = (isset($field_option['label']) && $field_option['label'] != ''   && $field_option['type'] != 'hidden')
            ? "<label ".$class.">".$field_option['label']."</label>" 
            : "";

            $out .= $label;
            if(strstr($field_option['placeholder'],'$')){
                $field_option['placeholder'] =  $values[str_replace('$','',$field_option['placeholder'])];
            }
            # affichage du champ
            foreach($this->field_struc as $k_struc){
               //if(!array_key_exists ($k_struc,$field_option)) $field_option[$k_struc] = '';
               if(!isset($ref[$f_id][$k_struc])) $ref[$f_id][$k_struc] = '';
            }
            $out .= $this->getFieldHtml($ref, $f_id, $label, $id, $values, $form_name, $idToEdit);




            if(   $field_option['type'] != 'key_value_store') { 
                $out .= '</div>'; //fermeture du form-group
            }

            $i++;
    }

    if(!empty($tabs)){
        $out .='</div>'; // fin tab-panel
    }

    if($this->onlyFields ) return $out;

    $out .='</div>'; // fin tab-content 

    $key = array_keys($idToEdit);
    if(! empty($idToEdit)) {
        $foot.= '<input type="hidden" id="'.$key[0].'" name="'.$key[0].'" value="'.$idToEdit[$key[0]].'"/>';
        $foot.= '<input type="hidden" id="pkey_name" value="'.$key[0].'"/>';
    }
    $head.='</div>';
    $out .= $meta_nav;
    $foot.= '<input type="hidden" id="i18n_show_translation" value="'.t("Voir les traductions").'"/>';
    $foot.= '<input type="hidden" id="i18n_hide_translation" value="'.t("Cacher les traductions").'"/>';
    $foot.= '</form>';
    return $head.$out.$foot;
  }

  function warning($lib){
    global $buildPwd;
    $out = '<div class="alert alert-danger">'.$lib."<br />";
    $out .= t("Vous pouvez relancer un build pour essayer de corriger ce problème");
    $out .= '<br /><br /><a class="btn btn-primary" href="/?build='.$buildPwd.'">'.t("Build").'</a></div>';
    echo $out;

  }

  function error($lib){
    die($lib); 
  }

  
 function savePostedDataByForm($form_name, $class, $form_fields = array()) {  
    $this->save_only = true;
    return  $this->createForm($form_name, $class, $form_fields);
  }



  function displayForm($form_name, $class, $form_fields = array()) {  
    $this->save_only = false;
    return $this->createForm($form_name, $class, $form_fields);
  }


  /*  createForm  */
  function createForm($form_name, $class, $form_fields = array(), $title='', $legend = null, $confirmMsg = null, $addAjaxSpan = false) {
    global $obj, $conf;
        if(empty($form_fields) && is_array($this->fields)) {
            $this->collector['form']->addKvFields($form_name);
            $form_fields = array_keys($this->fields);
        }else{

            if(file_exists(MODS.$class.'/admin-fields.php')){
                $form= $this;
                global $gen;
                include(MODS.$class.'/admin-fields.php');
                # affichage auto des champs KV
                $this->collector['form']->addKvFields($form_name);
                $form_fields = array_keys($this->fields);
            }
            elseif(file_exists(USR_MODS.$class.'/admin-fields.php' && false)){
                $form= $this;
                global $gen;
                include(USR_MODS.$class.'/admin-fields.php');
                # affichage auto des champs KV
                $this->collector['form']->addKvFields($form_name);
                $form_fields = array_keys($this->fields);
            }

            else{
                $form_fields = $obj[$class]->array_fields;
            }
        }

    if( ! isset($obj[$class]) ) {
      $this->error();
    }
    $this->form_options = array(
        'id'     => $obj[$class]->table_name
        ,'action'   => $_SERVER['PHP_SELF'] . (isset($_GET['redirParams']) ? '?'.urldecode($_GET['redirParams']) : '')
        ,'method'   => 'post'
        ,'enctype'  => 'multipart/form-data'
        ,'class'    => 'block-content form myform' 
    );
    $this->addAjaxSpan = $addAjaxSpan;

    $current_id = $i18n_filters = array() ;

    $current_id[$obj[$class]->primary_key] = (isset($_REQUEST[$obj[$class]->primary_key])) ? $_REQUEST[$obj[$class]->primary_key] : '';


    if(isset($_REQUEST[$obj[$class]->table_name])) {

      $tmp = $current_id[$obj[$class]->primary_key];

      if(empty($current_id[$obj[$class]->primary_key]) && isset($_REQUEST[$obj[$class]->table_name][$obj[$class]->primary_key]))
        $current_id[$obj[$class]->primary_key] = $_REQUEST[$obj[$class]->table_name][$obj[$class]->primary_key];

      # type list
      $list_fields = array();
      $list_fields_order = array();
      $files_fields = array();

      foreach($_REQUEST[$obj[$class]->table_name] as $request_key=>$request_value){
          if(!strstr($request_key,'_list') && !strstr($request_key,'_filelist')) continue;
          if (strstr($request_key,'_list_order')) {
              if (!isset($_REQUEST[$obj[$class]->table_name][str_replace('_order', '', $request_key)])) {
                  $list_fields[str_replace('_order', '', $request_key)] = [];
              }
              continue;
          }
          if(strstr($request_key,'_list')) {
              // Gestion du champ permettant de sauvegarder l'ordre des éléments dans le select multiple chosen
              if (isset($_REQUEST[$obj[$class]->table_name][$request_key . '_order'])) {
                  $request_value = explode(',', $_REQUEST[$obj[$class]->table_name][$request_key . '_order']);
                  $list_fields_order[$request_key] = $request_value;
                  unset($_REQUEST[$obj[$class]->table_name][$request_key . '_order']);
              } elseif(!is_array($request_value) && strstr($request_value, ',')) {
                  $request_value = explode(',', $request_value);
              }
              $list_fields[$request_key] = $request_value;
          } else {
              $files_fields[$request_key] = $request_value;
          }
      }
      foreach($_REQUEST[$obj[$class]->table_name] as $request_key=>$request_value) {
          if (strstr($request_key,'_list') || strstr($request_key,'_filelist')) {
              unset($_REQUEST[$obj[$class]->table_name][$request_key]);
          }
      }

     $default_i18n_values=array();	
     if(defined('ID_SITE') && in_array($form_name,$this->module_can_be_fully_edited_by_country_admin) && $form_name != 'sites') {
         if(array_key_exists('id_site', $obj[$class]->array_fields)) {
             $_REQUEST[$obj[$class]->table_name]['id_site']=ID_SITE;
         } elseif(array_key_exists('id_sites', $obj[$class]->array_fields)) {
             $_REQUEST[$obj[$class]->table_name]['id_sites']=ID_SITE;
         }
     }
      foreach($_REQUEST[$obj[$class]->table_name] as $request_key=>$request_value){
        if(is_array($request_value) && isset($request_value['i18n']) && !empty($request_value['i18n'])) {
          if(isset($request_value['i18n'][0])){
            $default_i18n_values[$request_key] = $request_value['i18n'][0]['value'];
            unset($request_value['i18n'][0]);
          }
          $i18n_filters[$request_key] = $request_value['i18n'];
          unset($_REQUEST[$obj[$class]->table_name][$request_key]);
        }
      }

      # setting	       	
      if(false && defined('ID_SITE') && !in_array($form_name,$this->module_can_be_fully_edited_by_country_admin)){
        #nop, pas le droit de modifier 
        die('modification not allowed (1)');
      }
      else{
        // Modif Ken pour poster des NULL au lieu de '' (pour les clés étrangères notamment)
        $saveValues = array_merge($_REQUEST[$obj[$class]->table_name],$default_i18n_values);
        foreach($saveValues as $k124=>&$saveVal) {
            # modif nico 
            if($k124=='password') {
                // Modif Ken pour ne pas prendre en compte le pass vide si on ne le modif pas
                if(array_key_exists('password', $saveValues) && empty($saveValues['password'])) {
                    unset($saveValues['password']);
                }
                continue;
            }
            $saveVal = ('' === $saveVal) ? NULL : $saveVal;
        }
  
        # auto update id_admins fields
        if(empty($current_id[$obj[$class]->primary_key]) && array_key_exists('id_admins', $obj[$class]->array_fields) && !array_key_exists('id_admins', $saveValues)) {
            $saveValues['id_admins'] = $_SESSION['admins']['id_admins'];
        }

        # auto update id_sites fields
        if(defined('ID_SITE') && !array_key_exists('id_site', $saveValues) && array_key_exists('id_site', $obj[$class]->array_fields)) {
            $saveValues['id_site'] = ID_SITE;
        }


        # filtering existing fields for fields to save
        $verifiedSaveValues = array();
        foreach($saveValues as $k_to_save=>$v_to_save){
            if(in_array($k_to_save, array_keys($this->collector[$class]->array_fields))){
                $verifiedSaveValues[$k_to_save] = $v_to_save;
            }
        } 

       $this->current_id = $current_id[$this->collector[$class]->primary_key] = $this->collector[$class]->set($current_id[$this->collector[$class]->primary_key], $verifiedSaveValues);
         
      }
       # np if this object use key_value_storage
        if(isset($this->collector[$class]->KVS_Properties)){
            # si editing an existing element 
            $kv_exist = false;
            if($current_id[$this->collector[$class]->primary_key]){
                if(isset($this->collector[$class]->KVS_Properties['child_tablename'])){
                # testing if existing externals datas for this element
                    #$this->collector[str_replace('gen_','',$this->collector[$class]->KVS_Properties['child_tablename'])]->db->debug=true;
                    $kv_exist = $this->collector[str_replace('gen_','',$this->collector[$class]->KVS_Properties['child_tablename'])]->get(
                        array(  $this->collector[$class]->primary_key  => $current_id[$this->collector[$class]->primary_key])
                    );
                }
            }
            $kv_exists_fields = array();
            if(is_array($kv_exist)){
                foreach($kv_exist as $kv_field){
                    $kv_exists_fields[$kv_field[$this->collector[$class]->KVS_Properties['child_keyfield']]] = $kv_field; 
                }
            }
            # foreach keys defined into to main element classe
            //var_dump($obj[$class]->KVS_Properties['keys']);
            //if(isset($obj[$class]->KVS_Properties['keys'])){

                if( is_array($this->fields)) {
                    $this->collector['form']->addKvFields($form_name);
                    $form_fields = array_keys($this->fields);
                }else{
                    # on va chercher les KV dans la conf
                    if(file_exists(MODS.$class.'/admin-fields.php')){
                        $form= $this;
                        global $gen;
                        include(MODS.$class.'/admin-fields.php');
                        $this->collector['form']->addKvFields($form_name);
                        $form_fields = array_keys($this->fields);
                    }
                }

                if(!$current_id[$this->collector[$class]->primary_key] && method_exists($this->collector[$class],'InsertForKVS')){
                    $this->current_id = $current_id[$this->collector[$class]->primary_key] = $this->collector[$class]->InsertForKVS();
                }

                if($this->fields)foreach($this->fields as $kvs_key=>$kvs_key_array){
                    if(!isset($kvs_key_array['kv']) || $kvs_key_array['kv']!=true)continue;
                    # if an key is not set, donop:we don't want to save it
                    if(!isset($saveValues[$kvs_key]))continue;
                    if(isset($kv_exists_fields[$kvs_key])){
                        # update value
                        $this->collector[str_replace('gen_','',$this->collector[$class]->KVS_Properties['child_tablename'])]->set(
                            $kv_exists_fields[$kvs_key]['id_'.str_replace('gen_','',$this->collector[$class]->KVS_Properties['child_tablename'])]
                            ,array(
                                $this->collector[$class]->KVS_Properties['child_valuefield'] => $saveValues[$kvs_key]
                            )
                        );
                    }
                    else{
                        # insert
                        if(!$current_id[$this->collector[$class]->primary_key]){
                        }
                        $this->current_id = $this->collector[str_replace('gen_','',$this->collector[$class]->KVS_Properties['child_tablename'])]->set(
                            '' 
                            ,array(
                                $this->collector[$class]->primary_key  => $current_id[$this->collector[$class]->primary_key]
                                ,$this->collector[$class]->KVS_Properties['child_valuefield']  => $saveValues[$kvs_key]
                                ,$this->collector[$class]->KVS_Properties['child_keyfield']    => $kvs_key
                            )
                        );
                    }
                }
           // }
        }
        # end key value storage management 
      # after save callback
      if($this->self_callback != false){
        call_user_func( array( $obj[$class],$this->self_callback),$current_id[$obj[$class]->primary_key]) ;
      }
      $validFlag = null;
      $checkSaveChanges = array_key_exists('pages', $this->collector) && method_exists($this->collector['pages'], 'checkSaveChanges') ? $this->collector['pages']->checkSaveChanges() : false;
      if($checkSaveChanges && isset($_SESSION['admins']['usertype']) && !empty($_SESSION['admins']['usertype'])) {
          $usertype = $this->collector['usertypes']->getOne($_SESSION['admins']['usertype']);
          $validFlag = $usertype['validation_flag'];
      }
      foreach($i18n_filters as $i18n_field=>$i18n_filter) {
          foreach($i18n_filter as $id_locale=>$i18n_values) {
              $i18nHasChanged = false;

              if(defined('AUTO_TRANSLATE') && AUTO_TRANSLATE==true && empty($i18n_values['value']) && !empty($saveValues[$i18n_field])){

                    $lang = $this->collector['locales']->getOneCached($id_locale);
                    $lang_dest = $lang['iso_translator'];
                    if(!isset($this->auto_translate_fields)) die('error : form->auto_translate_fields not defined');
                    if(isset($this->auto_translate_fields[$form_name])&& in_array($i18n_field,$this->auto_translate_fields[$form_name])){
                        $i18n_values['value'] = $this->collector[$form_name]->auto_translate($saveValues[$i18n_field],'',$lang_dest);
                    } else {
                        if(defined('AUTO_TRANSLATE_ALL') && AUTO_TRANSLATE_ALL==true){
                            $i18n_values['value'] = $this->collector[$form_name]->auto_translate($saveValues[$i18n_field],'',strtoupper($lang_dest));
                        }
                    }
              } 

              $array = array(
                   'module'	=> $form_name
                  ,'field_name'	=> $i18n_field
                  ,'id_locales'	=> $id_locale 
                  ,'lib'		=> $i18n_values['value']
                  ,'is_published' => $i18n_values['is_published']
                  ,'id_element'   => $current_id[$obj[$class]->primary_key]
              );

              $existing_i18n = $this->collector['i18n']->getOne(array('module'=> $form_name,'field_name'=> $i18n_field,'id_locales'=> $id_locale,'id_element'=>$current_id[$obj[$class]->primary_key]));
              $this->collector['i18n']->set((isset($existing_i18n['id_i18n'])) ? $existing_i18n['id_i18n'] : '' ,$array);
              if($checkSaveChanges && $validFlag == Usertypes::_NO_VALIDATION_ && array_key_exists($obj[$class]->getValidationField(), $obj[$class]->array_fields) && in_array($i18n_field, $obj[$class]->getFieldsToCheck())) {
                  if(!isset($existing_i18n['id_i18n'])) {
                      $i18nHasChanged = true;
                  } else {
                      if($i18n_values['value'] != $existing_i18n['lib']) {
                          $i18nHasChanged = true;
                      }
                  }
                  if($i18nHasChanged === true) {
                      $obj[$class]->set($current_id[$obj[$class]->primary_key], array($obj[$class]->getValidationField() => 0));
                  }
              }
          }
      }

      # is list detected
      foreach($list_fields as $request_key=>$request_value){

        $linked_object=$this->fields;
        $linkedField = array_key_exists('linked_field', $linked_object[$request_key]) ? $linked_object[$request_key]['linked_field'] : '';
        $linkCollector = array_key_exists('collector', $linked_object[$request_key]) ? $linked_object[$request_key]['collector'] : NULL;
        $linked_object= (isset($linked_object[$request_key]) && isset($linked_object[$request_key]['linked_object'])) ? $linked_object[$request_key]['linked_object'] : '';
        if(!$linked_object)continue;
        if (!isset($linkCollector)) {
            $linkCollector = $class.'_'.$linked_object;
        }
        $linkedIds = array();
        # get current value for merging
        $this->collector[$linkCollector]->RollBackUsedFields();
        $vals = $this->collector[$linkCollector]->get(array($obj[$class]->primary_key=>$current_id[$obj[$class]->primary_key]));
        $linked_current = null;
        if ($vals) {
            $linked_current = array_values($vals);
        }
        if(!is_array($linked_current)) $linked_current=array();
        foreach($linked_current as $k=>$v){
            # we don't need the primary key value
            $linkedIds[$v['id_'.$linkCollector]] = $v['id_' . $linked_object];
            $linked_values[$v['id_'.$linked_object]]=$v;
            unset($linked_values[$v['id_'.$linked_object]]['id_'.$linkCollector]);
        }
        # if editing/existing
        $id_to_del = array();
        if(!is_array($request_value)) {
            $request_value = array($request_value);
        }
        if($current_id[$obj[$class]->primary_key] != '' && !empty($linked_values))	{
            $to_del=$this->collector[$linkCollector]->get(array($obj[$class]->primary_key=>$current_id[$obj[$class]->primary_key]));
            if(is_array($to_del)){
                foreach($to_del as $k=>$v){
                    if(!in_array($v['id_' . $linked_object], $request_value)) {
                        $id_to_del[]=$v['id_'.$linkCollector];
                    }
                }
                if(count($id_to_del) > 0) {
                    if (isset($this->fields[$request_key]['resetLinkedFieldOnDelete']) && $this->fields[$request_key]['resetLinkedFieldOnDelete']) {
                        foreach ($id_to_del as $id) {
                            $this->collector[$linkCollector]->set($id, [$obj[$class]->primary_key => NULL]);
                        }
                    } else {
                        $this->collector[$linkCollector]->del($id_to_del);
                    }
                }
            }
        }
        
        foreach($request_value as $rel_value) {
            if(empty($rel_value)) continue;
            if(isset($linked_values[$rel_value]) && in_array($rel_value, $linkedIds)) {
                if (isset($list_fields_order[$request_key]) && in_array('n_order', $this->collector[$linkCollector]->GetFieldsList('gen_' . $linkCollector))) {
                    $linked_values[$rel_value]['n_order'] = array_search($rel_value, $list_fields_order[$request_key]) + 1;
                }
                $this->collector[$linkCollector]->set(array_search($rel_value, $linkedIds) ,$linked_values[$rel_value]);
            } else {
                if (empty($linkedField)) {
                    $linkedField = 'id_' . $linked_object;
                }
                if ($existingLink = $this->collector[$linkCollector]->get([$linkedField => $rel_value,$obj[$class]->primary_key => $current_id[$obj[$class]->primary_key]])) {
                    $array = $existingLink[0];
                    $array[$obj[$class]->primary_key] = $current_id[$obj[$class]->primary_key];
                } else {
                    $array = array(
                        $obj[$class]->primary_key => $current_id[$obj[$class]->primary_key]
                        , $linkedField => $rel_value
                    );
                }
                if (isset($list_fields_order[$request_key]) && in_array('n_order', $this->collector[$linkCollector]->GetFieldsList('gen_' . $linkCollector))) {
                    $array['n_order'] = array_search($rel_value, $list_fields_order[$request_key]) + 1;
                }

                $this->collector[$linkCollector]->set($existingLink ? $rel_value  : '',$array);
            }
        }
      }

      foreach($files_fields as $request_key => $request_value) {
          $linked_object=$this->fields;
          $callback = array_key_exists('saveCallback', $linked_object[$request_key]) ? $linked_object[$request_key]['saveCallback'] : null;
          $linkedField = array_key_exists('linked_field', $linked_object[$request_key]) ? $linked_object[$request_key]['linked_field'] : '';
          $linked_object = $linked_object[$request_key]['linked_object'];
          $n = 1;
          foreach($request_value as $id => $path) {
              if(empty($path)) continue;
              $fileExists = $this->collector[$form_name.'_'.$linked_object]->getOne($id);
              $fileId = $fileExists ? $fileExists['id_' . $form_name.'_'.$linked_object] : null;
              $params = array(
                  'id_' . $class => $current_id[$obj[$class]->primary_key],
                  'path' => $path,
                  'n_order' => $n
              );

              $this->collector[$form_name.'_'.$linked_object]->set($fileId, $params);
              if(!empty($callback)) {
                  if(strstr($callback, '::')) {
                      $expCb = explode('::', $callback);
                      $this->collector[$expCb[0]]->$expCb[1]($params);
                  } else {
                      $callback($params);
                  }
              }
              $n++;
          }
      }
       
      # ajout nico 16/08/2017 : l'utilisation de la methode child:set n'est pas utilisable dans le cas ou on veut utiliser les données i18n précédement postées car elles ne sont pas encore settées
      # dans ce cas, il faut donc utiliser la methode setCompleted
      if(isset($saveValues) && is_array($saveValues) && count($saveValues) > 0) {
          if(!isset($current_id[$obj[$class]->primary_key])) {
             $this->current_id =  $current_id[$obj[$class]->primary_key] = $obj[$class]->set($current_id[$obj[$class]->primary_key], $saveValues);
          }
          if(method_exists($class, 'setCompleted')) {
              $this->collector[$class]->setCompleted($current_id[$obj[$class]->primary_key]);
          }
      }

      // REQUEST est utilisé pour la fonction makeLists et current_id pour createForm
      $_REQUEST[$obj[$class]->primary_key] = ($current_id[$obj[$class]->primary_key] == '' || $current_id[$obj[$class]->primary_key] == 1) ? $tmp : $current_id[$obj[$class]->primary_key];
      $current_id[$obj[$class]->primary_key] = $_REQUEST[$obj[$class]->primary_key];
      $confirm = 1;

      if(isset($this->save_only) && $this->save_only===true)return true;
    }
    

    $fields = array();
    $temp = ( ! empty($form_fields) ? $form_fields : array_keys($obj[$class]->array_fields) );
    $fields = array_merge($fields, $temp);

    $display='';
       if(!empty($this->restrictions) && isset($this->restrictions['locales']) && isset($this->restrictions['pref_loc'])){
        if(!defined('ID_LOC')) define('ID_LOC',$this->restrictions['pref_loc']);
        $values = ((isset($_REQUEST) && $current_id[$obj[$class]->primary_key]) != '') ? $obj[$class]->getOne((int)$current_id[$obj[$class]->primary_key]) : array();
      }
      else{
                if(is_array($this->fields)) {
                    $this->collector['form']->addKvFields($form_name);
                    $form_fields = array_keys($this->fields);
                }else{
                    if(file_exists(MODS.$class.'/admin-fields.php')){
                        $form= $this;
                        global $gen;
                        include(MODS.$class.'/admin-fields.php');
                        $this->collector['form']->addKvFields($form_name);
                        $form_fields = array_keys($this->fields);
                    }
                }
                foreach($this->fields as $kvs_key=>$kvs_key_array){
                    if(!isset($kvs_key_array['kv']) || $kvs_key_array['kv']!=true)continue;
                    $this->collector[$class]->KVS_Properties['keys'][]=$kvs_key;
                }

                if(isset($this->collector[$class]->KVS_Properties['keys']) && !empty($current_id[$obj[$class]->primary_key])){
                    $current = $this->collector[$class]->getKeyValueStore((int)$current_id[$obj[$class]->primary_key]);
                    if(isset($current[0])) $values= $current[0];
                }
                else{
                    $values = ((isset($_REQUEST) && $current_id[$obj[$class]->primary_key]) != '') ? $obj[$class]->get((int)$current_id[$obj[$class]->primary_key]) : array();
                }

      }
      #XXX
      $formRequestVals = method_exists($obj[$class], 'getFormRequestKeys') ? $obj[$class]->getFormRequestKeys() : array('lang', 'groupe_id', 'sites_id_projet', 'id_clients');
      foreach($formRequestVals as $val) {
          if(isset($_REQUEST[$val]) && trim($_REQUEST[$val]) != '') $values[$val] = $_REQUEST[$val];
      }

      $display .= $this->get(
          $this->form_options['id']
          , $values
          , $form_name
          , ''
          , $current_id
          , $title
      );

    return $display;
  }

  public function getFieldHtml($ref, $f_id, $label, $id, array $values, $form_name, $idToEdit = array()) {
      $out = '';
      $forced_id_site = '';
      if(defined('ID_SITE')) {
          $forced_id_site = ID_SITE;
      }
     foreach($this->field_struc as $k_struc){
               if(!isset($field_option[$k_struc])) $field_option[$k_struc] = '';
            }
      switch($ref[$f_id]['type']) {
        case 'info':
          if(isset($ref[$f_id]['callBack']) && !empty($ref[$f_id]['callBack'])){
            if(isset($ref[$f_id]['args']) && !empty($ref[$f_id]['args']) && !empty($values[$ref[$f_id]['args']])) {
              # XXX eval is devil
              eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$ref[$f_id]['args']].'\');');
            } else {
              # XXX eval is devil
              if($values[$f_id] != '') eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$f_id].'\');');
              else $value = $ref[$f_id]['value'];
            }
          } else {
            $value = (($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] );
          }
          if($this->view_only === true){
            $out .= '<p class="read-only">'.$value.'</p>';
            break;
          }

          $value = ($value); 
          $out .= '<div class="info">'.$value.'</div>';
          break;
        case 'tab_separator':
          $out .= '<div class="tab_separator">'.$label.'</div>';
          break;
        case 'text':
        case 'number':
         
          if(isset($ref[$f_id]['callBack']) && !empty($ref[$f_id]['callBack'])){
            if(isset($ref[$f_id]['args']) && !empty($ref[$f_id]['args']) && !empty($values[$ref[$f_id]['args']])) {
              # XXX eval is devil
              eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$ref[$f_id]['args']].'\');');
            } else {
              # XXX eval is devil
              if($values[$f_id] != '') eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$f_id].'\');');
              else $value = $ref[$f_id]['value'];
            }
          } else {
            $value = (($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] );
          }
          if($this->view_only === true || isset($ref[$f_id]['view_only'])){
            $out .= '<p class="read-only">'.$value.'</p>';
            break;
          }
          $patternAttr = $ref[$f_id]['pattern'] != '' ? ' pattern="' . $ref[$f_id]['pattern'] . '" ' : '';
          //$value = ($value); 
          $out .= '<input 
            name        ="'.$id.'['.$f_id.']"
            id          ="'.$f_id.'"
            type        ="'.$ref[$f_id]['type'].'" 
            value       ="'.$value.'" 
            size        ="'.$ref[$f_id]['size'].'" 
            maxlength   ="'.$ref[$f_id]['maxlength'].'" 
            onclick     ="'.$ref[$f_id]['onclick'].'"
            ' . $patternAttr . '
            errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
            class       ="'.$ref[$f_id]['class'].'  full-width form-control col-md-8"
            style       ="'.$ref[$f_id]['style'].'"
            ' . (isset($ref[$f_id]['readonly']) ? 'readonly="readonly"' : '') . '
            ' . (isset($ref[$f_id]['required']) && $ref[$f_id]['required'] ? 'required="required"' : '') . '
            />';
          break;
        case 'submit':
          if($this->view_only === true) break;
        case 'button':
          if($this->view_only === true) break;
          $out .='<button type="'.$ref[$f_id]['type'].'" onclick     ="'.$ref[$f_id]['onclick'].'" class="'.$ref[$f_id]['class'].' btn btn-sm btn-outline-primary  ">'.$ref[$f_id]['value'].'</button>';  
          break;

        case 'textarea':
          if($this->view_only === true){
            $out .=nl2br((($values[$f_id] == '') ? $ref[$f_id]['value'] : $values[$f_id]));
            break;
          } 
          $out .='<textarea 
            name        ="'.$id.'['.$f_id.']"
            id          ="'.$f_id.'"
            onclick     ="'.$ref[$f_id]['onclick'].'"
            class       ="  full-width form-control col-md-8 '.$ref[$f_id]['class'].'"
            cols        ="'.$ref[$f_id]['cols'].'"
            rows        ="'.$ref[$f_id]['rows'].'"
            maxlength   ="'.$ref[$f_id]['maxlength'].'" 
            onclick     ="'.$ref[$f_id]['onclick'].'"
            pattern     ="'.$ref[$f_id]['pattern'].'"
            errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
            placeholder="'.$ref[$f_id]['placeholder'].'"
            style       ="'.$ref[$f_id]['style'].'"
            ' . (isset($ref[$f_id]['required']) && $ref[$f_id]['required'] ? 'required="required"' : '') . '        
            >'.(($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] ).'</textarea>'; 
          break;
        case 'html_i18n':
        case 'file_i18n':
        case 'i18n':
        case 'simple_html_i18n':
        case 'select_i18n':
          if(!empty($this->restrictions) && isset($this->restrictions['locales'])){
            $available_locales  = $this->collector['locales']->get(array('id_locales'=>$this->restrictions['locales']));
            $info ='';
          }
          else{
            $temp = $this->collector['sites']->getAvailabledLocalesForSite(ID_SITE); 
            foreach($temp as $k=>$v){
                $available_locales[$v['id_locales']] = $this->collector['locales']->getOneCached($v['id_locales']);
            }
            $info ='Only master locales are displayed, thanks to use the language selector to modify non master language';
          }

          if(isset($_SERVER['id_plateforme'])){
            $available_locales_for_admin = $this->collector['plateforme']->getAvailabledLanguage($_SERVER['id_plateforme']);
          }

          foreach($available_locales_for_admin as $k=>$v){
            if(!isset($available_locales[$v['id_locales']])){
                $available_locales[$v['id_locales']] =  $this->collector['locales']->getOneCached($v['id_locales']);
            }
          }


           //$out.='<div id="oldtab-locales'.$f_id.'" style="" class=" full-width form-control col-md-12 ">';
           $out.='<div class="col-md-8"><div class="row"><div class="col-md-11 p-0 m-0">';

        // $out .= ' <fieldset>';

         switch($ref[$f_id]['type']):
            case 'i18n':
             $readonly = (!empty($this->restrictions) && isset($this->restrictions['locales']) && empty($forced_id_site)) ? 'readonly' : '';
	      
             $disabled="";

             if(defined('ID_SITE') && !in_array($form_name,$this->module_can_be_fully_edited_by_country_admin)) {
                if(defined('USE_I18N_MASTER_SLAVE') && USE_I18N_MASTER_SLAVE === true){
                    $disabled="disabled";
                }
             }
             if(isset($ref[$f_id]['readonly']) && $ref[$f_id]['readonly']===true) $readonly= 'readonly';
             //$disabled="disabled";
		if(!isset($ref[$f_id]['hidemain']) || ($ref[$f_id]['hidemain']==false)){
                if(defined('AUTO_TRANSLATE') && AUTO_TRANSLATE===true){
                    if(defined('AUTO_TRANSLATE_ALL') && AUTO_TRANSLATE_ALL===true){
                        $out.='<span class="badge badge-success">'.t("Traduction automatique totale activée").'</span>'; 
                    }
                    else{
                        $out.='<span class="badge badge-success">'.t("Traduction automatique selective activée").'</span>'; 
                    }
                } 
                else{
                     $out.='<span class="badge badge-light">'.t("Traduction automatique désactivée").'</span>'; 
                }

                if(defined('USE_I18N_MASTER_SLAVE') && USE_I18N_MASTER_SLAVE ===true){
                     $out.='<span class="badge badge-light">'.t("Mode traducteur activé").'</span>'; 
                }
                else{ 
                     $out.='<span class="badge badge-light">'.t("Mode traducteur désactivé").'</span>'; 
                }

        
			    $out .= '<textarea '.$disabled.' 
			name        ="'.$id.'['.$f_id.'][i18n][0][value]"
			onclick     ="'.$ref[$f_id]['onclick'].'"
			class       ="  full-width form-control col-md-12 '.$ref[$f_id]['class'].'"
			cols        ="'.$ref[$f_id]['cols'].'"
			rows        ="'.$ref[$f_id]['rows'].'"
			maxlength   ="'.$ref[$f_id]['maxlength'].'" 
			onclick     ="'.$ref[$f_id]['onclick'].'"
			pattern     ="'.$ref[$f_id]['pattern'].'"
            placeholder="'.$ref[$f_id]['placeholder'].'"
			errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
			style       ="'.$ref[$f_id]['style'].'"
            id          ="'.$f_id.'"
			'.$readonly.'
            ' . (isset($ref[$f_id]['required']) && $ref[$f_id]['required'] ? 'required="required"' : '') . '        
			>'.( isset($values[$f_id]) && !empty($values[$f_id]) ? $values[$f_id] : ($f_id == 'translations' ? $values['lib'] : '') ).'</textarea>';
    //			>'.( (empty($values[$f_id])) ? $values['lib'] : $values[$f_id] ).'</textarea>'; 
		  }
            break;
      case 'select_i18n':
            $count = 1;
            $multiple = isset($ref[$f_id]['multiple']) ? 'multiple = "'.$ref[$f_id]['multiple'].'"' : '';
            $size     = isset($ref[$f_id]['size']) ? 'size = "'.$ref[$f_id]['size'].'"' : '';

            $readonly = (!empty($this->restrictions) && isset($this->restrictions['locales'])  && empty($forced_id_site)) ? 'disabled' : '';
            if(defined('ID_SITE') && !in_array($form_name,$this->module_can_be_fully_edited_by_country_admin) ){
                $readonly = "disabled";
            }
            $out .= '<div class="col-md-8 m-0 p-0"><select '.$readonly.'

              data-placeholder="'.$ref[$f_id]['placeholder'].'"
              name        ="'.$id.'['.$f_id.'][i18n][0][value]"
              onclick     ="'.$ref[$f_id]['onclick'].'"
              onchange    ="'.$ref[$f_id]['onchange'].'"
              pattern     ="'.$ref[$f_id]['pattern'].'"
              errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
              style       ="'.$ref[$f_id]['style'].'"
              class="'.$ref[$f_id]['class'].' form-control col-md-8"
              '.$multiple.'
              '.$size.'
              ' . (isset($ref[$f_id]['required']) && $ref[$f_id]['required'] ? 'required="required"' : '') . '        
              >';
            if(isset($ref[$f_id]['placeholder']) && !empty($ref[$f_id]['placeholder'])){
              $out .= '<option value=""></option>';
            }
            foreach ( $ref[$f_id]['arrayValues'] as $opt_val => $opt_lib ) {
              $opt_val = trim($opt_val);
              $sel = ( isset( $values[$f_id]) && ( strtolower($opt_val) ==  strtolower($values[$f_id])) ) ? ' selected' : '';
              $sel = ( ( (empty($sel)) && (empty($values[$f_id])) && (!$opt_val) ) || ! empty($sel)) ?  'selected' : '';
              $out .= ' <option value = "'.$opt_val .'" ';
              $out .= ' style="'.(str_replace('$'.$f_id, $opt_val,$ref[$f_id]['optionStyle'])).  '" ';
              $out .= $sel.'>'.$opt_lib.'</option>';
            }
            $out .= '</select></div>';
            unset($count,$opt_val,$opt_lib);
            break;


        case 'file_i18n':
            $patternAttr = $ref[$f_id]['pattern'] != '' ? ' pattern="' . $ref[$f_id]['pattern'] . '" ' : '';
            $errorTitleAttr = $ref[$f_id]['errorMsg'] != '' ? ' title="' . $ref[$f_id]['errorMsg'] . '" ' : '';
            $protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';
            $baseUrl = $protocol . '://' . $_SERVER['SERVER_NAME'];

            $readonly = (!empty($this->restrictions) && isset($this->restrictions['locales']) && empty($forced_id_site)) ? 'readonly' : '';
            $moxmanOpts = 'no_host: true,width:\'1024\',insert_filter: function(file) { file.url = file.url.replace(/\/\//g, \'/\');}';
            if(array_key_exists('browseOptions', $ref[$f_id]) && !empty($ref[$f_id]['browseOptions'])) {
                $moxmanOpts = $ref[$f_id]['browseOptions'];
            }
            if(array_key_exists($form_name, $this->collector)) {
                $collector = $this->collector[$form_name];
            } else {
                $collector = $this->collector['admins'];
            }
            $moxmanOpts = $this->getMoxmanCallbacks($collector, $moxmanOpts);
            $out .='<input '.$readonly.' value="'.$values[$f_id].'" type="text" ' . $patternAttr . ' '. $errorTitleAttr .' id="'.$f_id.'_i18n0" name="'.$id.'['.$f_id.'][i18n][0][value]" onchange    ="'.$ref[$f_id]['onchange'].'" class="input-xlarge custom-file-input" style="width:300px"/> <button  onclick="moxman.browse({fields: \''.$f_id.'_i18n0\', '.$moxmanOpts.'});return false;" class="btn" onclick="">Pick file</button>&nbsp;
            <button onclick="moxman.edit({ path: \''.$baseUrl.urldecode($values[$f_id]).'\',width:\'1024\'});return false;" class="btn"><i class="feather icon-edit"></i> Edit</button>&nbsp;<a href="'.$values[$f_id].'" class="btn btn-primary fancybox view-button">View</a>
             ';
            break;

            case 'html_i18n':
              $out .= '<textarea 
                name        ="'.$id.'['.$f_id.'][i18n][0][value]"
                style       ="display:none"
                class  ="html"

                >'.($values[$f_id]).'</textarea>'; 
               $out .= '<p>'.t("Enable translations to edit content").'</p>';
            break;
            case 'simple_html_i18n':
		if(!isset($values['media'])) $values['media'] = '';
                $out .= "<input type='hidden' id='bg_".$f_id."' value='".$values['media']."' />";
                $out .= '<textarea id="'.$f_id.'"    name="'.$id.'['.$f_id.'][i18n][0][value]" class="full-width html form-control col-md-12 i18n-textarea-child-html" rel="'.$values['media'].'">'.($values[$f_id]).'</textarea>'; 

            break;
           endswitch;

//            $out .='<div class="i18nchild" id="tab-locale'.$f_id.'"> <ul class="mini-tabs  js-tabs same-height tab'.$f_id.'"> <span style="font-size:8px; color: #666">'.$info.'</span>';
            $out .='<div class="i18nchild card bg-light p-4" id="tab-locale'.$f_id.'"> <ul class="nav nav-tabs    tab'.$f_id.'"><!--<span style="font-size:8px; color: #666">'.$info.'</span>-->';
            $j=0; 
            foreach($available_locales as $locale){
              $lib_lang= strtolower($locale['country']);
              $out .= '		<li class="'.(($j==0) ? 'nav-item' : 'nav-item' ).'"> 
                            <a 
                                   href="#tab-'.$f_id.$locale['id_locales'].'" 
                                   id="pill-tab-'.$f_id.$locale['id_locales'].'" 
                                   class="'.(($j==0) ? 'active nav-link' : 'nav-link' ).(($locale['is_master']==1) ? '  highlighted'  : '').'"
                                   role="tab"
                                   data-toggle="tab"
                                   aria-selected="true"
                                   aria-controls="tab-'.$f_id.$locale['id_locales'].'"
                                >
                                <img width="16" height="11" title="'.$locale['lib'].'" alt="'.$locale['lib'].'" src="/assets/images/icons/flags/'.$lib_lang.'.png" >&nbsp;'.strtoupper($locale['language']).'
                              </a></li>';
              $j++;
            }
            $out .='		</ul>';


            $j=0;
            $readonly = (!empty($this->restrictions) && isset($this->restrictions['locales']) && empty($forced_id_site)) ? 'readonly' : '';
            $out .= '<div class="tab-content hasi18nfields">';
            foreach($available_locales as $locale){

              $value= $this->collector['i18n']->getOne(array('module'=>$form_name,'field_name'=>$f_id,'id_locales'=>$locale['id_locales'],'id_element'=>array_values($idToEdit))); 
              if(!isset($value['lib']))  $value['lib'] = '';
              if(!isset($value['is_published']))  $value['is_published'] = 0;
              $lib_lang= strtolower($locale['country']);
              $out .= ' <div 
                            class="tab-pane fade '.(($j==0) ? 'show active ' : '' ).'" 
                            role="tabpanel" 
                            aria-labelledby="tab-'.$f_id.$locale['id_locales'].'" 
                            id="tab-'.$f_id.$locale['id_locales'].'" 
                            >';
              $j++;

              switch($ref[$f_id]['type']):
              case 'i18n':

                $out .= '<textarea 
                  name        ="'.$id.'['.$f_id.'][i18n]['.$locale['id_locales'].'][value]"
                  id          ="'.$f_id.'['.$lib_lang.']"
                  onclick     ="'.$ref[$f_id]['onclick'].'"
                  class       ="    full-width form-control col-md-12  '.$ref[$f_id]['class'].' i18n-textarea-child"
                  cols        ="'.$ref[$f_id]['cols'].'"
                  rows        ="'.$ref[$f_id]['rows'].'"
                  maxlength   ="'.$ref[$f_id]['maxlength'].'" 
                  onclick     ="'.$ref[$f_id]['onclick'].'"
                  pattern     ="'.$ref[$f_id]['pattern'].'"
                  errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
                  style       ="'.$ref[$f_id]['style'].'"
		'.$readonly.'
                  >'.($value['lib']).'</textarea>'; 

                break;
                 case 'simple_html_i18n':

                $out .= '<textarea 
                  name        ="'.$id.'['.$f_id.'][i18n]['.$locale['id_locales'].'][value]"
                  id          ="'.$f_id.'['.$lib_lang.$locale['id_locales'].']"
                  onclick     ="'.$ref[$f_id]['onclick'].'"
                  class       = "full-width html   full-width form-control col-md-12 '.$ref[$f_id]['class'].' i18n-textarea-child-html"
                  cols        ="'.$ref[$f_id]['cols'].'"
                  rows        ="'.$ref[$f_id]['rows'].'"
                  maxlength   ="'.$ref[$f_id]['maxlength'].'" 
                  onclick     ="'.$ref[$f_id]['onclick'].'"
                  pattern     ="'.$ref[$f_id]['pattern'].'"
                  errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
                  style       ="'.$ref[$f_id]['style'].'"
                  >'.($value['lib']).'</textarea>'; 

                break;

          case 'select_i18n': 
             $count = 1;
            $multiple = isset($ref[$f_id]['multiple']) ? 'multiple = "'.$ref[$f_id]['multiple'].'"' : '';
            $size     = isset($ref[$f_id]['size']) ? 'size = "'.$ref[$f_id]['size'].'"' : '';
            $out .= '<div class="col-md-8 m-0 p-0"><select
              data-placeholder="'.$ref[$f_id]['placeholder'].'"
              name        ="'.$id.'['.$f_id.'][i18n]['.$locale['id_locales'].'][value]"
              id          ="'.$form_name . '_' . $f_id.'['.$lib_lang.']"
              onclick     ="'.$ref[$f_id]['onclick'].'"
              onchange    ="'.$ref[$f_id]['onchange'].'"
              pattern     ="'.$ref[$f_id]['pattern'].'"
              errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
              style       ="'.$ref[$f_id]['style'].'"
              class="'.$ref[$f_id]['class'].' form-control col-md-8"
              '.$multiple.'
              '.$size.'
              >';
            if(isset($ref[$f_id]['placeholder']) && !empty($ref[$f_id]['placeholder'])){
          //    $out .= '<option value="">Not set</option>';
            }

            $out .= '<option value="">Not set</option>';
            foreach ( $ref[$f_id]['arrayValues'] as $opt_val => $opt_lib ) {
              $opt_val = trim($opt_val);
              $sel = ($opt_val ==  $value['lib'] ) ? ' selected' : '';
              //$sel = ( ( (empty($sel))  && (!$opt_val) ) || ! empty($sel)) ?  'selected' : '';
              //$sel = ( ( (empty($sel)) && (empty($value['lib'])) && (!$opt_val) ) || ! empty($sel)) ?  'selected' : '';
              $out .= ' <option value = "'.$opt_val .'" ';
              $out .= ' style="'.(str_replace('$'.$f_id, $opt_val,$ref[$f_id]['optionStyle'])).  '" ';
              $out .= $sel.'>'.$opt_lib.'</option>';
            }
            $out .= '</select></div>';
            unset($count,$opt_val,$opt_lib);
 
            break;
              case 'file_i18n':
                  $moxmanOpts = 'no_host: true,width:\'1024\',insert_filter: function(file) { file.url = file.url.replace(/\/\//g, \'/\');}';
                  if(array_key_exists('browseOptions', $ref[$f_id]) && !empty($ref[$f_id]['browseOptions'])) {
                      $moxmanOpts = $ref[$f_id]['browseOptions'];
                  }
                  if(array_key_exists($form_name, $this->collector)) {
                      $moxmanOpts = $this->getMoxmanCallbacks($this->collector[$form_name], $moxmanOpts);
                  }
                  $out .='<input  value="'.$value['lib'].'" type="text" id="'.$id.$f_id.$locale['id_locales'].'" name="'.$id.'['.$f_id.'][i18n]['.$locale['id_locales'].'][value]" class="form-control cold-md-9 input-xlarge media-child" /> <button  onclick="moxman.browse({fields: \''.$id.$f_id.$locale['id_locales'].'\', '.$moxmanOpts.'});return false;" class="btn">Pick file</button>';
                  break;
              case 'html_i18n':
                $out .= '<textarea 
                  name        ="'.$id.'['.$f_id.'][i18n]['.$locale['id_locales'].'][value]"
                  id          ="'.$f_id.'['.$lib_lang.']"
                  style       ="display:none"
                  >'.($value['lib']).'</textarea>'; 

                //$out .='<button id="">Edit html content</button><br /><br />';
                //$out .="<button id=\"\" onclick=\"EditInLine('".$locale['language']."','".$locale['id_locales']."');return false\">Edit html content</button><br />";
                
                $furl = substr(FRONT_URL, 0, 1) == '.' ? ID_SITE . FRONT_URL : FRONT_URL;
                $site = $this->collector['sites']->getOne(ID_SITE);
                $checkSaveChanges = method_exists($this->collector['pages'], 'checkSaveChanges') ? $this->collector['pages']->checkSaveChanges() : false;
                $checkContentParam = '';
                if($checkSaveChanges && isset($_SESSION['admins']['usertype']) && !empty($_SESSION['admins']['usertype'])) {
                    $usertype = $this->collector['usertypes']->getOne($_SESSION['admins']['usertype']);
                    $validFlag = $usertype['validation_flag'];
                    if($validFlag == Usertypes::_NO_VALIDATION_) {
                        $checkContentParam = '&checkContent=1';
                    }
                }

                // Prévalence des internal_url sur les prod_url pour palier aux soucis de noms de domaines utilisés par prod_url pas encore actifs
                if(isset($site['internal_url']) && !empty($site['internal_url'])) {
                    $out .='<a class="big-button ifancybox2" href="'.$site['internal_url'].'/tiny.php?editable=true&id_pages='.((isset($_REQUEST['id_pages'])) ? $_REQUEST['id_pages'] : '').'&id_locales='.$locale['id_locales'].'&hash='.HASH_KEY.$checkContentParam.'"><i class="feather icon-edit"></i> Edit html content ['.$locale['language'].']</a><br />';
                } elseif(isset($site['prod_url']) && !empty($site['prod_url'])) {
                    $out .='<a class="big-button ifancybox2" href="'.$site['prod_url'].'/tiny.php?editable=true&id_pages='.((isset($_REQUEST['id_pages'])) ? $_REQUEST['id_pages'] : '').'&id_locales='.$locale['id_locales'].'&hash='.HASH_KEY.$checkContentParam.'"><i class="feather icon-edit"></i> Edit html content ['.$locale['language'].']</a><br />';
                } else{
                    $out .='<a class="big-button ifancybox2" href="http://'.$furl.'/tiny.php?editable=true&id_pages='.((isset($_REQUEST['id_pages'])) ? $_REQUEST['id_pages'] : '').'&id_locales='.$locale['id_locales'].'&hash='.HASH_KEY.$checkContentParam.'"><i class="feather icon-edit"></i> Edit html content ['.$locale['language'].']</a><br />';
                }
                break;

                endswitch;


               $out .= '<div class="colx2-left green-bg"><span>'.t("Activé pour cette langue ?").' </span><select
                  name        ="'.$id.'['.$f_id.'][i18n]['.$locale['id_locales'].'][is_published]"
                  id          ="'.$f_id.'" 
                  onclick     ="'.$ref[$f_id]['onclick'].'"
                  onchange    ="'.$ref[$f_id]['onchange'].'"
                  pattern     ="'.$ref[$f_id]['pattern'].'"
                  errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
                  style       ="'.$ref[$f_id]['style'].'"
                  class="'.$ref[$f_id]['class'].' form-control col-md-8"
                  data-placeholder="" >';
                if(isset($ref[$f_id]['placeholder']) && !empty($ref[$f_id]['placeholder'])){
                  $out .= '<option value=""></option>';
                }
                foreach ( array('1'=>t("Oui"),'0'=>t("Non")) as $opt_val => $opt_lib ) {
                  if(!isset($value['is_published']) && $opt_val=='1') {
                    $out .= ' <option value = "'.$opt_val .'" selected="selected">'.$opt_lib.'</option>';
                  } 
                  else{
                    $out .= ' <option value = "'.$opt_val .'" '.(($value['is_published'] == $opt_val)  ? 'selected="selected"' : '' ).'>'.$opt_lib.'</option>';
                  }
                }
                unset($opt_val, $opt_lib);
                $out .= '</select></div>';
                $out . '</div>';

                if(!$locale['is_master']): #add userright
                                            endif; #is_master
                                             // if($this->id_locales_only==0):
                                               /* $out .= '<div class="colx2-right grey-bg"><span class="label">Can be overwrited by noadmin ? <select
                                                  name        ="'.$id.'['.$f_id.'][i18n]['.$locale['id_locales'].'][editable]"
                                                  id          ="'.$f_id.'" 
                                                  onclick     ="'.$ref[$f_id]['onclick'].'"
                                                  onchange    ="'.$ref[$f_id]['onchange'].'"
                                                  pattern     ="'.$ref[$f_id]['pattern'].'"
                                                  errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
                                                  style       ="'.$ref[$f_id]['style'].'"
                                                  class="'.$ref[$f_id]['class'].'"
                                                  data-placeholder="" >';
                if(isset($ref[$f_id]['placeholder']) && !empty($ref[$f_id]['placeholder'])){
                  $out .= '<option value=""></option>';
                }
                foreach ( array('0'=>("No"),'1'=>("Yes")) as $opt_val => $opt_lib ) {
                  $out .= ' <option value = "'.$opt_val .'" '.(($value['is_published'] == $opt_val)  ? 'selected="selected"' : '' ).'>'.$opt_lib.'</option>';
                }
                unset($opt_val, $opt_lib);
                $out .= '</select></span></div>';

  */
                //endif;


                $out .='</div>';
            }
            $out .='</div></div></div></div></div>';

# on va chercher les locales qui sont existe et sont actives ou non.
# pour chaque locale, afficher un champ avec le libelle de la locale dans un section avec tab pour chaque locale.
            break;

        case 'key_value_store':
            /*$objName = str_replace('gen_', '', $id);
            if(!isset($this->collector[$objName]->KVS_Properties)){die('key_value_store has no properties');}
            $linked_object = str_replace('gen_', '', $this->collector[$objName]->KVS_Properties['child_tablename']);
            if(current($idToEdit) != '') {

                if(file_exists(MODS.$objName.'/admin-fields.php')){
                    $form= $this;
                    global $gen;
                    include(MODS.$objName.'/admin-fields.php');
                    $form_fields = array_keys($this->fields);
                }
                foreach($this->fields as $kvs_key=>$kvs_key_array){
                    if(!isset($kvs_key_array['kv']) || $kvs_key_array['kv']!=true)continue;
                    $this->collector[$objName]->KVS_Properties['keys'][]=$kvs_key;
                }

                //$this->collector[$objName]->KVS_Properties['keys']
                $current = $this->collector[$objName]->getKeyValueStore(current($idToEdit));
                if(isset($current[0])) $current = $current[0];
            }*/
            /*foreach($this->collector[$objName]->KVS_Properties['keys'] as $field){
                # recursive
                $this->onlyFields=true;
                $this->fields = array('support'=>array('lib'=>'test'));
                //$out .= '<!-- KV start -->'.$this->get($id, array($field), (isset($current[$field]) ? array($field=>$current[$field]) : ''), $objName).'<!-- KV end -->';
                $this->onlyFields=false;

            }*/

        break;

        case 'relationel_list':
            $count = 1;
            $multiple = (isset($ref[$f_id]['multiple'])) ? ' multiple="multiple"' : '';
            //$multiple = isset($ref[$f_id]['multiple']) ? 'multiple = "'.$ref[$f_id]['multiple'].'"' : '';
            $size = isset($ref[$f_id]['size']) ? 'size = "'.$ref[$f_id]['size'].'"' : '';
            $linked_object = $ref[$f_id]['linked_object']; 
            $linkCollector = array_key_exists('collector', $ref[$f_id]) ? $ref[$f_id]['collector'] : $form_name.'_'.$linked_object;

            if($this->_checkLinkedCollector === true && !isset($this->collector[$linkCollector])) {
                $this->warning(t("Erreur: l'objet appelé n'existe pas, vérifiez la table correspondante en base et relancez un build").'<br /><b>'.$linkCollector.'<b>');
            }
            if($this->_checkLinkedCollector === true && is_array($this->collector) && array_key_exists($linkCollector, $this->collector)) {
                $this->collector[$linkCollector]->SetUsedFields(array('id_'.$linked_object));
            }

            $linked_values = array();
            if(is_array($idToEdit) && current($idToEdit) != '') {
                $orderBy = null;
                if (in_array('n_order', $this->collector[$linkCollector]->GetFieldsList('gen_' . $linkCollector))) {
                    $orderBy = 'n_order';
                }
                $current = is_array($this->collector[$linkCollector]->get($idToEdit, $orderBy)) ? array_values($this->collector[$linkCollector]->get($idToEdit, $orderBy)) : [];
                if(!is_array($current)) $current = array();
                foreach($current as $k=>$v){
                    $linked_values[]=$v['id_'.$linked_object]; 
                }
            } elseif(array_key_exists('values', $ref[$f_id]) && is_array($ref[$f_id]['values'])) {
                $linked_values = $ref[$f_id]['values'];
            }
            $out .='<div class="col-md-8 m-0 p-0"><select
              name        ="'.$id.'['.$f_id.'][]"
              id          ="'.$f_id.'" 
              onclick     ="'.$ref[$f_id]['onclick'].'"
              onchange    ="'.$ref[$f_id]['onchange'].'"
              pattern     ="'.$ref[$f_id]['pattern'].'"
              errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
              style       ="test-select '.$ref[$f_id]['style'].'"
              class="'.$ref[$f_id]['class'].' form-control col-md-8"
              data-placeholder="'.$ref[$f_id]['placeholder'].'"
              '.$multiple.'
              '.$size.'
              >';
            if(isset($ref[$f_id]['placeholder']) && !empty($ref[$f_id]['placeholder'])){
              $out .= '<option value=""></option>';
            }

            if(is_array($ref[$f_id]['arrayValues'])) {
                foreach ( $ref[$f_id]['arrayValues'] as $opt_val => $opt_lib ) {
                  $opt_val = trim($opt_val);
                  $sel = in_array($opt_val, $linked_values) ? 'selected' : '' ;
                  $out .= ' <option value = "'.$opt_val .'" ';
                  $out .= ' style="'.(str_replace('$'.$f_id, $opt_val,$ref[$f_id]['optionStyle'])).  '" ';
                  $out .= $sel.'>'.$opt_lib.'</option>';
                }
            }
            $out .= '</select>'
                    . '<input type="hidden" name="'.$id.'['.$f_id.'_order]" value="' . implode(',',$linked_values)  . '" />'
                    . '</div>';
            unset($count,$opt_val,$opt_lib);
            break;
        case 'select':
          if($this->view_only === true || isset($ref[$f_id]['view_only'])) {
            $out .= '<p class="read-only">'.$ref[$f_id]['arrayValues'][$values[$f_id]].'</p>';
            break; 
          }
            if(!is_array($ref[$f_id]['arrayValues'])){
              $this->warning("Oups ! Sorry but it look's you are trying to display element using an empty arrayValues: <br />the field named '".  ($ref[$f_id]['label']). "' is type '". ($ref[$f_id]['type']). "' but no values are passed. Please change the configuration of your module and add datas for this field by editiong<pre>admin-fields.php</pre>");

              break;
            }
            $count = 1;
            $multiple = isset($ref[$f_id]['multiple']) ? 'multiple = "'.$ref[$f_id]['multiple'].'"' : '';
            $size     = isset($ref[$f_id]['size']) ? 'size = "'.$ref[$f_id]['size'].'"' : '';
            $out .='<div class="col-md-8 m-0 p-0"><select
              data-placeholder="'.$ref[$f_id]['placeholder'].'"
              name        ="'.$id.'['.$f_id.']' . (isset($ref[$f_id]['multiple']) ? '[]' : '') . '"
              id          ="'.$form_name . '_' . $f_id.'" 
              onclick     ="'.$ref[$f_id]['onclick'].'"
              onchange    ="'.$ref[$f_id]['onchange'].'"
              pattern     ="'.$ref[$f_id]['pattern'].'"
              errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
              style       ="'.$ref[$f_id]['style'].'"
              class       ="'.$ref[$f_id]['class'].' form-control col-md-8"
              '.$multiple.'
              '.$size.'
            ' . (isset($ref[$f_id]['required']) && $ref[$f_id]['required'] ? 'required="required"' : '') . '        
              >';
            if(isset($ref[$f_id]['placeholder']) && !empty($ref[$f_id]['placeholder'])){
              $out .= '<option value=""></option>';
            }
            $selectedVal = isset($values[$f_id]) && '' != $values[$f_id] ? $values[$f_id] : (array_key_exists('value', $ref[$f_id]) ? $ref[$f_id]['value'] : '') ;
            if (isset($ref[$f_id]['multiple'])) {
                $selectedVal = json_decode($selectedVal);
            }
            foreach ( $ref[$f_id]['arrayValues'] as $opt_val => $opt_lib ) {
              $opt_val = trim($opt_val);
              $sel = ((is_array($selectedVal) && in_array($opt_val, $selectedVal)) || (!is_array($selectedVal) && strtolower($opt_val) ==  strtolower($selectedVal))) ? ' selected' : '';
              $sel = ( ( (empty($sel)) && (empty($selectedVal)) && (!$opt_val) ) || ! empty($sel)) ?  'selected' : '';
              $out .= ' <option value = "'.$opt_val .'" ';
              $out .= ' style="'.(str_replace('$'.$f_id, $opt_val,$ref[$f_id]['optionStyle'])).  '" ';
              $out .= $sel.'>'.$opt_lib.'</option>';
            }
            $out .= '</select></div>';
            unset($count,$opt_val,$opt_lib);
            break;
        case 'radio':
            if(!is_array($ref[$f_id]['arrayValues'])){
              $this->warning(_("radio without arrayValues:").$f_id);
              break;
            }
            $count = 1;
            $out  = '';
            foreach ( $ref[$f_id]['arrayValues'] as $opt_val => $opt_lib ) {
              $sel = (isset( $values[$f_id]) && ( $opt_val ==  $values[$f_id]) ) ? ' checked' : '';
              $out .= '<input 
                type        ="'.$ref[$f_id]['type'].'" 
                name        ="'.$id.'['.$f_id.']"
                id          ="'.$f_id.'" 
                value       ="'.$opt_val.'" '.$sel.'  
                onclick     ="document.getElementById(\''.$id.'['.$f_id.']'.'_validator\').value=this.value;'.$ref[$f_id]['onclick'].'" class="form-control"

                />'.$opt_lib;
            }
            $out .= '<input type="hidden" value="'.$values[$f_id].'" id="'.$id.'['.$f_id.']'.'_validator" pattern     = "'.$ref[$f_id]['pattern'].'" errorMsg    = "'.$ref[$f_id]['errorMsg'].'"/>';
            unset($count,$opt_val,$opt_lib);
            break;

        case 'switch':
            $count      = 1;
            $out  = '
              <div class="columns">
              <div class="colx2-left">
              <legend for="simple-switch-off">Switch off<span>[-]</span></legend>
              <input type="checkbox" name="simple-switch-off" id="simple-switch-off" value="1" class="switch">
              <legend for="simple-switch-off">Switch off<span>[-]</span></legend> <input type="checkbox" name="'.$f_id.'" id="'.$f_id.'" value="1" class="switch"></div>';
            break;

            /*	checkbox {{{ */
        case 'checkbox':
            if(!is_array($ref[$f_id]['arrayValues'])){
              $this->warning(_("checkbox  without arrayValues: ").$f_id);
              break;
            }
            $count      = 1;
            $out  = '';
            if(!empty($ref[$f_id]['pattern'])) $this->warning(_("pattern on checkbox is not supported yet").$f_id);
            if( ! is_array($values[$f_id])  ) $values[$f_id] = array($values[$f_id]);
            foreach ( $ref[$f_id]['arrayValues'] as $opt_val => $opt_lib ) {
              $sel = (is_array( $values[$f_id]) && in_array( $opt_val, $values[$f_id] )) ? ' checked' : '';
              $out .= '<input 
                type        ="'.$ref[$f_id]['type'].'" 
                name        ="'.$id.'['.$f_id.'][]"
                id          ="'.$f_id.'_'.$count++.'" 
                value       ="'.$opt_val.'" '.$sel.'  
                onclick     ="'.$ref[$f_id]['onclick'].'"
                onchange    ="'.$ref[$f_id]['onchange'].'"
                pattern     ="'.$ref[$f_id]['pattern'].'"
                errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
                class		="form-control checkbox"
                style     	="'.$ref[$f_id]['style'].'"
                />'.( (isset($ref[$f_id]['make_lib_float']) && $ref[$f_id]['make_lib_float'] == true) ? '<div class="float-left-block">'.$opt_lib.'</div>' : $opt_lib);
            }
            unset($count,$opt_val,$opt_lib);
            break;
            /*	}}}	*/
            /*	password {{{ */
        case 'password':
              //value       ="'.(($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] ).'" 
            $out .='<input 
              name        ="'.$id.'['.$f_id.']"
              id          ="'.$f_id.'"
              type        ="'.$ref[$f_id]['type'].'" 
              value       ="" 
              size        ="'.$ref[$f_id]['size'].'" 
              maxlength   ="'.$ref[$f_id]['maxlength'].'" 
              onclick     ="'.$ref[$f_id]['onclick'].'"
              pattern     ="'.$ref[$f_id]['pattern'].'"
              errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
              class       ="form-control '.$ref[$f_id]['class'].' col-md-8"
              style       ="'.$ref[$f_id]['style'].'"
              placeholder ="'.(isset($ref[$f_id]['placeholder']) ? $ref[$f_id]['placeholder'] : '').'"
              />';  
            break;
            /*	}}}	*/
            /* hidden {{{ */
        case 'hidden':
            $out .= '<input
              type   = "hidden" 
              name   = "'.$id.'['.$f_id.']"
              id     = "'.$f_id.'" 
              value  = "'.(($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] ).'" 
              />';
            break;
            /*	}}}	*/
            /*	html {{{ */
        case 'html':
              $out .= '<textarea 
              name        ="'.$id.'['.$f_id.'][i18n][0][value]" class="html_editable   full-width form-control col-md-8 " style="'.$ref[$f_id]['style'].'">'.($values[$f_id]).'</textarea>'; 

            break;
            /*	}}}	*/
            /*	file {{{ */
        case 'file':
            $moxmanOpts = 'no_host: true,width:\'1024\',insert_filter: function(file) { file.url = file.url.replace(/\/\//g, \'/\');}';
            if(array_key_exists('browseOptions', $ref[$f_id]) && !empty($ref[$f_id]['browseOptions'])) {
                $moxmanOpts = $ref[$f_id]['browseOptions'];
            }
            if(is_array($this->collector)) {
                if(array_key_exists($form_name, $this->collector)) {
                    $moxmanOpts = $this->getMoxmanCallbacks($this->collector[$form_name], $moxmanOpts);
                } else {
                    $moxmanOpts = $this->getMoxmanCallbacks($this->collector['admins'], $moxmanOpts);
                }
            }
            $name = isset($ref[$f_id]['name']) ? $ref[$f_id]['name'] : $id.'['.$f_id.']';
//            $out .='<div class="custom-file "style="width:300px;" onclick="moxman.browse({fields: \''.$f_id.'\', '.$moxmanOpts.'});return false;"> <input type="text"  name ="'.$id.'['.$f_id.']"  id="'.$f_id.'"   value  = "'.(($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] ).'" class="custom-file-input input-xlarge"/><label class="custom-file-label" for="inputGroupFile01">Choose file</label></div>';
            $out .=' <div class="col-md-8" >
                    <div class="row"> 
                          <div class="col-md-11 p-0 m-0">
                                <input onclick="moxman.browse({fields: \''.$f_id.'\', '.$moxmanOpts.'});$(\'#view_'.$f_id.'\').hide();return false;" type="text" name="'.$name.'"  id="'.$f_id.'"   value  = "'.(($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] ).'" class=" full-width form-control col-md-12 "/>
                          </div>
                    </div>
                    <div class="row"> 
                        <div clas="col-md-4"> 
                           <a class="btn btn-primary"  onclick="moxman.browse({fields: \''.$f_id.'\', '.$moxmanOpts.'});$(\'#view_'.$f_id.'\').hide();return false;" style="margin-top:5px;margin-right:8px;"><i class="fa fa-file" aria-hidden="true"></i> '.t("Choisir un fichier").'</a>
                        </div>';
                    
 

            //$out .= '&nbsp;<button onclick="moxman.edit({ path: \''.$values[$f_id].'\',width:\'1024\'});return false;" class="btn">Edit</button>&nbsp;<a href="'.$values[$f_id].'" class="big-button  fancybox">View</a>';
            if(!empty($values[$f_id])) {
                $out .= '<div clas="col-md-4">
                            <a id="view_'.$f_id.'" href="'.$values[$f_id].'" class="btn  btn-primary  view-button fancybox"><i class="fa fa-link" aria-hidden="true"></i> '.t("Voir").' '. basename($values[$f_id]).'</a>
                            <a id="erase'.$f_id.'" href="#"  onclick="$(\'#'.$f_id.'\').val(null)" class="btn btn-sm btn-warning view-button "><i class="fa fa-trash" aria-hidden="true"></i> '.t("Supprimer l'image").'</a>
                        </div>';
            }
            $out .=' </div></div>';
            /*
               $out .='<span id="input_'.$f_id.'"></span><input
               name        ="'.$id.'['.$f_id.']"
               id          ="'.$f_id.'"
               type        ="'.$ref[$f_id]['type'].'"
               value       ="'.(($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] ).'"
               size        ="'.$ref[$f_id]['size'].'"
               maxlength   ="'.$ref[$f_id]['maxlength'].'"
               pattern     ="'.$ref[$f_id]['pattern'].'"
               errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
               class       ="'.$ref[$f_id]['class'].'"
               /><div id="fichiers'.$f_id.'"> </div> ';
               $src = '';
               if( ! empty ($values[$f_id]) ){
               $src = $values[$f_id];
               } else {
            // Si on n'a pas la valeur en base, on regarde si toutefois on ne peut pas retrouver le fichier si les parametres sont definis
            if(isset($GLOBALS['obj'][$id]->_upload_params[$f_id]['dir']) && isset($GLOBALS['obj'][$id]->_upload_params[$f_id]['field_used']) && !empty($values[$GLOBALS['obj'][$id]->_upload_params[$f_id]['field_used']])) {
            $file_name = $GLOBALS['obj'][$id]->_upload_params[$f_id]['dir'].'/'.$values[$GLOBALS['obj'][$id]->_upload_params[$f_id]['field_used']];
            $exts = isset($GLOBALS['obj'][$id]->_allowed_ext) ? $GLOBALS['obj'][$id]->_allowed_ext : array('.jpg');
            foreach($exts as $ext) { if(file_exists($GLOBALS['conf']['dir']['usr'].'/'.$file_name.$ext)) $src = $file_name.$ext; }
            }
            }
            if(! empty($src))
            $out .= '&nbsp;<a href="/usr/'.$src.'" target="fileview"><img src="templates/default/images/kview.png" style="border:0; text-decoration:none;"/></a>';
             */
            break;


            /*	}}}	*/
            /*	calendar {{{ */
        case 'datetime':
        case 'calendar':
            $format_date = isset($ref[$f_id]['format']) ? $ref[$f_id]['format'] : 'format-d-m-y';
            if(isset($ref[$f_id]['callBack']) && !empty($ref[$f_id]['callBack'])){
              if(isset($ref[$f_id]['args']) && !empty($ref[$f_id]['args']) && !empty($values[$ref[$f_id]['args']])) {
              # XXX eval is devil
                eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$ref[$f_id]['args']].'\');');
              } else {
              # XXX eval is devil
                if($values[$f_id] != '') eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$f_id].'\');');
                else $value = $ref[$f_id]['value'];
              }
            } else {
              $value = (($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] );
            }
            $out .='<input 
              name        ="'.$id.'['.$f_id.']"
              id          ="'.$f_id.'"
              type        ="text" 
              value       ="'.$value.'" 
              size        ="'.$ref[$f_id]['size'].'" 
              maxlength   ="'.$ref[$f_id]['maxlength'].'" 
              onclick     ="'.$ref[$f_id]['onclick'].'"
              ' . (isset($ref[$f_id]['pattern']) && $ref[$f_id]['pattern'] != '' ? 'pattern     ="'.$ref[$f_id]['pattern'].'"' : '') . '
              errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
              class       ="'.$format_date.' '.$ref[$f_id]['class'].' '.(($ref[$f_id]['type']=='datetime') ? 'datetimepicker' : 'datepicker').'"

              style       ="'.$ref[$f_id]['style'].'"
              />';
            break;
            /*	}}}	*/
            /*	autocompletion {{{ */
/*        case 'autocompletion':
            if(isset($ref[$f_id]['callBack']) && !empty($ref[$f_id]['callBack'])){
              if(isset($ref[$f_id]['args']) && !empty($ref[$f_id]['args']) && !empty($values[$ref[$f_id]['args']])) {
                eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$ref[$f_id]['args']].'\');');
              } else {
                if($values[$f_id] != '') eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$f_id].'\');');
                else $value = $ref[$f_id]['value'];
              }
            } else
              $value = (($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] );

            $out .='<input
              id          ="'.$f_id.'"
              type        ="text"
              value       ="'.$value.'"
              size        ="'.$ref[$f_id]['size'].'"
              maxlength   ="'.$ref[$f_id]['maxlength'].'"
              onclick     ="'.$ref[$f_id]['onclick'].'"
              pattern     ="'.$ref[$f_id]['pattern'].'"
              errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
              class       ="'.$ref[$f_id]['class'].'"
              style       ="'.$ref[$f_id]['style'].'"
              />';

            #	resultFilledId : S'il y a ce parametre, on stocke dans un champ hidden l'id  du champs a mettre a jour
#                , j'ai pas trouvé mieux vu que l'Autocompleter ne permet pas de passer de params ... 
            if(isset($ref[$f_id]['resultFilledId']) && !empty($ref[$f_id]['resultFilledId']))
              $out .= '<input type="hidden" id="resultFilled_'.$f_id.'" value="'.$ref[$f_id]['resultFilledId'].'"/>';
            $out .= "
              <div id='update_".$f_id."'></div>
              <script type='text/javascript'>
              new Ajax.Autocompleter('".$f_id."', 'update_".$f_id."', '/auto_complete.php', {method: 'post', paramName: '".$f_id."'});
            </script>";
            break;*/
            /*	}}}	*/
        case 'generated':
            $out .= $ref[$f_id]['value'];
            break;
        case 'autocomplete':
            if(isset($ref[$f_id]['callBack']) && !empty($ref[$f_id]['callBack'])) {
                if(isset($ref[$f_id]['args']) && !empty($ref[$f_id]['args']) && !empty($values[$ref[$f_id]['args']])) {
                    eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$ref[$f_id]['args']].'\');');
                } else {
                    if($values[$f_id] != '') eval('$value = '.$ref[$f_id]["callBack"].'(\''.$values[$f_id].'\');');
                    else $value = $ref[$f_id]['value'];
                }
            } else {
                $value = (($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] );
            }
            if($this->view_only === true || isset($ref[$f_id]['view_only'])) {
                $out .= '<p class="read-only">'.$value.'</p>';
                break;
            }

            $acVals = array();
            $acIds = array();
            if(array_key_exists('linked_object', $ref[$f_id])) {
                $linked_object = $ref[$f_id]['linked_object'];
                if(!isset( $this->collector[$form_name.'_'.$linked_object])){
                    $this->warning(t("Erreur: l'objet appelé n'existe pas, vérifiez la table correspondante en base et relancez un build").'<br /><b>'.$form_name.'_'.$linked_object.'<b>');
                }
                $linkedField = array_key_exists('linked_field', $ref[$f_id]) ? $ref[$f_id]['linked_field'] : 'id_'.$linked_object;
                $this->collector[$form_name.'_'.$linked_object]->SetUsedFields(array($linkedField));

                $current = @array_values($this->collector[$form_name.'_'.$linked_object]->get($idToEdit));
                if(!is_array($current)) $current = array();
                foreach($current as $k=>$v) {
                    $acVals[$linkedField][]=$v[$linkedField];
                    $acIds[] = $v[$linkedField];
                }
            } else {
                $acVals = array($f_id => array($value));
                $acIds = array($value);
            }

            $displayValues = array();
            $displayValue = '';
            foreach($acVals as $acField => $acValues) {
                foreach($acValues as $acValue) {
                    if(substr($acField, 0, 3) == 'id_' && '' != $acValue) {
                        $collector = str_replace(array('id_', '_dest'), '', $acField);
                        if(array_key_exists($collector, $this->collector)) {
                            $col = $this->collector[$collector];
                            $searchFields = $col->getAutocompleteSearchFields(true);
                            $tmp = $col->getOne((int) $acValue);
                            if(is_array($tmp)) {
                                $lib = array();
                                foreach($searchFields as $field) {
                                    $lib[] = $tmp[$field];
                                }
                                $displayValues[$acValue] = implode($col->getAutocompleteResultSeparator(), $lib);
                            }
                        }
                    }
                }
            }
            $isMultiple = array_key_exists('ac_type', $ref[$f_id]) && $ref[$f_id]['ac_type'] == 'jqueryUi';
            $displayValue = $isMultiple ? '' : implode(', ', $displayValues);
    
            $patternAttr = $ref[$f_id]['pattern'] != '' ? ' pattern="' . $ref[$f_id]['pattern'] . '" ' : '';
            $out .= '
                <input 
                    placeholder ="' . $ref[$f_id]['placeholder'] . '"
                    id          ="ac_'.$f_id.'"
                    name        ="ac_'.$id.'['.$f_id.']"
                    type        ="'.$ref[$f_id]['type'].'" 
                    value       ="'.$displayValue.'" 
                    size        ="'.$ref[$f_id]['size'].'" 
                    maxlength   ="'.$ref[$f_id]['maxlength'].'" 
                    onclick     ="'.$ref[$f_id]['onclick'].'"
                    data-url    ="'.$ref[$f_id]['data-url'].'"
                    ' . $patternAttr . '
                    errorMsg    ="'.$ref[$f_id]['errorMsg'].'"
                    class       ="'.$ref[$f_id]['class'].' gen_autocomplete"
                    style       ="'.$ref[$f_id]['style'].'"
                    ' . (isset($ref[$f_id]['readonly']) ? 'readonly="readonly"' : '') . '
                    />';
            $out .= '
            <input type="hidden" name="'.$id.'['.$f_id.']" id="'.$f_id.'" value="'.implode(',', $acIds).'" />
            ';
            if($isMultiple) {
                $out .= '
                <div class="chosen-container chosen-container-multi ui_ac_chosen" id="' . $f_id . '_chosen">
                    <ul class="chosen-choices">';
                foreach($displayValues as $acK => $acV) {
                    $out .= '
                    <li class="ac_item search-choice" data-value="' . $acK . '">
                        <span>' . $acV . '</span>
                        <a class="search-choice-close" data-option-array-index="'. $acK .'"></a>
                    </li>';
                }
                $out .= '</ul></div> ';
            }
            if(isset($ref[$f_id]['extra_html'])) {
                $out .= $ref[$f_id]['extra_html'];
            }
            break;
        case 'files_moxman':
            $linked_object = $ref[$f_id]['linked_object'];
            if(!isset( $this->collector[$form_name.'_'.$linked_object])){
                $this->warning(t("Erreur: l'objet appelé n'existe pas, vérifiez la table correspondante en base et relancez un build").'<br /><b>'.$form_name.'_'.$linked_object.'<b>');
            }
            $linked_values = array();
            if(is_array($idToEdit) && current($idToEdit) != '') {
                $current = @array_values($this->collector[$form_name.'_'.$linked_object]->get($idToEdit, (array_key_exists('n_order', $this->collector[$form_name.'_'.$linked_object]->array_fields) ? 'n_order' : '')));
                if(!is_array($current)) $current = array();
                foreach($current as $k=>$v){
                    $linked_values[]=$v['id_'.$form_name . '_' . $linked_object]; 
                }
            } elseif(array_key_exists('values', $ref[$f_id]) && is_array($ref[$f_id]['values'])) {
                $linked_values = $ref[$f_id]['values'];
            }
            $out .= '<div class="row multi_moxman">
                        <button class="addRow btn" data-field="' . $f_id . '" data-obj="' . $form_name . '">' . t("Ajouter une image") . '</button>
                        ';

            if(is_array($linked_values) && count($linked_values)) {
                foreach($linked_values as $idx => $fileId) {
                    $baseRef = array(
                        $f_id => array(
                            'label' => t("Fichier"),
                            'type' => 'file', 
                            'browseOptions' => 'no_host:true,width:\'1024\',insert_filter: function(file) { file.url = file.url.replace(/\/\//g, \'/\'); file.url = file.url.replace(/http(s?):(\/?)\/admin(-?v?2?)\.decleor\.com/gi, \'\');}',
                            'value' => '',
                            'name' => 'gen_' . $form_name . '[' . $f_id . '][' . $fileId . ']'
                        )
                    );
                    $fileRow = $this->collector[$form_name . '_' . $linked_object]->getOne((int) $fileId);
                    $out .= '<div class="picture-row">';
                    $out .= '<span class="sorthandle"></span>';
                    $out .= $this->getFieldHtml($baseRef, $f_id, $label, 'gen_' . $form_name, array($f_id => $fileRow['path']), 'dyn_bloc');
                    $out .= '<a class="delete-row" title="Delete" data-id="' . $fileId . '" data-obj="' . $form_name . '"><img src="/assets/images/delete.png" alt="Delete" /></a>';
                    $out .= '</div>';
                }
            } else {
                $out .= '<div class="picture-row"><p class="no-pic">'.t("Aucune image pour le moment").'</p></div>';
            }
            $out .= '</div>';
            break;
        case 'files':
            $fieldButtons = '';
            foreach($ref[$f_id]['fields'] as $upField => $upLabel) {
                if($this->view_only === true) {
                    $fieldButtons .= '<input class="fupload_readonly" type="hidden" id="'.$upField.'" name="'.$id.'['.$upField.'][]">';
                } else {
                    $fieldButtons .= '
                        <span class="btn btn-sm green fileinput-button">
                        <i class="fa fa-plus"></i>
                        <span> ' . $upLabel . ' </span>
                        <input type="file" id="'.$upField.'" name="'.$id.'['.$upField.'][]" multiple> </span>';
                }
            }
            if($this->view_only === true) {
                $out .= $fieldButtons;
            } else {
                $out .= '
                <div class="row fileupload-buttonbar">
                    <div class="col-lg-12">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        ' . $fieldButtons . '
                        <!--<button type="submit" class="btn btn-sm blue start">
                            <i class="fa fa-upload"></i>
                            <span> Envoyer tout </span>
                        </button>
                        <button type="reset" class="btn btn-sm dark cancel">
                            <i class="fa fa-ban-circle"></i>
                            <span> Annuler tout </span>
                        </button>
                        <button type="button" class="btn btn-sm red delete">
                            <i class="fa fa-trash"></i>
                            <span> Supprimer la sélection </span>
                        </button>
                        <input type="checkbox" class="toggle">-->
                        <!-- The global file processing state -->
                        <span class="fileupload-process"> </span>
                    </div>
                    <!-- The global progress information -->
                    <div class="col-lg-5 fileupload-progress fade">
                        <!-- The global progress bar -->
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;"> </div>
                        </div>
                        <!-- The extended global progress information -->
                        <div class="progress-extended"> &nbsp; </div>
                    </div>
                </div>';
            }
            $out .= '
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped clearfix">
                    <tbody class="files"> </tbody>
                </table>
            ';

            // attention : fileupload ne gère qu'un formulaire à la fois
            $out .= '<input type="hidden" value="'.$ref[$f_id]['directory'].'" id="directory_fileupload" />';

            break;

        case 'colorpicker':
          $out .='<input type="text" class="colorPicker" name="'.$id.'['.$f_id.']" value="'. (($values[$f_id] == '') ? $ref[$f_id]['value']  : $values[$f_id] ) .'" />';
          $out .='<input type="hidden" class="setColors" value="' . htmlentities(json_encode($ref[$f_id]['colors'])) . '" />';
          break;
      }
      return $out;
    }

    public function getFieldStruct() 
    {
        return $this->_field_struc;
    }
    public function getCheckLinkedCollector() 
    {
        return $this->_checkLinkedCollector;
    }
    public function setCheckLinkedCollector($status) 
    {
        $this->_checkLinkedCollector = (bool) $status;
    }
    /**
     * Get Moxiemanager callback options
     *
     * @static
     * @param GenClass $collector
     * @param string $moxmanOpts
     * @return string
     */
    public static function getMoxmanCallbacks(GenClass $collector, $moxmanOpts = '') 
    {
        if(method_exists($collector, 'getLogEvents') && $collector->getLogEvents() === true) {
            $callbacks = array('onupload', 'onsave', /*'oninsert',*/ 'oncreate', 'ondelete');
            foreach($callbacks as $cb) {
                if(false === strstr($moxmanOpts, $cb)) {
                    $moxmanOpts .= (empty($moxmanOpts) ? '' : ',') . $cb . ':function(args){logAct(\'' . str_replace('on', '', $cb) . '\', args);}';
                }
            }
        }
        return $moxmanOpts;
    }


    public static function addKvFields($module_name){
        global $form, $gen;
        if(isset($gen->collector[$module_name]->KVS_Properties['keys']) && is_array($gen->collector[$module_name]->KVS_Properties['keys']) && !empty($gen->collector[$module_name]->KVS_Properties['keys'])){
            $form->fields['_add'] =  'Champs additionnels';
            foreach($gen->collector[$module_name]->KVS_Properties['keys'] as $k=> $field){
               switch ($field['type']){
                    case 'text':
                    case 'number':
                        $form->fields[$k]=array_merge($form->gen_field['lib'],array('label'=>$field['lib'],'kv'=>true));
                    break;
                    case 'select':
                    case 'radio':
                        $form->fields[$k]=array_merge($form->gen_field['select'],array('label'=>$field['lib'],'kv'=>true,'arrayValues'=>$field['values']));
                    break;
                    case 'content':
                        $form->fields[$k]=array_merge($form->gen_field['desc'],array('label'=>$field['lib'],'kv'=>true));
                    break;
                    case 'image':
                        $form->fields[$k]=array_merge($form->gen_field['file'],array('label'=>$field['lib'],'kv'=>true));
                    break;
               }
            }
        }
        return $form;
    }

    public function getCurrentId(){
        return $this->current_id;
    }
}
