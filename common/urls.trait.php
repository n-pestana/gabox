<?php
trait urls
{
    /**
     * Force delete on _url table before generation (avoid request to search for existing url)
     *
     * @var boolean
     */
    public $deleteBeforeBuildUrl = false;
    /**
     * Build only items with column url === null
     *
     * @var boolean
     */
    public $buildOnlyEmptyUrl = false;
    /**
     * Use treeview for URL creation
     *
     * @var boolean
     */
    public $use_treeview = true;
    /**
     * Append .html to url ?
     *
     * @var string
     */
    public $urlSuffix = '.html';
    /**
     * Prepend URL_LOC to canonical url ?
     *
     * @var boolean
     */
    public $prependLangToCanonical = false;
    /**
     * Separator between $_canonical_url element
     *
     * @var string
     */
    protected $_separator = '-';
    /**
     * Field(s) used for canonical url creation 
     *
     * @access protected
     * @var array
     */
    public $_canonical_url = array('lib');
    /**
     * Table(s) used by resetProductsUrl method to delete extra data
     *
     * @access protected
     * @var array
     */
    protected $_resetTables = array();
    /**
     * Treeview hook used in final url (Used instead of treeview field)
     *
     * @access protected
     * @var string
     */
    protected $_treeviewHook = null;
    /**
     * Set treeview hook used in final url
     *
     * @param string $field
     * @return Oils
     */
    public function setTreeviewHook($field)
    {
        $this->_treeviewHook = $field;
        return $this;
    }
    /**
     * Get treeview field
     *
     * @return string
     */
    public function getTreeviewHook() 
    {
        return $this->_treeviewHook;
    }
    /**
     * Treeview field used in final url
     *
     * @access protected
     * @var string
     */
    protected $_treeviewField = 'id_treeview';
    /**
     * Set treeview field used in final url
     *
     * @param string $field
     * @return Oils
     */
    public function setTreeviewField($field = null)
    {
        $this->_treeviewField = $field;
        return $this;
    }
    /**
     * Get treeview field
     *
     * @return string
     */
    public function getTreeviewField() 
    {
        return $this->_treeviewField;
    }

    /**
     * @return string
     */
    public function getSeparator(): string {
        return $this->_separator;
    }

    /**
     * @param string $separator
     */
    public function setSeparator(string $separator) {
        if (strlen($separator) !== 1) {
            throw new LogicException('Separator has to be only one character');
        }
        $this->_separator = $separator;
    }

    # possibilité de changer l'apparence de l'url produit : 
    # Par exemple  array(
    #     'id_villes'=>array('villes.nom','villes.departement')
    #     ,'lib'
    #     );
    # ça va chercher les relations
    public function makeUrl($p,$update = true, $add_html = false)
    {
        if(is_int($p)){
            $p = $this->getOneI18n($p);
        }
        foreach ($this->_canonical_url as $url_k=>$url_v) {
            if (!is_array($url_v)) {
                $temp_url[] = urlizeUtf8(strtolower($p[$url_v]));
            } else {
                foreach ($url_v as $key => $url_part) {
                    if ($key === 'makeurl' && isset($this->collector[$url_part])) {
                        $temp_url[] = $this->collector[$url_part]->makeUrl($this->collector[$url_part]->getCached($p[$url_k]), $update, $add_html);
                    } else {
                        $temp = explode('.', $url_part);
                        if (!isset($p[$url_k]) || empty($p[$url_k])) {
                            continue;
                        }
                        $temp2 = $this->collector[$temp[0]]->getCached($p[$url_k]);
                        if (empty($temp2)) {
                            continue;
                        }
                        $temp_url[] = urlizeUtf8(strtolower($temp2[$temp[1]]));
                    }
                }
            }
        }
        $p['url'] = implode($this->getSeparator(), $temp_url);
        if($add_html === true) $p['url'] .= '.html';
        if($update !== false && in_array('url', array_keys($this->array_fields))) {
            parent::set($p[$this->primary_key], array('url' => $p['url']));
        }
        return $p['url'];
    }
 
    // Retro compatibilité   
    public function resetProductsUrl()
    {
        return $this->resetCollectorUrl();
    }
    /**
     * Reset collector URL data 
     *
     * @param boolean $updateOriginUrl
     * @return void
     */
    public function resetCollectorUrl($updateOriginUrl = true)
    {
        $sql = array(
            "TRUNCATE " . $this->table_name . "_url"
        );
        if($updateOriginUrl === true) {
            $sql[] = "UPDATE " . $this->table_name . " SET url = ''";
        }
        foreach($this->_resetTables as $table => $where) {
            if(empty($where)) {
                $sql[] = 'TRUNCATE ' . $table;
            } else {
                $sql[] = 'DELETE FROM ' . $table . ' WHERE ' . $where;
            }
        }
        foreach($sql as $query) {
            $this->query($query);
        }
    }

    public function BuildUrl($id='')
    {
        if ($this->deleteBeforeBuildUrl && !$this->buildOnlyEmptyUrl) {
            $this->query("TRUNCATE TABLE " . $this->table_name . "_url");
        }
        $currentClass = strtolower(get_class($this));
        $logOrigins = array();
        // Set log origin to "SYSTEM" for updated collectors
        foreach(array($currentClass . '_url', $currentClass . '_url_history') as $col) {
            if(array_key_exists($col, $this->collector) && method_exists($this->collector[$col], 'setLogOrigin')) {
                $logOrigins[$col] = $this->collector[$col]->getLogOrigin();
                $this->collector[$col]->setLogOrigin('SYSTEM');
            }
        }
        //$this->disableCache();
        $conditions = array();
        if(array_key_exists('is_valid',$this->array_fields)) {
            $conditions['is_valid']=1;
        }

        if(array_key_exists('status',$this->array_fields)) {
            $conditions['status']='OK';
        }

        if ($this->buildOnlyEmptyUrl) {
            $conditions['url'] = null;
        }

        $cond = (empty($id)) ? $conditions : array($this->primary_key=>$id);

        # xxx bug on peut get plus que ça !!??
        # XXX a essayer avec mysqli
        // KM: Il semblerait que ce soit un problème de mémoire interne, getRows d'adodb étant semble-t-il assez gourmand
        //$urlItems = $this->get($cond, '', 5999);
        $urlItems = $this->get($cond, '');

        //$id_site = ($this->collector['treeview']->use_site_id && defined('ID_SITE')) ? ID_SITE  : 0;
        if(defined('ID_SITE')) {
           $sites = $this->collector['sites']->get(array('id_sites'=>ID_SITE));
        } else {
            $sites = $this->collector['sites']->get();
        }

        $hookTreeview = null;
        $hookCache = null;
        //On regarde s'il y a des entrées dans le collector i18n pour la classe afin d'éviter des requêtes inutiles
        $checkI18nCache = $this->collector['i18n']->getOne(array('module' => $currentClass));

        if(is_array($sites))foreach($sites as $site) {
            $id_site = $site['id_sites'];

            $locales = $this->collector['sites']->getAvailabledIdLocalesForSite($id_site, false);

            if(is_array($urlItems) && !empty($urlItems))
            foreach($urlItems as $p){
                if(!is_array($p)){
                    continue; 
                }
                if(!empty($this->_treeviewField) && empty($p[$this->_treeviewField]) || (empty($this->_treeviewField) && empty($this->_treeviewHook))) {
                    continue;
                }
                if(empty($p['url'])) {
                    $p['url'] = $this->makeUrl($p);
                }

                if(array_key_exists($currentClass . '_treeview', $this->collector)) {
                    $this->collector[$currentClass . '_treeview']->RollBackUsedFields();
                    $this->collector[$currentClass . '_treeview']->SetUsedFields(array('id_treeview'));
                }
                # on selectonne tous les cats affectés pour cet élément + la main cat
                $this->query("DELETE FROM " . $this->table_name . "_url WHERE " . $this->primary_key . " = " . $this->db->qstr($p[$this->primary_key]) . " AND id_site = " . $this->db->qstr($id_site));
                
                if(array_key_exists($currentClass . '_treeview', $this->collector)) {
                    $temp = $this->collector[$currentClass . '_treeview']->get(array($this->primary_key => $p[$this->primary_key]));
                }
                if(!isset($temp) || (isset($temp) && !is_array($temp))) $temp = array();

                if(!empty($this->_treeviewField)) {
                    $treeviewUrlFieldName = $this->_treeviewField;
                    $all_treeview = array_merge($temp, array(array($this->_treeviewField=>$p[$this->_treeviewField],'is_canonical'=>1))); 
                } else {
                    $treeviewUrlFieldName = 'id_treeview';
                    if(null === $hookTreeview) {
                        $hookTreeview = $this->collector['treeview']->getOneByHook($this->_treeviewHook);
                    }
                    $all_treeview = array(array($treeviewUrlFieldName => $hookTreeview['id_treeview'],'is_canonical'=>1));
                }

                foreach($locales as $locale) {
                    $finale_url = '';
                    $is_canonical = 1;
                    if ($checkI18nCache) {

                        $id_loc_master = $this->getIdLocMasterByLoc($locale);

                        $i18n_url = $this->collector['i18n']->getOne(array(
                            'id_locales' => $locale
                            , 'module' => $currentClass
                            , 'field_name' => 'url'
                            , 'id_element' => $p[$this->primary_key]
                        ));

                        # si url traduite
                        if (isset($i18n_url['lib']) && (!empty($i18n_url['lib']))) {
                            $finale_url = strtolower(urlizeUtf8($i18n_url['lib']));
                        } else {
                            # si y'a une langue master correspondante
                            if ($locale != $id_loc_master) {
                                # le master a il une url traduite lui ?
                                $i18n_url_master = $this->collector['i18n']->getOne(array(
                                    'id_locales' => $id_loc_master
                                    , 'module' => $currentClass
                                    , 'field_name' => 'url'
                                    , 'id_element' => $p[$this->primary_key]
                                ));
                                if (isset($i18n_url_master['lib']) && !empty($i18n_url_master['lib'])) {
                                    # on set pas produit, on laisse en mode cascade
                                    $finale_url = strtolower(urlizeUtf8($i18n_url_master['lib']));
                                }
                            }
                            #le master n'a pas d'url traduite, on va voir si lib traduit
                            if (empty($finale_url)) {
                                # si on a un lib traduite pas vide
                                if (!empty($p['url'])) {
                                    $finale_url = strtolower(urlizeUtf8($p['url']));
                                } else {
                                    $i18n_lib = $this->collector['i18n']->getOne(array(
                                        'id_locales' => $locale
                                        , 'module' => $currentClass
                                        , 'field_name' => 'lib'
                                        , 'id_element' => $p[$this->primary_key]
                                    ));
                                    $finale_url = strtolower(urlizeUtf8($i18n_lib['lib']));
                                }
                            }
                        }
                    } else {
                        $finale_url = strtolower(urlizeUtf8($p['url']));
                    }
                    $finale_url = preg_replace('/' . $this->urlSuffix .'$/', '', $finale_url);
                    
                    foreach($all_treeview as $all_treeviewK=>$treeview) {
                        $fullurl = '';
                        if ($this->use_treeview) {
                            // Si on est sur un hook tjs identique, on le get une seule fois
                            if (!isset($hookCache[$locale])) {
                                $fullurl = $this->collector['treeview_url']->getOne(array(
                                    'id_locale' => $locale
                                    , 'id_treeview' => $treeview[$treeviewUrlFieldName]
                                    , 'id_site' => $id_site
                                ));
                                $fullurl = $fullurl['url'];
                                if (empty($this->_treeviewField)) {
                                    $hookCache[$locale]['url'] = $fullurl;
                                }
                            } else {
                                $fullurl = $hookCache[$locale]['url'];
                            }
                        }

                        if ($this->deleteBeforeBuildUrl) {
                            $exist = false;
                        } else {
                            $exist = $this->collector[$currentClass . '_url']->getOne(array(
                                'id_site' => $id_site
                                , 'id_locale' => $locale
                                , $this->primary_key => $p[$this->primary_key]
                                , 'id_treeview' => $treeview[$treeviewUrlFieldName]
                            ));
                        }

                        $doublon = $this->collector[$currentClass . '_url']->getOne(array(
                            'id_site'       => $id_site 
                            ,'id_locale'    => $locale
                            ,'url'          => $fullurl.$finale_url . $this->urlSuffix
                            ,'!=' . $this->primary_key => $p[$this->primary_key]
                        ));
                        if(isset($doublon[$this->primary_key])){
                            $finale_url= $finale_url.'-'.$p[$this->primary_key]; 
                        }
                        $this->collector[$currentClass . '_url']->set( (isset($exist[$this->primary_key . '_url'])) ? $exist[$this->primary_key . '_url'] : '', array(
                            'id_site'       => $id_site 
                            ,'id_locale'    => $locale
                            ,$this->primary_key  => $p[$this->primary_key]
                            ,'is_canonical' => (isset($treeview['is_canonical'])) ? 1 : 0 //$is_canonical
                            ,'url'          => trim($fullurl.$finale_url . $this->urlSuffix)
                            ,'id_treeview'  => $treeview[$treeviewUrlFieldName]
                        ));

                        # si table d'historisation, on garde l'historique
                        if(isset($this->collector[$currentClass. '_url_history'])){
                            $this->collector[$currentClass . '_url_history']->set('', array(
                                'id_site'       => $id_site 
                                ,'id_locale'    => $locale
                                ,$this->primary_key  => $p[$this->primary_key]
                                ,'is_canonical' => (isset($treeview['is_canonical'])) ? 1 : 0
                                ,'url'          => trim($fullurl.$finale_url . $this->urlSuffix)
                                ,'id_treeview'  => $treeview[$treeviewUrlFieldName]
                            ));
                        }
                    }
                }
            }
        }
        //$this->enableCache();
        foreach($logOrigins as $col => $ori) {
            $this->collector[$col]->setLogOrigin($ori);
        }
    }
   
    public function getCanonicalUrl($idElement, $idloc='', $id_site='')
    {
        if(empty($idloc)) $idloc = defined('ID_LOC') ? ID_LOC : '';
        $currentClass = strtolower(get_class($this));
        # utilisation de treeview à la place du menus et des pcats.
        if($this->use_treeview  === true){
            if(isset( $this->collector[$currentClass . '_url'])){
                if($idloc){
                    $temp = $this->collector[$currentClass . '_url']->getOneI18n(
                        array(
                             'id_locale' => $idloc
                            ,'id_site' => (!empty($id_site)) ? $id_site : (defined('ID_SITE') ? ID_SITE : null)
                            ,$this->primary_key => $idElement
                            ,'is_canonical' => true
                        )
                    );
                }
                else{
                    $temp = $this->collector[$currentClass . '_url']->getOneI18n(
                        array(
                            'id_site' => (!empty($id_site)) ? $id_site : (defined('ID_SITE') ? ID_SITE : null)
                            ,$this->primary_key => $idElement
                            ,'is_canonical' => true
                        )
                    );
                }
            }
            return strtolower(($this->prependLangToCanonical ? URL_LOC : '') . $temp['url']);
        } else {
            die('error');
        }
    }

    public function getAllAvailableUrlForAProduct($idElement) 
    {
        # utilisation de treeview à la place du menus et des pcats.
        $currentClass = strtolower(get_class($this));
        $temp = $this->collector[$currentClass . '_url']->getCached(
            array(
                 'id_locale'=>ID_LOC
                ,'id_site'=>ID_SITE
                ,$this->primary_key=>$idElement
            )
        );
        return $temp;
    }

    public function getElementByUrl($url)
    {
        $currentClass = strtolower(get_class($this));
        $is_page = (strstr($url,'.html')) ? true : false;
        # utiliser if(empty($id_loc_master)) $id_loc_master = $this->getIdLocMasterByLoc($id_loc);
        if($this->use_treeview  === true) {
            if(!$is_page){
                return $this->collector['treeview']->getByUrl($url);
            } else {
                $temp = $this->collector[$currentClass . '_url']->getCached(
                    array(
                         'id_locale'=>ID_LOC
                        ,'id_site'=>ID_SITE
                        ,'url' => $url
                    )
                );
                if($temp) {
                    foreach($temp as $t) {
                        if(isset($t[$this->primary_key])) {
                            $p = $this->collector[$currentClass]->getOneI18n($t[$this->primary_key]);
                            if($p['is_valid'] && $p['status'] == 'ok') return $p;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function getOtherLinks($idElem)
    {
        $languages  = $this->collector['sites']->getAvailabledLanguageForCountry($_SERVER['id_site'],$_SERVER['prod']);
        $in = array();
        foreach($languages as $lang){
            $loc[$lang['id_locales']]=$lang;
            $in[]=$lang['id_locales'];
        }
        $currentClass = strtolower(get_class($this));
        $rs = $this->collector[$currentClass . '_url']->getCached(array($this->primary_key => $idElem,'is_canonical'=>1,'id_locale'=>$in));
        return $rs;
    }

    public function getHrefLang($id)
    {
        if(!defined('IS_MULTIBRAND') || (defined('IS_MULTIBRAND') && IS_MULTIBRAND === true))
            $languages  = $this->collector['sites']->getAvailabledLanguageForCountry($_SERVER['id_site'],$_SERVER['prod']);
        else
            $languages  = $this->collector['sites']->getAvailabledLanguageForCountry('',$_SERVER['prod']);
        $in = array();
        foreach($languages as $lang){
            $loc[$lang['id_locales']]=$lang;
            $in[]=$lang['id_locales'];
        }
        $currentClass = strtolower(get_class($this));
        $rs = $this->collector[$currentClass . '_url']->getCached(array('id_' . $currentClass => $id, 'is_canonical'=>1, 'id_locale'=>$in));
        $out ='';
        if(is_array($rs)) foreach($rs as $k=>$v) {
            if(!isset($v['id_locale']))continue;
            if(!defined('IS_MULTIBRAND') || (defined('IS_MULTIBRAND') && IS_MULTIBRAND === true))
                $out .= "\n".'<link rel="alternate" href="/'.$loc[$v['id_locale']]['language'].$v['url'].'" hreflang="'.strtolower(str_replace('_','-',$loc[$v['id_locale']]['locale'])).'" />';
            else {
                if($v['id_site'] == 0) continue;
                $temp = $this->collector['sites']->getOne($v['id_site']);
                if($temp['is_published'] != 1) continue;
                $out .= "\n".'<link rel="alternate" href="'.$temp['prod_url'].'/'.$loc[$v['id_locale']]['language'].$v['url'].'" hreflang="'.strtolower(str_replace('_','-',$loc[$v['id_locale']]['locale'])).'" />';
            }
        }
        return $out;
    }

    /**
     * Get rewrited image patch
     *
     * @param integer $idElement
     * @param array $array_lib
     * @param array $size
     * @param array $additionnalNameParts
     *
     * @return string Rewrited builded path
     */
    public function getImg($idElement, $array_lib = array(), $size = array(), $additionnalNameParts = array(), $rewriteBase = 'dimg')
    {
        if(empty($array_lib)) {
            $array_lib = array($this->getLib());
        }
        if(!defined('USE_IMG_REWRITE') || (defined('USE_IMG_REWRITE') && (USE_IMG_REWRITE==1))){
            $is_empty = true;
            $simu = $this->simulation;
            foreach($array_lib as $k=>$v) {
                $array_lib[$k] = strtolower(urlizeUtf8($v));
                if(!empty($array_lib[$k])) $is_empty = false;
            }
            // if the current language can't be urlize
            if($is_empty === true) {
                $this->simulation = false;
                $array_lib = array(urlizeUtf8($this->getLib($idElement)));        
            }
            $this->simulation = $simu;
            $url = array_merge(array(current(explode('_',LOC))), array($rewriteBase), $array_lib);
            $file = array_pop($url);
            $temp = '/' . implode('/',$url) . '/' . $idElement . '-' . $file;
            if(isset($size[0]) && $size[0]>0) $temp .= '--w' . $size[0];
            if(isset($size[1]) && $size[1]>0) $temp .= '--h' . $size[1];
            if(count($additionnalNameParts) > 0) {
                $temp .= '-' . implode('-', $additionnalNameParts);
            }
            return $temp . '.jpg';
        } else {
            $t = $this->getCached($idElement);
            return $t['media'];
        }
    }
}
