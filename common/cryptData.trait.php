<?php
require_once(dirname(__FILE__) . '/../libs/random_compat-2.0.18/lib/random.php');
trait cryptData
{
    /**
     * Crypted columns
     *
     * @access protected
     * @var array
     */
    protected $_cryptedColumns = array();
    /**
     * Use internal cache overflow
     *
     * In case of big data to store in cache, database backend can be buggy (max_allowed_packet issue) when trying to save the cache data
     * This property set to true allows the cache load to test the internal cache backend if nothing has been found in the DB backend
     *
     * @access protected
     */
    protected $_useInternalCacheOverflow = true;
    /**
     * Is encryption enabled
     *
     * @access protected
     * @var boolean
     */
    protected $_encryptionEnabled = true;
    /**
     * Set encryption status
     *
     *  @var $encryption
     */
    public function setEncryptionEnabled($encryption) 
    {
        $this->_encryptionEnabled = (bool) $encryption;
    }
    /**
     * Get crypted columns
     *
     * @return array 
     */
    public function getCryptedColumns() 
    {
        return $this->_cryptedColumns;
    }
    /**
     * Set crypted columns
     *
     * @param array $cols
     * @return GenClass
     */
    public function setCryptedColumns($cols = array()) 
    {
        $this->_cryptedColumns = $cols;
        return $this;
    }
    /**
     * Get crypt key
     *
     * @return string
     */
    public function getCryptKey($password, $salt) 
    {
        $key = hash_pbkdf2("sha1", $password, $salt, 100, 32, TRUE);
        return $key;
    }
    /**
     * Decrypt data with AES
     *
     * @params string $cipherText
     * @params string $key
     * @return mixed
     */
    public function decryptWithAes($cipherText, $key) 
    {
        $iv = substr($cipherText, 0, 16);
        $encryptedText = substr($cipherText, 16);
        $data = openssl_decrypt($encryptedText, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);

        if (ord($data[0]) == 1) {
            $signature = substr($data,1, 32);
            $content = substr($data, 32 + 1);
            $computedSignature = hash('sha256', $content, true);

            if ($signature != $computedSignature) {
                return false;
            } else {
                return $content;
            }
        }
        return false;
    }
    /**
     * Encrypt data with AES
     *
     * @params string $plainText
     * @params string $key Hexadecimal encoded key (via hex2bin)
     * return string
     */
    public function encryptWithAes($plainText, $key) 
    {
        $iv = random_bytes(16);
        $sha256 = hash('sha256', $plainText, true);

        $contentToEncrypt = chr(1) . $sha256 . $plainText;
        $data = openssl_encrypt($contentToEncrypt, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
        return $iv . $data;
    }
    /**
     * Crypt data based on collector's configuration
     *
     * @params array $data
     * @return void
     */
    public function cryptData(&$data = array()) 
    {
        if(!is_array($this->_cryptedColumns) || false === $this->_encryptionEnabled) return;
        $key = $this->getCryptKey(CRYPT_KEY_PWD, hex2bin(CRYPT_KEY_SALT));
        foreach($this->_cryptedColumns as $col) {
            if(array_key_exists($col, $data) && trim($data[$col]) != '') {
                $crypted = $this->encryptWithAes($data[$col], $key);
                $newVal = bin2hex($crypted);
                $data[$col] = $newVal;
                if(array_key_exists($col, $this->_diffSavedValues) && array_key_exists('new', $this->_diffSavedValues[$col])) {
                    $this->_diffSavedValues[$col]['new'] = $newVal;
                }
            }
        }
    }
    /**
     * Decrypt data based on collector's configuration
     *
     * @params array $data
     * @return void
     */
    public function decryptData(&$data = array(), $filteredCryptedCols = array()) 
    {
        if(!is_array($this->_cryptedColumns) || false === $this->_encryptionEnabled) return;
        $key = $this->getCryptKey(CRYPT_KEY_PWD, hex2bin(CRYPT_KEY_SALT));
        foreach($data as $i => &$row) {
            foreach($this->_cryptedColumns as $col) {
                if(array_key_exists($col, $row) && trim($row[$col]) != '') {
                    $decrypted = $this->decryptWithAes(hex2bin($row[$col]), $key);
                    $row[$col] = $decrypted;
                    // Application des filtres sur les valeurs décryptées
                    // TODO: Implémenter la solution en base via des blind index chiffrés si utilisation sur un volume de données plus conséquent
                    if(array_key_exists($col, $filteredCryptedCols) && false === stristr($row[$col], $filteredCryptedCols[$col])) {
                        $this->OutputDebug('CryptData', 'Unset filtered crypted data "' . $row[$col] . '"');
                        unset($data[$i]);
                    }
                }
            }
        }
        // S'il y a des filtres, on réassigne les clés pour éviter tout souci avec les getOne
        if(count($filteredCryptedCols) > 0) $data = array_values($data);
    }
    /**
     * Get filtered crypted columns
     *
     * @access protected
     * @params array $filters
     * @return array $columns
     */
    protected function _getFilteredCryptedColumns(&$criteres = array())
    {
        $columns = array();
        if(!is_array($this->_cryptedColumns) || false === $this->_encryptionEnabled) return $columns;
        foreach($criteres as $field => $val) {
            $baseField = $field;
            $field = $this->cleanFieldName($field);
            if(in_array($field, $this->_cryptedColumns)) {
                $columns[$baseField] = $val;
                unset($criteres[$baseField]);
            }
        }
        return $columns;
    }
}
