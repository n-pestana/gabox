<?php 
class Treeview extends gen_treeview
{
    var $menu_order = 0;
    var $menu_level = 1;
    var $out         = '';
    var $id_locales = '';
    var $id_site   = '';
    var $menu = '';
    var $fullurl;
    var $use_site_id=false;
    /**
     * Get children treeview IDs in getChildren
     *
     * @access protected
     * @var array
     */
    protected $_getChildrenTreeviewIds = array();
    /**
     * Set retrieval of treeview properties in an array for further use
     *
     * @access protected
     * @var boolean
     */    
    protected $_getTreeviewProperties = false;
    /**
     * Treeview properties by id_treeview
     *
     * @access protected
     * @var array
     */
    protected $_treeviewProperties = array();

    protected $_linkedIdsLoaded = array();

    protected $_buildOnSave = true;
    /**
     * Boolean columns
     *
     * @access protected
     * @var array
     */
    protected $_boolColumns = array('is_valid');
    /**
     * Use join in product search (getProductsByTreeview) ?
     * If true, return cumulative result (products must be linked with all the categories supplied)
     *
     * @access protected
     * @var boolean
     */
    protected $_useJoinsInProductSearch = true;
    /**
     * Retrieve only valid children when using getChildrenIds ?
     *
     * @access protected
     * @var boolean
     */
    protected $_retrieveOnlyValidChildren = true;
    /**
     * Force the translation of url with the label of the item during buildUrl
     *
     * @access protected
     * @var boolean
     */
    protected $_forceBuildTranslationUrl = false;
    /**
     * Linked collectors (used for associations)
     *
     * @access protected
     * @var array
     */
    protected $_linkedCollectors = array('produits_treeview', 'produits_url', 'treeview_url');
    /**
     * Loaded last depth elements (called by getItemLastDepthElements method)
     *
     * @access protected
     * @var array
     */
    protected $_loadedLastDepthElements = array();
    /**
     * Loaded elements for select (called by getItemsHierarchyForSelect method)
     *
     * @access protected
     * @var array
     */
    protected $_loadedItemsHierarchyForSelect = [];
    /**
     * Array of treeview id selected for current site (with their children)
     *
     * @access protected
     * @var array
     */
    protected $_affectedTreeviews;
    /**
     * Array of treeview id saved by saveTreeview method
     * 
     * @access protected
     * @var array
     */
    protected $_savedIdTreeviews;

    public function set($primary_key_value = '' , $array_fields = '', $criteres = array(), $force_build=true) {
                
        if(isset($array_fields['lib']) && ($array_fields['lib'] != 'New Item') && ( empty($array_fields['url']) || is_null($array_fields['url']) )){
            $array_fields['url']=strtolower(urlizeUtf8($array_fields['lib']));
        }
        if(isset($array_fields['lib']) && ($array_fields['lib'] != 'New Item') && ( empty($array_fields['hook']) || is_null($array_fields['hook']) )){
            $array_fields['hook']=strtolower(urlize2($array_fields['lib']));
        }
        if(isset($array_fields['url']) && $array_fields['url'] == 'new-item'){
            $array_fields['url']=strtolower(urlizeUtf8($array_fields['lib']));
        }

        $id = parent::set($primary_key_value, $array_fields, $criteres);

        # DEPRECATED  16/08/2016
        #    $this->BuildUrl($id);  

        return $id;
    }
    public function setCompleted($id_treeview){
        $this->BuildUrl($id_treeview);
    }

    public function BuildUrl($id_treeview){
        $this->BuildAllUrl($id_treeview);
    }

    public function BuildAllUrl($id_treeview=''){

        $this->disableCache();

        if($id_treeview == ''){
//            die('empty treeview');
//            $this->query('truncate gen_treeview_url');
        }

        $treeviews = $this->get((empty($id_treeview)) ? null : array('id_treeview'=>$id_treeview));

        if(defined('ID_SITE')){
           $sites = $this->collector['sites']->get(array('id_sites'=>ID_SITE)); 
        }
        else{
            $sites = $this->collector['sites']->get();
        }

        if(is_array($sites))foreach($sites as $site){
            $id_site = $site['id_sites'];
            if(is_array($treeviews)) {
                foreach($treeviews as  $treeview){
                
                   if(!is_array($treeview)){
                        //echo '.';
                        continue;
                    }
                    if(empty($treeview['id_treeview'])){
                        //echo '-';
                        continue;
                    }
                    
                    if($treeview['url'] == '/') continue;

                    if(empty($treeview['url']) && !empty($treeview['lib'])) {
                        $treeview['url'] = strtolower(urlizeUtf8($treeview['lib']));
                        parent::set($treeview['id_treeview'],array('url' => $treeview['url']));
                    }

                    $locales = $this->collector['sites']->getAvailabledIdLocalesForSite($id_site, false);

                    $rs = $this->getReversePath($treeview['id_treeview']);

                    $fullurl = array();
                    foreach($rs as $k=>$v){
                        $fullurl[]=strtolower($v['url']);
                    }

                    #XXX utilité du champ fullurl ? 
                    parent::set($treeview['id_treeview'], array('fullurl'=>$this->cleanTreeviewUrl($fullurl)));
                    foreach($locales as $locale){

                        $id_loc_master = $this->getIdLocMasterByLoc($locale);
                        $rs = $this->getReversePathByLocale($treeview['id_treeview'],$locale,$id_loc_master);
                        $fullurl_i18n = array();

                        foreach($rs as $kfurl=>$vfurl){
                            $itemUrl = strtolower($vfurl['url']);
                            $libUrl = strtolower(urlizeUtf8($vfurl['lib']));
                            if($this->_forceBuildTranslationUrl === true && $libUrl != $itemUrl && $itemUrl != '/') {
                                $itemUrl = $libUrl;
                            }
                            $fullurl_i18n[] = $itemUrl;
                        }
                        $i18n_url = $this->cleanTreeviewUrl($fullurl_i18n);
                        $exist   = $this->collector['treeview_url']->getOne(array(
                            'id_site'       => $id_site 
                            ,'id_locale'    => $locale
                            ,'id_treeview'  => $treeview['id_treeview']
                        ));
                        if($i18n_url != '/' && !empty($i18n_url)){
                            $doublon= $this->collector['treeview_url']->getOne(array(
                                'id_site'       => $id_site 
                                ,'id_locale'    => $locale
                                ,'url'          => $i18n_url 
                                ,'!=id_treeview' => $treeview['id_treeview'] 
                            ));
                        }

                        # si doublon sauf si url = / (dans ce cas c'est juste un parent non affichable) 
                        if(isset($doublon['id_treeview'])&& $i18n_url!='/'){
                            $i18n_url = $i18n_url.'-'.$treeview['id_treeview']; 
                        }


                        $this->collector['treeview_url']->set( (isset($exist['id_treeview_url'])) ?  $exist['id_treeview_url'] : '', array(
                            'id_site'       => $id_site 
                            ,'id_locale'    => $locale
                            ,'url'          => $i18n_url
                            ,'id_treeview'  => $treeview['id_treeview']
                        ));

                        $loc = $this->collector['locales']->getCached($locale);
                        if($loc['is_master']==1){

                                $exist = $this->collector['i18n']->getOne(array(
                                     'id_locales'   => $locale
                                    ,'module'       => 'treeview'
                                    ,'field_name'   => 'fullurl'
                                    ,'id_element'   => $treeview['id_treeview']
                                ));

                                # si url traduite
                                if( !isset($i18n_url['lib']) ||  (!empty($i18n_url['lib'])) ){
                                
                                    $i18n_lib= $this->collector['i18n']->set(  (isset($exist['id_i18n'])) ?  $exist['id_i18n'] : '' ,array(
                                         'id_locales'   => $locale
                                        ,'module'       => 'treeview'
                                        ,'field_name'   => 'fullurl'
                                        ,'id_element'   => $treeview['id_treeview']
                                        ,'lib'          => $i18n_url
                                        ,'is_published' => 1
                                        ,'level'        => 1
                                    ));

                                }
                            //parent::set($treeview['id_treeview'], array('fullurl'=>$this->cleanTreeviewUrl($fullurl)));
                            
                            //echo "MASTER".$locale;
                        }
                    }
                }
            }
        }
        if(isset($this->collector['search_info'])){
                $this->collector['search_info']->db->Execute(" delete from gen_search_info;");
                $this->collector['search_info']->db->Execute("insert into gen_search_info (lib_table,element_id, id_locale, html,url)
                    (select 'gen_treeview_url'
                        ,gen_treeview_url.id_treeview
                        ,gen_treeview_url.id_locale
                        ,gen_treeview.lib
                        ,gen_treeview_url.url 
                    from 
                        gen_treeview_url 
                        left join gen_treeview on gen_treeview_url.id_treeview = gen_treeview.id_treeview 
                    where gen_treeview.id_parent != 0);");

                $extraWhere = '';
                if(isset($this->collector['produits'])){
                    if(array_key_exists('is_valid', $this->collector['produits']->array_fields)) {
                        $extraWhere .= ' AND gen_produits.is_valid = 1';
                    }
                    if(array_key_exists('status', $this->collector['produits']->array_fields)) {
                        $extraWhere .= " AND gen_produits.status = 'ok'";
                    }
    
                    $this->collector['search_info']->db->Execute("insert into gen_search_info (lib_table,element_id, id_locale, html,url,media)
                    (
                        SELECT  'gen_produits_url', gen_produits_url.id_produits, gen_produits_url.id_locale
                        ,CONCAT(gen_produits.lib,'|',gen_treeview.lib,'|',gen_produits.ville) 
                        , gen_produits_url.url
                        ,gen_produits.media
                        FROM gen_produits_url
                        LEFT JOIN gen_produits ON ( gen_produits_url.id_produits = gen_produits.id_produits ) 
                        LEFT JOIN gen_treeview ON ( gen_produits.id_treeview = gen_treeview.id_treeview ) 
                        WHERE gen_produits_url.is_canonical =1
                        AND gen_produits.lib IS NOT NULL 
                        AND gen_produits.lib !=  ''
                        AND gen_produits.id_treeview IS NOT NULL 
                        ".$extraWhere."
                    );");
                }
        }

        $this->enableCache();
    }

    public function cleanTreeviewUrl($fullurl=array())
    {
        $temp = strtolower(urlizeUtf8('/'.implode('/',$fullurl).'/'));
        # bugfix multiple / in url
        $temp = preg_replace('(//+)','/',$temp);
        # if anchor, delete last /
        if(strstr($temp,'#')){
            return preg_replace('/\/$/','',$temp);
        }
        return $temp;
    }
    
    function getStrictChildrenCachedKV($hook, $max_level=0, $order = 'order by gen_treeview.int_order asc',$param=array()){
        die('getStrictChildrenCachedKV');
        $temp = $this->getStrictChildren($hook, $max_level,$order);
           if(empty($temp)) return array();
           foreach($temp as $k => $realArray) {
               $libField = $this->_libField;
               $val=$realArray['fullurl'];
               $retArr[$realArray[$this->primary_key]] = $val;
           }
           return $retArr;
    }
    function getStrictChildrenCached($hook, $max_level=0, $order = 'order by gen_treeview.int_order asc',$param=array()) {
        $var_name = md5($this->id_site.$this->id_locales.'getStrictChildrenCached'.json_encode([$hook, $max_level, $order, $param]));

        $var_value = $this->collector['caches']->getVar($var_name);
        if(!$var_value){
            $var_value = $this->getStrictChildren($hook, $max_level, $order, $param);
            $this->collector['caches']->setVar($var_name,$var_value);
        }
        return $var_value;
    }

    public function getChildrenCached($hook='', $max_level=0, $order = 'order by gen_treeview.int_order asc',$param=array(),$id_loc='',$id_loc_master='') {
        $var_name = md5($this->id_site.$this->id_locales.'getChildrenCached'.json_encode([$hook, $max_level, $order, $param]));
        $var_value = $this->collector['caches']->getVar($var_name);
        if(!$var_value){
            $var_value = $this->getChildren($hook, $max_level, $order, $param);
            $this->collector['caches']->setVar($var_name,$var_value);
        }
        return $var_value;
    }

    function getStrictChildren($hook, $max_level=0, $order = 'order by gen_treeview.int_order asc',$param=array()) {
         if(is_array($hook) && isset($hook['hook'])){
            $temp = $hook;
            $hook = $temp['hook'];
            $max_level = (isset($temp['max_level'])) ? $temp['max_level'] : 0;
            $order = (isset($temp['order'])) ? $temp['order'] : 'order by gen_treeview.int_order asc';
            $param = (isset($temp['param'])) ? $temp['param'] : array();
        }

        $temp = $this->getChildren($hook, $max_level,$order,$param);
        if(!empty($temp)) {
            if(!isset($temp[key($temp)]['children'])) return array();
            return $temp[key($temp)]['children'];
        }
    }

    function getStrictChildren2($hook, $max_level=0, $order = 'order by gen_treeview.int_order asc',$param=array()) {

        if(is_array($hook) && isset($hook['hook'])){
            $hook = $hook['hook'];
            $max_level = (isset($hook['max_level'])) ? $hook['max_level'] : 0;
            $order = (isset($hook['order'])) ? $hook['order'] : 'order by gen_treeview.int_order asc';
            $param = (isset($hook['param'])) ? $hook['param'] : array();
        }

        $temp = $this->getChildren2($hook, $max_level,$order,$param);
        return $temp;
        if(!empty($temp)) {
            if(!isset($temp[key($temp)]['children'])) return array();
            return $temp[key($temp)]['children'];
        }
    }
    function getMenuForFront($array=array()){
        if(isset($array['hook'])){
            return $this->getStrictChildren($array['hook'],((isset($array['deep'])) ?  $array['deep'] : 2),'order by gen_treeview.int_order asc',array('is_valid'=>1));
        }
        else{
            return false;
        }

    }

    public function getChildrenByProduct($hook, $idProduits,$id_loc='',$id_loc_master='') 
    {
        $this->_getTreeviewProperties = true;
        $this->_getChildrenTreeviewIds = array();
        $this->_treeviewProperties = array();
        $all_fields = $this->SetUsedFields(array('id_treeview'));
        
        $treeRows = $this->getChildren($hook,0,'',array(),ID_LOC,ID_LOC_MASTER);

        $this->array_fields = $all_fields;
        
        $ptRows = $this->collector['produits_treeview']->getCached(array('id_treeview' => $this->_getChildrenTreeviewIds, 'id_produits' => $idProduits));
        if (isset($ptRows['id_produits_treeview'])) {
            $ptRows = [$ptRows];
        }
        $treeviews = array();
        if($ptRows) {
            foreach($ptRows as $ptRow) {
                if(isset($ptRow['id_treeview'])){
                    if(array_key_exists($ptRow['id_treeview'], $this->_treeviewProperties)) {
                        $treeviews[] = $this->_treeviewProperties[$ptRow['id_treeview']];
                    }
                }
            }
        }
        $out = array();
        foreach($treeviews as $k=>$v){
            $out[]=$this->getOneI18n($v['id_treeview']);
        }

        return $out;
    }

    public function getMaxLevel()
    {
        $sql = 'SELECT max(level) as max FROM '.$this->table_name.' where id_site = '.ID_SITE ;
        $rs = $this->Query($sql);
        return (int) $this->fetchField($rs, 'max');
    }

    public function getIdParents($id_treeview,$id_locale=''){
        $path[$id_treeview] = $this->getOneI18n($id_treeview); 
        if(isset($path[$id_treeview]['id_parent']) && !empty($path[$id_treeview]['id_parent'])){
            $path[$id_treeview] = $this->getIdParents($path[$id_treeview]['id_parent']);
        }
        else{
            unset($path[$id_treeview]['id_treeview']);
            unset($path[$id_treeview]['id_parent']);
            unset($path[$id_treeview]['id_site']);
            unset($path[$id_treeview]['level']);
        }
        return $path; 
    }


    public function getSortedParentsById($id_treeview){
        $this->SetUsedFields(array('id_treeview','level','id_parent'));
        $out = $sorted =  array();
        $max_level = $this->getMaxLevel();

        for($i = $max_level ; $i >= 1 ; $i--){
            $out[$id_treeview]['level']=$i;
            foreach($this->getCached(array('level'=>$i,'id_site'=>ID_SITE)) as $el){
                $out[$el['id_treeview']]  = $this->getIdParents($el['id_treeview'],ID_LOC);
            } 
        }

        foreach($out as $k=>$v){
            $sorted[$k] = getArrayKeysFlat($v);
        }
        $this->RollBackUsedFields();
        return $sorted;
    }

        
    public function getListByHook($hook, $idProduits, $returnType = 'simple') 
    {
        $rows = $this->getChildrenByProduct($hook, $idProduits);
        $result = array();
        if($rows) {
            foreach($rows as $d) {
                if($returnType == 'simple') {
                    $result[$d['id_treeview']] = $d['lib'];
                } else {
                    $result[$d['id_treeview']] = $d;
                }
            }
        }
        return $result;
    }

    public function getMetas($hook, $idProduits = null) 
    {
        $treeRows = $this->getChildren($hook);
        
        $ptRows = $this->collector['produits_treeview']->getI18n(array('id_treeview' => $this->_getChildrenTreeviewIds, 'id_produits' => $idProduits));
        $ptdCollector = $this->collector['produits_treeview_data'];
        $result = array();
        if($ptRows) {
            foreach($ptRows as $row) {
                $result[$row['id_treeview']] = $row;
                $metas = $ptdCollector->getCached(array('id_produits_treeview' => $row['id_produits_treeview']));
                if($metas) {
                    $result[$row['id_treeview']]['metas'] = array();
                    foreach($metas as $meta) {
                        $result[$row['id_treeview']]['metas'][$meta['name']] = $meta;
                    }
                }
            }
        }
        return $result;
    }
    
    public function getParents($id_treeview,$id_locale=''){
        $path[$id_treeview] = $this->getOneI18n($id_treeview); 
               if(isset($path[$id_treeview]['id_parent']) && !empty($path[$id_treeview]['id_parent'])){
            $path[$id_treeview]['parents'] = $this->getParents($path[$id_treeview]['id_parent']);
        }
        return $path; 
    }

    public function getPath($id_treeview,$id_locale=''){
        if(empty($id_locale)) {
            $temp = $this->getOneCached($id_treeview);
        }
        else{
            $temp = $this->getOneI18n($id_treeview,'','',$id_locale);
        }
        $path[$temp['int_order']]=$temp;
        if(isset($temp['id_parent']) && !empty($temp['id_parent'])){
            $path= array_merge($path,$this->getPath($temp['id_parent'],$id_locale));
        }
        return $path;
        //return str_replace('//','/',$path); 
    }

    public function getFilteredReversePath($id_treeview,$id_locale=''){
        $temp = $this->getPath($id_treeview,$id_locale); 
        $temp = array_reverse($temp);
        $out = array();
        foreach($temp as $k=>$v){
            if($v['url'] == '/' || $v['url'] == '') continue;
            $out[$k]=$v;
        }
        return $out;
    }


    public function getReversePath($id_treeview,$id_locale=''){
        $temp = $this->getPath($id_treeview,$id_locale); 
        return array_reverse($temp);
    }
    public function getReversePathByLocale($id_treeview,$id_loc,$id_loc_master=''){
        $temp = $this->getPathByLocale($id_treeview,$id_loc,$id_loc_master); 
        return array_reverse($temp);
    }
    public function getPathByLocale($id_treeview, $id_loc='',$id_loc_master=''){
        $temp =  $this->getOneI18n($id_treeview,'','',$id_loc,$id_loc_master);
           $path[$temp['int_order']]=$temp;
        if(isset($temp['id_parent']) && !empty($temp['id_parent'])){
            $path= array_merge($path,$this->getPathByLocale($temp['id_parent'],$id_loc,$id_loc_master));
        }
        return $path; 
    }

    public function getUrl($id_treeview,$id_locale=''){
        $temp =  $this->getOneI18n($id_treeview,$id_locale);
        return (isset($temp['fullurl'])) ? $temp['fullurl'] : '';
    }

    public function getByUrl($url,$id_locale=''){
        
        if( $this->use_site_id ===true ){ 
            $cond = array('id_locale'=>ID_LOC,'id_site'=>ID_SITE,'url'=>$url);
        }
        else{
            $cond = array('url'=>$url);
        }
        $temp = $this->collector['treeview_url']->getOne($cond);
        return (isset($temp['id_treeview'])) 
            ? $this->collector['treeview']->getOneI18n($temp['id_treeview'])
            : false;
    }
    

        public function getKvRtl($conditions=array(), $order='',$limit=''){

           $rs = $this->getCached($conditions,'int_order asc', $limit);

           if(empty($rs)) return array();
           foreach($rs as $k => $realArray) {
           /* if($realArray['level'] == 0) 
               $space = str_repeat("_",100);
            else        
               $space = str_repeat("_",50-(int)$realArray['level']*10);
    
           $libField = $this->_libField;
           $val = str_pad($realArray[$libField].'('.$realArray[$this->primary_key].')',180,' ').$space;
            */
           $libField = $this->_libField;
            //$val = substr(str_pad($realArray[$libField].'('.$realArray['level'].')',80,'-',STR_PAD_RIGHT),0,80-((int)$realArray['level']*10));
             $val=$realArray['fullurl'];
           $retArr[$realArray[$this->primary_key]] = $val;
           }
           return $retArr;

    }
        public function getKvWidthSpace($conditions=array(), $order='',$limit=''){
           return $this->getKv();
           $rs = $this->get($conditions, $order, $limit);
           if(empty($rs)) return array();
           foreach($rs as $k => $realArray) {
           $space = str_repeat("-",$realArray['level']*2);
           $libField = $this->_libField;
           $val = $space.$realArray[$libField].'('.$realArray[$this->primary_key].')';
           $retArr[$realArray[$this->primary_key]] = $val;
           }
           return $retArr;
       }

    public function getController($id_treeview){
        foreach($this->getPath($id_treeview) as $k=>$v){
            if(!empty($v['controller'])) return  $v['controller'];
        }
    }


    public function saveTreeview($obj,$parent=0, $parentOrder=0, $parentLevel=1)
    {
        if ($parent === '') {
            $this->menu_order = $parentOrder - 1;
            $this->menu_level = $parentLevel;
            // On récupère le int_order du sous-treeview suivant
            $NextSubTV = $this->aget(['conditions' => ['level' => $parentLevel, '>int_order' => $parentOrder], 'order' => 'int_order ASC', 'limit' => 1]);
            $orderNextSubTV = $NextSubTV[0]['int_order'];
            // On récupère les ids dont le int_order sera décalé
            $idsToSchift = $this->getKandMore(['int_order'], ['>int_order' => $parentOrder]);
            $this->_savedIdTreeviews = [];
        }
        foreach ($obj as $menu => $menu_val) {
            $this->menu_order++;

            if ($this->use_site_id === true) {
                parent::set($menu_val['id'],
                    array(
                        'id_parent' => (int)$parent
                    , 'int_order' => $this->menu_order
                    , 'level' => $this->menu_level
                    , 'id_site' => ID_SITE
                    , 'id_locales' => $this->id_locales

                    ));
            } else {
                parent::set($menu_val['id'],
                    array(
                        'id_parent' => (int)$parent
                    , 'int_order' => $this->menu_order
                    , 'level' => $this->menu_level
                    ));
            }
            $this->_savedIdTreeviews[] = $menu_val['id'];

            if (isset($menu_val['children']) && is_array($menu_val)) {
                $this->menu_level++;
                $this->saveTreeview($menu_val['children'], $menu_val['id']);
                $this->menu_level--;
            }
        }
        
        // Si on édite un sous-treeview, on redécale si besoin tous les int_order des éléments d'après
        if ($parent === '') {
            // Calcul du décalage
            $decalage = $this->menu_order - $orderNextSubTV + 1;
            foreach ($idsToSchift as $id_treeview => $int_order) {
                if (!in_array($id_treeview, $this->_savedIdTreeviews)) {
                    $this->set($id_treeview, ['int_order' => (int)$int_order + $decalage]);
                }
            }
        }
    }

    function displayAdmin($restrictions=array(), $conditions=array(), $last_level=1) {
        $loop=0;
        $dd=(!empty($restrictions)) ? ' dd-nodrag ' : '';
        $this->out = "\n".'<ol class="dd-list '.$dd.'">';

        if (isset($conditions['hook']))  {
            $menus = $this->getChildrenIds($conditions['hook'], 0, 'order by gen_treeview.int_order asc', [], '', '', true);
            if (isset($conditions['hook']) && !is_array($conditions['hook'])) {
                $redirParams = '&redirParams='.urlencode('parent-hook='.$conditions['hook']);
            } elseif (isset($_GET['parent-id'])) {
                $redirParams = '&redirParams='.urlencode('parent-id='.$_GET['parent-id']);
            }
        } else {
            $menus = $this->get((($this->use_site_id===true) ? array('id_site'=>ID_SITE) :
                (!empty($this->getAffectedTreeviews()) ? ['id_treeview' => $this->getAffectedTreeviews()] : [])), 'int_order asc');
        }

        if(is_array($menus) && !empty($menus)) foreach($menus as $menu){

            $cntParams = array('id_treeview'=>$menu['id_treeview'],'is_valid'=>1);
            if(array_key_exists('status', $this->array_fields)) {
                $cntParams['status'] = 'ok';
            }
            if(isset($this->collector['produits'])){
                $cnt = $this->collector['produits']->getCount($cntParams);
            }
            if(isset($this->collector['produits_treeview'])){
                $cnt2 = $this->collector['produits_treeview']->getCount(array('id_treeview'=>$menu['id_treeview']));
            }
            $total = '';
            if(isset($cnt) && isset($cnt2)){
                $total = $cnt+$cnt2;
                $total = ($total == 0) ? '<span style="color:red">['.$total.']</span>' : '<span style="color:green">['.$total.']</span>' ; 
            }
            $loop++;

            if($loop>1 && $menu['level'] == $last_level){
                $this->out .= "\n".'</li>';
            }
            if($menu['level'] < $last_level){
                for($j=$menu['level'];$j<$last_level;$j++){
                    $this->out .= "\n</li>\n</ol>\n";
                }
            }
            elseif($menu['level'] > $last_level){
                $this->out .= "\n".'<ol class="dd-list">';
            }

            $color = '';
            
            $this->out .= "\n".'<li class="dd-item" data-id="'.$menu['id_treeview'].'">'.
                          "\n\t".'<div class="dd-handle '.$dd.' '.$color.'">'.$menu['lib'].' <b>('.$menu['id_treeview'].') '.$total.'</b></div>';
            $this->out .= "\n\t".''.
                                   '<!--<button type="button" class="treeview-btn-info btn btn-sm  btn-light btn-tooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="'.$menu['fullurl'].'"><i class="fa fa-info"></i></button>-->
                                    <a  href="treeview.edit.php?hash=&id_treeview='.$menu['id_treeview'] . (isset($redirParams) ? $redirParams : '') .'" class=" edit editbutton btn btn-primary btn-sm"><span class="smaller">'.t("Editer").'</span></a>'
                                   .'<a  href="#" id="'.$menu['id_treeview'].'" class="treeviewDel editbutton btn btn-danger btn-sm"><span class="smaller">'.t("Effacer").'</span></a>';
            if(empty($restrictions))
                $this->out.= '<p rel="'.securitize($menu['id_treeview']).'" class="edit_in_place switch" id="treeview@is_valid@'.$menu['id_treeview'].'@switch"><input type="checkbox" name="" id="" value="1" class="eip-switch mini-switch with-tip" title="Enable/disable switch" '.(($menu['is_valid'] == 1)  ? 'checked="checked"' : '').' /></p>';
                  $this->out .= '';

            $last_level= $menu['level'];

        }
        for($i=$last_level;$i>1;$i--){
            $this->out .= "\n</li>\n</ol>\n</li>";
        }    
        $this->out  .= "\n"."</li>\n</ol>";
    }


    function addElement(){
        if( $this->use_site_id ===true ){
            $id=$this->set('',array(
                        'id_parent'    => 0 
                        ,'lib'        => 'New Item' 
                        ,'id_site'    => ID_SITE 
                        ,'id_locales'=> $this->id_locales
                        ,'int_order' => time() 
                        ,'level'     =>  '1' 
                        ,'is_valid'     => '0' 
                        ,'controller'     => '' 
                        ,'url'     => '' 
                        ,'hook'     => '' 
                ));
        }
        else{
            $id=$this->set('',array(
                        'id_parent'    => 0 
                        ,'lib'        => 'New Item' 
                        ,'int_order' => time() 
                        ,'level'     =>  '1' 
                        ,'is_valid'     => '0' 
                        ,'controller'     => '' 
                        ,'url'     => '' 
                        ,'hook'     => '' 
                ));

        }
        $this->collector['i18n']->SetMassStatus('treeview',$id,0);
        $this->out .= "\n".'<li class="dd-item" data-id="'.$id.'">'.
                          "\n\t".'<div class="dd-handle red">New Item '.$id.'</div>';
        $this->out .= "\n\t".'<div class="actions">'.
                            '<a  href="treeview.edit.php?hash=&id_treeview='.$id.'" class="button edit editbutton"><span class="smaller">'.t("Editer").'</span></a>'.
                            ''.
                            '</div>';

        $this->out .= "\n".'</li>';
        
        return $this->out;

    }

    public function getMostUsed() 
    {
        $parent = $this->getOneByHook('coach-sportif');
        $sql = '
            SELECT count(*) as nb, ' . $this->table_name . '.id_treeview, ' . $this->table_name . '.lib, fullurl
            FROM `gen_produits_treeview` 
            INNER JOIN ' . $this->table_name . ' ON gen_produits_treeview.id_treeview = ' . $this->table_name . '.id_treeview
            WHERE ' . $this->table_name . '.level = 3
            AND id_parent = ' . $this->db->qstr($parent['id_treeview']) . '
            GROUP BY id_treeview, lib
            ORDER BY nb DESC
            LIMIT 10';
        return $this->getRows($this->CachedQuery($sql));
    }

    public function getProductsByTreeview($id_treeview, $id_locale='', $type = 'produit') 
    {
        if(empty($id_locale))$id_locale = ID_LOC;
        $joins = '';
        if(array_key_exists('produits_treeview_order', $this->collector)) {
            $orderTree = "CASE WHEN gen_produits_treeview_order.n_order IS NOT NULL THEN gen_produits_treeview_order.n_order ELSE IFNULL(gen_treeview_products.int_order, 9999) END";
        } else {
            $orderTree = 'gen_treeview.int_order';
        }
        if(is_array($id_treeview)) {
            $where = ''; 
            $allHookIds = array();
            foreach($id_treeview as $hi => $id) {
                if($this->_useJoinsInProductSearch === true) {
                    $joins .= ' INNER JOIN gen_produits_treeview t' . $hi . ' ON t' . $hi . '.id_produits = gen_produits.id_produits AND t' . $hi . '.id_treeview = ' . (int) $id . ' ';
                }
                array_push($allHookIds, (int) $id);
            }
            if(count($allHookIds) > 0) {
                $where .= ' and (gen_produits_treeview.id_treeview IN (' . implode(',', $allHookIds) . ') OR ' .$this->table_name .'.id_treeview IN (' . implode(',', $allHookIds) . '))';
            }
            if(array_key_exists('produits_treeview_order', $this->collector)) {
                $joins .= "LEFT JOIN gen_produits_treeview_order ON (gen_produits_treeview_order.id_treeview IN ( " . implode(',', $allHookIds) . ") AND gen_produits_treeview_order.id_produits = gen_produits.id_produits)";
            }
        } else {
            $where = " AND gen_produits_treeview.id_treeview = ". (int) $id_treeview;
            if(array_key_exists('produits_treeview_order', $this->collector)) {
                $joins .= "LEFT JOIN gen_produits_treeview_order ON (gen_produits_treeview_order.id_treeview = " . intval($id_treeview) . " AND gen_produits_treeview_order.id_produits = gen_produits.id_produits)";
                $orderTree = "CASE WHEN gen_produits_treeview_order.n_order IS NOT NULL THEN gen_produits_treeview_order.n_order ELSE CASE WHEN gen_produits.id_treeview = " . intval($id_treeview) . " THEN 0 ELSE IFNULL(gen_treeview_products.int_order, 9999) END END";
            }
        }

        if($type != '') {
            $where .= " AND gen_produits.type = " . $this->db->qstr($type);
        }
        $idLocales = array((int) $id_locale);
        if(defined('ID_LOC_MASTER')) $idLocales[] = ID_LOC_MASTER;

        $sql = "SELECT DISTINCT 
                    gen_produits.id_produits 
                FROM 
                    gen_produits_treeview 
                    INNER JOIN gen_produits   on (gen_produits.id_produits = gen_produits_treeview.id_produits) 
                    LEFT JOIN gen_treeview    on (gen_treeview.id_treeview = gen_produits_treeview.id_treeview) 
                    LEFT JOIN gen_i18n        on (gen_produits.id_produits = gen_i18n.id_element 
                                                    and gen_i18n.module='produits' 
                                                    and gen_i18n.field_name='lib' 
                                                    and gen_i18n.id_locales IN(".implode(',',$idLocales).")) 
                                                    and gen_i18n.is_published = 1 
                    " . $joins . "
                WHERE 
                    gen_produits.is_valid=1 
                    AND gen_treeview.is_valid=1 
                    " . $where . " 
                
                ORDER BY " . $orderTree . ", IFNULL(gen_produits.item_order, 9999)";
        return $this->getRows($this->CachedQuery($sql));
    }

    public function getChildren($hook='', $max_level=0, $order = 'order by gen_treeview.int_order asc',$param=array(),$id_loc='',$id_loc_master='') {
        $path= $this->getChildrenIds($hook,$max_level, $order,$param,$id_loc,$id_loc_master);
        return $path;
    }

    public function getChildrenIds($hook='', $max_level=0, $order = 'order by gen_treeview.int_order asc',$param=array(),$id_loc='',$id_loc_master='', $flat=false) 
    {
        $checkAffectedTreeview = true;
        if (isset($hook['check_sites_treeview'])) {
            $checkAffectedTreeview = $hook['check_sites_treeview'];
            $hook = $hook['id_parent'];
        }
        $path = [];
        $cond = (is_int($hook)) ? 'id_parent' : (is_array($hook) ? 'id_treeview' : 'hook');

        if( $this->use_site_id ===true ){
            $getParams = array($cond=>$hook,'id_site'=>ID_SITE,'is_valid'=>1);
        } else {
            $getParams = array($cond=>$hook,'is_valid'=>1);
        }
        if($this->_retrieveOnlyValidChildren === false) {
            unset($getParams['is_valid']);
        }
        
        // Gestion des treeview disponibles pour un site
        if ($checkAffectedTreeview && isset($this->collector['sites_treeview']) && !empty($this->collector['treeview']->getAffectedTreeviews())) {
            if (isset($getParams['id_treeview'])) {
                if (!in_array($getParams['id_treeview'][0], $this->collector['treeview']->getAffectedTreeviews())) {
                    return false;
                }
            } else { 
                $getParams['id_treeview'] = $this->collector['treeview']->getAffectedTreeviews();
            }
        }

        if(empty($hook)) {
            $children = $this->getI18n(array_merge((array)$param, isset($getParams['id_treeview']) ? ['id_treeview' => $getParams['id_treeview']] : []), $order);
        } else {
            $children = $this->getI18n(array_merge((array)$param,$getParams), $order);
        }

        if(is_array($children)) foreach($children as $k=>$v) {

            if($max_level>0 && ($v['level']>=$max_level+2))continue;
            $path[$v['id_treeview']] = $v;
            $this->_getChildrenTreeviewIds[] = $v['id_treeview'];
            if($this->_getTreeviewProperties === true) {
                $this->_treeviewProperties[$v['id_treeview']] = $v;
            }
            if(isset($v['show_products']) && $v['show_products'] == 1) {
                $products = $this->collector['produits_treeview']->getI18n(array('id_treeview' => $v['id_treeview']));
                $productsPath = array();
                if(is_array($products) && count($products) > 0) {
                    foreach($products as $p) {
                        if(!isset($p['id_produits'])) continue;
                        $product = $this->collector['produits']->getOneI18n($p['id_produits']);
                        if(is_array($product) && $product['is_valid'] == 0 || !is_array($product)) continue;
                        $productCanonical = $this->collector['produits']->getCanonicalUrl($p['id_produits']);
                        $productsPath[] = array('id_produits' => $p['id_produits'], 'lib' => $product['lib'], 'url' => $product['url'], 'fullurl' => $productCanonical);
                    }
                }
                $path[$v['id_treeview']]['children'] = $productsPath;

            } elseif(isset($v['id_link_treeview']) && $v['id_link_treeview'] != '') {
                if(!in_array($v['id_link_treeview'], $this->_linkedIdsLoaded)) {
                    $this->_linkedIdsLoaded[] = $v['id_link_treeview'];
                    if ($flat) {
                        $temp = $this->getChildrenIds(['id_parent' => (int)$v['id_link_treeview'], 'check_sites_treeview' => $checkAffectedTreeview],$max_level, $order, $param,$id_loc,$id_loc_master,$flat);
                        $path = $path + $temp;
                    } else {
                        $path[$v['id_treeview']]['children'] = $this->getChildrenIds(['id_parent' => (int)$v['id_link_treeview'], 'check_sites_treeview' => $checkAffectedTreeview],$max_level, $order, $param,$id_loc,$id_loc_master,$flat);
                    }

                } else {
                    $path[$v['id_treeview']]['children'] = ''; // Recursion détectée, on ne prend pas en compte
                }
            } else {
                if ($flat) {
                    $temp = $this->getChildrenIds(['id_parent' => (int)$v['id_treeview'], 'check_sites_treeview' => $checkAffectedTreeview],$max_level, $order, $param,$id_loc,$id_loc_master,$flat);
                    $path = $path + $temp;
                } else {
                    $path[$v['id_treeview']]['children'] = $this->getChildrenIds(['id_parent' => (int)$v['id_treeview'], 'check_sites_treeview' => $checkAffectedTreeview],$max_level, $order, $param,$id_loc,$id_loc_master,$flat); 
                }
                   
            }
            if(empty($path[$v['id_treeview']]['children'])) {
                unset($path[$v['id_treeview']]['children']);
            }
        }
        
        return $path; 
    }


    public function getChildren2($id_parent='', $max_level=0, $order = 'order by gen_treeview.int_order asc',$param=array()) {
        $path = [];
        if(!is_int($id_parent)){
           $father =  $this->getOneCached(array('hook'=>$id_parent));
           $id_parent = $father['id_treeview'];
        }

        if( $this->use_site_id ===true ){
            $getParams = array('id_parent'=>$id_parent,'id_site'=>ID_SITE,'is_valid'=>1);
        } else {
            $getParams = array('id_parent'=>$id_parent,'is_valid'=>1);
        }

        if($max_level != 0){
            //$getParams['<=gen_treeview.level']=(int)$max_level;
        }

        //if(empty($id_paret)) {
        if(false) {
            $children = $this->getI18n($param, $order);
        } else {
            $children = $this->getI18n(array_merge($param,$getParams), $order);
        }

        if($max_level == 1){
            return $children;
        }
        
        if(is_array($children)) {
            foreach($children as $k=>$v) {
                if($max_level>0 && ($v['level']>=$max_level+2))continue;
                $path[$v['id_treeview']] = $v;
                $this->_getChildrenTreeviewIds[] = $v['id_treeview'];
                if($this->_getTreeviewProperties === true) {
                    $this->_treeviewProperties[$v['id_treeview']] = $v;
                }
                if($v['show_products'] == 1) {
                    $products = $this->collector['produits_treeview']->getI18n(array('id_treeview' => $v['id_treeview']));
                    if(is_array($products) && count($products) > 0) {
                        $productsPath = array();
                        foreach($products as $p) {
                            $product = $this->collector['produits']->getOneI18n($p['id_produits']);
                            if($product['is_valid'] == 0) continue;
                            $productCanonical = $this->collector['produits']->getCanonicalUrl($p['id_produits']);
                            $productsPath[] = array('lib' => $product['lib'], 'url' => $product['url'], 'fullurl' => $productCanonical);
                        }
                    }
                    $path[$v['id_treeview']]['children'] = $productsPath;
                } elseif($v['id_link_treeview'] != '') {
                    if(!in_array($v['id_link_treeview'], $this->_linkedIdsLoaded)) {
                        $this->_linkedIdsLoaded[] = $v['id_link_treeview'];
                        $path[$v['id_treeview']]['children'] = $this->getChildren2((int)$v['id_link_treeview'],$max_level, $order, $param);

                    } else {
                        $path[$v['id_treeview']]['children'] = ''; // Recursion détectée, on ne prend pas en compte
                  

                    }
                } else {
                    $path[$v['id_treeview']]['children'] = $this->getChildren2((int)$v['id_treeview'],$max_level, $order, $param); 
                       
                }
                if(empty($path[$v['id_treeview']]['children'])) {
                    unset($path[$v['id_treeview']]['children']);
                }
            }
        }
        return $path; 
    }

    public function setBuildOnSave($status) {
        $this->_buildOnSave = (bool) $status;
    }

    public function importFromBO(array $xlsData, $localeId)
    {
        $idx = array(
            $this->primary_key => 0,
            'lib' => 2,
            'url' => 4,
            'description' => 6,
            'meta_title' => 8,
            'is_valid' => 9
        );

        return parent::importTranslations($xlsData, $localeId, $idx);
    }

    public function exportFromBO($localeId, $extraParams)
    {
        $cols = array(
            $this->primary_key,
            'lib',
            'url',
            'description',
            'meta_title',
            'is_valid'
        );
        return parent::exportTranslations($localeId, $cols, $extraParams);
    }

    public function getGtmCat($id_treeview) 
    {
        $cat = null;
        if(array_key_exists('gtm_cat', $this->array_fields)) {
            foreach($this->getPath($id_treeview) as $k=>$v) {
                if(!empty($v['gtm_cat'])) return $v['gtm_cat'];
            }
        }
        return !empty($cat) ? $cat : 'others';
    }

    public function getFieldValue($id_treeview, $field) 
    {
        $val = null;
        if(array_key_exists($field, $this->array_fields)) {
            foreach($this->getPath($id_treeview) as $k=>$v) {
                if(!empty($v[$field])) return $v[$field];
            }
        }
        return !empty($val) ? $val : '';
    }

    public function setUseJoinsInProductSearch($status = true) 
    {
        $this->_useJoinsInProductSearch = (bool) $status;
    }

    public function setRetrieveOnlyValidChildren($flag = true) 
    {
        $this->_retrieveOnlyValidChildren = (bool) $flag;
    }

    public function getRetrieveOnlyValidChildren() 
    {
        return $this->_retrieveOnlyValidChildren;
    }

    public function setLinkedCollectors($collectors = array()) 
    {
        $this->_linkedCollectors = $collectors;
    }
    public function getLinkedCollectors() 
    {
        return $this->_linkedCollectors;
    }
    /**
     * Fully remove an element : Treeview + children + associations
     *
     * @param integer $id
     * @return boolean
     */
    public function deleteFull($id) 
    {
        $id = (int) $id; 
        // Delete associations
        foreach($this->_linkedCollectors as $conf) {
            $collector = is_array($conf) && array_key_exists('collector', $conf) ? $conf['collector'] : $conf;
            $field = is_array($conf) && array_key_exists('field', $conf) ? $conf['field'] : 'id_treeview';
            if(isset($this->collector[$collector])) {
                $sql = "DELETE FROM " . $this->collector[$collector]->table_name . " 
                        WHERE " . $field . " = " . $this->db->qstr($id);
                $this->Query($sql);
            }
        }
        // Delete children
        $children = $this->getChildren($id);
        if(is_array($children)) {
            foreach($children as $child) {
                $this->deleteFull($child['id_treeview']);
            }
        }
        // Delete element
        $this->del($id);
    }

    public function getTreeviewHrefLang($treeviewId) 
    {
        $out = '';
        $altUrls = $this->collector['treeview_url']->getCached(array('id_treeview' => $treeviewId, '!=id_site' => ID_SITE));
        if(is_array($altUrls)) {
            foreach($altUrls as $u) {
                $altSite = $this->collector['sites']->getOneCached((int) $u['id_site']);
                $siteLoc = $this->collector['sites_locales']->getOneCached(array('id_locales' => $u['id_locale']));
                if($altSite['is_published'] != 1 || $siteLoc['is_enabled'] != 1) continue;
                $altLoc = $this->collector['locales']->getOneCached((int) $u['id_locale']);
                $out .= "\n".'<link rel="alternate" href="' . $altSite['prod_url'] . '/'.$altLoc['language'].$u['url'].'" hreflang="'.strtolower(str_replace('_','-',$altLoc['locale'])).'" />';
            }
        }
        return $out;
    }

    /**
     * @deprecated use getLinkByHook instead
     * 
     * Get the fullurl of a treeview
     * @param mixed $hook
     * @return bool|string
     */
    public function LinkByHook($hook){
       $treeview =  $this->getOneI18n(array('hook'=>$hook));
       return 
        (isset($treeview['fullurl'])) 
           ? $treeview['fullurl'] . $treeview['get_parameters'] 
           : false;
    }


    /**
     * Get the fullurl of a treeview.
     * 
     * The default mode gets the fullurl of the hook's treeview
     * By setting the followToLastNode parameter to `true`, the
     * fullurl returned is the one related to the last node 
     * found by following the id_link_treeview field.
     *  
     *  
     * @param string $hook the hook of the treeview
     * @param bool $followToLastNode whether to follow the id_link_treeview field and get the deepest treeview node fullurl
     * @return bool|string the fullurl of the treeview or false if not found
     */
    public function getLinkByHook(string $hook, bool $followToLastNode = false)
    {
        $treeview = $followToLastNode 
            ? $this->getLastNodeByHook($hook) 
            : $this->getOneI18n(['hook' => $hook]);

        return  isset($treeview['fullurl']) 
            ? $treeview['fullurl'] . $treeview['get_parameters'] 
            : false;
    }

    /**
     * Get the deepest treeview node found by following the id_link_treeview field 
     * 
     * This function is recursive and will follow the id_link_treeview field until it reaches a node with no id_link_treeview
     * 
     * @param int $id id of the treeview node to follow from.
     * @return false|array the last treeview node found or false if not found
     */
    public function getLastNodeById(int $id)
    {
        $treeview = $this->getOneI18n($id);

        return $treeview['id_link_treeview'] !== null
            ? $this->getLastNodeById($treeview['id_link_treeview'])
            : $treeview; 
    }

    /**
     * Get the deepest treeview node found by following the id_link_treeview field
     * 
     * 
     * @param string $hook the hook of the treeview to follow from
     * @return false|array the last treeview node found or false if not found
     */
    public function getLastNodeByHook(string $hook)
    {
        $treeview = $this->getOneI18n(['hook' => $hook]);

        return $treeview['id_link_treeview'] !== null
            ? $this->getLastNodeById($treeview['id_link_treeview'])
            : $treeview;
    }

    public function setForceBuildTranslationUrl($status = true) 
    {
        $this->_forceBuildTranslationUrl = (bool) $status;
    }

    /**
     * Retrieve all element's children but only these without child
     *
     * @param string $baseHook
     * @return array
     */
    public function getItemLastDepthElements($baseHook) 
    {
        $rows = $this->collector['treeview']->getStrictChildren($baseHook);
        if(!is_array($rows)) $rows = array();
        foreach($rows as $row) {
            if(array_key_exists('children', $row) && is_array($row['children'])) {
                $this->getItemLastDepthElements($row['hook']);
            } elseif(array_key_exists('id_treeview', $row)) {
                $this->_loadedLastDepthElements[$row['id_treeview']] = $row['lib'] . ' (' . $row['id_treeview'] . ')';
            }
        }
        return $this->_loadedLastDepthElements;
    }
    /**
     * Displays products treeview
     * For each element, a list of the related products (either linked by the main category or secondary categories)
     *
     * @param array $restrictions
     */
    public function displayProductsTreeview($restrictions=array())
    {
        $last_level= 1; 
        $loop=0;
        $dd=(!empty($restrictions)) ? ' dd-nodrag ' : '';
        $this->out = "\n".'<ol class="dd-list '.$dd.'">';

        $params = ($this->use_site_id===true) ? array('id_site'=>ID_SITE) : array();
        /*
        $treeviewIds = array();
        $linkedItems = $this->collector['produits_treeview']->getDistinct('id_treeview');
        foreach($linkedItems as $item) {
            $treeviewIds[] = $item['id_treeview'];
        }
        $linkedItems = $this->collector['produits']->getDistinct('id_treeview');
        foreach($linkedItems as $item) {
            if(in_array($item['id_treeview'], $treeviewIds)) continue;
            $treeviewIds[] = $item['id_treeview'];
        }*/

        $tree = $this->getChildrenIds('horizontal-menu');
        
        $treeviewIds = array();
        if (is_array($tree)) {
            $this->getFlatIds($tree, $treeviewIds);
        }
        $params['id_treeview'] = $treeviewIds;

        $menus = $this->get($params, 'int_order asc');
        if(is_array($menus) && !empty($menus)) foreach($menus as $menu) {
            $cntParams = array('id_treeview'=>$menu['id_treeview'],'is_valid'=>1);
            if(array_key_exists('status', $this->array_fields)) {
                $cntParams['status'] = 'ok';
            }
            $linkedProducts = $this->collector['produits_treeview']->get(array('id_treeview'=>$menu['id_treeview']));
            $linkedProducts = is_array($linkedProducts) ? $linkedProducts : array();
            $productIds = array();
            foreach($linkedProducts as $product) {
                $productIds[] = $product['id_produits'];
            }
            $totalNb = count($productIds);
            $total = ($totalNb == 0) ? '<span style="color:red">['.$totalNb.']</span>' : '<span style="color:green">['.$totalNb.']</span>' ; 

            $loop++;

            if($loop>1 && $menu['level'] == $last_level) {
                $this->out .= "\n".'</li>';
            }
            if($menu['level'] < $last_level) {
                for($j=$menu['level'];$j<$last_level;$j++) {
                    $this->out .= "\n</li>\n</ol>\n";
                }
            } elseif($menu['level'] > $last_level) {
                $this->out .= "\n".'<ol class="dd-list">';
            }

            $color = '';
            
            $this->out .= "\n".'<li class="dd-item" data-id="tree-'.$menu['id_treeview'].'">'.
                          "\n\t".'<div class="dd-content '.$dd.' '.$color.'">'.$menu['lib'].' <b>('.$menu['id_treeview'].') '.$total.'</b></div>';

            if($totalNb > 0) {
                // Reorder productIds
                $orderedProducts = $this->collector['produits_treeview_order']->get(array('id_treeview' => $menu['id_treeview']));
                if(is_array($orderedProducts) && count($orderedProducts) > 0) {
                    $orderProductIds = array();
                    foreach($orderedProducts as $p) {
                        $orderProductIds[] = $p['id_produits'];
                        $k = array_search($p['id_produits'], $productIds);
                        if(false !== $k) {
                            unset($productIds[$k]);
                        }
                    }
                    if(count($productIds) > 0) {
                        $productIds = array_merge($orderProductIds, $productIds);
                    } else {
                        $productIds = $orderProductIds;
                    }
                }
                $this->out .= '<ol class="dd-list product-list">';
                foreach($productIds as $pId) {
                    $product = $this->collector['produits']->getOne($pId);
                    if(!$product) continue;
                    $this->out .= "\n\t" . '<li class="dd-item dd-handled" data-id="product-'.$pId.'">'.
                                  "\n\t" . '  <div class="dd-handle dd-handler">Drag</div><div class="dd-content '.$dd.' '.$color.'">'.$product['lib'].' <b>('.$pId.')</b></div>' .  
                                  "\n\t" . '</li>';
                }
                $this->out .= '</ol>';
            }
            $last_level= $menu['level'];
        }
        for($i=$last_level;$i>1;$i--){
            $this->out .= "\n</li>\n</ol>\n</li>";
        }
        $this->out  .= "\n"."</li>\n</ol>";
    }

    /**
     * Get a flat representation of a treeview (based on getChildrenIds format)
     *
     * @param array $tree
     * @param array $treeviewIds (pointer)
     * @param boolean $onlyWithProducts
     * @return void
     */
    public function getFlatIds(array $tree, &$treeviewIds)
    {
        foreach($tree as $id => $item) {
            $treeviewIds[] = $item['id_treeview'];
            if(is_array($item) && array_key_exists('children', $item) && is_array($item['children'])) {
                $this->getFlatIds($item['children'], $treeviewIds);
            }
        }
    }
    /**
     * Retrieve treeview structure for select : id_treeview => label (with - for hierarchy)
     *
     * @param string $baseHook
     * @param int $maxDepth
     * @param string $indent
     * @return array
     */
    public function getItemsHierarchyForSelect($baseHook, $maxDepth = 20, $indent = '')
    {
        if (!empty($this->_loadedItemsHierarchyForSelect[$baseHook])) {
            return $this->_loadedItemsHierarchyForSelect[$baseHook];
        }

        if (is_numeric($baseHook)) {
            $rows = $this->getChildren2((int)$baseHook);
        } else {
            $rows = $this->getStrictChildren($baseHook);
            $this->_loadedItemsHierarchyForSelect['tmp'] = [];
        }
        if(!is_array($rows)) $rows = [];
        foreach($rows as $row) {
            $this->_loadedItemsHierarchyForSelect['tmp'][$row['id_treeview']] = $indent . $row['lib'];
            if(array_key_exists('children', $row) && is_array($row['children']) && $maxDepth > 1) {
                $this->getItemsHierarchyForSelect($row['id_treeview'], $maxDepth - 1, $indent . '--');
            }
        }
        if (!is_numeric($baseHook)) {
            $this->_loadedItemsHierarchyForSelect[$baseHook] = $this->_loadedItemsHierarchyForSelect['tmp'];
            unset($this->_loadedItemsHierarchyForSelect['tmp']);
            return $this->_loadedItemsHierarchyForSelect[$baseHook];
        } else {
            return $this->_loadedItemsHierarchyForSelect['tmp'];

        }
    }

    /**
     * Retrieve treeviews selected for current site with children
     *
     * @return array
     */
    public function getAffectedTreeviews() {
        $plateforme = $this->collector['plateforme']->getFirstCached();
        if (!isset($plateforme['is_multisite']) || $plateforme['is_multisite'] != 1) {
            return false;
        }
        if (isset($this->collector['sites_treeview']) && !isset($this->_affectedTreeviews) && defined('ID_SITE')) {
            $this->_affectedTreeviews = [];
            $this->_retrieveOnlyValidChildren = false;
            $results = $this->collector['sites_treeview']->get(['id_sites' => ID_SITE]);
            $availableTreeviews = [];
            if(!$results)return false;
            foreach($results as $treeview) {
                $availableTreeviews[] = $treeview['id_treeview'];
                $children = $this->collector['treeview']->getChildrenIds(['id_parent' => (int)$treeview['id_treeview'], 'check_sites_treeview' => false], 0, 'order by gen_treeview.int_order asc', [], '', '', true);
                foreach($children as $child) {
                    $availableTreeviews[] = $child['id_treeview'];
                }
            }
            $this->_affectedTreeviews = $availableTreeviews;
        }
        return $this->_affectedTreeviews;
    }

    /**
     * Fonction qui récupère toutes les urls dynamiques renseignées dans le treeview, et qui regarde celle qui match à la requête
     * Si c'est le cas, les parties dynamiques sont stockées dans $display->vars['matchedVars']
     * Il est possible de lancer une callback par variable pour par exemple aller chercher un id à partir du libellé (à définir dans le BO)
     * La méthode lance également les callback définies pour les meta title et desc afin de pouvoir les dynamiser
     *
     * @return array|null
     */
    public function matchDynamicUrl() {
        $this->setReturnRawResults(true);
        $dynamicUrls = $this->getCached(['!=url_dynamicpart' => null]);
        $this->setReturnRawResults(false);
        foreach ($dynamicUrls as $dynamicUrl) {
            // On remplace les variables saisies dans la route par des sous-pattern nommés
            $routeSchemaNamed = preg_replace('/\{([a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*)\}/',
                '(?P<$1>.*)',
                $dynamicUrl['fullurl'] . $dynamicUrl['url_dynamicpart']);
            $routeSchemaNamed = '/^' . str_replace('/', '\/', $routeSchemaNamed) . '$/';
            $urlCallbacks = explode(',', $dynamicUrl['url_callback']);
            if (preg_match_all($routeSchemaNamed, $_SERVER['REQUEST_URI'], $matches)) {
                $compteur = 0;
                foreach ($matches as $key => $value) {
                    // On ne prend en compte que les matching nommés (sinon doublon)
                    if (!is_int($key)) {
                        $value = $value[0];
                        $callback = $urlCallbacks[$compteur];
                        // Si une callback est définie pour la variable, on l'exécute
                        // NB: l'ordre doit être le même que dans la route cf compteur
                        if (!empty(trim($callback))) {
                            if (strstr($callback, '::')) {
                                $expCb = explode('::', $callback);
                                $value = $this->collector[$expCb[0]]->{$expCb[1]}($value);
                            } else {
                                $value = $callback($value);
                            }
                        }
                        $dynamicUrl['matchedVars'][$key] = $value;
                        $compteur++;
                    }
                }
                if (isset($dynamicUrl['seo_callback'])) {
                    // La callback SEO doit retourner les valeurs à dynamiser dans le title et la desc
                    $callback = $dynamicUrl['seo_callback'];
                    if (strstr($callback, '::')) {
                        $expCb = explode('::', $callback);
                        $data = $this->collector[$expCb[0]]->{$expCb[1]}($dynamicUrl);
                    } else {
                        $data = $callback($dynamicUrl);
                    }
                    foreach (['title', 'desc'] as $field) {
                        if (isset($dynamicUrl['meta_' . $field])) {
                            foreach ($data as $key => $value) {
                                $dynamicUrl['meta_' . $field] = str_replace('{' . $key . '}', $value, $dynamicUrl['meta_' . $field]);
                            }
                        }
                    }
                }
                return $dynamicUrl;
            }
        }
        return null;
    }
}
