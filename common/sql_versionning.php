<?php
$sql_versionning['sites']['key_gg_ana'] = "ALTER TABLE `gen_sites` ADD `key_gg_ana` VARCHAR(255) NULL AFTER `debug_sql`;";
$sql_versionning['sites']['css_file'] = "ALTER TABLE `gen_sites` ADD `css_file` VARCHAR(255) NULL;";
$sql_versionning['create'][] = "CREATE TABLE IF NOT EXISTS `gen_sites_treeview` (
  `id_sites_treeview` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_sites` mediumint(8) UNSIGNED NOT NULL,
  `id_treeview` mediumint(8) UNSIGNED NOT NULL,
  FOREIGN KEY (`id_sites`) REFERENCES `gen_sites` (`id_sites`) ON DELETE CASCADE ON UPDATE NO ACTION,
  FOREIGN KEY (`id_treeview`) REFERENCES `gen_treeview` (`id_treeview`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
$sql_versionning['modules']['is_enabled'] = "ALTER TABLE `gen_modules` CHANGE `is_enabled` `is_enabled` TINYINT(1) NOT NULL DEFAULT '0';";
$sql_versionning['sites']['key_gg_maps_2'] = "ALTER TABLE `gen_sites` ADD `key_gg_maps_2` VARCHAR(255) NULL AFTER `debug_sql`;";
$sql_versionning['treeview']['class'] = "ALTER TABLE `gen_treeview` ADD `class` VARCHAR(255) NULL;";
$sql_versionning['sites']['head_code'] = "ALTER TABLE `gen_sites` ADD `head_code`  text NULL;";
$sql_versionning['sites']['foot_code'] = "ALTER TABLE `gen_sites` ADD `foot_code`  text NULL;";
$sql_versionning['create'][] = "ALTER TABLE `gen_sites` CHANGE `locale` `locale` VARCHAR(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'deprecated';";
$sql_versionning['create'][] = "ALTER TABLE `gen_pages404` CHANGE `destination` `destination` VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;";
$sql_versionning['pagesbuilder']['n_order'] = "ALTER TABLE `gen_pagesbuilder` CHANGE `n_order` `n_order` MEDIUMINT(8) UNSIGNED NULL DEFAULT '0';";
$sql_versionning['treeview']['seo_callback'] = "ALTER TABLE `gen_treeview` ADD seo_callback VARCHAR(255) NULL AFTER meta_desc;";
$sql_versionning['treeview']['url_dynamicpart'] = "ALTER TABLE `gen_treeview` ADD url_dynamicpart VARCHAR(255) NULL AFTER url;";
$sql_versionning['treeview']['url_callback'] = "ALTER TABLE `gen_treeview` ADD url_callback VARCHAR(255) NULL AFTER url_dynamicpart;";
$sql_versionning['pagesbuilder']['picture'] = "ALTER TABLE `gen_pagesbuilder` ADD picture VARCHAR(255) NULL AFTER type;";
$sql_versionning['apps_admins']['filters'] = "alter table gen_apps_admins modify filters mediumtext null;";
$sql_versionning['plateforme']['favicon_code'] = "ALTER TABLE `gen_plateforme` ADD `favicon_code` TEXT NULL DEFAULT NULL AFTER `autoupdate_url`;";

$sql_versionning['create'][] = "CREATE TABLE  IF NOT EXISTS `gen_plateforme_locales` (
  `id_plateforme_locales` mediumint UNSIGNED NOT NULL PRIMARY KEY,
  `id_plateforme` mediumint UNSIGNED NOT NULL,
  `id_locales` mediumint UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;";

$sql_versionning['create'][] = "ALTER TABLE `gen_plateforme_locales`   ADD PRIMARY KEY IF NOT EXISTS (`id_plateforme_locales`),   ADD KEY IF NOT EXISTS `id_plateforme` (`id_plateforme`,`id_locales`,`is_enabled`);";
$sql_versionning['create'][] ="ALTER TABLE `gen_plateforme_locales`   MODIFY `id_plateforme_locales` mediumint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;  ";
$sql_versionning['create'][] ="ALTER TABLE `gen_plateforme` CHANGE `bgcolor_head` `bgcolor_head` VARCHAR(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '#242e3e';";
$sql_versionning['create'][] ="ALTER TABLE `gen_plateforme` CHANGE `bgcolor_menu` `bgcolor_menu` VARCHAR(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '#242e3e';";
$sql_versionning['create'][] ="ALTER TABLE `gen_plateforme` CHANGE `menu_linkcolor` `menu_linkcolor` VARCHAR(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '#b5bdca';";
$sql_versionning['create'][] ="ALTER TABLE `gen_plateforme` CHANGE `menu_hovercolor` `menu_hovercolor` VARCHAR(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '#4886ff';";
$sql_versionning['create'][] = "ALTER TABLE `gen_backevents` CHANGE `id_admins` `id_admins` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL;";
$sql_versionning['sites']['key_deepl'] = "ALTER TABLE `gen_sites` ADD `key_deepl` VARCHAR(255) NULL ;";
$sql_versionning['sites']['use_i18n_master_slave'] = "ALTER TABLE `gen_sites` ADD `use_i18n_master_slave`  TINYINT(1) NOT NULL DEFAULT '0';";
$sql_versionning['sites']['use_i18n_all_fields'] = "ALTER TABLE `gen_sites` ADD `use_i18n_all_fields`  TINYINT(1) NOT NULL DEFAULT '0';";
$sql_versionning['create'][] = "CREATE TABLE IF NOT EXISTS `gen_log` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) DEFAULT NULL,
  `level` int DEFAULT NULL,
  `message` longtext,
  `time` int UNSIGNED DEFAULT NULL,
  `id_admins` text,
  `action` text,
  `id_gabox` text,
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`),
  KEY `level` (`level`),
  KEY `time` (`time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$sql_versionning['create'][] = "CREATE TABLE IF NOT EXISTS gen_pagesbuilder_i18n (
    id_pagesbuilder_i18n mediumint unsigned auto_increment primary key,
    lib                  varchar(1024)      null,
    meta_desc            varchar(1024)      null,
    meta_title           varchar(1024)      null,
    is_published         tinyint(1)         null,
    content              longtext           null,
    created_at           datetime           null,
    updated_at           datetime           null,
    id_pagesbuilder      mediumint unsigned not null,
    id_locales           mediumint unsigned not null,
    id_treeview          mediumint unsigned null,
    constraint gen_pagesbuilder_i18n_gen_locales_id_locales_fk
        foreign key (id_locales) references gen_locales (id_locales),
    constraint gen_pagesbuilder_i18n_gen_pagesbuilder_id_pagesbuilder_fk
        foreign key (id_pagesbuilder) references gen_pagesbuilder (id_pagesbuilder)
);";
$sql_versionning['create'][] = "CREATE TABLE IF NOT EXISTS gen_pagesbuilder_draft_i18n (
    id_pagesbuilder_draft_i18n mediumint unsigned auto_increment primary key,
    created_at                 datetime           null,
    updated_at                 datetime           null,
    content                    longtext           null,
    id_pagesbuilder_i18n       mediumint unsigned null,
    constraint gen_pagesbuilder_draft_i18n_id_pagesbuilder_i18n_fk
        foreign key (id_pagesbuilder_i18n) references gen_pagesbuilder_i18n (id_pagesbuilder_i18n)
);";
global $gen;
if (($columns = $gen->db->MetaColumnNames('gen_pagesbuilder_draft')) && in_array('id_sites', $columns)) {
    $sql_versionning['pagesbuilder_draft']['id_sites'] = "ALTER TABLE gen_pagesbuilder_draft DROP COLUMN id_sites;";
}
if (($columns = $gen->db->MetaColumnNames('gen_pagesbuilder')) && !in_array('id_locales', $columns)) {
    $sql_versionning['pagesbuilder']['id_locales'] = "ALTER TABLE gen_pagesbuilder 
    ADD COLUMN id_locales mediumint unsigned,
    ADD foreign key (id_locales) references gen_locales (id_locales) on delete set null on update no action;";
}

$sql_versionning['pagesbuilder_i18n']['version'] = "ALTER TABLE gen_pagesbuilder_i18n 
    ADD COLUMN version mediumint(9) DEFAULT NULL";

// 2FA
$sql_versionning['plateforme']['2fa_enabled'] = "ALTER TABLE `gen_plateforme` ADD `2fa_enabled` TINYINT NOT NULL DEFAULT 0;";
$sql_versionning['plateforme']['2fa_issuer'] = "ALTER TABLE `gen_plateforme` ADD `2fa_issuer` VARCHAR(255) NULL;";
$sql_versionning['admins']['2fa_key'] = "ALTER TABLE `gen_admins` ADD `2fa_key` VARCHAR( 255 ) NULL ;";
$sql_versionning['admins']['2fa_key_enabled'] = "ALTER TABLE `gen_admins` ADD `2fa_key_enabled` TINYINT NOT NULL DEFAULT 0";

$sql_versionning['sites']['key_gg_ana'] = "ALTER TABLE `gen_sites` ADD `mailer_sign` mediumtext null;";

#bug fix adding new treeview with interface 
$sql_versionning['create'][] = "ALTER TABLE `gen_treeview` CHANGE `lib` `lib` VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL;";
$sql_versionning['treeview']['get_parameters'] = "ALTER TABLE `gen_treeview` ADD `get_parameters` VARCHAR(255) NULL;";
