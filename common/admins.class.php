<?php class Admins extends  gen_admins
{
    var $id='';

#    const _PWD_HASH_ = '!$s; vos[*sv!*µ';

    protected $_specials = array('!', '^', '$', '*', '~', '@', ')', '+', '#', '-', ':');
    protected $_nbSpecialsRequired = 5;
    protected $_passwordNbLetters = 12;
    protected $_libField = 'email';

    public function getLib($id){
        $admin = $this->getOne($id);
        return (isset($admin['email'])) ? $admin['email'] : 'unknown';
    }

	public function can($what,$where)
    {
        if ($this->isSuperAdmin()) {
            return true;
        }
        if (empty($this->id)) {
            return false;
        }
        $user = $this->getOne($this->id);
        if (!isset($user['id_admins']) || empty($user['id_admins'])) {
            return false;
        }
        // NB : La vérification sur la colonne id_modules est utilisé pour la médiathèque (qui n'a pas d'entrée dans gen_modules)
        $sql = "SELECT * FROM gen_admins admin 
                left join gen_usertypes type on (admin.usertype = type.id_usertypes)
                left join gen_usertypes_modules on (gen_usertypes_modules.id_usertypes=admin.usertype)
                left join gen_modules on (gen_modules.id_modules = gen_usertypes_modules.id_modules)
                where admin.id_admins=".$user['id_admins']."
                AND (gen_modules.lib = '" . $where . "' 
                    OR gen_usertypes_modules.id_modules = '" . $where . "')";
        $rs = $this->Query($sql);
        return $this->GetRows($rs);
    }

    public function canSEO()
    {
        if(empty($this->id)) return false;
        $sql = "select t.can_seo from gen_admins  a left join gen_usertypes  t on (a.usertype=t.id_usertypes) where a.id_admins=".(int)$this->id;
        $rs = $this->Query($sql);
        $r = $this->GetRows($rs);
        if(isset($r[0]) && isset($r[0]['can_seo']) && ($r[0]['can_seo']==1)) return true;
        return false; 
    }

    // admin['id'] or admin['email']
    public function passwordForget($admin=array())
    {
        if(isset($admin['id']))         $admin_to_reset = $this->getOne($admin['id']);
        elseif(isset($admin['email']))  $admin_to_reset = $this->getOne(array('email'=>filter_var($admin['email'], FILTER_VALIDATE_EMAIL)));
        else die('error: password forget');

        if(!is_array($admin_to_reset) || empty($admin_to_reset)) return false;
        /*$letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!^$*~';
        $new_pass = substr(str_shuffle($letters), 0, 8);*/

        $new_pass = $this->_getNewPassword();

        $mailVals = $admin_to_reset;
        $mailVals['new_pass'] = $new_pass;
        $this->set($admin_to_reset['id_admins'],array('password'=>$this->encodePassword($new_pass, $admin_to_reset['email']),'has_change_pass'=>0));
        #$this->collector['mails']->sendMail('password_forgotten', 2, $mailVals);
        # on préfère ici ne pas utiliser la table mail reservée au front
        $headers='Content-Type: text/html; charset="utf-8"'."\n";
        $headers.='Content-Transfer-Encoding: 8bit'."\n";

        $plateforme = $this->collector['plateforme']->getFirst();

       $plateform_name = $plateforme['lib']; #XXX replace by plateforme name from db;
       $res =  mail($admin_to_reset['email'],$plateform_name.' : password reset',"Hi,<br /><br />
            Your access has been reseted .<br/><br />
            You will find below your new access :<br/>
            E-mail : ".$admin['email']."<br />
            Password : ".$new_pass."<br /><br />
            Best regards,",$headers);
       return $res; 

    }

    protected function _getNewPassword() 
    {
        $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890' . implode('', $this->_specials);
        $new_pass = substr(str_shuffle($letters), 0, $this->_passwordNbLetters);
        $missingLetters = array();
        foreach($this->_specials as $char) {
            if(false === strstr($new_pass, $char)) {
                $missingLetters[] = $char;
            }
        }
        // 5 special chars min
        $limit = count($this->_specials) - $this->_nbSpecialsRequired;
        while(count($missingLetters) > $limit) {
            $idx = rand(0, $this->_passwordNbLetters - 1);
            $new_pass[$idx] = $missingLetters[mt_rand(0, count($missingLetters) - 1)];
            $missingLetters = array();
            foreach($this->_specials as $char) {
                if(false === strstr($new_pass, $char)) {
                    $missingLetters[] = $char;
                }
            }
        }
        return $new_pass;
    }

    public function set($primary_key_value = '' , $array_fields = '', $criteres = array())
    {
        if(array_key_exists('usertype', $array_fields) && isset($_SESSION['admins']['usertype']) && $array_fields['usertype'] != $_SESSION['admins']['usertype'] && $_SESSION['admins']['usertype'] != 1) {
            $array_fields['usertype'] = $_SESSION['admins']['usertype'];
        }
        if(array_key_exists('id_sites', $array_fields) && isset($_SESSION['admins']['id_sites']) && $array_fields['id_sites'] != $_SESSION['admins']['id_sites'] && $_SESSION['admins']['usertype'] != 1) {
            $array_fields['id_sites'] = $_SESSION['admins']['id_sites'];
        }
        if(!empty($primary_key_value)) {
            unset($array_fields['email']);
            $vals = $this->getOne((int) $primary_key_value);
            $array_fields['email'] = $vals['email'];
        }
        if(is_array($array_fields) 
                && array_key_exists('password', $array_fields) 
                && (strlen($array_fields['password'])!=32) 
                && (strlen($array_fields['password']) > 0))     
            {
            $array_fields['password'] = $this->encodePassword($array_fields['password'], $array_fields['email']);
        }
        return parent::set($primary_key_value, $array_fields,$criteres);
    }

    public function encodePassword($clear, $mail)
    {
        #return md5(self::_PWD_HASH_ . $clear . $mail);
        return md5(PWD_HASH . $clear . $mail);
    }

    public function del($id='') 
    {
        if(isset($_SESSION['admins']['usertype']) && $_SESSION['admins']['usertype'] != 1 && $id != $_SESSION['admins']['id_admins']){
             return false;
        }
        return parent::del((int)$id);
    }

    public function checkLogFromFront($sessionId) 
    {
        if(!is_file(implode(DIRECTORY_SEPARATOR, array(GABOX_USR_DIR, 'admin', 'check-log.php')))) die('check-log.php not found');
        if(!defined('CHECK_LOG_HASH')) { error_log('CHECK_LOG_HASH not defined'); return false; }
        if(empty($sessionId)) { error_log('SESSION ID is empty'); return false; }
        $url = BACK_URL . 'check-log.php';
        $url .= '?checkLogHash=' . urlencode(CHECK_LOG_HASH);
        $opts = array(
            'http' => array(
                'method'=>"GET",
                'header'=>"Accept-language: en\r\n" .
                "Cookie: ".session_name()."=".$sessionId."\r\n"
            )
        );
        $context = stream_context_create($opts);
        session_write_close();
        $response = file_get_contents($url, false, $context);
        $isLogged = false;
        if(!empty($response)) {
            $response = str_replace(array("\n", "\r", ' '), '', $response);
            $j = json_decode($response, true);
            $isLogged = is_array($j) && array_key_exists('logged', $j) ? (bool) $j['logged'] : false;
        }
        return $isLogged;
    }

    function isSuperAdmin(){
        if(isset($_SESSION['admins']['usertype']) && $_SESSION['admins']['usertype']==1){
            return true;
        }
        if(isset($_SESSION['admins']) && isset($_SESSION['admins']['id_apps']) && ($_SESSION['admins']['id_apps'] == '-1')){
           return true;
        }

        return false;
    }

}
