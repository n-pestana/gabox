<?php 
function partition( $list, $p ) {
    $listlen = count( $list );
    $partlen = floor( $listlen / $p );
    $partrem = $listlen % $p;
    $partition = array();
    $mark = 0;
    for ($px = 0; $px < $p; $px++) {
        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
        $partition[$px] = array_slice( $list, $mark, $incr );
        $mark += $incr;
    }
    return $partition;
}
function chrono($line){
$time_end = microtime(true);
$time = $time_end - TIME_START;
echo "\nTIME [".$line."]".date("d/m/Y").":".$time;

}
function chrono_comment($line){
$time_end = microtime(true);
$time = $time_end - TIME_START;
echo "\n/*TIME [".$line."]".date("d/m/Y").":".$time.'*/';

}


function log_chrono($line){
$time_end = microtime(true);
$time = $time_end - TIME_START;
error_log("\nTIME [".$line."]".date("d/m/Y").":".$time);

}

function getNoneAsciiTable() {
    return array(
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
         ' '=>'-','\''=>'','"'=>'',";"=>'',","=>"",":"=>"","\n"=>"-",
         "."=>"","\t"=>"-","\r"=>"-","™"=>"","€"=>"", 
        "&"=>"","»"=>"","«"=>"","’"=>"",
        "А"=>"a", "Б"=>"b", "В"=>"v", "Г"=>"g", "Д"=>"d",
        "Е"=>"e", "Ё"=>"yo", "Ж"=>"zh", "З"=>"z", "И"=>"i", 
        "Й"=>"j", "К"=>"k", "Л"=>"l", "М"=>"m", "Н"=>"n", 
        "О"=>"o", "П"=>"p", "Р"=>"r", "С"=>"s", "Т"=>"t", 
        "У"=>"u", "Ф"=>"f", "Х"=>"kh", "Ц"=>"ts", "Ч"=>"ch", 
        "Ш"=>"sh", "Щ"=>"sch", "Ъ"=>"", "Ы"=>"y", "Ь"=>"", 
        "Э"=>"e", "Ю"=>"yu", "Я"=>"ya", "а"=>"a", "б"=>"b", 
        "в"=>"v", "г"=>"g", "д"=>"d", "е"=>"e", "ё"=>"yo", 
        "ж"=>"zh", "з"=>"z", "и"=>"i", "й"=>"j", "к"=>"k", 
        "л"=>"l", "м"=>"m", "н"=>"n", "о"=>"o", "п"=>"p", 
        "р"=>"r", "с"=>"s", "т"=>"t", "у"=>"u", "ф"=>"f", 
        "х"=>"kh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", "щ"=>"sch", 
        "ъ"=>"", "ы"=>"y", "ь"=>"", "э"=>"e", "ю"=>"yu", 
        "я"=>"ya", " "=>"-", "."=>"", ","=>"", "/"=>"-",  
        ":"=>"", ";"=>"","—"=>"", "–"=>"-",
        'Å'=>'A', 'Æ'=>'A', 'Ā'=>'A', 'Ą'=>'A', 'Ă'=>'A',
        'Ç'=>'C', 'Ć'=>'C', 'Č'=>'C', 'Ĉ'=>'C', 'Ċ'=>'C',
        'Ď'=>'D', 'Đ'=>'D', 'È'=>'E', 'É'=>'E', 'Ê'=>'E',
        'Ë'=>'E', 'Ē'=>'E', 'Ę'=>'E', 'Ě'=>'E', 'Ĕ'=>'E',
        'Ė'=>'E', 'Ĝ'=>'G', 'Ğ'=>'G', 'Ġ'=>'G', 'Ģ'=>'G',
        'Ĥ'=>'H', 'Ħ'=>'H', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
        'Ï'=>'I', 'Ī'=>'I', 'Ĩ'=>'I', 'Ĭ'=>'I', 'Į'=>'I',
        'İ'=>'I', 'Ĳ'=>'IJ', 'Ĵ'=>'J', 'Ķ'=>'K', 'Ľ'=>'K',
        'Ĺ'=>'K', 'Ļ'=>'K', 'Ŀ'=>'K', 'Ł'=>'L', 'Ñ'=>'N',
        'Ń'=>'N', 'Ň'=>'N', 'Ņ'=>'N', 'Ŋ'=>'N', 'Ò'=>'O',
        'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O',
        'Ō'=>'O', 'Ő'=>'O', 'Ŏ'=>'O', 'Œ'=>'OE', 'Ŕ'=>'R',
        'Ř'=>'R', 'Ŗ'=>'R', 'Ś'=>'S', 'Ş'=>'S', 'Ŝ'=>'S',
        'Š'=>'S', 'Ť'=>'T', 'Ţ'=>'T', 'Ŧ'=>'T', 'Ù'=>'U',
        'Ú'=>'U', 'Û'=>'U', 'Ü'=>'Ue', 'Ū'=>'U', 'Ů'=>'U',
        'Ű'=>'U', 'Ŭ'=>'U', 'Ũ'=>'U', 'Ų'=>'U', 'Ŵ'=>'W',
        'Ŷ'=>'Y', 'Ÿ'=>'Y', 'Ý'=>'Y', 'Ź'=>'Z', 'Ż'=>'Z',
        'Ž'=>'Z', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a',
        'ä'=>'a', 'ā'=>'a', 'ą'=>'a', 'ă'=>'a', 'å'=>'a',
        'æ'=>'ae', 'ç'=>'c', 'ć'=>'c', 'č'=>'c', 'ĉ'=>'c',
        'ċ'=>'c', 'ď'=>'d', 'đ'=>'d', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ē'=>'e', 'ę'=>'e', 'ě'=>'e',
        'ĕ'=>'e', 'ė'=>'e', 'ƒ'=>'f', 'ĝ'=>'g', 'ğ'=>'g',
        'ġ'=>'g', 'ģ'=>'g', 'ĥ'=>'h', 'ħ'=>'h', 'ì'=>'i',
        'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ī'=>'i', 'ĩ'=>'i',
        'ĭ'=>'i', 'į'=>'i', 'ı'=>'i', 'ĳ'=>'ij', 'ĵ'=>'j',
        'ķ'=>'k', 'ĸ'=>'k', 'ł'=>'l', 'ľ'=>'l', 'ĺ'=>'l',
        'ļ'=>'l', 'ŀ'=>'l', 'ñ'=>'n', 'ń'=>'n', 'ň'=>'n',
        'ņ'=>'n', 'ŉ'=>'n', 'ŋ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'oe', 'ø'=>'o', 'ō'=>'o',
        'ő'=>'o', 'ŏ'=>'o', 'œ'=>'oe', 'ŕ'=>'r', 'ř'=>'r',
        'ŗ'=>'r', 'ś'=>'s', 'š'=>'s', 'ť'=>'t', 'ù'=>'u',
        'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ū'=>'u', 'ů'=>'u',
        'ű'=>'u', 'ŭ'=>'u', 'ũ'=>'u', 'ų'=>'', 'ŵ'=>'w',
        'ÿ'=>'y', 'ý'=>'y', 'ŷ'=>'y', 'ż'=>'z', 'ź'=>'z',
        'ž'=>'z', 'ß'=>'ss', 'ſ'=>'ss', 'Α'=>'A', 'Ά'=>'A',
        'Β'=>'B', 'Γ'=>'G', 'Δ'=>'D', 'Ε'=>'E', 'Έ'=>'E',
        'Ζ'=>'Z', 'Η'=>'I', 'Ή'=>'I', 'Κ'=>'K', 'Λ'=>'L',
        'Μ'=>'M', 'Ν'=>'N', 'Ξ'=>'KS', 'Ο'=>'O', 'Ό'=>'O',
        'Π'=>'P', 'Ρ'=>'R', 'Σ'=>'S', 'Ϋ'=>'Y', 'Φ'=>'F',
        'Ψ'=>'PS', 'Ω'=>'O', 'Ώ'=>'O', 'α'=>'a', 'ά'=>'a',
        'β'=>'b', 'γ'=>'g', 'δ'=>'d', 'ε'=>'e', 'έ'=>'e',
        'ζ'=>'z', 'η'=>'i', 'ή'=>'i', 'κ'=>'k', 'λ'=>'l',
        'μ'=>'m', 'ν'=>'n', 'ξ'=>'ks', 'ό'=>'o', 'π'=>'p',
        'ρ'=>'r', 'φ'=>'f', 'χ'=>'x', 'ψ'=>'ps', 'ω'=>'o',
        'ώ'=>'o', 'Б'=>'B', 'Г'=>'G', 'Д'=>'D', 'Ё'=>'E',
        'Ж'=>'ZH', 'З'=>'Z', 'И'=>'I', 'Й'=>'I', 'Л'=>'L',
        'П'=>'P', 'У'=>'U', 'Ф'=>'F', 'Ц'=>'TS', 'Ч'=>'CH',
        'Ш'=>'SH', 'Щ'=>'SHCH', 'Ы'=>'Y', 'Э'=>'E', 'Ю'=>'YU',
        'Я'=>'YA', 'б'=>'B', 'в'=>'V', 'г'=>'G', 'д'=>'D',
        'е'=>'E', 'ё'=>'E', 'ж'=>'ZH', 'з'=>'Z', 'и'=>'I',
        'й'=>'I', 'к'=>'K', 'л'=>'L', 'м'=>'M', 'н'=>'N', 'п'=>'P',
        'т'=>'T', 'у'=>'U', 'ф'=>'F', 'х'=>'KH', 'ц'=>'TS', 'ч'=>'CH',
        'ш'=>'SH', 'щ'=>'SHCH', 'ы'=>'Y', 'э'=>'E', 'ю'=>'YU',
        'я'=>'YA', 'Ъ'=>'', 'ъ'=>'', 'Ь'=>'', 'ь'=>'',
        'ð'=>'d', 'Ð'=>'D', 'þ'=>'th', 'Þ'=>'TH','é'=>'é');
}
function asciify($value){
    $noneAsciiTable = getNoneAsciiTable();
    $value = strtr("$value", $noneAsciiTable);
    return $value;
}

if(!function_exists('getBackgroundColor')){
function getBackgroundColor($img){
	try {
        	$image = new Imagick($img);
	}
	catch(Exception $e){
		return false;
	}
        $x = 10;
        $y = 10;
        $p1= $image->getImagePixelColor(10, 10);
        $p2= $image->getImagePixelColor(10, 100);
        $controle=$p1->getColor();
        $colors=$p2->getColor();
        if(array_diff($colors,$controle)==array() && $colors['r']<120 && $colors['g']<120 && $colors['b']<120)
         return '#'.dechex($colors['r']).dechex($colors['g']).dechex($colors['b']);
        else
         return false;
}
}

if(!function_exists('DetectLocale')){
function DetectLocale(){
       if(!isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))return array();
        preg_match_all('|.*(..-..).*|isU',$_SERVER["HTTP_ACCEPT_LANGUAGE"],$out);
        if((empty($out) || empty($out[1])) && strlen($_SERVER["HTTP_ACCEPT_LANGUAGE"])==2){
          $out[1] = array($_SERVER["HTTP_ACCEPT_LANGUAGE"],$_SERVER["HTTP_ACCEPT_LANGUAGE"]);;
        }
        $formated_locales = array();
        foreach($out[1] as $langs){
                $formated_locales[] = strtolower(current(explode('-',$langs))).'_'.strtoupper(@end(explode('-',$langs)));
        }
        return $formated_locales;

}
/*
        if(!isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))return array();
	preg_match_all('|.*(..-..).*|isU',$_SERVER["HTTP_ACCEPT_LANGUAGE"],$out);
	$formated_locales = array();
	foreach($out[1] as $langs){
		$formated_locales[] = strtolower(current(explode('-',$langs))).'_'.strtoupper(end(explode('-',$langs)));
		
	}
	return $formated_locales;
*/
}
if(!function_exists('SuppAccents')){
  function SuppAccents($chaine){
     return  SuppAccents2($string);
    $tofind = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
    $replac = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";
    return(strtr($chaine,$tofind,$replac));
  }
}

if(!function_exists('urlize')){
  function urlize($string){
    $find   = array(
     '/[^A-Za-z0-9ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ]/'  #alphanum + accents
     ,'/(^http)/'                # - as end
     ,'/[-]+/'              # multi -
     ,'/(^-)/'              # - as begin
     ,'/(-$)/'                # - as end
    );
    $repl   = array('-','','-','','');
    return  preg_replace($find, $repl, $string);
  }
}
if(!function_exists('getDefaultLanguage')){
function getDefaultLanguage() {

/*
   if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
      return parseDefaultLanguage($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
   else
      return parseDefaultLanguage(NULL);*/
   }
}

if(!function_exists('parseDefaultLanguage')){
function parseDefaultLanguage($http_accept, $deflang = "en") {


   if(isset($http_accept) && strlen($http_accept) > 1)  {
      # Split possible languages into array
      $x = explode(",",$http_accept);
      foreach ($x as $val) {
         #check for q-value and create associative array. No q-value means 1 by rule
         if(preg_match("/(.*);q=([0-1]{0,1}\.\d{0,4})/i",$val,$matches))
            $lang[$matches[1]] = (float)$matches[2];
         else
            $lang[$val] = 1.0;
      }

      #return default language (highest q-value)
      $qval = 0.0;
      foreach ($lang as $key => $value) {
         if ($value > $qval) {
            $qval = (float)$value;
            $deflang = $key;
         }
      }
   }
   return strtolower($deflang);
}
}


if(!function_exists('get_include_contents')){
function get_include_contents($filename) {
    if (is_file($filename)) {
        ob_start();
        include $filename;
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }
    return false;
}
}

if(!function_exists('largeStockByID')){
function largeStockByID($root, $id){
 $path = $root;
 $md = $id;
 $r1 = substr($md,0,1).'/';
 $r2 = substr($md,1,1).'/';
 $r3 = substr($md,2,1).'/';
 $r4 = substr($md,3,1).'/';
 if(!is_dir($path.$r1)) mkdir ($path.$r1);
 if(!is_dir($path.$r1.$r2)) mkdir ($path.$r1.$r2);
 if(!is_dir($path.$r1.$r2.$r3)) mkdir ($path.$r1.$r2.$r3);
 if(!is_dir($path.$r1.$r2.$r3.$r4)) mkdir ($path.$r1.$r2.$r3.$r4);
 return $path.$r1.$r2.$r3.$r4;
}
}

if(!function_exists('SuppAccents3')){
    function SuppAccents3($string){
        //$string = html_entity_decode(valid_utf8($string),null,'UTF-8');
        $search = array( 'ç','ñ','Ä','Â','ä','â','à','Ë','Ê','é','è','ë','ê','Ï','Î','ï','î','Ö','Ô','ö','ô','Ü','Û','ü','û','ó','á','ú','í','+'   ,'&');
        $replace = array('c','n','A','A','a','a','a','E','E','e','e','e','e','I','I','i','i','O','O','o','o','U','U','u','u','o','a','u','i','plus','');
        $string = str_replace($search, $replace, $string);
        return $string;
    }
}

if(!function_exists('urlizeUtf8')){
    function urlizeUtf8($string){
        $string = SuppAccents3(strip_tags(html_entity_decode($string)));
        $find   = array(
            '/[^A-Za-z0-9\/]/'  #alphanum
           ,'/\s+/'                # - as end
           ,'/\ /'                # - as end
           ,'/:/'                # - as end
           ,'/(^http)/'                # - as end
           ,'/[-]+/'              # multi -
           ,'/(^-)/'              # - as begin
           ,'/(-$)/'                # - as end
           ,'/(-\/$)/'                # -/ as end
        );

        $repl   = array('-','-','-','','-','-','','','/');
        return  trim(preg_replace($find, $repl, $string));
    }
}


if(!function_exists('urlize2')){
function urlize2($string){
  
  $string = SuppAccents3(strip_tags(html_entity_decode($string)));

  $find   = array(
   '/[^A-Za-z0-9]/'  #alphanum
   ,'/(^http)/'                # - as end
   ,'/[-]+/'              # multi -
   ,'/(^-)/'              # - as begin
   ,'/(-$)/'                # - as end
  );

  $repl   = array('-','','-','','');
  return  preg_replace($find, $repl, $string);
}
}

if(!function_exists('myasort')){
# because asort cant return value, and i need it into form_fields
function myasort($array = array()){
  asort($array);
  return $array;  

}
}

/*  au cas ou gettext n'est pas installe   {{{ */
if(!function_exists("_")) { function _($str){ return $str ; } };
/*  }}} */

if(!function_exists('kz_sort_by_key')){
# permet de trier un tableau sur un clé niveau2 {{{
function kz_sort_by_key( &$array, $key, $type = 'string'){
     if( empty( $array ) || empty( $key ) ){ return false ;}
     switch($type){
         case 'string':
             return uasort( $array, create_function(
                 '$k1, $k2', 'return strcmp( $k1[\''.$key.'\'], $k2[\''.$key.'\'] );')
             );
             break;
         case 'int':
             return uasort( $array, create_function(
                 '$k1, $k2', 'return ( $k1[\''.$key.'\'] > $k2[\''.$key.'\'] );')
             );
             break;
     }
}
}
# }}}

# memory_get_usage {{{
if (!function_exists('memory_get_usage')) {
    function memory_get_usage(){
        return 'unable to get memory usage under windows';
    }
}
# }}}

if(!function_exists('array_merge_keys')){
# array_merge_keys {{{
function array_merge_keys(){
	$args = func_get_args();
	$result = array();
	foreach($args as &$array){
		foreach($array as $key=>&$value){
			$result[$key] = $value;
		}
	}
	return $result;
}
# }}}
}

if(!function_exists('mkpath')){
function mkpath($path){
    $temp     = explode('/', $path);
    $fullpath = '';
    foreach($temp as $k=>$v){
        $fullpath .= $v.'/';
        if(!is_dir($fullpath)){
            if(!mkdir($fullpath)) return false;
        }
    }
    return true;
}
}

if(!function_exists('readdirr')){
function readdirr($dir){
    $handle = opendir($dir);
    for(;(false !== ($readdir = readdir($handle)));){
        if($readdir != '.' && $readdir != '..'){
            $path = $dir.'/'.$readdir;
            if(is_dir($path))    $output[$readdir] = readdirr($path);
            if(is_file($path))   $output[] = $readdir;
        }
    }
    return isset($output)?$output:false;
    closedir($handle);
}
}


if(!function_exists('path_to_url')){
# path_to_url {{{
function path_to_url($path,$absolute = false){
    # XXX ajouter le protocol
    return (($absolute === true) ? 'http://'.$_SERVER['HTTP_HOST'] : '' ).'/'.str_replace($GLOBALS['conf']['dir']['root'],'',realpath($path));
}
#}}}
}

if(!function_exists('SuppAccents2')){
function SuppAccents2($string){
  //$string = html_entity_decode(valid_utf8($string),null,'UTF-8');
  $search = array( 'ç','ñ','Ä','Â','ä','â','à','Ë','Ê','é','è','ë','ê','Ï','Î','ï','î','Ö','Ô','ö','ô','Ü','Û','ü','û','+');
  $replace = array('c','n','A','A','a','a','a','E','E','e','e','e','e','I','I','i','i','O','O','o','o','U','U','u','u','plus');
  $string = str_replace($search, $replace, $string);
  return $string;
}
}

if(!function_exists('urlizator')){
function urlizator($string){
  $string = SuppAccents2($string);

  $find   = array(
   '/[^A-Za-z0-9]/'  #alphanum
   ,'/(^http)/'                # - as end
   ,'/[-]+/'              # multi -
   ,'/(^-)/'              # - as begin
   ,'/(-$)/'                # - as end
  );

  $repl   = array('-','','-','','');
  return  preg_replace($find, $repl, $string);
}
}

if(!function_exists('get_distance_m')){
function get_distance_m($lat1, $lng1, $lat2, $lng2) {

  $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
  $rlo1 = deg2rad($lng1);
  $rla1 = deg2rad($lat1);
  $rlo2 = deg2rad($lng2);
  $rla2 = deg2rad($lat2);
  $dlo = ($rlo2 - $rlo1) / 2;
  $dla = ($rla2 - $rla1) / 2;
  $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin($dlo));
  $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
  return ($earth_radius * $d);

}
}


if(!function_exists('dateFrtoDbDate')){
function dateFrtoDbDate($dateFr){
	if(empty($dateFr)) return false;
	$reg = '/^(0[1-9]|[12][0-9]|3[01])([- \/.])(0[1-9]|1[012])([- \/.])(\d\d?\d\d)$/';
	preg_match($reg, $dateFr, $matches);
	if( empty($matches) ) return;
	$d = $matches[1];
	$m = $matches[3];
	$y = $matches[5];
	return $y.'-'.$m.'-'.$d;
}
}

if(!function_exists('DbDatetoDateFr')) {
    function DbDatetoDateFr($dbdate) {
        if(empty($dbdate)) return false;
        $e = explode('-', $dbdate);
        if(count($e) === 3) {
            if(strstr($e[2], ':')) {
                $e[2] = substr($e[2], 0, strpos($e[2], ' '));
            }
            return $e[2] . '/' . $e[1] . '/' . $e[0];
        }
        return '';
    }
}
if(!function_exists('checkPassword')){
function checkPassword($pwd) {
    $errors=array();

    if (strlen($pwd) < 8) {
        $errors[] = "Password too short!";
    }

    if (!preg_match("#[0-9]+#", $pwd)) {
        $errors[] = "Password must include at least one number!";
    }

    if (!preg_match("#[a-zA-Z]+#", $pwd)) {
        $errors[] = "Password must include at least one letter!";
    }     
    return (empty($errors)) ? true : $errors;
}
}

function xml_to_array($root) {
    $result = array();

    if ($root->hasAttributes()) {
        $attrs = $root->attributes;
        foreach ($attrs as $attr) {
            $result['@attributes'][$attr->name] = $attr->value;
        }
    }

    if ($root->hasChildNodes()) {
        $children = $root->childNodes;
        if ($children->length == 1) {
            $child = $children->item(0);
            if ($child->nodeType == XML_TEXT_NODE) {
                $result['_value'] = $child->nodeValue;
                return (is_countable($result) && count($result) == 1)
                    ? $result['_value']
                    : $result;
            }
        }
        $groups = array();
        foreach ($children as $child) {
            if (!isset($result[$child->nodeName])) {
                $result[$child->nodeName] = xml_to_array($child);
            } else {
                if (!isset($groups[$child->nodeName])) {
                    $result[$child->nodeName] = array($result[$child->nodeName]);
                    $groups[$child->nodeName] = 1;
                }
                $result[$child->nodeName][] = xml_to_array($child);
            }
        }
    }

    return $result;
}

function Truncate($input, $numWords)
{
  if(str_word_count($input,0)>$numWords)
  {
    $WordKey = str_word_count($input,1);
    $PosKey = str_word_count($input,2);
    reset($PosKey);
    foreach($WordKey as $key => &$value)
    {
        $value=key($PosKey);
        next($PosKey);
    }
    return substr($input,0,$WordKey[$numWords]);
  }
  else {return $input;}
} 

function imgNeedResize($path, $max) {
    list($width, $height) = getimagesize($path);
    $resize = false;
    if(null === $width) return $resize;
    if($width > $height) {
        $resize = $width > $max;
        $w = $max;
        $h = ceil(($max * $height) / $width);
    } else {
        $resize = $height > $max;
        $w = ceil(($max * $width) / $height);
        $h = $max;
    }
    return $resize ? array('w' => $w, 'h' => $h) : false;
}

function resizeImg($finalPath, $maxWidth, $maxHeight, $savePath = null) {
    $percent = 1;
    // Calcul des nouvelles dimensions
    list($width, $height) = getimagesize($finalPath);
    if($width > $maxWidth && $width > $height) {
        $percent = $maxWidth / $width;
    } elseif($height > $maxHeight) {
        $percent = $maxHeight / $height;
    }
    $thumb = null;
    if($percent != 1) {
        $new_width = $width * $percent;
        $new_height = $height * $percent;

        // Redimensionnement
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $type = strtolower(substr(strrchr($finalPath,"."),1));

        // preserve transparency
        if($type == "gif" || $type == "png") {
            imagecolortransparent($image_p, imagecolorallocatealpha($image_p, 0, 0, 0, 127));
            imagealphablending($image_p, false);
            imagesavealpha($image_p, true);
        }

        if($type == "jpeg") $type = "jpg" ;
        switch($type) {
            case "bmp" : $image = imagecreatefromwbmp($finalPath); break;
            case "gif" : $image = imagecreatefromgif($finalPath); break;
            case "jpg" : $image = imagecreatefromjpeg($finalPath); break;
            case "png" : $image = imagecreatefrompng($finalPath); break;
            default: return $thumb;
        }
        //$image = imagecreatefromjpeg($finalPath);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        $f = ($savePath === null) ? '/tmp/thumb_test'.time().'.jpg' : $savePath;
        switch($type) {
            case "bmp" : imagewbmp($image_p, $f); break;
            case "gif" : imagegif($image_p, $f); break;
            case "jpg" : imagejpeg($image_p, $f); break;
            case "png" : imagepng($image_p, $f); break;
            default: imagejpeg($image_p, $f); break;
        }

        imagedestroy($image_p);
        $thumb = file_get_contents($f);
        if($savePath === null) {
            //unlink($f);
        }
    }

    return $thumb;
}

function getRealUserIp(){
    switch(true){
        case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
        case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
        case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
        default : return $_SERVER['REMOTE_ADDR'];
    }
}

function getFOLoginButton($params) {
    if(!defined('AUTOLOG_KEY')) return '';
    $curr_obj = defined('LOGIN_COLLECTOR') ? LOGIN_COLLECTOR : 'clients';
    $vals = $params['values'];
    $checkPwd = defined('AUTOLOG_CHECK_PWD') ? AUTOLOG_CHECK_PWD : true;
    if(empty($vals['email']) || ($checkPwd && empty($vals['password']))) return '';
    $idP = $vals['id_' . $curr_obj];
    $loginKey = base64_encode(md5($idP . $vals['email'] . date('Ymd_') . AUTOLOG_KEY));
    $url = isset($_SERVER['env']) && $_SERVER['env'] == 'prod' ? FRONT_URL : ((defined('ID_SITE')) ? ID_SITE.FRONT_URL : FRONT_URL);
    return '<a target="_blank" href="https://'.$url.'?hash='.HASH_KEY.'&id='.$idP.'&lk='.$loginKey.'"><button type="button" class="small" style="color:white;">'.t("Login").'</button></a>';
}

function resumeSentence($string, $length = 150){
  if( mb_strlen($string) <=  $length ) return $string;
  return preg_replace('@(^.{'.$length.',})[,;\.].*$@isU',"$1.",$string);
}

function convert($size)
{
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function t($string){
 global $gen;
 return nl2br($gen->collector['translations']->translate($string));
}

function translate($str){
    return '__'.$str.'__';
}
function plan($array,$item){
    return $array;
}
function array_flatten_recursive($array) { 
    if($array) { 
        $flat = array(); 
        foreach(new RecursiveIteratorIterator(new RecursiveArrayIterator($array), RecursiveIteratorIterator::SELF_FIRST) as $key=>$value) { 
            if(!is_array($value)) { 
                $flat[] = $value; 
            } 
        } 
        
        return $flat; 
    } else { 
        return false; 
    } 
} 
function getArrayKeysFlat($array) {
    if(!isset($keys) || !is_array($keys)) {
        $keys = array();
    }
    foreach($array as $key => $value) {
        $keys[] = $key;
        if(is_array($value)) {
            $keys = array_merge($keys,getArrayKeysFlat($value));
        }
    }
    return $keys;
}

if(!function_exists('getExcelContent')) {
    function getExcelContent($file, $firstLineContainsName = false)
    {
        require_once INC.'PHPExcel/Classes/PHPExcel/IOFactory.php';
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $values = array();
        $worksheetIdx = 1;
        $beginRow = $firstLineContainsName ? 2 : 1;
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $worksheetTitle     = $worksheet->getTitle();
            $highestRow         = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $nrColumns = ord($highestColumn) - 64;
            for ($row = $beginRow; $row <= $highestRow; ++ $row) {
                for ($col = 0; $col < $highestColumnIndex; ++ $col) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                    $val = $cell->getValue();
                    $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                    /*if(strstr($file, 'email') && ($col == 5 || $col == 6)) {
                    //Dates
                    $val = PHPExcel_Shared_Date::ExcelToPHP($val);
                    $val = date('Y-m-d', $val);
                    }*/
                    $values[$worksheetIdx][$row][$col] = $val;
                }
            }
            $worksheetIdx++;
        }
        return $values;
    }
}

function getCurrentProtocol() {
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
    $protocol = isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https' ? 'https' : $protocol;
    return $protocol;
}

if (!function_exists('random_int')) {
    function random_int($min, $max) {
        if (!function_exists('mcrypt_create_iv')) {
            trigger_error(
                'mcrypt must be loaded for random_int to work',
                E_USER_WARNING
            );
            return null;
        }
       
        if (!is_int($min) || !is_int($max)) {
            trigger_error('$min and $max must be integer values', E_USER_NOTICE);
            $min = (int)$min;
            $max = (int)$max;
        }
       
        if ($min > $max) {
            trigger_error('$max can\'t be lesser than $min', E_USER_WARNING);
            return null;
        }
       
        $range = $counter = $max - $min;
        $bits = 1;
       
        while ($counter >>= 1) {
            ++$bits;
        }
       
        $bytes = (int)max(ceil($bits/8), 1);
        $bitmask = pow(2, $bits) - 1;

        if ($bitmask >= PHP_INT_MAX) {
            $bitmask = PHP_INT_MAX;
        }

        do {
            $result = hexdec(
                bin2hex(
                    mcrypt_create_iv($bytes, MCRYPT_DEV_URANDOM)
                )
            ) & $bitmask;
        } while ($result > $range);

        return $result + $min;
    }
}

function getConditionedClass($string, $resultClassName, $maxLength = 10, $separator = ' ') {
    $res = '';
    $e = explode($separator, $string);
    foreach($e as $word) {
        if(mb_strlen($word) > $maxLength) $res = $resultClassName;
    }
    return $res;
}

function getSelectList($params) {
    global $gen;
    $field = $params['current_field'];
    $id = $params['conf']['class'];
    $objName = $params['options'][$field]['object'];
    $libField = $gen->collector[$objName]->getLibField();
    $selectedValue = isset($params['values'][$field]) && !empty($params['values'][$field]) ? $params['values'][$field] : '';
    $values = $params['values'];
    $html = '<select data-placeholder=" ---- " name="'.$id.'['.$field.']" class="maincat_' . $field . ' chosen-select-deselect" data-el="' . $values['id_'.$id] . '">';
    $html .= '<option value=""></option>';

    $cacheKey = md5(serialize(array($field . $id . $objName)));
    $listVals = $gen->collector[$objName]->getSelectListCache($cacheKey);
    if(null === $listVals) {
        if(method_exists($gen->collector[$objName], 'getChildren')) {
            /*if(method_exists($gen->collector[$objName], 'setRetrieveOnlyValidChildren')) {
                $gen->collector[$objName]->setRetrieveOnlyValidChildren(false);
            }*/
            $listVals = array();
            $listToRetrieve = $params['options'][$field]['hooks'];
            foreach($listToRetrieve as $hook) {
                $rows = $gen->collector[$objName]->getChildren($hook);
                if(!is_array($rows)) $rows = array();
                foreach($rows as $row) {
                    if(array_key_exists('children', $row)) {
                        foreach($row['children'] as $c) {
                            $listVals[$c['id_' . $objName]] = $c[$libField] . ' (' . $c['id_' . $objName] . ')';
                            if(array_key_exists('children', $c)) {
                                foreach($c['children'] as $c2) {
                                    $listVals[$c2['id_' . $objName]] = '&nbsp;-> ' . $c2[$libField] . ' (' . $c2['id_' . $objName] . ')';
                                    if(array_key_exists('children', $c2)) {
                                        foreach($c2['children'] as $c3) {
                                            $listVals[$c3['id_' . $objName]] = '&nbsp;&nbsp;-> ' . $c3[$libField] . ' (' . $c3['id_' . $objName] . ')';
                                            if(array_key_exists('children', $c3)) {
                                                foreach($c3['children'] as $c4) {
                                                    if(!isset($c4['id_' . $objName])) continue;
                                                    $listVals[$c4['id_' . $objName]] = '&nbsp;&nbsp;&nbsp;-> ' . $c4[$libField] . ' (' . $c4['id_' . $objName] . ')';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $gen->collector[$objName]->setSelectListCache($cacheKey, $listVals);
        } else {
            $rows = $gen->collector[$objName]->get();
            $listVals = array();
            foreach($rows as $v) {
                $listVals[$v['id_' . $objName]] = $v[$libField];
            }
            $gen->collector[$objName]->setSelectListCache($cacheKey, $listVals);
        }
    }
    if(is_array($listVals)) {
        foreach($listVals as $key => $val) {
            $selected = $selectedValue == $key ? ' selected="selected"' : '';
            $html .= '<option ' . $selected . ' value="' . $key . '">' . $val . '</option>';
        }
    }
    $html .= '</select>';
    return $html;
}

function getMultiSelectList($params) {
    global $gen;
    $f_id = 'treeview_list';
    $field = $params['current_field'];
    $objName = $params['options'][$field]['object'];
    $ref = $params['options'][$field]['ref_config'];
    $field_struc = array('type' ,'id' ,'label' ,'pattern' ,'errorMsg' ,'maxlength' ,'size' ,'value' ,'arrayValues' ,'cols' ,'rows' ,'onclick' ,'onchange' ,'onsubmit' ,'class' ,'style' ,'optionStyle' ,'placeholder');
    foreach($field_struc as $k_struc) {
        if(!isset($ref[$f_id][$k_struc])) $ref[$f_id][$k_struc] = '';
    }
    $label = $ref[$f_id]['label'];
    $linked_values = array();
    $values = $params['values'];
    return $gen->collector['form']->getFieldHtml($ref, $f_id, $label, 'produits', $linked_values, 'produits', array('id_produits' => $values[$field]));
}
define('PATTERN_EMAIL','');
function resumeSentenceCMS($string, $length = 150){
  if( mb_strlen($string) <=  $length ) return $string;
  return preg_replace('@(^.{'.$length.',})[,:;\.-].*$@isU',"$1.",$string);
}

if(!function_exists("_")) { function _($str){ return $str ; } };

# color_offset{{{

function color_offset($color, $offset, $add = true){
    if(strlen($offset) != 6)     return false;
    if($add === true){
        $r = hexdec(substr($color, 0,2)) + hexdec(substr($offset,0,2));
        $g = hexdec(substr($color, 2,2)) + hexdec(substr($offset,2,2));
        $b = hexdec(substr($color, 4,2)) + hexdec(substr($offset,4,2));
    }
    else{
        $r = hexdec(substr($color, 0,2)) - hexdec(substr($offset,0,2));
        $g = hexdec(substr($color, 2,2)) - hexdec(substr($offset,2,2));
        $b = hexdec(substr($color, 4,2)) - hexdec(substr($offset,4,2));
    }
    
    $r = ($r > 255) ? 255 : (($r<0) ? 0 : $r);
    $g = ($g > 255) ? 255 : (($g<0) ? 0 : $g);
    $b = ($b > 255) ? 255 : (($b<0) ? 0 : $b);

    return  str_pad(dechex($r), 2, '0', STR_PAD_LEFT).dechex($g).str_pad(dechex($b), 2, '0', STR_PAD_LEFT);
}

# }}}

# permet de trier un tableau sur un clé niveau2 {{{
/*function kz_sort_by_key( &$array, $key, $type = 'string'){
     if( empty( $array ) || empty( $key ) ){ return false ;}
     switch($type){
         case 'string':
             return uasort( $array, create_function(
                 '$k1, $k2', 'return strcmp( $k1[\''.$key.'\'], $k2[\''.$key.'\'] );')
             );
             break;
         case 'int':
             return uasort( $array, create_function(
                 '$k1, $k2', 'return ( $k1[\''.$key.'\'] > $k2[\''.$key.'\'] );')
             );
             break;
     }
}
# }}}
*/
# memory_get_usage {{{
if (!function_exists('memory_get_usage')) {
    function memory_get_usage(){
        return 'unable to get memory usage under windows';
    }
}
# }}}

# is_Get_Post{{{
if(!function_exists('is_Get_Post')){
    function is_Get_Post($var){
        $temp = (isset($_GET[$var])) ? $_GET[$var] : '';
        $temp = (empty($temp) && (isset($_POST[$var]))) ? $_POST[$var] : $temp;
        $temp = (!empty($temp)) ? $temp : false;
        return $temp;
    }
}
# }}}

# ajout ou modifie des parametre get {{{
function AddToMyRequestUrl($array, $url = ''){
    # voir utilisation de parse_url
    $url = (empty($url)) ? $_SERVER['PHP_SELF'] : $url;
    $out = array();
    $temp = explode('&', str_replace('?', '&amp;' , $_SERVER['QUERY_STRING']));
    foreach($temp as $k => $v){
        if(empty($v)) continue;
        $temp2 = explode ('=',$v);
        $out[$temp2[0]] = urlencode( (isset($temp2[1])) ? urldecode($temp2[1]) : '');
    }
    $out = array_merge($out, $array);
    $i = 0;
    foreach($out as $k => $v){
        if(trim((string) $v) == '') continue;
        $url .= ($i == 0) ? '?' : '&amp;';
        $url .= $k.'='.urlencode(urldecode($v));
        $i++;
    }
    return $url;
}
# alias a gettext au cas ou {{{
function _kz($string){
    return $string;
    //die('deprecated');
}
# }}}

# fonction utilisée pour les batchs {{{
function echo_ok(){
    return  "\033[1;00m     ["."\033[0;32mOK"."\033[1;00m]\n";
}

function echo_error(){
    return "\033[1;00m  ["."\033[1;31mERROR"."\033[1;00m]\n";
}

function echo_color($string, $color){
    return "\033[1;00m"."\033[1;".$color."m".$string."\033[1;00m";
}


function echo_warning(){
    return "\033[1;00m["."\033[1;33mWARNING"."\033[1;00m]\n";
}
#}}}

function rmdirr($dir) {
    if($objs = glob($dir."/*")){
        foreach($objs as $obj) {
            is_dir($obj)? rmdirr($obj) : unlink($obj);
        }
    }
    rmdir($dir);
}

/* valid_utf8 {{{ */
 /*
 * Converts a UNI CODE number to a UTF-8 multibyte character
 * Algorithm based on script found at From: http://czyborra.com/utf/
 *
 * The binary representation of the character's integer value is thus simply spread across the bytes and the number of high bits set in the lead byte announces the number of bytes in the multibyte sequence:
 *
 *  bytes | bits | representation
 *      1 |    7 | 0vvvvvvv
 *      2 |   11 | 110vvvvv 10vvvvvv
 *      3 |   16 | 1110vvvv 10vvvvvv 10vvvvvv
 *      4 |   21 | 11110vvv 10vvvvvv 10vvvvvv 10vvvvvv
 *      5 |   26 | 111110vv 10vvvvvv 10vvvvvv 10vvvvvv 10vvvvvv
 *      6 |   31 | 1111110v 10vvvvvv 10vvvvvv 10vvvvvv 10vvvvvv 10vvvvvv
 *
 * @param       integer         UNICODE integer
 * @return      string          UTF-8 multibyte character string
 * @see utf8CharToUnumber()
 */
function UnumberToChar($cbyte)  {
        $str='';
        $noCharByteVal=127;

        if ($cbyte < 0x80) {
                $str.=chr($cbyte);
        } else if ($cbyte < 0x800) {
                $str.=chr(0xC0 | ($cbyte >> 6));
                $str.=chr(0x80 | ($cbyte & 0x3F));
        } else if ($cbyte < 0x10000) {
                $str.=chr(0xE0 | ($cbyte >> 12));
                $str.=chr(0x80 | (($cbyte >> 6) & 0x3F));
                $str.=chr(0x80 | ($cbyte & 0x3F));
        } else if ($cbyte < 0x200000) {
                $str.=chr(0xF0 | ($cbyte >> 18));
                $str.=chr(0x80 | (($cbyte >> 12) & 0x3F));
                $str.=chr(0x80 | (($cbyte >> 6) & 0x3F));
                $str.=chr(0x80 | ($cbyte & 0x3F));
        } else if ($cbyte < 0x4000000) {
                $str.=chr(0xF8 | ($cbyte >> 24));
                $str.=chr(0x80 | (($cbyte >> 18) & 0x3F));
                $str.=chr(0x80 | (($cbyte >> 12) & 0x3F));
                $str.=chr(0x80 | (($cbyte >> 6) & 0x3F));
                $str.=chr(0x80 | ($cbyte & 0x3F));
        } else if ($cbyte < 0x80000000) {
                $str.=chr(0xFC | ($cbyte >> 30));
                $str.=chr(0x80 | (($cbyte >> 24) & 0x3F));
                $str.=chr(0x80 | (($cbyte >> 18) & 0x3F));
                $str.=chr(0x80 | (($cbyte >> 12) & 0x3F));
                $str.=chr(0x80 | (($cbyte >> 6) & 0x3F));
                $str.=chr(0x80 | ($cbyte & 0x3F));
        } else { // Cannot express a 32-bit character in UTF-8
                $str .= chr($noCharByteVal);
        }
        return $str;
}
function valid_1byte($char) {
    if(!is_int($char)) return false;
    return ($char & 0x80) == 0x00;
}

function valid_2byte($char) {
    if(!is_int($char)) return false;
    return ($char & 0xE0) == 0xC0;
}

function valid_3byte($char) {
    if(!is_int($char)) return false;
    return ($char & 0xF0) == 0xE0;
}

function valid_4byte($char) {
    if(!is_int($char)) return false;
    return ($char & 0xF8) == 0xF0;
}

function valid_5byte($char) {
    if(!is_int($char)) return false;
    return ($char & 0xFC) == 0xF8;
}

function valid_6byte($char) {
    if(!is_int($char)) return false;
    return ($char & 0xFE) == 0xFC;
}

function valid_nextbyte($char) {
    if(!is_int($char)) return false;
    return ($char & 0xC0) == 0x80;
}

function valid_utf8($string) {
    $mapping = array();
    $len = strlen($string);
    $i = 0;
    $output = "";

    $ret = true;

    while( $i < $len ) {
        $char = ord(substr($string, $i++, 1));
        $ret = true;

        if(valid_1byte($char)) {    // continue
            $ret = true;
        } else if(valid_2byte($char)) { // check 1 byte
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
        } else if(valid_3byte($char)) { // check 2 bytes
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
        } else if(valid_4byte($char)) { // check 3 bytes
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
        } else if(valid_5byte($char)) { // check 4 bytes
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
        } else if(valid_6byte($char)) { // check 5 bytes
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
            if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                $ret = false;
        } // goto next char

        if (!$ret &&  UnumberToChar($char)) {
           $mapping[chr($char)] = UnumberToChar($char);
        }
    }
    if(count($mapping)) {
        $string = str_replace(array_keys($mapping), array_values($mapping), $string);
    }
    return $string;
}
/*  }}} */

/*
    @name   : copy_recursive
    @params : $srcDir       => Dossier source
              $destDir      => Dossier de destination
              $banned_ext   => tableau qui gere les extensions (permet de ne copier que les fichiers autorisés)
*/
function copy_recursive($srcDir, $destDir, $banned_ext = array()) {
    if ($dh = opendir($srcDir)) {
        while (($readdir = readdir($dh)) !== false) {
            if($readdir != '.' && $readdir != '..'){
                $path = $srcDir.'/'.$readdir;
                // Dossier
                if(is_dir($path)) {
                    if( ! is_dir($destDir . $readdir) ) {
                        mkdir($destDir . $readdir);
                    }
                    copy_recursive($path, $destDir . $readdir.'/', $banned_ext);
                }
                // Fichier
                if(is_file($path)) {
                    if(is_file($destDir . $readdir)) unlink($destDir . $readdir);
                    $ext = trim(strtolower(strrchr($readdir,'.')));
                    //echo "'".$ext."'". ' ' . (in_array($ext,$banned_ext) ? 'OUI' : 'NON').' '.$readdir;
                    if ( ! in_array($ext,$banned_ext) ) {
                        copy($path, $destDir . $readdir);
                        @chmod($destDir . $readdir, 0777); 
                    }
                }
            }
        }
    }
    closedir($dh);
    return true;
}



function getRequest($var, $class, $func, $key, $val) {
    if( ! isset($_REQUEST[$var]) ) return array();
    $rs = $GLOBALS['obj'][$class]->$func($_REQUEST[$var]);
    if(empty($rs)) return array();
    return array($rs[$key] => $rs[$val]);
}


function makeTableCheckboxes($values, $properties = array()) {
	if(!is_array($values)) return NULL;
	$display = '<table><tbody>';
	$i = 0;
	foreach($values as $k => $v) {
		//if($i%2) $display .= '<tr>';
		$display .= '<tr><td><input type="checkbox" name="'.$properties['class'].'['.$properties['name'].'][]" class="check" value="'.$k.'" />'.$v.'</td></tr>';
		//if($i%2) $display .= '</tr>';
		$i++;
	}
	$display .= '</tbody></table>';
	return $display;
}


/*  date2char  {{{ */
/**
 * fonction pour mettre les dates sous le bon format de base
 *
 * @param mixed $date
 * @param mixed $format
 * @access public
 * @return void
 */
function date2char($date, $format = 'd-m-y') {
	if( empty($date) ) return NULL;
	switch($format) {
		case 'd-m-y': // dd-mm-yyyy
			$reg = '/^(0[1-9]|[12][0-9]|3[01])([- \/.])(0[1-9]|1[012])([- \/.])(\d\d?\d\d)$/';
			preg_match($reg, $date, $matches);
			if( empty($matches) ) return;
			$d = $matches[1];
			$m = $matches[3];
			$y = $matches[5];
			break;
		case 'm-d-y': // mm-dd-yyyy
			$reg = '/^(0[1-9]|1[012])([- \/.])(0[1-9]|[12][0-9]|3[01])([- \/.])(\d\d?\d\d)$/';
			preg_match($reg, $date, $matches);
			if( empty($matches) ) return;
			$d = $matches[3];
			$m = $matches[1];
			$y = $matches[5];
			break;
		case 'y-m-d': // yyyy-mm-dd
			$reg = '/^(\d\d?\d\d)([- \/.])(0[1-9]|1[012])([- \/.])(0[1-9]|[12][0-9]|3[01])$/';
			preg_match($reg, $date, $matches);
			if( empty($matches) ) return;
			$d = $matches[5];
			$m = $matches[3];
			$y = $matches[1];
			break;
	}
	//$timestampTZ = mktime(0, 0 , 0, intval($m), intval($d), intval($y));
	return (isset($y) && isset($m) && isset($d)) ? $y.$m.$d : NULL;
}
/*  }}} */

function getmicrotime($start = '') {
    list($mirco_sec, $sec) = explode(" ",microtime());
    if(!empty($start))
        return round(((float)$mirco_sec + (float)$sec)-$start,4);
    else
        return ((float)$mirco_sec + (float)$sec);
}

function valid_format($fields_values, $className) {
	// Verif du format de certains champs
	foreach($fields_values as $k => $v) {
		// date ? (voir pour étendre le format si besoin a l'aide du fichier de conf)
		preg_match('/^(0[1-9]|[12][0-9]|3[01])([- \/.])(0[1-9]|1[012])([- \/.])(\d\d?\d\d)$/', $v, $res);
		if(!empty($res)) $fields_values[$k] = date2char($v);
		if(is_array($v)) {
			if(isset($GLOBALS['obj'][$className]->champsTableaux) && in_array($k, array_keys($GLOBALS['obj'][$className]->champsTableaux))) $fields_values[$k] = arrayFieldFormat($v);
			else $fields_values[$k] = $v[key($v)];
		}
	}
	return $fields_values;
}

/*  setLocale   {{{ */
/**
 * Initialise la locale actuelle
 * @param string $localeId Id de la locale
*/

function _setLocale($localeId = null, $LANG = null) {

        //if(FROM_ZENI) die ($localeId.'-'.$LANG);
    try {
        $_SESSION[$GLOBALS['conf']['global']['env']]['LOCALE'] = $LANG;
        // Si on est sur le front, on change la langue du CMS a chaque fois
        if($GLOBALS['conf']['global']['env'] == 'front') {
            if(isset($_SESSION['zp2'][$GLOBALS['conf']['global']['env']]['langue']))
                $_SESSION['zp2'][$GLOBALS['conf']['global']['env']]['langue']  = strtolower($LANG);
        }
        $localeId .= '.UTF-8';
        # c la que ca plante
        $locale = isset($localeId) ? $localeId :  'fr_FR.utf-8';
        $locale = str_replace('utf-8', 'UTF-8', $locale);
        $l = array($locale, substr($locale, 0, 5) , substr($locale, 0, 2));
        setlocale(LC_ALL,  $l);

        $domain = $GLOBALS['conf']['global']['site'];
        bindtextdomain($domain, $GLOBALS['conf']['dir']['i18n']);
        textdomain($domain);
        bind_textdomain_codeset($domain, 'UTF-8');
    } catch (Exception $e) {
        die('setlocaleerror');
    }
}
/*  }}} */


function charToDate($val) {
	if(strlen($val) != 8) return $val;
	// Format fixe pour le moment
	$format = 'aaaammdd';
	$date = substr($val, 6, 2) . '/' . substr($val, 4, 2) . '/' . substr($val, 0, 4);
	return $date;
}

function cleanForDB($param) {
	$conv = array("'" => "\'", "é" => "É", "è" => "È");
	foreach($conv as $k => $v) { $param = str_replace($k, $v, $param); }
	return $param;
}

function toHTML($sql,$fieldsOptions = array(), $nbr_per_ligne = 20, $current_page = 1, $extra_conf = array('more_info' => false, 'class' => '', 'excludeFromDisplay' => NULL,'limited'=>false)){
    global $form, $gen;
	if(!defined('AJAX_PASSPHRASE')) define('AJAX_PASSPHRASE',rand());
    $getRow = (isset($extra_conf['getrow']) && $extra_conf['getrow']==true) ? true : false;
	if($extra_conf['limited'] == true){
		$sql .= ' limit 50';
		$rs = $GLOBALS['obj']['db']->Execute($sql);
        $totalEntries = 50;
	} else {
        $rsCount = $GLOBALS['obj']['db']->Execute($sql);
        $totalEntries = $gen->RecordCount($rsCount);
        $sql .= ' LIMIT '. $nbr_per_ligne . ' OFFSET ' . ($nbr_per_ligne * ($current_page - 1));
        $rs = $GLOBALS['obj']['db']->Execute($sql);
	}

	$body = $pager = '';
    if(!isset($extra_conf['title'])) $extra_conf['title'] = '';
	$before='<h3>'.$extra_conf['title'].' '. $totalEntries .' '.t("résultat(s)").'</h3>';
    if(isset($extra_conf['heading']) && !empty($extra_conf['heading'])){
        $before .= '<div class="card-header bg-light">'.$extra_conf['heading'].'</div>';
    }
    $before .=' <div class="block-border">';

	$thead= '<form id="genlist_form" method="POST"><input type="hidden" name="bulk_action"><div class="row"><table class="table table table-bordered nowrap dataTable table table-striped >
                    <thead>
                     <tr role="row">';

	$firstLoop = true;
	$no_table = (isset($extra_conf['no_table']) && $extra_conf['no_table'] === true);
	$j = 0;
	if(!is_object($rs)){
		echo 'To html error, no object found by the request :'."<br />".$sql;
		return false; 
	}
	$rs_array = $gen->GetRows($rs);
    $curr_obj = $extra_conf['class'];
    
    if(empty($rs_array)){
        if(!isset($action)) $action = '';
        $thead =
            ' <div class="p-4 m-4 alert alert-primary" role="alert">
                <p>'.t("Aucun enregistrement trouvé !").' <br><br> '.t("Vous pouvez en ajouter").' :<br /><br /> 
                    <a class="btn btn-outline-primary" href="'.$action.'?id_'.$curr_obj.'">
                    <i class="feather icon-plus-square"></i>&nbsp;'.t("Ajouter un nouvel élément").'</a>
                </p>
             </div>';
    }

	if(!empty($extra_conf['excludeFromDisplay'])) {
		if(isset($extra_conf['excludeFromDisplay']) && !empty($extra_conf['excludeFromDisplay']) ) unset($extra_conf['excludeFromDisplay']);
	}
	foreach($rs_array as $kc=>$vc){
		$brut_values=$rs_array[$kc];
		$i = 0;
		$j ++;
		$tr_id = ($extra_conf['more_info'] == true && isset($GLOBALS['obj'][$extra_conf['class']]) && isset($vc[$GLOBALS['obj'][$extra_conf['class']]->primary_key]) ) 
			? 'id="tr_'.$vc[$GLOBALS['obj'][$extra_conf['class']]->primary_key].'"'
			: 'id='.$kc;


		foreach($vc as $k_temp => $v_temp){
			# pour les liens $ 
			$$k_temp = $v_temp;
		}
		if($extra_conf['more_info'] && (isset($extra_conf['tr_class'])) && !empty($extra_conf['tr_class'])){
			$tr_class = eval('return '.$extra_conf['tr_class'].';');
		}
		else{
			$tr_class = ($j%2) ? 'odd' : 'even';
		}

		$tr_class = 'class="'.$tr_class.'"';

		$body .= "\t".'<tr role="row" '.$tr_class.' '.$tr_id.'>'."\n";
		$first_field= true;	
		foreach($fieldsOptions as $kf=>$vf){
			if($getRow && $vc[$GLOBALS['obj'][$extra_conf['class']]->primary_key]!=$_GET['id']) continue;
			$rs_array[$kc][$kf] = (isset($rs_array[$kc][$kf])) 
				? $rs_array[$kc][$kf]
				: '';

			if($firstLoop == true){
				if(false){
					$thead .= '<th class=""><span class="">ID</span></th>';
				}	
				else{
				$thead .='<th style="color:black" scope="col">';
				if(!isset($fieldsOptions[$kf]['sortable']) || ($fieldsOptions[$kf]['sortable'] === true)){
					$thead .='<span class="column-sort">';
					$thead .='<a href="'.AddToMyRequestUrl(array('orderby'=>$kf,'sens'=>'desc')).'" title="Sort up" class="sort-up active"></a>';
					$thead .='<a href="'.AddToMyRequestUrl(array('orderby'=>$kf,'sens'=>'asc')).'" title="Sort down" class="sort-down"></a>';
					$thead .='</span>';

				}
				$thead.= ((isset($fieldsOptions[$kf]['label'])) ? $fieldsOptions[$kf]['label'] : $kf  ) ;
				}
				$thead .='</th>';
				$first_field=false;
		
			}
			if(!isset($fieldsOptions[$kf])) $fieldsOptions[$kf] = array();
			if(isset($fieldsOptions[$kf]['callback']) && !empty($fieldsOptions[$kf]['callback'])) {
                            $cbVal = $rs_array[$kc][$kf];
                            if(isset($fieldsOptions[$kf]['callbackFieldValue']) && (isset($brut_values[$fieldsOptions[$kf]['callbackFieldValue']]) || $fieldsOptions[$kf]['callbackFieldValue'] == 'rowset')) {
                                $fv = $fieldsOptions[$kf]['callbackFieldValue'];
                                $cbVal = $fv == 'rowset' ? $brut_values : $brut_values[$fv];
                            }
                            if(strstr($fieldsOptions[$kf]['callback'], '::')) {
                                $expCb = explode('::', $fieldsOptions[$kf]['callback']);
                                $meth = (string)$expCb[1];
                                if(isset($GLOBALS['gen']->collector[$expCb[0]]) && method_exists($GLOBALS['gen']->collector[$expCb[0]],$meth)){
                                    $rs_array[$kc][$kf] = $GLOBALS['gen']->collector[$expCb[0]]->$meth($cbVal);
                                }
                            } else {
                                $rs_array[$kc][$kf] = $fieldsOptions[$kf]['callback']($cbVal);
                            }
                        }
			if(isset($fieldsOptions[$kf]['advanced_callback']) && !empty($fieldsOptions[$kf]['advanced_callback'])) $rs_array[$kc][$kf] = $fieldsOptions[$kf]['advanced_callback'](array('options'=>$fieldsOptions,'conf'=>$extra_conf,'values'=>$brut_values, 'current_field' => $kf));

			if(isset($fieldsOptions[$kf]['before'])) $rs_array[$kc][$kf] = $fieldsOptions[$kf]['before'].$rs_array[$kc][$kf];
			if(isset($fieldsOptions[$kf]['after'])) $rs_array[$kc][$kf] .= $fieldsOptions[$kf]['after'];
			if($rs_array[$kc][$kf]=='' && isset($fieldsOptions[$kf]['label'])) $rs_array[$kc][$kf] = '';

                    
            $btn = ($kf=='delete') ? 'danger' : (($kf=='edit') ? 'success' : 'primary');
			if(isset($fieldsOptions[$kf]['href'])) $rs_array[$kc][$kf] = '<a  href="'.eval('return "'.$fieldsOptions[$kf]['href'].'";').'" class="button btn btn-sm btn-outline-'.$btn.' '.$kf.'">'. $fieldsOptions[$kf]['label'].'<span>'.$rs_array[$kc][$kf].'</span></a>';

			$body .= "\t\t".'<'.(($no_table) ? 'li' : 'td').' class="'.((isset($fieldsOptions[$kf]['class'])) ? $fieldsOptions[$kf]['class']  : $kf).'" ';
			$body .=' >';
			if(isset($fieldsOptions[$kf]['edit_in_place']) && $fieldsOptions[$kf]['edit_in_place']!=false){
				if(!isset($fieldsOptions[$kf]['type'])) $fieldsOptions[$kf]['type']='string';
				
				if( $fieldsOptions[$kf]['type']=='enum'){
					$body.='<select  rel="'.securitize($vc[$GLOBALS['obj'][$extra_conf['class']]->primary_key]).'" class="edit_in_place '.$fieldsOptions[$kf]['type'].'" id="'.$extra_conf['class'].'@'.$kf.'@'.$vc[$GLOBALS['obj'][$extra_conf['class']]->primary_key].'@'.$fieldsOptions[$kf]['type'].'">';
					foreach($GLOBALS['obj'][$extra_conf['class']]->$kf  as $k_eps=>$v_eps){
						($k_eps==$rs_array[$kc][$kf]) ?  $selected='selected ="selected"' : $selected='';
						$body .= '<option value="'.$k_eps.'" '.$selected.' class="'.$extra_conf['class'].'-'.$k_eps.'">'.((isset($v_eps['lib'])) ? $v_eps['lib'] : $k_eps).'</option>';
					}
					$body .='</select>';
				}
				else{
					$body.='<p rel="'.securitize($vc[$GLOBALS['obj'][$extra_conf['class']]->primary_key]).'" class="edit_in_place '.$fieldsOptions[$kf]['type'].'" id="'.$extra_conf['class'].'@'.$kf.'@'.$vc[$GLOBALS['obj'][$extra_conf['class']]->primary_key].'@'.$fieldsOptions[$kf]['type'].'">';
					$display_more = ($extra_conf['more_info'] == true && !empty($extra_conf['class']) && $i == 0) ? true : false;
					$body .= $rs_array[$kc][$kf];
					$body.='</p>';

				}

			}
			else{
				$body .= $rs_array[$kc][$kf];
			}
			$body .= '</'.(($no_table) ? 'li' : 'td').'>'."\n";
			$i++;
		}
		$body .= "\t".'</'.(($no_table) ? 'ul' : 'tr').'>'."\n";
		$firstLoop = false;
		if($getRow) return $body;
	}

    $lastPageNo = ceil($totalEntries / $nbr_per_ligne);
    if( $lastPageNo > 1) {

		$pager= '<div class="block-controls"><ul class="pagination justify-content-center controls-buttons">';

        # previous
		if($current_page > 1)
			$pager.= '<li class="page-item"><a href="'.AddToMyRequestUrl(array('cp'=>$current_page-1)).'" title="Previous" class="page-link">'.t("Précédent").'</a></li>';

        # page list
		for($i=1;$i<=$lastPageNo;$i++){
            if($i==1 || $i==$current_page || $i==$lastPageNo || ((($i+5)>$current_page)&&(($i-5)<$current_page)) ){
			$pager.= ($current_page != $i) 
				? (' <li class="page-item"><a href="'.AddToMyRequestUrl(array('cp'=>$i)).'" title="Page '.$i.'" class="page-link"><b>'.$i.'</b></a></li>') 
				: (' <li class="page-item active"><span  class="current page-link" title="Page '.$i.'">'.$i.'</span></li>'); 
            }
            elseif($i==1 || $i==$current_page || $i==$lastPageNo || ((($i+6)>$current_page)&&(($i-6)<$current_page)) ){
                $pager.='<li class="page-item"><a href="#" class="page-link">...</a></li>';
            }
		}
        # next
		if($current_page < $lastPageNo)
			$pager.= '<li class="page-item"><a href="'.AddToMyRequestUrl(array('cp'=>$current_page+1)).'" title="Next" class="page-link">'.t("Suivant").'</a></li>';

		$pager .= '</ul> </div>';
	}
	$body .= '</tbody> </table></div></form>';
	$thead .='</tr> </thead>';

    global $hide_resultsPerPage, $searchTitle;
    $tools = '
                
                    <div class="row mt-4 p-4 gen-admin-pager">' . ($hide_resultsPerPage ? '' : '
                        <div class="col-sm-12 col-md-3">
                            <form action="'.$_SERVER['PHP_SELF'] .(  ($_SERVER['QUERY_STRING'] != '' ? '?' . $_SERVER['QUERY_STRING'] : '')).'" method="get" class="form-inline">
                                <div class="form-group mx-sm-3">
                                        '.t("Afficher").' &nbsp; 
                                            <select name="limit" class="custom-select custom-select-sm form-control form-control-sm" onchange="this.form.submit()">
                                                <option value="10" '.((isset($_SESSION['limit']) && $_SESSION['limit'] == 10) ? 'selected="selected"' : '').'>10</option>
                                                <option value="50" '.((isset($_SESSION['limit']) && $_SESSION['limit'] == 50) ? 'selected="selected"' : '').'>50</option>
                                                <option value="100" '.((isset($_SESSION['limit']) && $_SESSION['limit'] == 100) ? 'selected="selected"' : '').'>100</option>
                                                <option value="500" '.((isset($_SESSION['limit']) && $_SESSION['limit'] == 500) ? 'selected="selected"' : '').'>500</option>
                                                <option value="5000" '.((isset($_SESSION['limit']) && $_SESSION['limit'] == 5000) ? 'selected="selected"' : '').'>5000</option>
                                            </select>&nbsp;'.t("résultat(s)").'

                              </div>
                          </form>
                       </div>') . '
                        <div class="col-sm-12 col-md-6 gen-admin-search">
                            <div class="colum-rendr_filter">
                                <form method="get" class="form-inline">
                                    <label>' . ($searchTitle ?? t("Rechercher")) . ' :&nbsp;
                                       <input type="text" name="search" class="form-control form-control-sm" value="'.urldecode((isset($_GET['search'])) ? urldecode($_GET['search']) : '').'" />
                                        &nbsp;<a href="'.AddToMyRequestUrl(array('search'=>'')).'"><button type="button" class="btn btn-primary"><i class="feather icon-delete"></i> '.t("Annuler").'</button></a>
                                    </label>
                                    ' . ((!empty($extra_conf['search_extra'])) ? '<div class="pl-1">' . $extra_conf['search_extra'] . '</div>' : '') . ' 
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2 gen-admin-dl" >
                                <a href="'.AddToMyRequestUrl(array('dl'=>'1')).'" class="nowait"><button type="button" class="btn btn-primary"><i class="feather icon-download"></i> '.t("Télécharger").'</button></a>
                            </div>';

                         global $hide_add;
                         if(!isset($hide_add)|| $hide_add==false){

                                if(!isset($form->action)){
                                    if(file_exists( $curr_obj.'.edit.php')){
                                        $action = $curr_obj.'.edit.php';
                                    }
                                    else {
                                        $action = 'admin.php';
                                    }
                                }
                                else{
                                    $action = $form->action;
                                }

                                 $tools.='<div class="col-sm-12 col-md-1 gen-admin-add">';
                                $tools.='<a class="btn btn-outline-warning " href="'.$action.'?id_'.$curr_obj.'="><i class="feather icon-plus-square"></i>&nbsp'.t("Ajouter").'</a>';
                                ?>


                            <?php $tools .='';?>
                            <?php } ?>

                   <?php  if(isset($extra_search)) $tools .= $extra_search?>

        

               <?php $tools .= ' </div>
            </div>';

	return  $before.$tools.$pager.$thead.$body.$pager;

	
}

function myCheckbox($v){
    $r = '<input value="'.$v.'" type="checkbox" value="'.$v.'" class="check" name="check[]" />&nbsp;'.$v;
    return $r;
}
function dateFr($v){
	return date("d/m/Y - H:i:s",strtotime($v));
}
function getEnable($v){
	$enabled = ($v==1) ? 'checked="checked"' : '';  
	return '<input type="checkbox" name="" id="" value="1" class="eip-switch mini-switch with-tip" title="Enable/disable switch" '.$enabled.'>';  
}
function getPurchasableByID_SITE($v){
  #XXX 
  $status = $GLOBALS['gen']->collector['i18n']->getPurchasableByID_SITE(ID_SITE,$v,'produits');
  $return = 1;
  if(empty($status)) $return = 1; # all on 
  elseif(in_array(1,$status) && in_array(0,$status))$return = 2; # mixed on & off #
  elseif(in_array(1,$status) && !in_array(0,$status))$return = 1; #all on  
  elseif(!in_array(1,$status) && in_array(0,$status))$return = 3; #all off
  $enabled = ($return!=3) ? 'checked="checked"' : '';  
  return '<input type="checkbox" name="" id="" value="1" class="eip-switch-countrypurchase mini-switch with-tip" title="Enable/disable switch" '.$enabled.'>'; 
}

function getEnableByID_SITE($v){
  #XXX 
  $status = $GLOBALS['gen']->collector['i18n']->getStatusByID_SITE(ID_SITE,$v,'produits');
  $return = 1;
  if(empty($status)) $return = 1; # all on 
  elseif(in_array(1,$status) && in_array(0,$status))$return = 2; # mixed on & off
  elseif(in_array(1,$status) && !in_array(0,$status))$return = 1; #all on  
  elseif(!in_array(1,$status) && in_array(0,$status))$return = 3; #all off
  $enabled = ($return!=3) ? 'checked="checked"' : '';  
  return '<input type="checkbox" name="" id="" value="1" class="eip-switch-country mini-switch with-tip" title="Enable/disable switch" '.$enabled.'>'; 
}

function getDiagQuestionsEnableByID_SITE($v){
  #XXX 
  $status = $GLOBALS['gen']->collector['i18n']->getStatusByID_SITE(ID_SITE,$v,'diag_reponses');
  $return = 1;
  if(empty($status)) $return = 1; # all on 
  elseif(in_array(1,$status) && in_array(0,$status))$return = 2; # mixed on & off
  elseif(in_array(1,$status) && !in_array(0,$status))$return = 1; #all on  
  elseif(!in_array(1,$status) && in_array(0,$status))$return = 3; #all off
  $enabled = ($return!=3) ? 'checked="checked"' : '';  
  return '<input type="checkbox" name="" id="" value="1" class="eip-switch-country mini-switch with-tip" title="Enable/disable switch" '.$enabled.'>'; 
}
function getCatEnableByID_SITE($v){
  #XXX 
  $status = $GLOBALS['gen']->collector['i18n']->getStatusByID_SITE(ID_SITE,$v,'pcat');
  $return = 1;
  if(empty($status)) $return = 1; # all on 
  elseif(in_array(1,$status) && in_array(0,$status))$return = 2; # mixed on & off
  elseif(in_array(1,$status) && !in_array(0,$status))$return = 1; #all on  
  elseif(!in_array(1,$status) && in_array(0,$status))$return = 3; #all off
  $enabled = ($return!=3) ? 'checked="checked"' : '';  
  return '<input type="checkbox" name="" id="" value="1" class="eip-switch-country mini-switch with-tip" title="Enable/disable switch" '.$enabled.'>'; 
}

function OuiNon($v){
	return ($v==0) ?  _kz("Non") :  _kz("Oui") ; 
}

function tagWithStatus($param){
	$t = explode(',',$param['values']['locale']);
	$out = '<ul class="keywords">';
	foreach($t as $k=>$v){
		$loc = explode('|',$v);
		$enabled = ($loc[0]==1) ? 'checked="checked"' : '';  
		$color = ($loc[0]==0) ? 'purple' : 'green';
		$out .= '<li  class="'.$color.'-keyword" style="float: left;">'.$loc[1].'&nbsp;&nbsp;<input type="checkbox" name="" id="" value="1" class="eip-multi-switch mini-switch with-tip" title="Enable/disable language switch" '.$enabled.'><p id="sites_locales@is_enabled@'.$loc[2].'@string" class="" rel="'.securitize($loc[2]).'"></p></li>'; 
	}
	$out .= '</ul>';
	return $out;
}

function flagize($v){
	$t = explode(',',$v);
	$out ='';
	foreach($t as $k=>$f){
        $e = explode('_',$f);
        $flag = strtolower(end($e));
		$out .= '<img src="/assets/images/icons/flags/'. $flag .'.png" />&nbsp;&nbsp;';

	}
	return $out;
}

function actionize($id,$action){
	return '<a href="'.$action.'" rel="'.securitize($id,$action).'" class="'.$id.' btn btn-primary">'.$action.'</a>';
}
function YesNoBul($v){
        return ($v==0) ?  '<img src="/assets/images/icons/fugue/status-busy.png" />' :  '<img src="/assets/images/icons/fugue/tick-circle.png">' ;
}

function getTranslationsUrl($v) {
    if(empty($v)) return '';
    global $gen;
    $urls = $gen->collector['translations_url']->get(array('id_translations' => (int) $v));
    $out = '';
    if(is_array($urls) && count($urls) > 0) {
        $out = '<div class="more-events">View Urls<ul>';
        foreach($urls as $url) {
            if(empty(trim($url['url']))) continue;
            $out .= '<li>' . $url['url'] . '</li>';
        }
        $out .='</ul></div>';
    }
    return $out; 
    //  echo FRONT_CROSS_URL.$v;
}

function getTranslationsLib($v){
    //$temp = $GLOBALS['gen']->collector['translations']->getRows('select url from gen_translations_url where id_translations = '.(int)$v);
//    print_r($temp);

    
  //if(empty($v)) return '';
 $temp =explode('|',$v);
$out = '<div class="more-events">View Urls<ul><li>';
$out .= implode('<li>',array_unique($temp));  
$out .='</ul></div>';
  return  $out; 
//  echo FRONT_CROSS_URL.$v;
}


function getBloc($v){
  //$a = array(1=>'main bloc',2=>'block 1',3=>'block 2', 4=>'block 3');
//return $a[$v];
 return '<img src="/assets/images/bloc'.$v.'.png" width="60" style="border:1px solid #666" />';

}
function getMedia($v){
  $out = '<div class="thumb">
            <a href="'.$v.'" data-lightbox="'.$v.'" data-title="">
                <img src="'.$v.'" alt="" class="img-fluid img-thumbnail" style="max-width:100px">
            </a>
        </div>';
 return $out; 
}

function getLogo($v){
 return '<center><img src="'.$v.'" height="40" style="height:40px" /></center>';
}

function getProductName($product_id){
	if( ! isset($GLOBALS['obj']['produits'])) return $product_id;
	$object = $GLOBALS['obj']['produits'];
	$object->SetUsedFields(array('id_produits' ,'lib'));
	$rs = $object->getOne(array('id_produits' => $product_id));
	return $rs['lib'];
}
	

function hasTranslation($id){
    $total = count($GLOBALS['form']->restrictions['locales']);
    $cond = array('module'=>'translations','field_name'=>'translations','id_element'=>$id,'id_locales'=>$GLOBALS['form']->restrictions['locales']);
    $temp = $GLOBALS['gen']->collector['i18n']->get($cond);
    $cnt =0;
    $info = '';
    if(is_array($temp))foreach ($temp as $k=>$v){
	if(!empty($v['lib'])) $cnt++;
	$info.='<li>'.$v['lib'].'</li>';
	
    }
//    $cnt= count($temp);
    $color="orange";
    if($cnt == $total)$color="green";
    if($cnt<2)$color="purple";
    return "<center class='info-lib-popup'><ul class='keywords'><li class='".$color."-keyword'>".$cnt.'/'.$total."</li></ul><span class='lib-info'><ul>".$info."</ul></span></center>";
}

function getRelationnalValue($params)
{
    global $gen;
    $field = $params['current_field'];
    $objName = $params['options'][$field]['object'];
    $addBgClass = array_key_exists('bg_class', $gen->collector[$objName]->array_fields);
    if(!$addBgClass) {
        $addBgClass = isset($params['options'][$field]['bg_class']) ? (bool) $params['options'][$field]['bg_class'] : false;
    }
    $cache = array();
    if(isset($params['values'][$field]) && !empty($params['values'][$field])) {
        $v = $params['values'][$field];
        // Si déjà loadé par les filtres
        if(false === $addBgClass && isset($params['options'][$field]['filter']['values'][$v])) {
            $lib = $params['options'][$field]['filter']['values'][$v];
        } elseif(array_key_exists($v, $cache)) {
            $lib = $cache[$v];
        } else {
            $value = $gen->collector[$objName]->getOne((int) $v);
            $lib = $value['lib'];
            if(!$addBgClass && isset($value['bg_class'])) {
                $addBgClass = true;
            }
            if($addBgClass && isset($value['bg_class']) && !empty($value['bg_class'])) {
                $lib = '<span class="label ' . $value['bg_class'] . '">' . $lib . '</span>';
            }
            $cache[$v] = $lib;
        }
    } else {
        $lib = $params['values'][$field];
    }
    return $lib;
}

function get_internal_cache($var){
    // TODO: change md5 to a more recent hash algorithm
    if (defined('ID_LOC')) {
        $file = GABOX_USR_DIR . '/var/cache/db/' . md5($var . ID_LOC);
    } else {
        $file = GABOX_USR_DIR . '/var/cache/db/' . md5($var);
    }
    if (!file_exists($file)){
        return false;
    }
    return json_decode(file_get_contents($file),true);
}

function set_internal_cache($var, $data){
    if (defined('ID_LOC')) {
        $file = GABOX_USR_DIR . '/var/cache/db/' . md5($var.ID_LOC);
    } else {
        $file = GABOX_USR_DIR . '/var/cache/db/' . md5($var);
    }
    if (!is_dir(GABOX_USR_DIR . '/var/cache/db')) {
        mkdir(GABOX_USR_DIR . '/var/cache/db');
    }
    return file_put_contents($file, json_encode($data));
}

function securitize($id){
    return md5(AJAX_PASSPHRASE.$id);
}

function getLangUrls($uri, Displayer $displayer, $availableLocales = array()) {
    $urlByLocale = array();
    // Retrieve urls with collectors that use urlTrait
    foreach($displayer->collector as $collectorName => $collector) {
        if(substr($collectorName, -4) == '_url' && $collectorName != 'translations_url' && array_key_exists('url', $collector->array_fields)) {
            $testUrl = $collector->getOne(array('LIKE>url' => $uri));
            if(is_array($testUrl) && count($testUrl) > 0) {
                // Get all locales urls
                $keyName = 'id_' . str_replace('_url', '', $collectorName);
                $params = array($keyName => $testUrl[$keyName]);
                if(count($availableLocales) > 0) {
                    $params['id_locale'] = $availableLocales;
                }
                if(array_key_exists('is_canonical', $collector->array_fields)) {
                    $params['is_canonical'] = 1;
                }
                $allUrls = $collector->get($params);
                foreach($allUrls as $locUrl) {
                    $loc = $displayer->collector['locales']->getOneI18n((int) $locUrl['id_locale']);
                    $site = $displayer->collector['sites']->getOneI18n((int) $locUrl['id_site']);
                    $urlField = ($_SERVER['env'] == 'prod') ? 'prod_url' : 'internal_url';
                    $urlByLocale[$locUrl['id_locale']] = $site[$urlField] . '/' . $loc['language'] . $locUrl['url'];
                }
            }
        }
    }
    // Try to get CMS pages equivalents
    if(count($urlByLocale) == 0 && count($availableLocales) > 0) {
        $page = $displayer->collector['pages']->getOneI18n(array('url' => URL_LOC . $uri, 'is_published' => 1));
        if(isset($page['id_treeview'])) {
            $treeview = $displayer->collector['treeview']->getOneI18n((int) $page['id_treeview']);
            foreach($availableLocales as $loc) {
                $locTree = $displayer->collector['treeview']->getOneI18n((int) $page['id_treeview'], '', '', (int) $loc);
                if(is_array($locTree)) {
                    $urlByLocale[$loc] = !empty($locTree['href']) ? $locTree['href'] : $locTree['fullurl'];
                }
            }
        }
    }
    return $urlByLocale;
}

function getBlockName($file) {
    $blockDir = str_replace([VIEWS_DIR . 'blocks/', CONTROLLER_DIR . 'blocks/'], '', pathinfo($file)['dirname'] . '/');
    return $blockDir . pathinfo($file)['filename'];
}
function json_to_kv($json){
    $temp = json_decode($json,true);
    return array_column($temp,'lib','key');
}

function array_values_recursive($arr){
    $arr = array_values($arr);
    foreach($arr as $key => $val)
        if(array_values($val) === $val)
            $arr[$key] = array_values_recursive($val);

    return $arr;
}

/**
 * br2nl : br2nl opposite
 *
 * @param  mixed $string
 * @return string
 */
function br2nl($str)
{
    return preg_replace('#<br\s*/?>#i', "\n", $str);
}
