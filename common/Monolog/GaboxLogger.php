<?php

namespace Gabox\Monolog;

use Monolog\DateTimeImmutable;
use Monolog\Logger;

class GaboxLogger extends Logger
{

    /* Pour créer un logger spécialisé, il suffit de définir une classe enfant, puis
       surcharger les propriétés statiques channel, table et columns. */
    protected static string $channel = 'Gabox';
    protected static string $table = 'gen_log';
    protected static array $columns = ['id_admins', 'action', 'id_gabox'];
    protected static GaboxLogger $_instance;
    protected static array $_clones;

    public static function getInstance()
    {
        if (!isset(static::$_instance)) {
            static::$_instance = new static();
        }
        return static::$_instance;
    }

    public function withName(string $name): Logger
    {
        if (isset(static::$_clones) && isset(static::$_clones[$name])) {
            return static::$_clones[$name];
        } else {
            return static::$_clones[$name] = parent::withName($name);
        }
    }

    private function __construct() {
        global $gen;
        $mySQLHandler = new MySQLHandler($gen->getDb(), static::$table, static::$columns);
        return parent::__construct(static::$channel, [$mySQLHandler]);
    }

    public function addRecord(int $level, string $message, array $context = [], DateTimeImmutable $datetime = null): bool {
        $defaultContext = [
            'id_admins' => isset($_SESSION['admins']) ? $_SESSION['admins']['id_admins'] ?? NULL : NULL
        ];
        return parent::addRecord($level, $message, array_merge($defaultContext, $context), $datetime);
    }
}