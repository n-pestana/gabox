<?php 
require_once('urls.trait.php');

class Pages extends  gen_pages{

    use urls;

	public function set($primary_key_value = '' , $array_fields = '', $criteres = array()) 
    {
        if(empty($primary_key_value) && is_array($array_fields) &&  array_key_exists('id_admins',$this->array_fields)){
            $array_fields['id_admins']=$_SESSION['admins']['id_admins'];
        }

        if(is_array($array_fields) && array_key_exists('url',$array_fields) && $array_fields['url']===null ){
            $array_fields['url'] = $this->makeUrl($array_fields,false);
        }

        if(is_array($array_fields) && array_key_exists('id_admins',$array_fields) && $array_fields['id_admins']===null ){
            $array_fields['id_admins'] = $_SESSION['admins']['id_admins']; 
        }

		$id = parent::set($primary_key_value, $array_fields, $criteres);

        $this->BuildUrl($id);

		return $id;
    }


        function getPagesElements($id_pages, $id_locales,$cache = true){
        $sql = "
        SELECT 
            CASE WHEN i18n_id_pages.lib != '' THEN i18n_id_pages.lib ELSE gen_pages.id_pages END AS id_pages,
            CASE WHEN i18n_lib.lib != '' THEN i18n_lib.lib ELSE gen_pages.lib END AS lib,
            CASE WHEN i18n_created_at.lib != '' THEN i18n_created_at.lib ELSE gen_pages.created_at END AS created_at,
            CASE WHEN i18n_updated_at.lib != '' THEN i18n_updated_at.lib ELSE gen_pages.updated_at END AS updated_at,
            CASE WHEN i18n_url.lib != '' THEN i18n_url.lib ELSE gen_pages.url END AS url,
            CASE WHEN i18n_meta_desc.lib != '' THEN i18n_meta_desc.lib ELSE gen_pages.meta_desc END AS meta_desc,
            CASE WHEN i18n_meta_title.lib != '' THEN i18n_meta_title.lib ELSE gen_pages.meta_title END AS meta_title,
            CASE WHEN i18n_id_templates.lib != '' THEN i18n_id_templates.lib ELSE gen_pages.id_templates END AS id_templates,
            CASE WHEN i18n_is_published.lib != '' THEN i18n_is_published.lib ELSE gen_pages.is_published END AS is_published 
            , gen_pages_data.* 

            FROM gen_pages 
            LEFT JOIN gen_i18n i18n_id_pages ON ( i18n_id_pages.id_element = gen_pages.id_pages AND i18n_id_pages.id_locales =".$id_locales." AND i18n_id_pages.module = 'pages' AND i18n_id_pages.field_name = 'id_pages' ) 
            LEFT JOIN gen_i18n i18n_lib ON ( i18n_lib.id_element = gen_pages.id_pages AND i18n_lib.id_locales =".$id_locales." AND i18n_lib.module = 'pages' AND i18n_lib.field_name = 'lib' ) 
            LEFT JOIN gen_i18n i18n_created_at ON ( i18n_created_at.id_element = gen_pages.id_pages AND i18n_created_at.id_locales =".$id_locales." AND i18n_created_at.module = 'pages' AND i18n_created_at.field_name = 'created_at' ) 
            LEFT JOIN gen_i18n i18n_updated_at ON ( i18n_updated_at.id_element = gen_pages.id_pages AND i18n_updated_at.id_locales =".$id_locales." AND i18n_updated_at.module = 'pages' AND i18n_updated_at.field_name = 'updated_at' ) 
            LEFT JOIN gen_i18n i18n_url ON ( i18n_url.id_element = gen_pages.id_pages AND i18n_url.id_locales =".$id_locales." AND i18n_url.module = 'pages' AND i18n_url.field_name = 'url' ) 
            LEFT JOIN gen_i18n i18n_meta_desc ON ( i18n_meta_desc.id_element = gen_pages.id_pages AND i18n_meta_desc.id_locales =".$id_locales." AND i18n_meta_desc.module = 'pages' AND i18n_meta_desc.field_name = 'meta_desc' ) 
            LEFT JOIN gen_i18n i18n_meta_title ON ( i18n_meta_title.id_element = gen_pages.id_pages AND i18n_meta_title.id_locales =".$id_locales." AND i18n_meta_title.module = 'pages' AND i18n_meta_title.field_name = 'meta_title' ) 
            LEFT JOIN gen_i18n i18n_id_templates ON ( i18n_id_templates.id_element = gen_pages.id_pages AND i18n_id_templates.id_locales =".$id_locales." AND i18n_id_templates.module = 'pages' AND i18n_id_templates.field_name = 'id_templates' ) 
            LEFT JOIN gen_i18n i18n_is_published ON ( i18n_is_published.id_element = gen_pages.id_pages AND i18n_is_published.id_locales =".$id_locales." AND i18n_is_published.module = 'pages' AND i18n_is_published.field_name = 'is_published' )
            LEFT JOIN gen_pages_data on (gen_pages_data.id_pages = gen_pages.id_pages and  gen_pages_data.id_locales=".$id_locales.")
            WHERE  gen_pages.id_pages =".$id_pages."  ";

            $rs = $this->Query($sql);
	    return  $this->getRows($rs);
    }

    function Compil($page,$template,$is_front=false){
      if($is_front==true) $template['content'] = str_replace('editable','',$template['content']);
      $doc= new DOMDocument();
      $doc->loadHTML('<?xml encoding="UTF-8">'.$template['content']);

      $xpath = new DOMXPath($doc);
      foreach($page as $element){
        if(isset($element['element_name'])) {
            //$doc->getElementById($element['element_name'])->nodeValue = $element['element_value'];
            $node = $xpath->query("//*[@id='" . $element['element_name'] . "']")->item(0);
            if($node) $node->nodeValue = $element['element_value'];
        }
      }
      $xpath = new DOMXPath($doc);
      //$query = $xpath->query("//div")->item(0);
      return html_entity_decode($doc->saveHTML($query));
    }

    # aurel powaaaaaa
    function CallBackModule($content) {
        preg_match_all('/\{(\w+)\.(\w+)\(([^)]*)\)\}/i', $content, $callbacks, PREG_SET_ORDER);
        $modules = array_keys($this->collector);
        foreach ($callbacks as $callback) {
            if (in_array($callback[1], $modules)) {
                $method = 'return $this->collector["' . $callback[1] . '"]->' . $callback[2] . '(' . $callback[3] . ')';
                $result = eval($method . ';');
                $content = str_replace($callback[0], $result, $content);
            }
        }
        return $content;
    }
}
