<?php

namespace Gabox\Interfaces;

interface URLToAction
{
    public function launchCallableByURL(string $url): void;
}