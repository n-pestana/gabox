<?php 
class Sites extends gen_sites 
{

/*  public $KVS_Properties = array ( 
    'primary_key'          => 'id_sites' 
   ,'parent_tablename'     => 'gen_sites' 
   ,'child_tablename'      => 'gen_sites_kv' 
   ,'child_keyfield'       => 'title' 
   ,'child_valuefield'     => 'value' 
   ,'keys'                 => array( 
        'departure' 
        ,'cabinet' 
        ,'power' 
        ,'sati' 
     )
   );

*/

    protected $_shopIsAvailable = null;

	function getAvailabledLanguageForCountry ($id_site='', $enable_only=true)
    {
          $sql = "SELECT sites.id_sites, locales.id_locales,locales.locale,locales.id_country, locales.lib_language, locales.country, locales.language
                  FROM gen_sites sites
                  LEFT JOIN gen_sites_locales sites_locales ON sites.id_sites = sites_locales.id_sites
                  LEFT JOIN gen_locales locales ON sites_locales.id_locales = locales.id_locales
                  LEFT JOIN gen_country country on country.id_country = sites.id_country
                ";
          if(!empty($id_site)) 
                  $sql .= "WHERE sites.id_sites  = '".(string)$id_site."'";

          if($enable_only) $sql .= 'AND sites_locales.is_enabled =1 AND sites.is_published=1 order by locales.id_locales desc';
      	  $rs = $this->CachedQuery($sql);
          return  $this->getRows($rs);
	}
    function getAvailabledLocalesBySiteAndLang($id_site,$lang,$enable_only=true)
    {
        $sql = "SELECT sites_locales.id_locales
                FROM gen_sites sites, gen_sites_locales sites_locales,gen_locales locales
                WHERE sites.id_sites =".$id_site."
                AND sites.id_sites = sites_locales.id_sites
                AND sites_locales.id_locales = locales.id_locales
                AND locales.language='".$lang."'";

        if($enable_only) $sql .= ' AND sites_locales.is_enabled =1 AND is_published =1 ';
	$rs = $this->CachedQuery($sql);
		$temp = $this->getRows($rs);
        if(isset($temp[0])) return $temp[0];
    }
	function getAvailabledLocalesForSite($id_site, $enable_only=true)
    {
          $sql = "SELECT sites.id_sites, locales.is_master, locales.id_locales,locales.locale,locales.id_country, locales.lib_language, locales.lib_language as lib,  locales.country
                  FROM gen_sites sites
                  LEFT JOIN gen_sites_locales sites_locales ON sites.id_sites = sites_locales.id_sites
                  LEFT JOIN gen_locales locales ON sites_locales.id_locales = locales.id_locales
                  LEFT JOIN gen_country country on country.id_country = sites.id_country
                  WHERE sites.id_sites  = '".(string)$id_site."'";
          if($enable_only) $sql .= 'AND sites_locales.is_enabled =1 AND sites.is_published=1';
	  $rs = $this->CachedQuery($sql);
          return  $this->getRows($rs);
	}
	function getAvailabledIdLocalesForSite($id_site, $enable_only=true)
    {
        $temp  = $this->getAvailabledLocalesForSite($id_site, $enable_only);
        $return = array();
        foreach ($temp as $k=>$v){
            $return[]  = $v['id_locales'];
        }
        return $return;
    }
    function getZoneCountryLang($id_zone,$enable_only=true)
    {
        $sql  = "SELECT l.lib_language,c.id, c.lib ,c.id_country,s.lib as site,l.language
            FROM gen_country c, gen_zones_country zc, gen_sites s, gen_sites_locales sl, gen_locales l
            WHERE zc.id_zones =".$id_zone."
            AND zc.id_country = c.id_country
            AND s.id_country = c.id_country
            AND sl.id_sites = s.id_sites
            AND sl.id_locales = l.id_locales ";

        if($enable_only) $sql .= ' AND s.is_published =1 AND sl.is_enabled =1 ';
        $sql .=" ORDER BY c.id_country";
	$rs = $this->CachedQuery($sql);
//        $rs = $this->Query($sql);
        return  $this->getRows($rs);
    }
	public function shopIsAvailable($options = array()) 
    {
        if(null === $this->_shopIsAvailable) {
            if(!isset($options['id_site']) && $options['shop_mod']==1){
                $this->_shopIsAvailable = true;
                return true;
            }
            $idSite = $options['id_site'];
            $shopMod = $options['shop_mod'];
            if((bool) $shopMod === false) {
                $this->_shopIsAvailable = false;
                return false;
            }
            $deliveryModes = $this->collector['sites_delivrymode']->get(array('id_sites' => $idSite));
            if(is_array($deliveryModes) && count($deliveryModes) > 0) {
                $this->_shopIsAvailable = true;
            } else {
                $this->_shopIsAvailable = false;
            }
        }
        return $this->_shopIsAvailable;
    }

    public function getOption($key,$default=''){
        return (isset($this->conf['sites_options'][$key]) && !empty($this->conf['sites_options'][$key])) ? $this->conf['sites_options'][$key] : $default;
    }

    # @return array list of controllers php files
    public function getControllers($id_site){
        #$site = $this->getOne($id_site);
        #if(empty($site['www_themes']))return array();
        $array = array();
        $list =  glob(GABOX_USR_DIR.'/www/controller/*.php');
        foreach($list as $k=>$v){
            $array[basename($v)] = Ucfirst(basename(str_replace('.php','',$v)));
        }
        return $array;
    }

}
