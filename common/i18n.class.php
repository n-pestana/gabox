<?php class I18n extends  gen_i18n{

	var $id_locales= '';

	public function formatPrice($price,$conf=array()){
		if(isset($conf['no_cents']) && ($conf['no_cents']==true)){
			$price = number_format ($price, 0, CURRENCY_DEC_SEP ,CURRENCY_THOUSANDS_SEP );
		}
		else{
			$price = number_format ($price,CURRENCY_DECIMALS_NB,CURRENCY_DEC_SEP,CURRENCY_THOUSANDS_SEP );
		}
        	return (CURRENCY_POSITION=='BEFORE') ? CURRENCY.$price : $price.' '.CURRENCY;
	}
	
	public function formatDate($date, $locale='fr'){
        if(defined('LOC')) $locale = LOC;
		return date("d/m/Y",strtotime($date));
	}	

	function translate($string){
		if(isset($_GET['auto-add-i18n'])){

			$exist = $this->getCount(array(
				'module'=>'commun'
				,'field_name'=>'lib'
				,'source'=>$string
				,'id_element'=>0
				,'id_locales'=>ID_LOC

	                ));
			if(!$exist){
				$this->set(array(
					'module'=>'commun'
					,'field_name'=>'lib'
					,'source'=>$string
					,'id_element'=>0
					,'id_locales'=>ID_LOC
	                        ));
			}
		}
		$trad = $this->getOne(array(
			'module'=>'commun'
			,'field_name'=>'lib'
			,'id_locales'=>$this->id_locales
			,'is_published'=>1
			,'source'=>$string
			,'id_element'=>0

		));
		if(isset($trad['lib'])) return $trad['lib'];
		return $string;

	}
	
    function getPurchasableByID_SITE($id_site,$id_produit,$mod='produits'){
          global $gen;
          $locales = $gen->collector['sites']->getAvailabledIdLocalesForSite($id_site,false);
              $sql = "select lib as is_purchasable from gen_i18n where  module = '".$mod."' and id_element =".(int)$id_produit." AND field_name = 'is_purchasable' ";
              $sql .= "and id_locales in (".implode(",",$locales).")";
		$rs = $this->CachedQuery($sql);
          //$rs = $this->Query($sql);
          $result = array();
          foreach( $this->GetRows($rs) as $is_purchasable){
            $result []= (int)$is_purchasable['is_purchasable'];
          }
          return $result;
        }

     function getStatusByID_SITE($id_site,$id_produit,$mod='produits'){
      global $gen;
      $sql = "select is_published from gen_i18n where  module = '".$mod."' and id_element =".(int)$id_produit." AND field_name = 'lib' ";
        $locales = $gen->collector['sites']->getAvailabledIdLocalesForSite($id_site,false);
        $sql .= "and id_locales in (".implode(",",$locales).")";
        $rs = $this->CachedQuery($sql);

      $result = array();
      foreach( $this->GetRows($rs) as $is_published){
        $result []= $is_published['is_published'];
      }
      return $result;
    }

    function SetPurchaseForElementByIdSite($id,$id_site,$newvalue,$mod='produits'){
      global $gen;
      $locales=$gen->collector['sites']->getAvailabledIdLocalesForSite($id_site, false);
      foreach($locales as $id_loc){
          $cond = array('module'=>$mod,'field_name'=>'is_purchasable','id_element'=>$id);
          $cond['id_locales']=$id_loc;
          $temp = $gen->collector['i18n']->get($cond);
          if(isset($temp[0]) && isset($temp[0]['id_i18n'])) $current_id = $temp[0]['id_i18n'] ;
          elseif(empty($temp)) $current_id='';
          else  die('error #434'); # 434 ?? WTF ? 
          $cond['lib']=$newvalue;
          $gen->collector['i18n']->set($current_id,$cond);
      }
      return true;
    }


    function SetStatusForElementByIdSite($id,$id_site,$newvalue,$mod='produits'){
      global $gen;
      $locales=$gen->collector['sites']->getAvailabledIdLocalesForSite($id_site, false);
      foreach($locales as $id_loc){
          $cond = array('module'=>$mod,'field_name'=>'lib','id_element'=>$id);
          $cond['id_locales']=$id_loc;
          $temp = $gen->collector['i18n']->get($cond);
          if(isset($temp[0]) && isset($temp[0]['id_i18n'])) $current_id = $temp[0]['id_i18n'] ;
          elseif(empty($temp)) $current_id='';
          else  die('error #434');
          $cond['is_published']=$newvalue;
          $gen->collector['i18n']->set($current_id,$cond);
      }
      return true;
    }


    function SetMassStatus($mod,$id_element,$is_published){
        foreach($this->collector['locales']->get() as $k=>$v){
            $t= $this->collector['i18n']->getOne(array(
                'id_locales'=>$v['id_locales']
                ,'id_element'=>$id_element
                ,'module'=>$mod
                ,'field_name'=>'lib'


            ));
            if(empty($t))$t=array(
               'id_locales'=>$v['id_locales']
                ,'id_element'=>$id_element
                ,'module'=>$mod
                ,'field_name'=>'lib'
                ,'level'=>'1'
            );

            $t['is_published']=$is_published;
            $this->collector['i18n']->set( (!empty($t['id_i18n'])) ? $t['id_i18n'] : '', $t );
        }
    }
}
