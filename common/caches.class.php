<?php 
class Caches extends gen_caches
{
	
	function getVar($var){
     if(!defined('ID_SITE'))return false; 
	   $temp = $this->getOne(array('id_sites'=>ID_SITE,'name'=>$var));
	   if(!$temp)return false;
	   return json_decode($temp['lib'],true);
	}

	function setVar($var,$data,$info=''){
    if (!defined('ID_SITE')) {
      return false;
    }
    $this->purge($var);
		return $this->set('',array('lib'=>json_encode($data),'id_sites'=>ID_SITE,'name'=>$var));
	}

	function purge($var){
    $sql = "delete from ".$this->table_name;
		if(defined('ID_SITE')) $sql .= ' where id_sites='.ID_SITE;
    $sql .= " and name='$var'";
		$this->query($sql);
	}

}

