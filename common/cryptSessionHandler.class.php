<?php
require_once(dirname(__FILE__) . '/cryptData.trait.php');
/**
 * Makes session's files contents encrypted on server
 * Usage of cryptData's trait methods
 */
class cryptSessionHandler extends SessionHandler
{
    use cryptData;

    /**
     * Encryption and authentication key
     * @var string
     */
    protected $_key;

    /**
     * Set AES key for further use
     */
    public function __construct() 
    {
        $this->_key = $this->getCryptKey(CRYPT_KEY_PWD, hex2bin(CRYPT_KEY_SALT));
    }
    
    /**
     * Read from session and decrypt
     *
     * @param string $id
     */
    public function read($id)
    {
        $data = parent::read($id);
        return empty($data) ? '' : ($this->decryptWithAes(base64_decode($data), $this->_key));
    }

    /**
     * Encrypt the data and write into the session
     *
     * @param string $id
     * @param string $data
     */
    public function write($id, $data)
    {
        return parent::write($id, base64_encode($this->encryptWithAes($data, $this->_key)));
    }
}
