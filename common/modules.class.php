<?php class Modules  extends  gen_modules{
    public function isEnabled($module_name){
       $mod = $this->getOne(array('lib'=>$module_name));
       if(isset($mod['is_enabled']) && $mod['is_enabled'] == 1) return true;
       return false;
    }	
}
