<?php
class Gender extends gen_gender 
{
    public function getLocaleGender($obj, $loc = null) 
    {
        $loc = null === $loc ? ID_LOC : $loc;
        $rows = $obj->collector['locales_gender']->get(array('id_locales' => $loc));
        if(!is_array($rows) || is_array($rows) && count($rows) === 0) {
            return $this->getI18n();
        } else {
            $genders = array();
            foreach($rows as $row) {
                $genders[] = $row['id_gender'];
            }
            return $this->getI18n(array('id_gender' => $genders));
        }
    }
}
