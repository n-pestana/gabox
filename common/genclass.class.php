<?php
use Gabox\Event\CollectorItemEvent;
use Gabox\Event\EventDispatcher;
use Gabox\Event\EventRegistererTrait;
use Gabox\Event\GenclassEvents;
use Gabox\Monolog\GaboxLogger;
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Settings;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

Class GenClass {

    use EventRegistererTrait;

    /**
     * Dispatcher d'événements
     * Ce dernier est partagé par tous les collectors (static)
     */
    public static EventDispatcher $eventDispatcher;

    /**
     * Tableau des événements à écouter par le collector
     * Ce tableau contient en clé le nom de l'événement et en valeur la méthode à appeler
     * Le nom de la méthode peut être :
     *   - soit le nom d'une méthode de l'objet en cours
     *   - soit le nom d'une méthode de Listener devant implémenter l'interface 'Gabox\Event\ListenerInterface'
     * Attention dans le cas d'un listener autonome, il faut préciser son namespace complet
     */
    public array $listenedEvents;

    protected ?array $automaticFilteringMethods = null;
    protected bool $automaticFilteringEnabled = true;

    /**
     * nom de la table courante
     */
    var $table_name ;
    var $obj;
    var $versionning = false; # set versionning off, you can active with $gen->enableVersioningByModule(array('myMod'))
    var $versionning_table = 'gen_versions';
    var $id_locales ;
    var $id_locales_master ;
    var $origin_array_fields = '';
    /**
     * nom du champ primary key
     */
    var $primary_key;

    /**
     * tableau associatif comportant le nom des champs utilisé et eventuellement
     * leurs valeurs
     */
    var $array_fields ;

    /**
     * Object de connexion à la base de données
     */
    var $db ;

    var $collector;

    /**
     * Valeur de la clé primaire
     */
    var $primary_key_value;

    /**
     * si true alors on utilise = pour les criteres sinon like %
     */
    var $usetransaction = false; 
    var $simulation = false;
    var $cache_time = 36000;
    var $debug=false;
    //var $log_events = true;  > DEPRECATED
    /**
     * Champ utilisé dans la méthode getKV pour construire les valeurs des selects automatisées
     *
     * @access protected
     * @var string
     */
    protected $_libField = 'lib';
    /**
     * Champs définis pour cibler les champs utilisés pour la recherche
     * Nécessaire pour éviter les soucis de collation (recherche LIKE UTF-8 sur un champ entier par exemple)
     *
     * @access protected
     * @var array
     */
    protected $_search_fields = array();
    /**
     * Erreur du formulaire en cours de validation
     *
     * @access protected
     * @var array
     */
    protected $_formErrors = array();
    /**
     * Définit si on recherche automatiquement avec un % ou non
     *
     * @access protected
     * @var boolean
     */
    protected $_searchLike = false;
    /**
     * Définit les champs utilisés dans la recherche autocomplete
     *
     * @access protected
     * @var array
     */
    protected $_autocompleteSearchFields = array();
    /**
     * Séparateur utilisé entre les différentes colonnes pour l'autocomplete
     *
     * @access protected
     * @var string
     */
    protected $_autocompleteResultSeparator = ' / ';
    /**
     * Champ utilisé comme référence pour les stats sur les dates (utilisé par la méthode getNbToday par exemple)
     *
     * @access protected
     * @var string
     */
    protected $_dateReferenceField = 'created_at';

    protected $_libSeparator = ' ';

    protected $_cacheEnabled = true;
    /**
     * Default validation field
     *
     * @access protected
     * @var string
     */
    protected $_validationField = 'is_validated';

    protected $cache_name = '';
    /**
     * Generated password
     *
     * @access protected
     * @var string
     */
    protected $_generatedPassword;
    /**
     * Pagination - Number by page
     *
     * @access protected
     * @var integer
     */
    public $_numberByPage = 20;
    /**
     * Pagination - Current page
     *
     * @access protected
     * @var integer
     */
    protected $_currentPage = 1;
    /**
     * Pagination - Number of visible bullets
     *
     * @access protected
     * @var integer
     */
    protected $_nbBulletsVisible = 2;
    /**
     * Pagination - Total number of results
     *
     * @access protected
     * @var integer
     */
    protected $_nbResults;
    /**
     * Pagination - Tous les résultats
     *
     * @access protected
     * @var array
     */
    protected $_paginatedResults;
    /**
     * Clean edition template (for dynamic blocks in pages)
     *
     * @access protected
     * @var boolean
     */
    protected $_cleanEditTpl = true;
    /**
     * Traits used by class
     *
     * @access protected
     * @var array
     */
    protected $_usedTraits = array();
    /**
     * Classes name organized by trait
     *
     * @access protected
     * @var array
     */
    protected $_classesByTrait = array();
    /**
     * DB path used for auto classes storage
     *
     * @access protected
     * @var string
     */
    protected $_dbClassAutoPath = null;
    /**
     * Cache set by getSelectList function (used to retrieve data lists)
     *
     * @access protected
     * @var array
     */
    protected $_selectListCache = null;

    /*
    * Configuration pour KeyValueStore
    */
    var $KVS_Properties = array();
    /**
     * Flag to set if the get/getCached method returns all results even if only one row is found
     *
     * @access protected
     * @var bool
     */
    protected $_returnRawResults = false;
    /**
     * Max items allowed to be stored in DB cache
     *
     * @access protected
     * @var integer
     */
    protected $_maxDBCacheItems = 1500;

    public function getDb() 
    {
        return $this->db;
    }
    /*
     * @param table_name   : nom de la table
     * @param primary_key  : nom du champ primary key
     * @param array_fields : tableau associatif comportant le nom des champs 
     */

    protected $_formatDateExport = array();

    protected $_extractCharsList = array(
        'in' => array('<li>','é',' ','’','î','É','à','ê','œ','”','“','‘','’','–'),
        'out' => array('- ','é',' ',"'",'î','É','à','ê','oe','"','"','"','"','-')
    );
    /**
     * Does the collector log events ?
     *
     * @access protected
     * @var boolean
     */
    protected $_logEvents = false;
    /**
     * Log origin : can be OPERATOR / SYSTEM
     *
     * @access protected
     * @var string
     */
    protected $_logOrigin = 'OPERATOR';
    /**
     * Values before save for diff purposes
     *
     * @access protected
     * @var array
     */
    protected $_diffSavedValues = array();
    /**
     * Admin ID - for page editing
     *
     * @access protected
     * @var integer
     */
    protected $_adminId;
    /**
     * getSearch crypted columns
     *
     * @access protected
     * @var array
     */
    protected $_searchCryptedColumns = array();

    public function getClassAutoPath(){
        return $this->_dbClassAutoPath;
    }

    public function add($class)
    {
        if(trim($class)=='') return false;
        if(defined('DEBUG') && DEBUG==true)error_log("Auto Loading class:".strtolower($class));
        $class_name=strtolower($class).'.class.php' ;
        $class_folder = null;
        if (file_exists(GABOX_USR_DIR.'/classes/auto/' . $this->_dbClassAutoPath . 'Gen_'.$class_name)) {
            require_once(GABOX_USR_DIR . '/classes/auto/' . $this->_dbClassAutoPath . 'Gen_' . $class_name);
        }
        if (file_exists(INC.'common/'.$class_name) && !file_exists(GABOX_USR_DIR.'/classes/'.$class_name)) {
            $class_folder = INC . 'common/';
        } elseif (file_exists(GABOX_USR_DIR.'/classes/'.$class_name)) {
            $class_folder = GABOX_USR_DIR.'/classes/';
        } elseif (file_exists(USR_MODS . $class . '/classes/' . $class_name)) {
            $class_folder = USR_MODS . $class . '/classes/';
        } elseif (file_exists(MODS . $class . '/classes/' . $class_name)) {
            $class_folder = MODS . $class . '/classes/';
        } else {
            // Si on n'a pas trouvé la classe, on regarde tout de même dans tous les dossiers classes des modules
            // Utile en cas d'objets liés dans un seul et même module
            $mods = array_unique(glob('{' . MODS . ',' . USR_MODS . '}/*', GLOB_ONLYDIR | GLOB_BRACE));
            foreach ($mods as $path) {
                if (file_exists($path . '/classes/' . $class_name)) {
                    $class_folder = $path . '/classes/';
                }
            }
        }

        if (!isset($class_folder)) {
            $gen = 'gen_' . $class;
            #XXX il faudrait remplacer cet eval
            if (!class_exists($class)) eval("class " . $class . " extends gen_" . $class . "{};");
        } else {
            require_once($class_folder . $class_name);
            // Si on a une surcharge du collector, on regarde si le dossier de gestion des événements existe
            // et on le load. Ce dossier contient les listeners et custom events
            if (is_dir($class_folder . 'Event')) {
                foreach (scandir($class_folder . 'Event/') as $file) {
                    if (is_file($class_folder . 'Event/' . $file)) {
                        require_once $class_folder . 'Event/' . $file;
                    }
                }
            }
        }

        if(defined('DEBUG') && DEBUG==true)error_log("Class:".strtolower($class)." loaded");
        //XXX On ne peut plus utiliser Object en tant que nom de classe depuis PHP 7.2
        if ($class === 'object') {
            $n = new Objet();
        } else {
            $n = new $class();
        }
        $n->table_name  = 'gen_'.strtolower($class);
        $n->primary_key = $n->array_fields[0];
        $n->db=&$this->db;
        if (!is_array($n->array_fields)) {
            GaboxLogger::getInstance()->error("Error array_fields must be an array", [
                'action' => 'GenClass::add',
            ]);
        }
        foreach($n->array_fields as $key=>$val){
            unset($n->array_fields[$key]);
            $n->array_fields[$val] = '';
        }
        $n->setUsedTraits($this->_classUsesDeep($n));
        return $n;
    }
  
  function __construct($db='')
  {
      if (!empty($db)) {
          $this->db = $db;
      }
      if (defined("DEBUG")) {
          $this->debug = DEBUG;
      }
      if (empty($this->_autocompleteSearchFields)) {
          $this->_autocompleteSearchFields = [$this->getLibField()];
      }
      if (isset($_SERVER['DB_NAME'])) {
          $this->_dbClassAutoPath = trim($_SERVER['DB_NAME']) . DIRECTORY_SEPARATOR;
      } elseif (isset($_SERVER['id_site'])) {
          $this->_dbClassAutoPath = $_SERVER['id_site'] . DIRECTORY_SEPARATOR;
      } elseif (defined('ID_SITE')) {
          $this->_dbClassAutoPath = ID_SITE . DIRECTORY_SEPARATOR;
      } else {
          $this->_dbClassAutoPath ='';
      }
      // Si le dispatcher n'est pas initialisé, on le fait une seule fois
      if (!isset(self::$eventDispatcher)) {
          self::$eventDispatcher = new EventDispatcher();
      }
      // On écoute les événements définis par la propriété $listenedEvents
      if (method_exists($this, 'initListenedEvents')) {
          $this->initListenedEvents();
      }
  }

  function getCached($primary_key_value = '', $order = '', $limit = '',$offset = '', $tables = '', $join = ''){
    $temp = $this->simulation ;
    $this->simulation = true;
    $sql = $this->get($primary_key_value, $order, $limit,$offset, $tables, $join);
    $this->simulation = $temp;

    if($this->_cacheEnabled && defined('USE_CACHE') && USE_CACHE) {
        $rs = $this->db->CacheExecute($this->cache_time, $sql);
    }
    else $rs = $this->db->Execute($sql);

    if ($rs === false) {
        GaboxLogger::getInstance()->error($this->db->errorMsg(), [
            'action' => 'GenClass::getCached',
            'id_gabox' => $primary_key_value,
        ]);
        return false;
    }
    # si pas de resultats
    $results_count = $this->RecordCount($rs);
    if($results_count == 0){
      return false;
    }
    # si resultat(s)
    elseif($this->_returnRawResults === false && $results_count == 1){
      if(!empty($primary_key_value)){
        # selection d'un element en base
        $rs = $this->GetRows($rs);

        if (!empty($this->automaticFilteringMethods) && $this->isAutomaticFilteringEnabled()) {
            foreach ($this->automaticFilteringMethods as $callable) {
                $rs = call_user_func($callable, $rs);
            }
        }
        if (empty($rs)) {
            return false;
        }

        return $rs[0];
      }
    }

    $rows = $this->GetRows($rs);
    if (!empty($this->automaticFilteringMethods) && $this->isAutomaticFilteringEnabled()) {
        foreach ($this->automaticFilteringMethods as $callable) {
            $rows = call_user_func($callable, $rows);
        }
    }
    if (empty($rows)) {
        return false;
    }

    return $rows;
  }

  /**
   * Gets the first database row that matches the parameters.
   *
   * @param $primary_key_value DB primary key or condition array
   * @param $order SQL ORDER BY (sorting of the query response)
   * @param $limit SQL LIMIT (Amount of row returned (one in this case))
   * @param $offset SQL OFFSET
   * @param $tables
   * @param $join
   *
   * @return $rs query response from the database, or false if nothing matched description.
   */
  function getOne($primary_key_value='', $order = '', $limit = 1, $offset = '', $tables = '', $join = '')
  //TODO: is $limit parameter really usefull ? The first row of the recordset is always returned.
  //Check on all project if we can remove it or keep it for backward compatibility.
  {
    if (empty($primary_key_value)) {
        return false;
    }

	$rs = $this->get($primary_key_value, $order, $limit, $offset, $tables, $join);
	if(isset($rs[0])) return $rs[0];
	return $rs;
  }

   /**
   * Gets the cached first database row that matches the parameters.
   *
   * @param $primary_key_value DB primary key or condition array
   * @param $order SQL ORDER BY (sorting of the query response)
   * @param $limit SQL LIMIT (Amount of row returned (one in this case))
   * @param $offset SQL OFFSET
   * @param $tables
   * @param $join
   *
   * @return $rs query response from cache or database, or false if nothing matched description.
   */
  function getOneCached($primary_key_value='', $order = '', $limit = 1, $offset = '', $tables = '', $join = '')
  //TODO: is $limit parameter really usefull ? The first row of the recordset is always returned.
  //Check on all project if we can remove it or keep it for backward compatibility.
  {
	$rs = $this->getCached($primary_key_value, $order, $limit, $offset, $tables, $join);
	if(isset($rs[0])) return $rs[0];
	return $rs;
  }

  /**
   * Get the first row from a dabatabe table.
   *
   * @return $rs query response rows from the database or false if nothing was found.
   */
  function getFirst()
  {
    $rs = $this->get('', '', 1);
	if(isset($rs[0])) return $rs[0];
	return $rs;
  }
  /**
   * Get the first row from a dabatabe table.
   *
   * @return $rs query response rows from the database or false if nothing was found.
   */
  function getFirstCached()
  {
    $rs = $this->getCached('', '', 1);
	if(isset($rs[0])) return $rs[0];
	return $rs;
  }

  function getLibCached($primary_key_value)
  {
	$rs = $this->getCached($primary_key_value);
	return isset($rs[$this->_libField]) ? $rs[$this->_libField] : '';
  }

  function getLib($primary_key_value)
  {
	$rs = $this->get($primary_key_value);
	return isset($rs[$this->_libField]) ? $rs[$this->_libField] : '';
  }

  function getFieldToString($primary_key_value, $field)
  {
	$rs = $this->get($primary_key_value);
	return isset($rs[$field]) ? $rs[$field] : '';
  }

  function getId($primary_key_value = '', $order = '', $limit = '', $offset = '', $tables = '', $join = '')
  {
        $temp = $this->getOne($primary_key_value, $order ,$limit, $offset, $tables, $join);
        if(isset($temp[$this->primary_key])) return $temp[$this->primary_key];
        return false;
  }

  public function get($primary_key_value = '', $order = '', $limit = '', $offset = '', $tables = '', $join = '', $version = null)
  {
    if($offset < 0) $offset = 0;
    $criteres = '';
    $filteredCryptedCols = array();
    if(is_array($primary_key_value)){
        $criteres = $primary_key_value;
        if(method_exists($this, '_getFilteredCryptedColumns')) {
            $filteredCryptedCols = $this->_getFilteredCryptedColumns($criteres);
        }
        $primary_key_value = '';
    }
    $this->OutputDebug('get',$primary_key_value);
    if(!empty($primary_key_value))
      $this->primary_key_value = $primary_key_value;
    else
      $this->primary_key_value = '';

    # construction de la requete de séléction
    $sql = 'SELECT ';
    if (!is_array($this->array_fields)) {
        GaboxLogger::getInstance()->error(
            "array_fields is not an array, cannot build the request for " . $this->table_name,
            [
                'action' => 'GenClass::get',
                'id_gabox' => $primary_key_value
            ]
        );
        return false;
    }

    $checkVersion = ($this->versionning === true && null !== $version);
    if($checkVersion) $version = (int) $version;

    foreach($this->array_fields as $field_name=>$k){
        $sql .= ($checkVersion ? 'COALESCE(version_' . $field_name . '_els.fieldvalue, ' . $this->table_name . '.' . $field_name . ') AS ' . $field_name : $field_name) . ', ';
    }
    $sql = substr($sql,0, -2);
    $sql .= ' FROM ';
# si requete sur plusieurs tables
    if(is_array($tables)){
        foreach($tables as $v){
            $sql .= $v.', ';
        }
        $sql = substr($sql,0, -2);
    } else {
        $sql .= $this->table_name;
        if($checkVersion) {
            foreach($this->array_fields as $field_name=>$k) {
                $sql .= " LEFT JOIN gen_versions AS version_" . $field_name . " ON " . $this->table_name . ".id_pages = version_" . $field_name . ".id_element and version_" . $field_name . ".module = 'gen_pages' AND version_" . $field_name . ".version_number = " . $version;
                $sql .= " LEFT JOIN gen_versions_elements AS version_" . $field_name . "_els ON version_" . $field_name . ".id_versions = version_" . $field_name . "_els.id_versions AND version_" . $field_name . "_els.fieldname = '" . $field_name . "'";
            }
        }
    }
    # si clé primaire, on prend pas les autres criteres, si on a pas de jointure. 
    if(!empty($primary_key_value)){
      $sql .= ' WHERE ( ('.$this->primary_key.' = '.$this->db->qstr($primary_key_value).'))';
    }
    elseif( ! empty($criteres)){
      $sql .= ' WHERE 1=1 ';
    }
    if(!empty($tables) && !empty($join) && ((empty($primary_key_value))&&(empty($criteres)) )){
      $sql .= ' WHERE '.$join;
    }
    if(!empty($tables) && !empty($join) && ((!empty($primary_key_value))||(! empty($criteres)) )){
      $sql .= ' AND '.$join;
    }
    if( ! empty($criteres)){
      $sql .= ' AND ' . $this->getSQLCriteres($criteres);
    }
    if(!empty($order) && is_string($order)) {
        $order = str_replace('order by', '', strtolower($order));
        if(strstr($order, 'n_order') && false === stristr($order, 'CAST')) {
            $order = 'CAST(' . $order . ' AS SIGNED)';
        }
        $sql .= ' order by '.$order;
    }
    if(!empty($limit) && is_int($limit))
      $sql .= ' limit '.$limit;
    if(!empty($offset) && is_int($offset))
      $sql .= ' offset '.$offset;

    $this->OutputDebug('query',$sql);
    if($this->simulation === true) return $sql;

    $rs = $this->Query($sql);
    if(!$rs){
       if($_SERVER['REMOTE_ADDR'] == IP_STAFF){
            echo $sql;
            die();            

        }
        GaboxLogger::getInstance()->error($this->db->errorMsg(), [
                'action' => 'GenClass::get',
                'id_gabox' => $primary_key_value
        ]);
        return false;
    }
    # si pas de resultats
    $results_count = $this->RecordCount($rs);
    $this->OutputDebug('results',$results_count.' result(s) found');
    if($results_count == 0){
      return false;
    }
    # si resultat(s)
    elseif($this->_returnRawResults === false && $results_count == 1) {
      if(!empty($primary_key_value)){
          # selection d'un element en base
          $this->OutputDebug('return','1 result(s) found');
          $rs = $this->GetRows($rs);
          if(method_exists($this, 'decryptData')) $this->decryptData($rs, $filteredCryptedCols);

          if (!empty($this->automaticFilteringMethods) && $this->isAutomaticFilteringEnabled()) {
              foreach ($this->automaticFilteringMethods as $callable) {
                  $rs = call_user_func($callable, $rs);
              }
          }
          if (empty($rs)) {
              return false;
          }

          return $rs[0];

      }
    }
    $this->OutputDebug('return',$results_count.' result(s) found');
    $rows = $this->GetRows($rs);
    if(method_exists($this, 'decryptData')) $this->decryptData($rows, $filteredCryptedCols);

    if (!empty($this->automaticFilteringMethods) && $this->isAutomaticFilteringEnabled()) {
        foreach ($this->automaticFilteringMethods as $callable) {
            $rows = call_user_func($callable, $rows);
        }
    }
    if (empty($rows)) {
        return false;
    }

    return $rows;
  }

  /**
   * Del
   * @param key_value : key_value de l'élément (ou tableau de key_value)
   * @return : false en cas de probleme, sinon true
   */
  public function del($primary_key_value = '')
  {
      $this->OutputDebug('delete',(is_countable($primary_key_value) ? count($primary_key_value) : 0 ) . ' element(s)');
      $i18nExists = array_key_exists('i18n', $this->collector);
      $i18nQueries = array();
      $sql = 'DELETE FROM '.$this->table_name.' WHERE ';
      $deletedItems = [];
      if(is_array($primary_key_value)) {
          if (empty($primary_key_value)) {
              GaboxLogger::getInstance()->warning(t("primary_key_value is an empty array, nothing will be deleted!"), [
                  'action' => 'GenClass::del'
              ]);
              return false;
          }
          foreach ($primary_key_value as $value) {
              $deletedItems[] = $this->get($value);
              $sql .= $this->primary_key.' = '.$this->db->qstr((int) $value) .' OR ' ;
              if($i18nExists) $i18nQueries[] = "DELETE FROM gen_i18n WHERE module = " . $this->db->qstr(str_replace('gen_', '', $this->table_name)) . " AND id_element = " . $this->db->qstr((int) $value);
          }
          $sql = substr($sql,0, -3);
      } else {
          if (empty($primary_key_value) && !empty($this->primary_key_value)) {
              $primary_key_value = $this->primary_key_value;
          }
          $deletedItems[] = $this->get($primary_key_value);
          $sql .= $this->primary_key.' = '.$this->db->qstr((int) $primary_key_value) .' ;' ;
          if ($i18nExists) {
              $i18nQueries[] = "DELETE FROM gen_i18n WHERE module = " . $this->db->qstr(str_replace('gen_', '', $this->table_name)) . " AND id_element = " . $this->db->qstr((int) $primary_key_value);
          }
      }

      $this->SaveVersion($primary_key_value,array(),true);
      $rs = $this->Query($sql);
      if (!$rs) {
          $this->log_events(array('type'=>'ERROR', 'lib' => 'DELETE ERROR : ' . $this->db->errorMsg(), 'id_element' => $primary_key_value));
          GaboxLogger::getInstance()->error($this->db->errorMsg(), [
              'action' => 'GenClass::del',
              'id_gabox' => $primary_key_value
          ]);
          return false;
      } else {
          // Delete i18n linked values
          $this->log_events(array('type'=>'DELETE', 'lib' => 'DELETE SUCCESS', 'id_element' => $primary_key_value));
          foreach ($deletedItems as $deletedItem) {
              if (is_array($deletedItem)) {
                  self::$eventDispatcher->dispatch(
                      new CollectorItemEvent($deletedItem, get_class($this)),
                      GenclassEvents::COLLECTOR_ITEM_DELETED
                  );
              }
          }
          if($i18nExists) {
              foreach($i18nQueries as $qid => $query) {
                  $rsI18n = $this->Query($query);
                  if(!$rs) {
                      $this->log_events(array('type'=>'ERROR', 'lib' => 'DELETE ERROR : ' . $this->db->errorMsg(), 'id_element' => $qid));
                      error_log($this->db->errorMsg());
                  } else {
                      $this->log_events(array('type'=>'DELETE', 'lib' => 'DELETE SUCCESS', 'id_element' => $qid));
                  }
              }
          }
          return true;
      }
  }

  /**
   * Set
   * Si key_value et libelle sont renseignés, on fait un update et on renvoi true si update = ok
   * Si un seul des deux est renseigné, on insert en autoincrement
   * @param key_value : key_value de l'élément
   * @param libelle : libelle de l'élement
   * @return :    
   */
  function set($primary_key_value = '' , $array_fields = '', $criteres = array()){
    if($this->usetransaction === true) $this->db->BeginTrans();

     # un seul parametre, et un tableau => insertion 
    if(empty($array_fields) && is_array($primary_key_value)){
      $array_fields       = $primary_key_value  ;
      $primary_key_value  = '';
    }
    if(empty($primary_key_value) && !empty($this->primary_key_value) && empty($array_fields)){
      $primary_key_value = $this->primary_key_value;
      $array_fields = $this->array_fields;
    }
    if(!empty($primary_key_value)){
        $sql = 'select count(*) as count from '.$this->table_name.'  WHERE '.$this->primary_key.' = '.$this->db->qstr($primary_key_value);
        if( ! empty($criteres) ) {
            $sql .= ' AND ';
            $sql .= $this->getSQLCriteres($criteres);
        }
        $sql .= ';';
        $rs = $this->Query($sql);
    }
    $this->_diffSavedValues = array();
    if((isset($rs)) && (($count = $this->fetchField($rs, 'count')) !== null) && ((int) $count > 0) && (!empty($primary_key_value))){
        if($this->_logEvents === true && $this->table_name != 'gen_backevents') {
            // Désactivation de l'encryption pour ne pas récupérer les données décryptées
            $canSetEncrypt = method_exists($this, 'setEncryptionEnabled');
            $cryptedColumns = array();
            if($canSetEncrypt) {
                $oldEncryptStatus = $this->_encryptionEnabled;
                $this->setEncryptionEnabled(true);
                $cryptedColumns = $this->getCryptedColumns();
                $cryptKey = $this->getCryptKey(CRYPT_KEY_PWD, hex2bin(CRYPT_KEY_SALT));
            }
            // Récupération des données avant sauvegarde pour enregistrer les données modifiées
            $oldVals = $this->getOne((int) $primary_key_value);
            if($canSetEncrypt) $this->setEncryptionEnabled($oldEncryptStatus);
            $newVals = $array_fields;
            foreach($newVals as $nk => $nv) {
                if(array_key_exists($nk, $oldVals) && $oldVals[$nk] != $nv) {
                    $fieldKey = ($this->table_name == 'gen_i18n') ? $nk . ' (' . $array_fields['field_name'] . ')' : $nk;
                    if(in_array($fieldKey, $cryptedColumns)) {
                        $this->setEncryptionEnabled(false);
                        $oldNonCryptedRow = $this->getOne((int) $primary_key_value);
                        $this->setEncryptionEnabled($oldEncryptStatus);
                        $oldVal = $oldNonCryptedRow[$nk];
                        $this->_diffSavedValues[$fieldKey] = array('old' => $oldVal, 'new' => $nv); // New value will be set afterwards with cryptData
                    } else {
                        $this->_diffSavedValues[$fieldKey] = array('old' => $oldVals[$nk], 'new' => $nv);
                    }
                }
            }
        }
        if(method_exists($this, 'cryptData')) $this->cryptData($array_fields);

        $this->OutputDebug('update','return '.$primary_key_value);    
        $sql = 'UPDATE '.$this->table_name.' SET ';
        if(isset($array_fields['url']) && empty($array_fields['url']) && isset($array_fields['lib'])){
            $array_fields['url'] = strtolower(urlize($array_fields['lib']));
        }
        foreach($array_fields as $field_name=>$field_value){
            if($field_name == $this->primary_key) continue;

            if($field_name == 'updated_at'){
                $sql .= 'updated_at = NOW(), ';
            }
            elseif(is_null($field_value)){
                $sql .= $field_name.' = NULL, ';
            } else {
                if (is_array($field_value)) {
                    $field_value = json_encode($field_value);
                }
                $sql .=$field_name.' = '.$this->db->qstr($field_value).', ';
            }
        }
        if(isset($this->array_fields['updated_at']) && !array_key_exists('updated_at', $array_fields)) {
            $sql .= 'updated_at = NOW(), ';
        }
        $sql = substr($sql,0, -2);
        $sql .= ' WHERE '.$this->primary_key.' = '.$this->db->qstr($primary_key_value) . ' ';
        if( ! empty($criteres) ) {
            $sql .= ' AND ';
            $sql .= $this->getSQLCriteres($criteres);
        }
        $sql .= ';';
        $this->OutputDebug('update SQL', $sql);
        $this->SaveVersion($primary_key_value,$array_fields);
        $rs = $this->Query($sql);

        if($this->usetransaction === true) $this->db->CommitTrans();

        if(!$rs) {
            $this->log_events(array('type'=>'ERROR', 'lib' => 'UPDATE ERROR : ' . $this->db->errorMsg(), 'id_element' => $primary_key_value));
            GaboxLogger::getInstance()->error($this->db->errorMsg() . '->' . $sql, [
                'action' => 'GenClass::set',
                'id_gabox' => $primary_key_value
            ]);
            return false;
        } else {
            $this->log_events(array('type'=>'UPDATE', 'lib' => 'UPDATE SUCCESS', 'id_element' => $primary_key_value));
            self::$eventDispatcher->dispatch(
                new CollectorItemEvent($this->get($primary_key_value), get_class($this)),
                GenclassEvents::COLLECTOR_ITEM_UPDATED
            );
            return $primary_key_value;
        }
    } else {
        if(method_exists($this, 'cryptData')) $this->cryptData($array_fields);
        $this->OutputDebug('insert','');
        if(empty($primary_key_value)){
            $primary_key_value = null; 
        }
        $sql = 'INSERT INTO '.$this->table_name.' (';
        if(!empty($primary_key_value)) $sql .= $this->primary_key.' ,';
        $sql_fields = '';
        $sql_values = '';
        if (!is_array($array_fields)) {
            GaboxLogger::getInstance()->error('Error you try to insert with no array datas', [
                'action' => 'GenClass::set',
                'id_gabox' => $primary_key_value
            ]);
        }
        if(isset($this->array_fields['created_at']) && !array_key_exists('created_at', $array_fields)) {
            $sql_fields .= 'created_at, ';
            $sql_values .= 'NOW(), ';
        }
        if(isset($this->array_fields['updated_at']) && !array_key_exists('updated_at', $array_fields)) {
            $sql_fields .= 'updated_at, ';
            $sql_values .= 'NOW(), ';
        }
        if(isset($array_fields['url']) && empty($array_fields['url']) && isset($array_fields['lib'])){
            $array_fields['url'] = strtolower(urlize($array_fields['lib']));
        }
        foreach($array_fields as $field_name=>$field_value) {
            if(($field_name == $this->primary_key)) continue;
            $sql_fields .= $field_name.', ';
            if(is_null($field_value)){
                $sql_values .= 'NULL, ';
            } else {
                if (is_array($field_value)) {
                    $field_value = json_encode($field_value);
                }
                $sql_values .= $this->db->qstr($field_value).', ';
            }
        }
        $sql .= $sql_fields ;
        $sql = substr($sql,0, -2);
        $sql .= ') VALUES ( ';
        if(!empty($primary_key_value)) $sql .= $this->db->qstr($primary_key_value).',';
        $sql .= $sql_values ;
        $sql = substr($sql,0, -2);
        $sql .= ');';
        $this->SaveVersion($primary_key_value,$array_fields);
        $this->OutputDebug('Insert SQL', $sql);    
        $rs = $this->Query($sql);
        //$this->log_events(array('lib'=>$this->table_name,'type'>='INSERT'));
        if($rs === false) {
            if($this->usetransaction === true) $this->db->RollbackTrans();
            $this->log_events(array('type'=>'ERROR', 'lib' => 'INSERT ERROR : ' . $this->db->errorMsg()));
            GaboxLogger::getInstance()->error($this->db->errorMsg() . '->' . $sql, [
                'action' => 'GenClass::set',
                'id_gabox' => $primary_key_value
            ]);
        } else {
            if(!is_int($primary_key_value)){
                 $primary_key_value = (int)$this->GetLastInsertId();
            }
            if($this->usetransaction === true){
                $this->db->CommitTrans();
            }
            $this->log_events(array('type'=>'INSERT', 'lib' => 'INSERT SUCCESS', 'id_element' => $primary_key_value));
            $temp = $this->get($primary_key_value);
            if($temp){
                self::$eventDispatcher->dispatch(
                    new CollectorItemEvent($this->get($primary_key_value), get_class($this)),
                    GenclassEvents::COLLECTOR_ITEM_CREATED
                );
            }
            $this->primary_key_value = $primary_key_value;
            return $primary_key_value;
        }
    }
  }

      /**
       * string GetField(string)
       * renvoi la valeur du champ passé en parametre.
       * Cette fonction doit etre appellée apres un Get
       * @param field_name : nom du champ
       * @return string : valeur contenu dans le champ
       */
      function GetField($field_name){
        if(!in_array($field_name, $this->array_fields)){
          $this->OutputDebug('get field',$field_name.' = '.$this->array_fields[$field_name]);
          return $this->array_fields[$field_name];
        }else{
          return false;
        }
      }

      /**
       * array GetFieldsList(string)
       * @param table_name : nom de la table
       * @return array : liste des champs de la table
       */
      function GetFieldsList($table_name){
        if(empty($table_name))
          $table_name = $this->table_name;
        if(empty($table_name))
          return false;
        return $this->db->MetaColumnNames($table_name);
      }

      /**
       * boolean SetField(string, string)
       * @param field_name : nom du champ
       * @param field_value : valeur du champ
       * @return boolean 
       */
      function SetField($field_name, $field_value){
        if(!in_array($field_name, $this->array_fields)){
          $this->array_fields[$field_name] = $field_value;
          return true;
        }else{
          return false;
        }
      }

      /**
       * GetLastInsertId
       * @return : l'id du dernier key_value en base (dans le cas de key_value numerique)
       */
      function GetLastInsertId($where=''){
        $sql = 'SELECT max('.$this->primary_key.') as max FROM '.$this->table_name.' ' .$where;
        $rs = $this->Query($sql);
        return (int) $this->fetchField($rs, 'max');
      }

      /**
       * SetError
       * @param string error_txt
       * @param int error_level
       * @return  false
       * @deprecated use GaboxLogger instead
       */
      function SetError($error_txt, $error_level = 1){
          return false;
      }


      function CachedQuery($sql_query){
        if(isset($_SERVER['front']) && $_SERVER['front']==1 && $this->_cacheEnabled === true && defined('USE_CACHE') && USE_CACHE){
            $rs = $this->db->CacheExecute($this->cache_time, $sql_query);
            if (!$rs) {
                GaboxLogger::getInstance()->error($this->db->errorMsg(), [
                    'action' => 'GenClass::CachedQuery',
                ]);
                return false;
            }
            return $rs;
        }
        else{
            return $this->Query($sql_query);
        }
      }



      /**
       * Query()
       * Execute une requete
       * @param $sql_query : requete à executer
       * @return : le resultat de la requete
       */
      function Query($sql_query) {
        return $this->db->Execute($sql_query);
      }

      /**
       * OutputDebug()
       * Affiche des messages debug
       */
      function OutputDebug($lib, $msg,$indent = false){
        if($this->debug == false) return false;
        echo '<div align="left"><b> ['.strtoupper(str_pad($lib.']',20)).'</b>';
        echo str_pad($msg,40)."\n<br></div>";
      }
      function SetTableName($TableName) {
        $this->table_name = $TableName;
      }


      /**
       * SetUsedFields()
       * Modifie la liste des champs à retourner par la methode get
       */
      function SetUsedFields($fields = array()){
        $this->origin_array_fields = $this->array_fields;
        $this->array_fields = array_flip($fields);
        return $this->origin_array_fields;
      }


    function GetRows($rs)
    {
        if (!$rs) {
            return false;
        }
        if ($rs instanceof PDOStatement) {
            return $rs->fetchAll(PDO::FETCH_ASSOC);
        } elseif (is_array($rs)) {
            return $rs;
        } else {
            return $rs->GetRows();
        }
    }

    function GetRow($rs)
    {
        if (!$rs) {
            return false;
        }
        if ($rs instanceof PDOStatement) {
            return $rs->fetch(PDO::FETCH_ASSOC);
        } elseif (is_array($rs)) {
            return $rs;
        } else {
            return $rs->fetchRow();
        }
    }

    function RecordCount($rs)
    {
        if (!$rs) {
            return 0;
        }
        if ($rs instanceof PDOStatement) {
            // TODO: check count validity for other dbms than mysql/maria
            return $rs->rowCount();
        } elseif (is_array($rs)) {
            return count($rs);
        } else {
            return $rs->RecordCount();
        }
    }

  function DebugOn(){
    $this->debug  = true;
  }

  function DebugOff(){
    $this->debug  = false;
  
  }

  function getSQLCriteres($criteres = array()) {
      $sql = '';
      foreach($criteres as $k => $v){
          if(is_array($v)){
              $temp = implode('\',\'',$v);
              if($k[0] == '!'){
                  $k = substr($k,1);
                  $sql .= $k.' not in (\''.$temp.'\') AND ';
              } elseif(substr($k, 0, 3) == 'ANY') {
                  $sql .= "'".$temp."' = ".$k. " AND ";
              } else{
                  $sql .= $k.' in (\''.$temp.'\') AND ';
              }
          } elseif($v == 'FALSE' || $v == 'TRUE') {
              $sql .= $k .' = '.$v.' AND ';
          } elseif(substr($k, 0, 2) == '>=' || substr($k, 0, 2) == '<='){
              $ope = substr($k, 0, 2);
              $k = substr($k,2, strlen($k));
              if($v == 'NOW()'){
                  $sql .= $k.$ope.$v.' AND ';
              } else {
                  $sql .= $k.$ope.$this->db->qstr($v).' AND ';
              }
          } elseif($k[0] == '>'){
              $k = substr($k,1);
              if($v == 'NOW()'){
                  $sql .= $k.' > '.$v.' AND ';
              } else {
                  $sql .= $k.' > '.$this->db->qstr($v).' AND ';
              }
          } elseif($k[0] == '<'){
              $k = substr($k,1);
              if($v == 'NOW()'){
                  $sql .= $k.' < '.$v.' AND ';
              } else {
                  $sql .= $k.' < '.$this->db->qstr($v) . ' AND ';
              }
          } elseif(substr($k, 0, 2) == '!=') {
              $k = substr($k, 2, strlen($k));
              if(is_null($v)) {
                  $sql .= $k .' IS NOT NULL AND ';
              } else {
                  $sql .= $k.' != '.$this->db->qstr($v).' AND ';
              }
          } elseif(is_null($v)) {
              $sql .= $k . ' IS NULL AND ';
          } elseif(substr($k, 0, 5) == 'LIKE>') {
              $k = substr($k, 5, strlen($k));
              $sql .= $k . ' LIKE ' . $this->db->qstr($v).' AND ';
          } elseif(substr($k, 0, 5) == 'LIKE*') {
              $k = substr($k, 5, strlen($k));
              $sql .= $k . ' LIKE ' . $this->db->qstr($v."%").' AND ';
          } elseif(substr($k, 0, 5) == '*LIKE') {
              $k = substr($k, 5, strlen($k));
              $sql .= $k . ' LIKE ' . $this->db->qstr("%".$v).' AND ';
          } elseif(substr($k, 0, 5) == '*LIK*') {
              $k = substr($k, 5, strlen($k));
              $sql .= $k . ' LIKE ' . $this->db->qstr("%".$v."%").' AND ';
          } elseif(substr($k, 0, 3) == 'OR>') {
              $orFields = explode('|', substr($k, 3, strlen($k)));
              $sql .= ' (';
              $orConcat = 'CONCAT(';
              foreach($orFields as $orF) {
                  $sql .= $orF . ' LIKE ' . $this->db->qstr($v) . ' OR ';
                  $orConcat .= $orF . ", ' ', ";
              }
              $orConcat = substr($orConcat, 0, -7);
              $orConcat .= ') LIKE ' . $this->db->qstr($v);
              $sql .= $orConcat;
              //$sql .= ' LIKE ' . $this->db->qstr($orConcat).' AND ';
              //$sql = substr($sql, 0, -3);
              $sql .= ') AND ';
          } elseif(substr($k, 0, 6) == '!LIKE>') {
              $k = substr($k, 6, strlen($k));
              $sql .= $k . ' NOT LIKE ' . $this->db->qstr($v).' AND ';
          } else {
              $sql .= $k.' = '.$this->db->qstr($v).' AND ';
          }
      }
      return substr($sql,0, -4);
  }

  function getCount($conditions=array(), $sql = null)
  {
      $old_status = $this->simulation ;
      if(null === $sql) {
          $this->simulation = true;
          $sql = $this->get($conditions);
      }
      $sql = preg_replace('/SELECT.*\s*FROM/i', 'SELECT COUNT(\'*\') as count FROM',$sql,1);
      $sql = preg_replace('/ORDER\sBY.*/i', '',$sql);
      $rs  = $this->Query($sql);
      $out = (($count = $this->fetchField($rs, 'count')) !== null) ? $count : 0;
      $this->simulation = $old_status;
      return $out;
  }

  function getCountCached($conditions=array(), $sql = null)
  {
      $old_status = $this->simulation ;
      if(null === $sql) {
          $this->simulation = true;
          $sql = $this->get($conditions);
      }
      $sql = preg_replace('/(SELECT).*(FROM)/', 'SELECT COUNT(\'*\') as count FROM',$sql);
      $rs  = $this->CachedQuery($sql);
      $out = (($count = $this->fetchField($rs, 'count')) !== null) ? $count : 0;
      $this->simulation = $old_status;
      return $out;
  }
  

  
  function getDistinct($field,$conditions = array()){
    $old_status = $this->simulation ;
    $this->simulation = true;
    $sql = $this->get($conditions);
    $sql = preg_replace('/(SELECT).*(FROM)/', 'SELECT distinct '.$field.' FROM',$sql);
    $alias = stristr($field, ' as ');
    $sql .= ' ORDER BY ' . ($alias ? substr($alias, 4) : $field);
    $rs  = $GLOBALS['obj']['db']->query($sql);
    $this->simulation = $old_status;
    return $this->GetRows($rs);
  }

  function getSearch($fields, $value, $order = '', $limit = '',$offset = '', $tables = '', $join = '',$cond=array()){
    $sim = $this->simulation;
    $this->simulation = true; 
    if(!is_int($value) && $this->_searchLike === true && trim($value) != '') {
        $value = '%' . $value . '%';
    }
    $filteredCryptedCols = array();
    $array = null;
    if(trim($value) != '') {
        if(method_exists($this, '_getFilteredCryptedColumns')) {
            $af = $this->array_fields;
            $filteredCryptedCols = $this->_getFilteredCryptedColumns($af);
            if(count($filteredCryptedCols) > 0) {
                $scc = array();
                foreach($fields as $field) {
                    if($value == '' || stristr($field,' as ')) continue;
                    $scc[$this->cleanFieldName($field)] = str_replace(array('*', '%'), '', $value);
                }
                $this->_searchCryptedColumns = $scc;
            }
        }
    }
    if(count($filteredCryptedCols) == 0) {
        foreach($fields as $field) {
            if($value == '') continue;
            if(stristr($field,' as ')) continue;
            $array[$field] = $value;
        }
    }
    if(empty($array)){
      $sql = $this->get($cond, $order, $limit,$offset, $tables,$join);
      return $sql;
    }
    $sql =  $this->get($array, $order, $limit,$offset, $tables);

    $sql = str_replace('1=1  AND', '', $sql);
    $sql = str_replace(' AND ',' OR ',$sql);
    $sql = str_replace('order by',') order by',$sql);
    $sql = str_replace('WHERE','WHERE(',$sql);
    foreach($cond as $k=>$v){
      $sql = str_replace('WHERE', 'WHERE '.$k.'='.$this->db->qstr($v).' AND',$sql);
    }
    $this->simulation = $sim; 
    $sql = str_replace('=','  like ',$sql);
    $sql = str_replace('*', '%',$sql);
    #XXX wtf ? 
    $sql = str_replace('WHERE(  OR','WHERE(',$sql);
    if(!empty($join)){
       $join .= ' and ';
      $sql = str_replace('WHERE(','WHERE '.$join.' (',$sql); 
    }
    if(strstr($sql, ')') === false) {
        $sql = str_replace('WHERE(','WHERE',$sql);
    }
    return $sql;
  }

  function RunSQLVersioning(){

    if(file_exists(INC.'common/sql_versionning.php')){
       include(INC.'common/sql_versionning.php');
    }
    foreach (array_keys($this->collector) as $module_k=>$module_name){
        if(isset($sql_versionning[$module_name])){
            foreach($sql_versionning[$module_name] as $field_name=>$field_struc){
                if( !in_array($field_name,$this->collector[$module_name]->db->metaColumnNames('gen_'.$module_name))){
                    $this->collector[$module_name]->db->Execute($field_struc);
                }
            }
        }
        if($module_name =='form')continue;
        if($module_name =='mailer')continue;
        if ($this->collector['modules']->get(crc32($module_name)) === false) {
            $this->collector['modules']->set(crc32($module_name), array('lib' => $module_name, 'columns_name' => json_encode($this->collector['modules']->db->metaColumnNames('gen_' . $module_name))));
        }
        if(is_object($this->collector)){
            $this->collector[$module_name]->collector=$this->collector;
        }
    }
    if(isset($sql_versionning['create'])) {
        foreach( $sql_versionning['create'] as $k=>$v) {
            $begin = strpos($v, 'gen_');
            $this->collector['modules']->db->Execute($v);

            if($begin !== false && stripos($v, 'CREATE TABLE') !== false) {
                $m = trim(str_replace(array('`', 'gen_'), '', substr($v, $begin, (strpos($v, ' (') - $begin))));
                if (!in_array($m, array_keys($this->collector))) {
                    $this->collector['modules']->set(crc32($m), array('lib' => $m, 'columns_name' => json_encode($this->collector['modules']->db->metaColumnNames('gen_' . $m))));
                }
            }
        }
    }
  }

    function buildClass()
    {
        $tables = $this->db->MetaTables();
        $include = [];
        if (!empty(GABOX_USR_DIR) && !is_dir(GABOX_USR_DIR . '/classes/auto/' . $this->_dbClassAutoPath)) {
            if(!mkdir(GABOX_USR_DIR.'/classes/auto/' . $this->_dbClassAutoPath, 0755, true)){
                die("Error : Unable to write into " . GABOX_USR_DIR . '/classes/auto/' . $this->_dbClassAutoPath);
            }
        }
        foreach ($tables as $table) {
            if (strpos($table,'gen_') !== 0) {
                continue;
            }
            $file = Ucfirst($table).'.class.php';
            $fields = $this->db->MetaColumnNames($table);
            $fields_list = [];
            foreach($fields as $field) {
                $fields_list[]=$field;
            }
            $out = '<?php class '.$table." extends GenClass {\n";
            $out .= "\t".'var $array_fields = array ('."\n";
            $out .= "\t\t'".implode("'\n\t\t, '",$fields_list)."'\n";
            $out .= "\t);\n";
            $out .= "}\n";
            $include[]=str_replace('gen_','',$table);
            file_put_contents(GABOX_USR_DIR.'/classes/auto/' . $this->_dbClassAutoPath . $file,$out);
        }
        file_put_contents(GABOX_USR_DIR.'/classes/auto/' . $this->_dbClassAutoPath . 'include.php','<?php $objects=array(\''.implode("','",$include).'\');');
        file_put_contents(GABOX_USR_DIR.'/classes/auto/' . $this->_dbClassAutoPath . 'version.php','<?php $version="'.date("YmdhIs").'";');
    }

    public function loadClassFromDb()
    {
        $path = GABOX_USR_DIR.'/classes/auto/' . $this->_dbClassAutoPath . 'include.php';
        $objects = array();
        if(is_file($path)) {
            require($path);
        }
        if(!is_array($this->collector)) $this->collector = array();
        foreach(array_values($objects) as $obj_name) {
            if(!array_key_exists($obj_name, $this->collector)) {
                $obj = $this->add($obj_name);
                $this->collector[$obj_name] = $obj;
                $traits = $this->collector[$obj_name]->getUsedTraits();
                if(count($traits) > 0) {
                    foreach($traits as $t) {
                        $this->_classesByTrait[$t][] = $obj_name;
                    }
                }
            }
        }
        $this->obj = $this->collector;
        return $this->collector;
   }
   
    function getCollector($class=''){
        return $this->collector[$class];
    }

   function SaveVersion($primary_key_value=0,$array_fields=array(),$is_del=false){
        if(isset($_GET['build'])) return false;
        if($this->versionning==false) return false;
        if(in_array($this->table_name, array('gen_backevents', 'gen_caches'))) return false;
        $sql = "SELECT max(version_number) as max FROM ".$this->versionning_table ." where module='".$this->table_name."' and id_element = " . (int)$primary_key_value;
        $rs = $this->Query($sql);
        $version = (int) $this->fetchField($rs, 'max') + 1;
        $id_admins = (isset($_SESSION['admins']) && isset($_SESSION['admins']['id_admins']) && !empty($_SESSION['admins']['id_admins'])) ? $_SESSION['admins']['id_admins'] : 0;
        $sql ="insert into ".$this->versionning_table." (remote_addr,id_element,module,version_number,id_admins,is_deleted) values('".$_SERVER['REMOTE_ADDR']."',".(int)$primary_key_value.",'".$this->table_name."',".$version.",".$id_admins.",".(($is_del==false) ? 0 : 1 ).")";
        $rs = $this->Query($sql);
        $sql = 'SELECT max(id_versions) as max FROM '.$this->versionning_table." where id_element =".(int)$primary_key_value." and module='".$this->table_name."'";
        $rs = $this->Query($sql);
        $id = (int) $this->fetchField($rs, 'max');
        # if update
        if($primary_key_value > 0){
          $before_change = $this->_diffSavedValues;
          //$before_change = $this->getOne($primary_key_value);
          $has_change = false;
        }
        else{
          $has_change = true;
        }
        if($is_del==true){
           $has_change= true;
           $array_fields = $before_change;
            $before_change = array();
        }
        if(!is_array($array_fields)) $array_fields = array();
        foreach($array_fields as $field_name=>$field_value){
            $old_value = (isset($before_change[$field_name])) ? $before_change[$field_name]['old'] : false;
            if(false === $old_value) continue;
            if((!$old_value) && !($field_value))continue;
            if($old_value == $field_value) continue;
            if($field_name == 'updated_at') continue;
            $has_change = true;
        	if($field_name == $this->primary_key) continue;
            $sql ="INSERT INTO gen_versions_elements (id_versions,fieldname,fieldvalue,field_old_value) VALUES (".$id.",'".$field_name."',".$this->db->qstr($field_value).",".$this->db->qstr($old_value).")";
        	$rs = $this->Query($sql);
        }
        if($has_change == false){
            $sql = 'DELETE FROM '.$this->versionning_table." where id_versions=" . $id . " AND id_element = " . (int) $primary_key_value;
            $rs = $this->Query($sql);
        }
   }
   
   public function reverseBoolean($id, $field_name, $updateTranslations = false)
   {
       $current = $this->get($id);
       $current_value = $current[$field_name];
       $new_value = (int)!$current_value; 
       $this->set($id,array($field_name=>$new_value));
       if(true === $updateTranslations && $new_value == 0) {
            $sql = "UPDATE gen_i18n SET lib = NULL 
                    WHERE module = " . $this->db->qstr(str_replace('gen_', '', $this->table_name)) . " 
                    AND field_name = " . $this->db->qstr($field_name) . " 
                    AND id_element = " . $this->db->qstr($id);
            $rs = $this->Query($sql);
       }
       return $new_value;
   }

   function getKV($conditions = array(), $order = '', $limit = '', $noMultiple = null) {
       $rs = $this->get($conditions, $order, $limit);
       if(empty($rs)) return array();
       foreach($rs as $k => $realArray) {
           if(empty($realArray))continue;
           $libField = $this->_libField;
           if(is_array($libField)) {
                $vals = array();
                foreach($libField as $f) {
                    if(array_key_exists($f, $realArray)) {
                        $vals[] = $realArray[$f];
                    }
                }
                $val = implode($this->_libSeparator, $vals);
           } else {
               $val = (isset($realArray[$libField])) ? $realArray[$libField] : '';
           }
           
           if(array_key_exists($this->primary_key,$realArray)){
               $retArr[$realArray[$this->primary_key]] = $val;
           }
       }
       return $retArr;
   }
   function getKandMore($fields=array(),$conditions = array(), $order = '', $limit = '') {
       $rs = $this->get($conditions, $order, $limit);
       if(empty($rs)) return array();
       foreach($rs as $k => $realArray) {
           $temp = '';
           foreach($fields as $kv=>$vv){
               if(isset($realArray[$vv])) $temp .= $realArray[$vv].' ';
               else $temp .= ' '.$vv.' ';
           } 
           $retArr[$realArray[$this->primary_key]] = $temp;
       }
       return $retArr;
   }
   function sqlGet($sql){
     $rs=$this->query($sql);
     return $rs->GetRows();
   }

   function is_enable_for_locale(){
     //$sql = 'select count(\'*from '.$this->table_name.' , gen_i8n  
   }
   function getCurrentIdLoc($id_locale){
        return (empty($id_locale) && defined('ID_LOC')) ?  ID_LOC : $id_locale;
   }
   function getIdLocMasterByLoc($id_locale){
        if(!isset($this->collector['locales'])) return false;
        if(defined('ID_LOC_MASTER') && ID_LOC_MASTER!=0 && !empty(ID_LOC_MASTER)){
                return ID_LOC_MASTER;
        }
        $loc = $this->collector['locales']->getOneCached($id_locale);
        $master_locale=$this->collector['locales']->getOneCached(array('is_master'=>1,'language'=>$loc['language']));
    	if(!$master_locale) return false;
        #master de lui même => slower queries ? 
        $return = $master_locale['id_locales']; 
        if(!empty($return)) return $return;
        else return 1;
   }


   function getOneI18n($mixed='',$order='',$search='',$id_loc='',$id_loc_master=''){
      if(empty($id_loc)) $id_loc = $this->getCurrentIdLoc($id_loc); 
      if(empty($id_loc_master)) $id_loc_master = $this->getIdLocMasterByLoc($id_loc);
      $rs = $this->getI18n($mixed, $order,$search, $id_loc,$id_loc_master);
      return (isset($rs[0])) ? $rs[0] : false;
   }

   function getOneI18nLib($mixed='',$order=''){
      $temp = $this->array_fields;       
      $rs = $this->getI18n($mixed, $order);
      $this->array_fields = array('lib');
      $return = (isset($rs[0]['lib'])) ? $rs[0]['lib'] : false;
      $this->array_fields=$temp;
      return $return;
   }

	

   public function log_events($logParams)
   {
       if($this->_logEvents === true && $this->table_name != 'gen_backevents') {
           // Check version plus élaborée du système de log
           if(array_key_exists('module', $this->collector['backevents']->array_fields)) {
               $logParams['module'] = str_replace('gen_', '', $this->table_name);
               $logParams['ip'] = ($this->_logOrigin != 'SYSTEM') ? getRealUserIp() : null;
               $logParams['origin'] = $this->_logOrigin;
           }
           if(!empty($this->_adminId)) {
               $idAdmin = $this->_adminId;
           } else {
               $idAdmin = isset($_SESSION['admins']['id_admins']) ? $_SESSION['admins']['id_admins'] : (isset($_SESSION['prelogged_admins']['id_admins']) ? $_SESSION['prelogged_admins']['id_admins'] : null);
           }
           $logParams['id_admins'] = $idAdmin;
           if(!isset($logParams['type'])) $logParams['type'] = 'MSG';

           if(array_key_exists('updates', $this->collector['backevents']->array_fields) && is_array($this->_diffSavedValues) && count($this->_diffSavedValues) > 0) {
               $logParams['updates'] = json_encode($this->_diffSavedValues);
           }
           if($logParams['type'] == 'UPDATE' && !isset($logParams['updates'])) {
               return false;
           }
           $this->collector['backevents']->set(null, $logParams);
       }
   }

   function downloadCSVBySQL($sql, $forExcel = false, $firstLine = array(), $addActionColumn = false, $br2nl = false){
       ob_get_clean();
       $filename = date('d-m-Y-H-i-S').'.csv';
       header('Content-Type: text/csv; charset=utf-8');
       header('Content-Disposition: attachment; filename='.$filename);
       $output = fopen('php://output', 'w');
       // Add BOM to avoid encoding issues
       fputs($output, "\xEF\xBB\xBF");

       $rows = $this->GetRows($this->Query($sql));
       if(method_exists($this, 'decryptData')) $this->decryptData($rows);
       $forceTextFields = method_exists($this, 'getForceExportTextFields') ? $this->getForceExportTextFields() : array();
       if(count($firstLine) > 0) {
           fputcsv($output, $firstLine, ";");
       }
       foreach($rows as &$row){
           if ($br2nl) {
               foreach ($row as &$rowVal) {
                   $rowVal = br2nl($rowVal);
               }
           }
           if($forExcel === true) {
               foreach($row as $field => &$rowVal) {
                   $rowVal = utf8_decode(html_entity_decode($rowVal));
                   if(in_array($field, $forceTextFields)) {
                       $rowVal = '="' . $rowVal . '"';
                   }
               }
           }
           if($addActionColumn === true) {
               $row[] = '';
           }
           fputcsv($output, $row, ";");
       }
       $this->log_events(array('type'=>'EXTRACT', 'lib' => 'EXTRACT data (CSV by SQL), module ' . str_replace('gen_', '', $this->table_name)));
       die();
   }
    function extractTranslationCSV($module,$fields=array('lib'),$id_site=ID_SITE){
         ob_get_clean();
        $filename = date('d-m-Y-H-i-S').'.csv';
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename);
        $output = fopen('php://output', 'w');

        $this->collector['i18n']->disableCache();
        $this->collector[$module]->disableCache();

        $locales = $this->collector['sites']->getAvailabledLocalesForSite($id_site);
        #XXX n-pestana to n-pestana
        # utiliser fputcsv !!!
        $r = $this->collector[$module]->get(array());
        $out='id_'.$module.';';

        foreach($fields as $field){
            $out .= $field.'_MASTER;';
        }

        foreach($locales as $loc){
            foreach($fields as $field){
                $out.= $field.'_'.$this->collector['locales']->getLib($loc['id_locales']).'['.$loc['id_locales'].'];';
            }
        }

        foreach($r as $string){
            $out.="\n".$string['id_'.$module].';';
            foreach($fields as $field){
                $out .= '"'.str_replace(array("\r",'"',';'),array("",'""',','),($module=='translations') ? $string['lib'] : $string[$field]).'";';
            }
            foreach($locales as $loc){
                foreach($fields as $field){
                    $temp= $this->collector['i18n']->getOne(array(
                        'id_element'=>$string['id_'.$module]
                        ,'field_name'=>$field
                        ,'module'=>$module
                        ,'id_locales'=>$loc['id_locales']
                    ));
                    if(isset($temp['lib'])){ 
                        $out.='"'.str_replace(array("\r",'"',';'),array("",'""',','),$temp['lib']).'";';
                    }
                    else $out .=';';
                }
            }
        }
        $this->log_events(array('type'=>'EXTRACT', 'lib' => 'EXTRACT translation CSV, module ' . $module));
        echo utf8_decode($out);
        die();
   }

    function extractTranslationCSV_V2(array $modules, $id_locale, $id_mast_loc = 2) {

        $locales = array($id_locale);


         $loc=$locales[0];
        $this->collector['i18n']->disableCache();

    	ob_get_clean();
        if(empty($curr_obj)) $curr_obj = "all";
        $filename ='export-'. date('d-m-Y-H-i-S').'-'.urlize($this->collector['locales']->getLib($id_locale)).'-'.urlize($this->collector['locales']->getLib($id_mast_loc)).'.csv';
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename);
        $output = fopen('php://output', 'w');


        fputcsv($output, array('id_element','id_i18n','id_locales','module','field_name','source','translation ['. utf8_decode(html_entity_decode($this->collector['locales']->getLib($id_locale))).']'),";");
        //fputcsv($output, array('id_element','id_i18n','id_locales','module','field_name','source','translation ['. utf8_decode(html_entity_decode($this->collector['locales']->getLib($id_locale))).']'),";");

        $translations_done=0;

        foreach($modules as $module)    {

            error_log("DECEX ".$module);

            $this->collector[$module]->disableCache();

            foreach($this->collector[$module]->array_fields as $field_name=>$v){


            error_log("DECEX ".$module . "->".$field_name);

               if(stristr($field_name,'_id'))continue;
               elseif(stristr($field_name,'id_'))continue;
               elseif(stristr($field_name,'_at'))continue;
               elseif(stristr($field_name,'_date'))continue;
               elseif(stristr($field_name,'date_'))continue;
               elseif(stristr($field_name,'_media'))continue;
               elseif(stristr($field_name,'media_'))continue;
               elseif(stristr($field_name,'media'))continue;
               elseif(stristr($field_name,'produit_associe_'))continue;
               elseif(stristr($field_name,'reference'))continue;
               elseif(stristr($field_name,'_value'))continue;
               elseif(stristr($field_name,'media'))continue;
               elseif(stristr($field_name,'is_'))continue;
               elseif(stristr($field_name,'href'))continue;
               elseif(stristr($field_name,'url'))continue;
               elseif(stristr($field_name,'show_'))continue;
               elseif(stristr($field_name,'hook'))continue;
               elseif(stristr($field_name,'file'))continue;
               elseif(stristr($field_name,'_order'))continue;
               elseif(stristr($field_name,'order_'))continue;
               elseif($field_name=='item_order') continue;
               elseif($field_name=='level') continue;
               elseif($field_name=='type') continue;
               elseif($field_name=='extra_class') continue;
               elseif($field_name=='color') continue;
               elseif($field_name=='gtm_cat') continue;
               elseif($field_name=='controller') continue;
               elseif($field_name=='url') continue;
               elseif($field_name=='is_valid') continue;
               elseif($field_name=='composition') continue;
               elseif($translations_done==1) continue; # du sale ! NP
               else{
                    if($module=='translations'){
                        $translations_done=1;
//                        $sql = str_replace("i.field_name LIKE  'lib'","i.field_name LIKE  'translations'",$sql)         ;
                           # une seule locale supportée pour le moment.
    
                        $all_translations = $this->collector['translations']->get();
                
                        foreach($all_translations as $translations=>$translation){
                            
                            $sql = "SELECT i.id_element, i.id_i18n, i.id_locales, i.module, i.field_name, 
                                ( CASE WHEN ( 
                                    SELECT lib
                                    FROM gen_i18n
                                    WHERE module =  'translations'
                                    AND id_element = ".$translation['id_translations']."
                                    AND id_locales =".$id_mast_loc."
                                    AND field_name =  'translations'
                                ) > ''
                                THEN (
                                    SELECT lib
                                    FROM gen_i18n
                                    WHERE module =  'translations'
                                    AND id_element  = ".$translation['id_translations']."
                                    AND id_locales =".$id_mast_loc."
                                    AND field_name =  'translations'
                                )
                                ELSE (
                                    SELECT lib
                                    FROM gen_translations
                                    WHERE id_translations = ".$translation['id_translations']."
                                )
                                END
                                ) AS source, (

                                    SELECT lib
                                    FROM gen_i18n
                                    WHERE module =  'translations'
                                    AND id_element = ".$translation['id_translations']."
                                    AND id_locales =".$loc."
                                    AND field_name =  'translations'
                                ) AS translation

                                FROM gen_i18n i
                                WHERE module =  'translations'
                                AND id_element = ".$translation['id_translations']."
                                AND id_locales =".$loc."
                                AND field_name =  'translations'";

                                $rows = $this->GetRows($this->Query($sql));
                                foreach($rows as $row){
                                    foreach($row as $field => $rowVal) {
                                        $row[$field] =strip_tags(utf8_decode(html_entity_decode(str_replace($this->_extractCharsList['in'],$this->_extractCharsList['out'],$rowVal))));
                                    }
                                    fputcsv($output, $row,";");
                                }
                        }
                    }
                    else{
                           foreach($this->collector[$module]->get() as $kid=>$vid){
                              $r = $this->collector['i18n']->getCount(array('module'=>$module,'field_name'=>$field_name,'id_locales'=>$loc,'id_element'=>$vid["id_".$module]));
                              if($r==0){    
                                $array= array('module'=>$module,'field_name'=>$field_name,'id_locales'=>$loc,'id_element'=>$vid["id_".$module],'is_published'=>1,'level'=>1);
                                $this->collector['i18n']->set('',$array);
                              }
                               

                              $sql = "
                                SELECT gen_".$module.".id_".$module.", i.id_i18n, i.id_locales,  i.module, i.field_name, gen_".$module.".".$field_name." as source ,i.lib as translation
                                FROM gen_".$module."
                                LEFT JOIN gen_i18n i ON ( i.id_element = gen_".$module.".id_".$module." and i.module LIKE  '".$module."'
                                AND i.field_name LIKE  '".$field_name."' ) WHERE 1  
                                    AND gen_".$module.".".$field_name." !='' 
                                    AND gen_".$module.".".$field_name." is not null 
                                    AND i.id_locales=".$loc."
                                    AND i.id_element=".$vid["id_".$module]
                                 ;
    /*                            if(!empty($locales)){
                                    $conditions_locales=array();
                                    foreach($locales as $locale){
                                        $conditions_locales[] = "i.id_locales=".$locale;
                                    }
                                    $sql .= "and (".implode(" or ",$conditions_locales).")";
                                }*/
                                $rows = $this->GetRows($this->Query($sql));
                                if(!$rows)error_log("DECEX Pas de rs" .$sql);
                                foreach($rows as $row){

                                    foreach($row as $field => $rowVal) {
                                        $row[$field] = strip_tags(utf8_decode(html_entity_decode(str_replace($this->_extractCharsList['in'],$this->_extractCharsList['out'],$rowVal))));
                                        
                                    }

                                    fputcsv($output, $row,";");
                                }
                            }


                    }
                    
                }

            }
        }

        die();
   }



  function downloadCSV($curr_obj='',$fields='',$conditions='',$filename=''){
	ob_get_clean();
        if(empty($curr_obj)) $curr_obj = str_replace('gen_','',$this->table_name);
        if(empty($fields )) $fields = $this->array_fields;
	if(empty($filename)) $filename = $curr_obj.'.csv';
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename);
        $output = fopen('php://output', 'w');
        fputcsv($output, array_values($fields),";");
        $sql = 'select '.implode(',',array_keys($fields)).' from gen_'.$curr_obj .' '.$conditions;
        $rows = $this->GetRows($this->Query($sql));
	foreach($rows as $row){
		fputcsv($output, $row,";");
	}
        die();
	

  }
  function loadCSV($curr_obj,$fields,$file){
	ob_get_clean();
	$csv = file_get_contents($file);
	$nbr_fields_await=count($fields);
   	$lines =explode("\n",$csv);	
	$sql = '';
	$out = array();
	$out[]= count($lines)." lines detected";
	$i=$j=0;
	$inserted=0;
	$error=0;
	#$this->collector[$curr_obj]->db->Query('update gen_'.$curr_obj.' set id_site = -'.ID_SITE.' where id_site='.ID_SITE);
	
	foreach($lines as $line){
		if(strstr($line,'PROD')){
			 $out[]='Header detected:'. $line;
			continue;
		}
		$i++;
		$user_fields = explode(";",$line);
		$nbr_field = count($user_fields);
		if($nbr_field !=  $nbr_fields_await){
		 	$out[]='Warning line : '.$i.': bad field number ('.$nbr_field.' found, '.$nbr_fields_await.' awaited)'; 
			continue;
		} 
		$j++;
		$array=array('id_site'=>ID_SITE);
		$i2=0;
		foreach($fields as $a_field=>$lib){
			$array[$a_field]=$user_fields[$i2];
			$i2++;
		}
		
		if($this->collector[$curr_obj]->set('',$array)) {
			$inserted++;
		}else{ 	
			$error++;
		}
		
		
	}
	$out[]= $inserted.' inserted values';
	$out[]= $error.' error';
	foreach($out as $log){
		echo '<li>'.$log.'</li>';
	}
	die();

  }

   function RollBackUsedFields(){
        if(is_array($this->origin_array_fields)) {
            $this->array_fields=$this->origin_array_fields;
        }
   }


   public function getI18n($mixed='',$order='',$search='',$id_loc='', $id_loc_master='', $limit = '', $offset = ''){

        if($this->_cacheEnabled && defined('USE_CACHE') && USE_CACHE == 1 && $this->simulation==false) {
            $this->cache_name = md5($_SERVER['SERVER_NAME'].__FUNCTION__.$id_loc.((defined('ID_LOC')) ? ID_LOC : '').$id_loc_master.json_encode($this->array_fields).json_encode(func_get_args()));

            if(isset($this->collector['caches'])){
                $res = $this->collector['caches']->getVar($this->cache_name);
                if($res === false && property_exists($this, '_useInternalCacheOverflow')) {
                    $res = get_internal_cache($this->cache_name);
                }
            } else {
                $res = get_internal_cache($this->cache_name);
            }

            if($res!==false) {
                if(method_exists($this, 'decryptData')) $this->decryptData($res);

                if (!empty($this->automaticFilteringMethods) && $this->isAutomaticFilteringEnabled()) {
                    foreach ($this->automaticFilteringMethods as $callable) {
                        $res = call_user_func($callable, $res);
                    }
                }
                if (empty($res)) {
                    return false;
                }

                return $res;
            }
        }

        if(empty($id_loc)){ $id_loc = $this->getCurrentIdLoc($id_loc); }
        if(empty($id_loc_master)) {
            if(defined('ID_LOC_MASTER') && ID_LOC_MASTER!=0) $id_loc_master=ID_LOC_MASTER;
            else $id_loc_master = $this->getIdLocMasterByLoc($id_loc);
        }

        $order = str_replace('order by order by', 'order by', strtolower($order));
        // CAST automatique en INT pour les colonnes n_order / int_order
        if((strstr($order, 'n_order') || strstr($order, 'int_order')) && false === stristr($order, 'CAST')) {
            $order = preg_replace(['/ ([^ .]*\.)?n_order/i', '/ ([^ .]*\.)?int_order/i'], [' CAST(\1n_order AS SIGNED)', ' CAST(\1int_order AS SIGNED)'], $order);
        }
        
        #n-pestana  05/05/2017 à tester XXX potentiel
        if(empty($id_loc)){
            return $this->get($mixed,$order,$search,$limit,$offset);
        }

        $this->id_locales = $id_loc;
        $this->id_locales_master = $id_loc_master;
        $search_sql = '';
        
        $fieldList = array();
        $havingCritere = '';
        $filteredCryptedCols = array();

        if(!empty($limit) && is_int($limit))
            $limit = ' limit '.$limit;
        if(!empty($offset) && is_int($offset))
            $offset = ' offset '.$offset;

        $fields = $this->array_fields;
        $nbCrit = is_array($mixed) ? count($mixed) : 0;
        $randomizeResult = false; // Utilisé dans le cas où l'on a un ORDER BY RAND() et une partition des champs

        if(count($fields) + $nbCrit >= 30) {
            $orderField = trim(str_ireplace(array('order by', ' asc', ' desc'), '', $order));
            if(stristr($orderField, 'rand()')) {
                $randomizeResult = true;
                $orderField = '';
                $order = '';
            }
            $partitionNb = 30;
            if(!empty($orderField)) {
                $removeParts = false;
                if(strstr($orderField, '(') && strstr($orderField, ')')) {
                    $removeParts = true;
                }
                $e = explode(',', $orderField);
                foreach($e as $k => $ee) {
                    if($removeParts || strstr($ee, '.') || strstr($ee, '(') || strstr($ee, ')')) {
                        unset($e[$k]);
                    }
                }
                $partitionNb -= count($e);
            }
            $fields = partition($this->array_fields, ceil((count($fields) + $nbCrit) / $partitionNb));
            if(!empty($orderField)) {
                foreach($fields as &$pFields) {
                    foreach($e as $f) {
                        if(!array_key_exists($f, $pFields)) {
                            $pFields[trim($f)] = '';
                        }
                    }
                }
            }
            if(count($fields) > 1 && is_array($mixed)) {
                foreach($mixed as $mf => $mv) {
                    foreach($fields as $idx => &$list) {
                        // XXX: A revoir lors du refactoring
                        if(!array_key_exists($mf, $list) && substr($mf, 0, 1) != '<' && substr($mf, 0, 1) != '>' && substr($mf, 0, 1) != '!') {
                            $list[$mf] = null;
                        }
                    }
                }
            }
        } else {
            $fields = array($fields);
        }
        foreach($fields as $set) {
            foreach($set as $f => $v) {
                $fieldList[] = $f;
            }
        }
        $loadedIds = array();
        foreach($fields as $fieldset) {
            $join_cnt = 0;
            $case = array();
            $join = '';
            $results = array();
            foreach($fieldset as $field_name=>$field_value) {
                if(substr($field_name, 0, 3) != 'id_' && !strstr($field_name,'id_')) continue;
                $case[] = $this->table_name.".".$field_name;
            }
            /*if(!empty($search)) {
                $search_sql =  'and ( false ';
            }*/

            $havingFields = array();
            $whereFields = array();
            $critere = '';
            $havingCritere = '';
            if(is_array($mixed)||is_object($mixed)) {
                if(!empty($mixed)) {
                    if(method_exists($this, '_getFilteredCryptedColumns')) {
                        $filteredCryptedCols = $this->_getFilteredCryptedColumns($mixed);
                    }
                    foreach($mixed as $mixedField => $mixedVal) {
                        if(array_key_exists($mixedField, $fieldset) && substr($mixedField, 0, 3) != 'id_' && !strstr($mixedField,'id_')) {
                            $havingFields[$mixedField] = $mixedVal;
                        } else {
                            $whereFields[$mixedField] = $mixedVal;
                        }
                    }
                    $critere = count($whereFields) > 0 ? 'and '.$this->getSQLCriteres($whereFields) : '';
                    $havingCritere = count($havingFields) > 0 ? ' having '.$this->getSQLCriteres($havingFields) : '';
                }
                else $critere='';
            } elseif(!empty($mixed)) {
                $mixed = (int)$mixed;
                $critere = ' and( '.$this->table_name.".".$this->primary_key.' = '.$mixed.')';
            } else {
                $critere='';
            }

            $has_lib=false;
            foreach($fieldset as $field_name=>$field_value) {
                if(empty($field_name))continue;
                if(substr($field_name, 0, 3) == 'id_' && strstr($field_name,'id_')) continue;
                if($field_name=='lib') $has_lib=true;
                if($field_name=='is_published') $has_lib=false;
                $case[] = "CASE WHEN i18n_".$field_name.".lib !='' THEN i18n_".$field_name.".lib ELSE CASE WHEN i18n_master_".$field_name.".lib !='' THEN i18n_master_".$field_name.".lib ELSE ".$this->table_name.".".$field_name." END END AS ".$field_name;
                $join.="LEFT JOIN gen_i18n i18n_".$field_name."  ON ( 
                        i18n_".$field_name.".id_element = ".$this->table_name.".".$this->primary_key."
                    AND i18n_".$field_name.".id_locales =".$this->id_locales."
                    AND i18n_".$field_name.".module = '".str_replace('gen_','',$this->table_name)."'
                    AND i18n_".$field_name.".field_name =  '".$field_name."' ) ";
                $join.="LEFT JOIN gen_i18n i18n_master_".$field_name."  ON ( 
                        i18n_master_".$field_name.".id_element = ".$this->table_name.".".$this->primary_key."
                    AND i18n_master_".$field_name.".id_locales =".$this->id_locales_master."
                    AND i18n_master_".$field_name.".module = '".str_replace('gen_','',$this->table_name)."'
                    AND i18n_master_".$field_name.".field_name =  '".$field_name."' ) ";
                $join_cnt=$join_cnt+1;
            }
            $having = $havingCritere != '' ? $havingCritere : '';
            if(count($loadedIds) > 0) {
                $critere .= ' AND ' . $this->primary_key . " IN (" . implode(',', $loadedIds) . ")";
            }
            if($has_lib) {
                $sql = "SELECT  ".implode(',',$case).","." i18n_lib.is_published  FROM ".$this->table_name." ".$join." where 1 ".$critere. " ".$search_sql." ".$having." ".$order . " " .$limit . " ".$offset; 
            }
            else{
               $sql = "SELECT  ".implode(',',$case). " FROM ".$this->table_name." ".$join." where 1  ".$critere. " ".$search_sql." ".$having." ".$order . " " .$limit . " ".$offset; 
            }
            $this->OutputDebug('query',$sql);


            if($this->simulation===true) {
                return $sql;
            }
            $rs=$this->Query($sql);
            if($rs!==false){
                $results = array_merge($results, $this->getRows($rs));
            }

            $allResults[] = $results;
            if(count($fields) > 1) {
                /* Dans le cas de champs partitionnés: load des IDs qui ont été chargés pour les utiliser dans le where de la seconde query vu que l'on peut avoir des données filtrées par HAVING qui ne le seront pas au second tour (car pas dans le pool des champs sélectionnés) */
                foreach($results as $row) {
                    if(isset($row[$this->primary_key])) {
                        $loadedIds[] = $row[$this->primary_key];
                    }
                }
            }
        }
        $merged = array();
        foreach($allResults as $key => $rows) {
            foreach($rows as $rowKey => $vals) {
                foreach($vals as $field => $val) {
                    $merged[$rowKey][$field] = $val;
                }
            }
        }
        if($randomizeResult === true) {
            shuffle($merged);
        }
        if($this->_cacheEnabled && defined('USE_CACHE') && USE_CACHE) {
            if(isset($this->collector['caches']) && count($merged) < $this->_maxDBCacheItems){
                $this->collector['caches']->setVar($this->cache_name,$merged);
            } else {
                set_internal_cache ($this->cache_name, $merged); 
            }
        }

        if(method_exists($this, 'decryptData')) $this->decryptData($merged, $filteredCryptedCols);

        if (!empty($this->automaticFilteringMethods) && $this->isAutomaticFilteringEnabled()) {
            foreach ($this->automaticFilteringMethods as $callable) {
                $merged = call_user_func($callable, $merged);
            }
        }
        if (empty($merged)) {
            return false;
        }

        return $merged;
   }

    protected function _getI18nFields($fieldset, $id_loc='', $id_loc_master='') {

        if(empty($id_loc)){ $id_loc = $this->getCurrentIdLoc($id_loc); }
        if(empty($id_loc_master)) $id_loc_master = $this->getIdLocMasterByLoc($id_loc);

        $join_cnt = 0;
        $case = array();
        $join = '';
        foreach($fieldset as $field_name=>$field_value) {
            if(substr($field_name, 0, 3) != 'id_' && !strstr($field_name,'id_')) continue;
            $case[] = $this->table_name.".".$field_name;
        }
        $has_lib=false;
        foreach($fieldset as $field_name=>$field_value) {
            if(empty($field_name))continue;
            if(substr($field_name, 0, 3) == 'id_' && strstr($field_name,'id_')) continue;
            if($field_name=='lib') $has_lib=true;
            if($field_name=='is_published') $has_lib=false;
            $case[] = "CASE WHEN i18n_".$field_name.".lib !='' THEN i18n_".$field_name.".lib ELSE CASE WHEN i18n_master_".$field_name.".lib !='' THEN i18n_master_".$field_name.".lib ELSE ".$this->table_name.".".$field_name." END END AS ".$field_name;
            $join.="LEFT JOIN gen_i18n i18n_".$field_name."  ON ( 
                i18n_".$field_name.".id_element = ".$this->table_name.".".$this->primary_key."
                AND i18n_".$field_name.".id_locales =".$id_loc."
                AND i18n_".$field_name.".module = '".str_replace('gen_','',$this->table_name)."'
                AND i18n_".$field_name.".field_name =  '".$field_name."' ) ";
            $join.="LEFT JOIN gen_i18n i18n_master_".$field_name."  ON ( 
                i18n_master_".$field_name.".id_element = ".$this->table_name.".".$this->primary_key."
                AND i18n_master_".$field_name.".id_locales =".$id_loc_master."
                AND i18n_master_".$field_name.".module = '".str_replace('gen_','',$this->table_name)."'
                AND i18n_master_".$field_name.".field_name =  '".$field_name."' ) ";
            $join_cnt=$join_cnt+1;
        }
        return array('case' => $case, 'join_cnt' => $join_cnt, 'join' => $join, 'has_lib' => $has_lib);
    }

   function getSearchI18n($mixed='',$order='',$search='',$id_loc='',$id_loc_master='')
   {
       if(empty($id_loc)){ $id_loc = $this->getCurrentIdLoc($id_loc); }
       if(empty($id_loc_master)) $id_loc_master = $this->getIdLocMasterByLoc($id_loc);

       $this->id_locales_master = $id_loc_master;
       $this->id_locales = $id_loc;
       $join_cnt = 0;
       //if(!defined('ID_LOC_MASTER')) define('ID_LOC_MASTER',0);

       $search_sql = '';
       if(is_array($mixed) && !empty($mixed)){
           $critere = 'and '.$this->getSQLCriteres($mixed);
       } elseif(is_int($mixed)) {
           $critere = ' and( '.$this->table_name.".".$this->primary_key.' = '.$mixed.')';
       } else {	
           $critere = '';
       }

       $join = '';
       $case = array();
       $fields = $this->_search_fields;
       array_unshift($fields, $this->primary_key);
       foreach($fields as $field_name) {
           // XXX pas la peine de i18n les id ?.
           if(substr($field_name, 0, 3) != 'id_' && !strstr($field_name,'id_')) continue;
           $case[] = $this->table_name.".".$field_name;
       }
       foreach($fields as $field_name) {
           if(empty($field_name)) continue;
           if(substr($field_name, 0, 3) != 'id_' && strstr($field_name,'id_')) continue;
           $case[] = "CASE WHEN i18n_".$field_name.".lib !='' THEN i18n_".$field_name.".lib ELSE CASE WHEN i18n_master_".$field_name.".lib !='' THEN i18n_master_".$field_name.".lib ELSE ".$this->table_name.".".$field_name." END END AS ".$field_name;
            $join.="LEFT JOIN gen_i18n i18n_".$field_name."  ON (
                        i18n_".$field_name.".id_element = ".$this->table_name.".".$this->primary_key."
                    AND i18n_".$field_name.".id_locales =".$this->id_locales."
                    AND i18n_".$field_name.".module = '".str_replace('gen_','',$this->table_name)."'
                    AND i18n_".$field_name.".field_name =  '".$field_name."' ) ";
                    $join.="LEFT JOIN gen_i18n i18n_master_".$field_name."  ON ( 
                        i18n_master_".$field_name.".id_element = ".$this->table_name.".".$this->primary_key."
                    AND i18n_master_".$field_name.".id_locales =".$this->id_locales_master."
                    AND i18n_master_".$field_name.".module = '".str_replace('gen_','',$this->table_name)."'
                    AND i18n_master_".$field_name.".field_name =  '".$field_name."' ) ";

                    $join_cnt=$join_cnt+1;
       }
       if(!empty($search)) {
          $search_sql =  'and ( false ';
          foreach($fields as $field_name) {
                  if(empty($field_name)) continue;
                  $search_sql .= ' OR '. 'i18n_'.$field_name.".lib" .' like ' . $this->db->qstr($search);
                  $search_sql .= ' OR '. 'i18n_'.$field_name.".lib" .' like ' . $this->db->qstr('%'.$search);
                  $search_sql .= ' OR '. 'i18n_'.$field_name.".lib" .' like ' . $this->db->qstr('%'.$search.'%');
                  // Utile si %valeur% ?  Pour perf ?
                  $search_sql .= ' OR '. $this->table_name.".".$field_name .' like '.$this->db->qstr($search);
                  // Utile si %valeur% ?  Pour perf ?
                  $search_sql .= ' OR '. $this->table_name.".".$field_name .' like ' . $this->db->qstr('%'.$search);
                  $search_sql .= ' OR '. $this->table_name.".".$field_name .' like ' . $this->db->qstr('%'.$search.'%');
          }
          $search_sql .= ' OR false )';
       }
       $sql = "SELECT  ".implode(',',$case). " FROM ".$this->table_name." ".$join." where 1  ".$critere. " ".$search_sql." group by ".$this->primary_key." ".$order; 
       $this->OutputDebug('query',$sql);
       if($this->simulation===true) return $sql;
       $rs=$this->query($sql);

/*         if($use_apc){
            set_internal_cache ($temp , $merged);
            return $merged;
        }*/

       return $rs->GetRows();
   }

    public function isFormValid($data = array(), $pkVal = null)
    {
        # XXX doit etre dans myForm
        return true;
       $errors = array();
       try {
           $className = strtolower(get_class($this));
           $file = defined('CODE_SITE') && is_file(GABOX_USR_DIR.'/etc/' . CODE_SITE . '.form_fields.php') ? GABOX_USR_DIR.'/etc/' . CODE_SITE . '.form_fields.php' : GABOX_USR_DIR.'/etc/form_fields.php';
           include_once($file);
           if(isset($$className)) {
               $ref = $$className;
           } else {
               $baseName = str_replace('_' . CODE_SITE, '', $className);
               $ref = $$baseName;
           }
           $isValid = true;
           foreach($ref as $key => $conf) {
               if(is_array($conf)){
                   if(array_key_exists('type', $conf) && in_array($conf['type'], array('generated', 'ouinon', 'relationel_list')) && !isset($data[$key])) {
                       $data[$key] = '';
                   }
               }
           }
           foreach($data as $key => $value) {
                if(isset($ref[$key]['pattern']) && $ref[$key]['pattern'] != '') {
                    $checkField = true;
                    if($key == 'password' && '' != $pkVal) {
                        $checkField = false;
                    }
                    if($checkField) {
                        if(!is_string($value)) {
                            if(isset($value['i18n'][0]['value'])) {
                                $value = $value['i18n'][0]['value'];
                            }
                        }
                        if(is_string($value)) {
                            preg_match('/' . $ref[$key]['pattern'] . '/', $value, $res);
                            if(!is_array($res) || is_array($res) && count($res) == 0) {
                                $isValid = false;
                                $errors[$key] = (defined('FIELD_NOT_VALID_MSG') ? FIELD_NOT_VALID_MSG : "This field is not valid : ") . $ref[$key]['label'];
                                if(array_key_exists('errorMsg', $ref[$key])) {
                                    $errors[$key] .= ' (' . $ref[$key]['errorMsg'] . ')';
                                }
                            }
                        }
                    }
                }
           }
           $this->_formErrors = $errors;
       } catch(Exception $e) {
           $errors['general'] = $e->getMessage();
           $isValid = false;
       }
       return $isValid;
   }

   public function getFormErrors() 
   {
       return $this->_formErrors;
   }

   public function setSearchLike($bool = true) 
   {
       $this->_searchLike = $bool;
   }

    public function __call($name, $args) 
    {
        if(strstr($name, 'getBy')) {
            $field = substr($name, 5, strlen($name));
            return $this->get(array($field => $args[0]));
        } elseif(strstr($name, 'getOneBy')) {
            $field = substr($name, 8, strlen($name));
            return $this->getOne(array($field => $args[0]));
        } elseif(strstr($name, 'getKvBy')) {
            $field = substr($name, 7, strlen($name));
            return $this->getKV(array($field => $args[0]));
        }

    }

    public function setLibField($lib) 
    {
     $this->_libField = $lib;
    }

    public function getLibField() 
    {
        return $this->_libField;
    }
    
    public function getSiteCollector($name, $codeSite = null)
    {
        if(null == $codeSite && defined('CODE_SITE')) {
            $codeSite = CODE_SITE;
        }
        if($codeSite !== null && array_key_exists($name . '_' . $codeSite, $this->collector)) {
            return $this->collector[$name . '_' . $codeSite];
        }
        return $this->collector[$name];
    }

    public function getObjectName($name, $codeSite = null) 
    {
        $class = strtolower(get_class($this->getSiteCollector($name, $codeSite)));
        return $class;
    }

    public function getAutocompleteSearchFields($force = false)
    {
        if($force === true && empty($this->_autocompleteSearchFields)) {
            $this->_autocompleteSearchFields = array($this->getLibField());
        }
        return $this->_autocompleteSearchFields;
    }
    
    public function getAutocompleteResultSeparator()
    {
        return $this->_autocompleteResultSeparator;
    }

    public function getNbToday() 
    {
        $sql = "
        SELECT COUNT(*) AS number
        FROM " . $this->table_name . "
        WHERE DATE_FORMAT(" . $this->_dateReferenceField . ", '%Y-%m-%d') = '" . date('Y-m-d') . "'";
        return $this->GetRows($this->Query($sql));
    }

    public  function getEnum($var){
        $out = [];

        foreach($this->$var as $k=>$v){
              $out[$k] = (isset($v['lib'])) ?  $v['lib'] : $v;
        }
        return $out;
    }

    public function enableCache() 
    {
        $this->_cacheEnabled = true;
        return $this;
    }

    public function disableCache() 
    {
        $this->_cacheEnabled = false;
        return $this;
    }

    public function getValidationField() 
    {
        return $this->_validationField;
    }

    public function importTranslations(array $xlsData, $localeId, $idx)
    {
        $display = array();
        foreach($xlsData as $k => $t) {
            if(!array_key_exists($idx[$this->primary_key], $t)) continue;
            $v = $t[$idx[$this->primary_key]];
            if($v instanceof RichText) {
                $v = $v->getPlainText();
            }
            $element = $this->getOne((int) $v);
            $module = str_replace('gen_', '', $this->table_name);
            if(is_array($element) && count($element) > 0) {
                foreach($idx as $field => $fieldIdx) {
                    if($field == $this->primary_key) continue;
                    $i18n = $this->collector['i18n']->getOne(array('module' => $module, 'id_element' => $t[$idx[$this->primary_key]], 'id_locales' => $localeId, 'field_name' => $field));
                    $lib = $t[$idx[$field]];
                    if(property_exists($this, '_boolColumns') && in_array($field, $this->_boolColumns)) {
                        $lib = $lib == 'Yes' ? 1 : 0;
                    }
                    if(trim($lib) == '' || '' == $t[$idx[$this->primary_key]]) continue;
                    if(is_array($i18n) && count($i18n) > 0) {
                        if(!array_key_exists('UPDATE', $display)) {
                            $display['UPDATE'] = array();
                        }
                        $display['UPDATE'][] = "UPDATE " . $field . " / element " . $t[$idx[$this->primary_key]] . ' / ' . $lib . ' / ID I18N : ' . $i18n['id_i18n'];
                        $this->collector['i18n']->set($i18n['id_i18n'], array('lib' => $lib, 'is_published' => 1));
                    } else {
                        if(!array_key_exists('INSERT', $display)) {
                            $display['INSERT'] = array();
                        }
                        $display['INSERT'][] = "INSERT " . $field . ' : ' . $lib;
                        $this->collector['i18n']->set(array('module' => $module, 'lib' => $lib, 'id_element' => $t[$idx[$this->primary_key]], 'id_locales' => $localeId, 'field_name' => $field, 'is_published' => 1, 'level' => 1, 'source' => ''));
                    }
                }
            }
        }
        return $display;
    }

    public function exportTranslations($localeId, $columns, $extraParams = array())
    {
        $Excel = new Spreadsheet();
        $Excel->getProperties()->setCreator("Gaboweb")
              ->setLastModifiedBy("Gaboweb")
              ->setTitle("Automatic export")
              ->setSubject("Automatic export")
              ->setDescription("Automatic export")
              ->setKeywords("export");
    
        $fields = array();
        $labels = array();
        foreach($columns as $f) {
            $fields[$f] = $f;
            $labels[$f] = $f;
            if(substr($f, 0, 3) != 'id_' && substr($f, 0, 3) != 'is_') {
                $fields[$f . '_i18n'] = $f . '_i18n';
                $labels[$f . '_i18n'] = $f . ' (translation)';
            }
        }

        $this->collector['i18n']->disableCache();
        $this->disableCache();
        $rows = $this->get($extraParams);
        $module = str_replace('gen_', '', $this->table_name);

        $cols = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
            'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
            'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ'
        );
        $letterByField = array();
        foreach($fields as $field => $colName) {
            $colLetter = current($cols);
            $Excel->setActiveSheetIndex(0)->setCellValue($colLetter . '1', $labels[$colName]);
            $letterByField[$field] = $colLetter;
            next($cols);
        }
        Settings::setLocale('fr_FR');
        Cell::setValueBinder(new AdvancedValueBinder());
        $line = 2;

        foreach($rows as $rowIdx => $row) {
            foreach($row as $field => $value) {
                if(!in_array($field, $fields)) {
                    continue;
                }
                $value = substr($field, 0, 3) == 'is_' ? ($value == 1 ? 'Yes' : 'No') : $value;
                $letter = $letterByField[$field];
                $Excel->getActiveSheet()->getCell($letter . $line)->setValueExplicit($value);
                if(in_array($field . '_i18n', $fields)) {
                    $fieldName = ($module == 'translations' && $field == 'lib') ? 'translations' : $field;
                    $temp = $this->collector['i18n']->getOne(array(
                        'id_element'=>$row['id_'.$module]
                        ,'field_name'=>$fieldName
                        ,'module'=>$module
                        ,'id_locales'=>$localeId
                    ));
                    $i18nValue = $temp ? $temp['lib'] : '';
                    $letter = $letterByField[$field . '_i18n'];
                    $Excel->getActiveSheet()->getCell($letter . $line)->setValueExplicit($i18nValue);
                }
            }
            $line++;
        }
        foreach($cols as $column) {
            $Excel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
        }
        $fileDir = GABOX_USR_DIR . '/var/exports/tmp/';
        if(!is_dir($fileDir)) {
            mkdir($fileDir, 0755, true);
        }
        $moduleName = array_key_exists('type', $extraParams) ? $extraParams['type'] : $module;
        $filePath = $fileDir . 'export_'. ID_SITE .'_'.$moduleName.'_'.date('d_m_Y').'_'.$_SESSION['admins']['id_admins'].'.xlsx';
        $objWriter = new Xlsx($Excel);
        $objWriter->save($filePath);
        $Excel->disconnectWorksheets();
        return $filePath;
    }

    //public function exportTranslations($localeId, $columns, $extraParams = array())
    public function downloadXLSBySQL($sql, $fields = array(), $addActionColumn = false)
    {
        $this->disableCache();
        $rows = $this->GetRows($this->Query($sql));
        if(method_exists($this, 'decryptData')) $this->decryptData($rows);
        $this->downloadXLS($rows, $fields, $addActionColumn);
    }

    function getCacheName(){
        return $this->cache_name;
    }

    function truncate(){
        $sql = 'TRUNCATE ' . $this->table_name;
		//$sql = "delete from ".$this->table_name;
	//	if(defined('ID_SITE')) $sql .= ' where id_sites='.ID_SITE;
		$this->query($sql);
	}
    
    public function getRandomPassword ($pass_len = 8, $pass_num = true, $pass_alpha = true, $pass_mc = true, $pass_exclude = '')
    {
        // Create the salt used to generate the password
        $salt = '';
        if ($pass_alpha) { // a-z
            $salt .= 'abcdefghijklmnopqrstuvwxyz';
            if ($pass_mc) { // A-Z
                $salt .= strtoupper($salt);
            }
        }
        if ($pass_num) { // 0-9
            $salt .= '0123456789';
        }
        // Remove any excluded chars from salt
        if ($pass_exclude) {
            $exclude = array_unique(preg_split('//', $pass_exclude));
            $salt = str_replace($exclude, '', $salt);
        }
        $salt_len = strlen($salt);
        // Seed the random number generator with current microtime & password's unique settings for extra randomness
        mt_srand($this->_makeSeed() * ($salt_len + $pass_len));
        // Generate random password
        $pass = '';
        for ($i = 0; $i < $pass_len; $i ++) {
            $pass .= substr($salt, mt_rand() % $salt_len, 1);
        }
        $this->_generatedPassword = $pass;
        return $pass;
    }

    protected function _makeSeed()
    {
        list($usec, $sec) = explode(' ', microtime());
        return (float) $sec + ((float) $usec * 100000);
    }

    function auto_translate($text, $src, $dest) {
        if(empty($src) && defined('AUTO_TRANSLATE_SRC')) $src= AUTO_TRANSLATE_SRC;
        if(!$src){
            $src = null;
        }
        if(defined('ID_SITE')){
            $temp = $this->collector['sites']->getOneCached(ID_SITE);
            # deepl : composer require DeepLcom/deepl-php
            if(isset($temp['key_deepl'])&&!empty($temp['key_deepl'])){
                $translator = new \DeepL\Translator($temp['key_deepl']);
                try {
                    $result = $translator->translateText($text, $src, $dest);
                } catch (Exception $e) {
                    $error = t("Une erreur est survenue pendant la traduction automatique, vérifier la clé DeepL ou la locale attendue:  locale > Code langue pour traducteur automatique");
                    GaboxLogger::getInstance()->error($error."<br />".$e->getMessage(), [
                        'action' => 'GenClass::auto_translate',
                    ]);
                }
                return $result->text;
            }

        }
        else{
            return $result;
        }
    }

    public function getNumberByPage()
    {
        return $this->_numberByPage;
    }
    public function setNumberByPage($nb)
    {
        $this->_numberByPage = $nb;
    }
    public function getNbResults()
    {
        return $this->_nbResults;
    }
    public function setCurrentPage($page) 
    {
        $this->_currentPage = $page;
    }

    public function getCurrentPage($page) 
    {
        return $this->_currentPage;
    }

    public function getPaginatedResults() 
    {
        return $this->_paginatedResults;
    }
    public function getNbBulletsVisible() 
    {
        return $this->_nbBulletsVisible;
    }
    function getWithPaginatedResults($conditions=array(),$currentPage=1,$order=''){
        if(!$currentPage) $currentPage = 1;
        $currentPage = (int)$currentPage;
        $this->setCurrentPage($currentPage);
        $old_status = $this->simulation ;
        if(null === $sql) {
            $this->simulation = true;
            $sql = $this->get($conditions,$order);
        }
        $sql_count = preg_replace('/(SELECT).*(FROM)/', 'SELECT COUNT(\'*\') as count FROM',$sql);
        $rs  = $this->Query($sql_count);
        $this->simulation = $old_status;
        $this->_nbResults = $this->fetchField($rs, 'count');

        if($this->_cacheEnabled && defined('USE_CACHE') && USE_CACHE) {
            $rs = $this->CachedQuery($sql);
        }
        else{
            $rs = $this->Query($sql);
        }
        $out= $this->getRows($rs);
        $paginated = array_slice($out, 0 + ($currentPage - 1) * $this->_numberByPage, $this->_numberByPage);
        $this->_paginatedResults = $paginated;

        $paginatedResults= $this->getPaginatedResults();

        $temp = array();

        $idsPaginated = array();
        if(is_array($paginatedResults)) {
            foreach($paginatedResults as $pa) {
                $idsPaginated[] = $pa[$this->primary_key];
            }
        }
        foreach($out as $k=>$v) {
            if(!in_array($v['id_liveraces'], $idsPaginated)) continue;
            $temp[$k] = $this->getOneI18n($v[$this->primary_key]);
        }
        return $temp;

    }
    function getHTMLPager(){
        $pager = '';
        $currentPage = $this->getCurrentPage();
        if( $this->getNbResults()  >  $this->getNumberByPage()) {
            $nbBullets = $this->getNbBulletsVisible();
            $pager = '<nav class="page-navigation text-center" aria-label="Page navigation">';
            $pager .= '<ul class="pagination">';
            $nbPages = (integer) ceil($this->getNbResults()  /  $this->getNumberByPage());
            if($nbPages > 1 && $currentPage > 1) {
                $pager .= '<li><a data-id="' . ($currentPage - 1) . '" href="'.$_SERVER['SCRIPT_URI'].'?page='.($currentPage - 1).'"><span class="glyphicon glyphicon-menu-left" aria-hidden="true" style="top:0px;">&lt;</span></a></li>';
            }
            for($idxPager = 1; $idxPager <= $nbPages; $idxPager++) {
                if($idxPager==1 || $idxPager==$currentPage || $idxPager==$nbPages || ((($idxPager+$nbBullets)>$currentPage)&&(($idxPager-$nbBullets)<$currentPage)) ){
                    $cur = $currentPage == $idxPager ? ' class="active"' : '';
                    $pager .= '<li ' . $cur . '><a data-id="' . $idxPager . '" href="'.$_SERVER['SCRIPT_URI'].'?page=' . $idxPager . '">' . $idxPager . '</a></li>';
                } elseif($idxPager==1 || $idxPager==$currentPage || $idxPager==$nbPages || ((($idxPager+($nbBullets + 1))>$currentPage)&&(($idxPager-($nbBullets + 1))<$currentPage)) ){
                    $pager .= '<li><a>...</a></li>';
                }
            }
            if($nbPages > 1 && $currentPage < $nbPages) {
                $pager .= '<li><a data-id="' . ($currentPage + 1) . '" href="'.$_SERVER['SCRIPT_URL'].'?page='.($currentPage + 1).'"><span class="glyphicon glyphicon-menu-right" aria-hidden="" style="top:0px;">&gt;</span></a></li>';
            }
            $pager .= '</ul></nav>';
        }
        return $pager;
    }

    public function renderBlock($type, $params = array())
    {
        $type = str_replace("'", "", $type);
        $limit = is_array($params) && array_key_exists('limit', $params) ? intval(trim($limit)) : null;
        $methodName = 'render' . ucfirst($type);
        if(method_exists($this, $methodName)) {
            return $this->$methodName($limit, $order);
        } elseif(is_file(implode(DIRECTORY_SEPARATOR, array(GABOX_USR_DIR, 'www', 'blocks', $type . '.php')))) {
            try {
                ob_start();
                include(implode(DIRECTORY_SEPARATOR, array(GABOX_USR_DIR, 'www', 'blocks', $type . '.php')));
                $temp = ob_get_contents();
                ob_get_clean();
            } catch(Exception $e) {
                $temp = 'Error while calling block ' . $type . ' : ' . $e->getMessage();
            }
            return $temp;
        }
    }

    public function setCleanEditTpl($cleanStatus = true) 
    {
        $this->_cleanEditTpl = (bool) $cleanStatus;
        return $this;
    }

    public function downloadXLS($rows = array(), $fields = array(), $addActionColumn = false) 
    {
        $Excel = new Spreadsheet();
        $Excel->getProperties()->setCreator("Gaboweb")
              ->setLastModifiedBy("Gaboweb")
              ->setTitle("Automatic export")
              ->setSubject("Automatic export")
              ->setDescription("Automatic export")
              ->setKeywords("export");
    
        $labels = array();
        $module = str_replace('gen_', '', $this->table_name);
        $this->log_events(array('type'=>'EXTRACT', 'lib' => 'EXTRACT data (XLS), module ' . $module));

        $cols = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $letterByField = array();
        foreach($fields as $field => $label) {
            if(is_array($label) && array_key_exists('label', $label)) {
                $label = $label['label'];    
            }
            $colLetter = current($cols);
            $Excel->setActiveSheetIndex(0)->setCellValue($colLetter . '1', $label);
            $letterByField[$field] = $colLetter;
            next($cols);
        }

        if($addActionColumn === true) {
            $colLetter = current($cols);
            $Excel->setActiveSheetIndex(0)->setCellValue($colLetter . '1', "Action");
            $letterByField['auto_action'] = $colLetter;
            next($cols);
        }
        Settings::setLocale('fr_FR');
        Cell::setValueBinder(new AdvancedValueBinder());
        $line = 2;

        if(count($this->_formatDateExport)>0) {
            $zone = new DatetimeZone('Europe/Paris');
            $dateTo = new DateTime();
            $dateTo->setTimeZone($zone);
        }

        foreach($rows as $rowIdx => $row) {
            foreach($row as $field => $value) {
                if(is_array($fields[$field])) {
                    $valField = $this->collector[$fields[$field]['collector']]->getOne(array($fields[$field]['field']=>$value));
                    if(isset($valField[$fields[$field]['label']]) && is_array($valField)) {
                        $value = $valField[$fields[$field]['label']];
                    }
                }
                if(!array_key_exists($field, $fields)) {
                    continue;
                }
                //$value = substr($field, 0, 3) == 'is_' ? ($value == 1 ? 'Yes' : 'No') : $value;
                $letter = $letterByField[$field];

                if(array_key_exists($field, $this->_formatDateExport)) {
                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $value, $zone);
                    $dateTo->setDate($date->format('Y'),$date->format('m'),$date->format('d'));
                    $value = $dateTo->format($this->_formatDateExport[$field]);
                    $Excel->getActiveSheet()->getCell($letter . $line)->setValueExplicit($value);
                } elseif(is_numeric($value)) {
                    $Excel->getActiveSheet()->getCell($letter . $line)->setValueExplicit($value, DataType::TYPE_NUMERIC);
                } else {
                    $Excel->getActiveSheet()->getCell($letter . $line)->setValueExplicit($value);
                }
            }
            $line++;
        }
        foreach($cols as $column) {
            $Excel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
        }
        $fileDir = GABOX_USR_DIR . '/var/exports/tmp/';
        if(!is_dir($fileDir)) {
            mkdir($fileDir, 0755, true);
        }
        $fileName = 'export_'. ID_SITE .'_'.$module.'_'.$_SESSION['admins']['id_admins'].'.xlsx';
        $filePath = $fileDir . $fileName;
        $objWriter = new Xlsx($Excel);
        $objWriter->save($filePath);
        $Excel->disconnectWorksheets();

        if(file_exists($filePath)) {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename='.$fileName);
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));
            flush(); // Flush system output buffer
            readfile($filePath);
            exit;
        }
        die();
    }

    /*
        Alias de get et getI18n avec passage des param en array.
        $this->collector['produits']->getter(
            array(
                 'conditions'=> $array
                ,'translate' => true
            )
        )
    */
    function aget( $param ){
        $mixed = (isset($param['conditions']))   ? $param['conditions']    : '';
        $order  = (isset($param['order']))  ? $param['order']   : '';
        $limit  = (isset($param['limit']))  ? $param['limit']   : '';
        $offset = (isset($param['offset'])) ? $param['offset']  : '';
        $tables = (isset($param['tables'])) ? $param['tables']  : '';
        $join   = (isset($param['join']))   ? $param['join']    : '';
        $search = (isset($param['search'])) ? $param['search']  : '';
        $id_loc = (isset($param['id_loc'])) ? $param['id_loc']  : '';
        $id_loc_master= (isset($param['id_loc_master']))   ? $param['id_loc_master']    : '';

        if(isset($param['translate']) && $param['translate']==true){
            return $this->getI18n($mixed,$order,$search,$id_loc, $id_loc_master, $limit, $offset);
        }
        else{
            return $this->get($mixed, $order, $limit, $offset, $tables, $join); 
        }
    }



    public function getBOFields()
    {
        $path = GABOX_USR_DIR . DIRECTORY_SEPARATOR . 'etc' . DIRECTORY_SEPARATOR . strtolower(get_class($this)) . '.BO.php';
        if(is_file($path)) {
            include($path);
        } 
        if(!isset($boFields)) {
            $boFields = $this->array_fields;
        }
        return $boFields;
    }

    protected function _classUsesDeep($class = null, $autoload = true) {
        $traits = [];
        if(null === $class) $class = $this;

        // Get all the traits of $class and its parent classes
        do {
            $class_name = is_object($class)? get_class($class): $class;
            if (class_exists($class_name, $autoload)) {
                $traits = array_merge(class_uses($class, $autoload), $traits);
            }
        } while ($class = get_parent_class($class));

        // Get traits of all parent traits
        $traits_to_search = $traits;
        while (!empty($traits_to_search)) {
            $new_traits = class_uses(array_pop($traits_to_search), $autoload);
            $traits = array_merge($new_traits, $traits);
            $traits_to_search = array_merge($new_traits, $traits_to_search);
        };

        return array_unique($traits);
    }
    /**
     * Set used traits 
     *
     * @param array $traits
     * @return array $this->_usedTraits
     */
    public function setUsedTraits($traits = array()) 
    {
        $this->_usedTraits = $traits;
    }
    /**
     * Get used traits 
     *
     * @return array $this->_usedTraits
     */
    public function getUsedTraits() 
    {
        return $this->_usedTraits;
    }
    /**
     * Get classes by traits 
     *
     * @return array $this->_classesByTrait
     */
    public function getClassesByTrait() 
    {
        return $this->_classesByTrait;
    }

    public function getSelectListCache($key)
    {
        return is_array($this->_selectListCache) && array_key_exists($key, $this->_selectListCache) ? $this->_selectListCache[$key] : null;
    }
    public function setSelectListCache($key, $data)
    {
        if(!is_array($this->_selectListCache)) $this->_selectListCache = array();
        $this->_selectListCache[$key] = $data;
    }
    function getDynImage($arrayElement,$media_name='media',$array_lib=''){

        $module = array_keys($arrayElement);
        $module = str_replace('id_','',$module[0]);
        $id_element = $arrayElement['id_'.$module];
        $ext = end(explode('.',$arrayElement[$media_name]));

        if(!defined('USE_IMG_REWRITE') || (defined('USE_IMG_REWRITE') && (USE_IMG_REWRITE==0))) return $arrayElement[$media_name];
            
        $is_empty=true;
        $simu = $this->simulation;
        if(is_array($array_lib)){
            foreach($array_lib as $k=>$v){
                $array_lib[$k] = strtolower(urlizeUtf8($v));
                if(!empty($array_lib[$k])) $is_empty=false;
            }
        }

        # if the current language can't be urlize
        if($is_empty===true){
            $this->simulation=false;
            $array_lib=array(urlizeUtf8($this->getLib($id_element)));        
        }

        $this->simulation=$simu;
        $url = array_merge (array('dimg2',$module,$media_name,$id_element),$array_lib);
        $temp = '/'.implode('/',$url).'.'.$ext;
        return $temp;
    }

    public function importFlatTranslations(array $xlsData, $localeId)
    {
        $display = array();
        // Squelette d'une ligne
        $idx = array(
            'id_element' => 0,
            'id_i18n' => 1,
            'id_locales' => 2,
            'module' => 3,
            'field_name' => 4,
            'source' => 5,
            'translation' => 6
        );
        $autoTradExists = array_key_exists('is_auto_trad', $this->collector['i18n']->array_fields);
        foreach($xlsData as $k => $t) {
            $v = $t[$idx['id_i18n']];
            if($v instanceof RichText) {
                $v = $v->getPlainText();
            }
            if(empty($v)) continue;
            $resourcesCache = array();
            $module = $t[$idx['module']];
            $i18n = $this->collector['i18n']->getOne(array('module' => $module, 'id_locales' => $localeId, 'id_i18n' => (int) $v));
            if(is_array($i18n) && count($i18n) > 0) {
                $key = $t[$idx['module']] . '_' . $t[$idx['id_element']];
                if(!array_key_exists($key, $resourcesCache)) {
                    $element = $this->collector[$t[$idx['module']]]->getOne((int) $t[$idx['id_element']]);
                } else {
                    $element = $resourcesCache[$key];
                }
                $lib = $t[$idx['translation']];
                $t[$idx['source']] = preg_replace('/_x([0-9a-fA-F]{4})_/', "\r", $t[$idx['source']]);
                if(str_replace($this->_extractCharsList['in'], $this->_extractCharsList['out'], $element[$t[$idx['field_name']]]) != str_replace($this->_extractCharsList['in'], $this->_extractCharsList['out'], $t[$idx['source']])) {
                    if(!array_key_exists('ERRORS', $display)) {
                        $display['ERRORS'] = array();
                    }
                    $display['ERRORS'][] = 'File source column differs from database source : ID I18N : ' . $v . ' / ID ELEMENT : ' . $t[$idx['id_element']];// . ' ' . $element[$t[$idx['field_name']]] . ' / ' . $t[$idx['source']];
                    continue;
                }
                
                $saveParams = array('lib' => $lib, 'is_published' => 1);
                if($autoTradExists) {
                    $saveParams['is_auto_trad'] = null;
                }
                $res = $this->collector['i18n']->set($i18n['id_i18n'], $saveParams);
                if($res) {
                    if(!array_key_exists('UPDATE', $display)) {
                        $display['UPDATE'] = array();
                    }
                    $display['UPDATE'][] = "UPDATE element " . $t[$idx['id_element']] . ' / ' . $lib . ' / ID I18N : ' . $i18n['id_i18n'];
                } else {
                    if(!array_key_exists('ERRORS', $display)) {
                        $display['ERRORS'] = array();
                    }
                    $display['ERRORS'][] = "Error while saving ID " . $i18n['id_i18n'] . ' / ' . $lib;
                }
            } else {
                if(!array_key_exists('ERRORS', $display)) {
                    $display['ERRORS'] = array();
                }
                $display['ERRORS'][] = 'Translation not found (locale column may differ from database) : ID ' . $t[$idx['id_i18n']];
            }
        }
        return $display;
    }

    
    function getVersion($version = false){
        if(!$version){
            $sql = "SELECT max(version_number) as max FROM ".$this->versionning_table ." where module='".$this->table_name."'";
            $rs = $this->Query($sql);
            $version = (int) $this->fetchField($rs, 'max');
        }
       $sql = "select id_versions from ".$this->versionning_table ." where module='".$this->db->qstr(str_replace('gen_', '', $this->table_name))."' and version_number=".$version;
       $rs = $this->Query($sql);
       $id_versions = (int) $this->fetchField($rs, 'id_versions');
    }

    function disableVersioningByModule($modules){
        return $this->setVersioning($modules, false);
    }

    function enableVersioningByModule($modules){
        return $this->setVersioning($modules, true);
    }

    function setVersioning($modules,$value){
        if(is_array($modules)) {
            foreach($modules as $module){
                if(isset($this->collector[$module])){
                    $this->collector[$module]->versionning = $value;
                }
            }
            return true;
        }elseif(isset($this->collector[$modules])){
            $this->collector[$module]->versionning = $value;
            return true;
        }
        else return false;
    }



    public function agetKeyValueStore( $param, $properties = array() ){
        $mixed = (isset($param['conditions']))   ? $param['conditions']    : '';
        $order  = (isset($param['order']))  ? $param['order']   : '';
        $limit  = (isset($param['limit']))  ? $param['limit']   : '';
        $offset = (isset($param['offset'])) ? $param['offset']  : '';
        $tables = (isset($param['tables'])) ? $param['tables']  : '';
        $join   = (isset($param['join']))   ? $param['join']    : '';
        $search = (isset($param['search'])) ? $param['search']  : '';
        $id_loc = (isset($param['id_loc'])) ? $param['id_loc']  : '';
        $id_loc_master= (isset($param['id_loc_master']))   ? $param['id_loc_master']    : '';

        if(isset($param['translate']) && $param['translate']==true){
            //return $this->getKeyValueStore($mixed,$order,$search,$id_loc, $id_loc_master, $limit, $offset);
            die('translation in getKeyValueStore not supported yet');
        }
        else{
            return $this->getKeyValueStore($mixed, $properties); 
        }
    }


    public function getOneKeyValueStore($id_element='', $properties = array()){
        $rs = $this->getKeyValueStore($id_element, $properties);
        return (isset($rs[0])) ? $rs[0] : false;
    }

   /**
    * NP : KeyValueStore
    * Permet d'aller chercher toutes les données liées sous forme clé=>valeur en mode colone ex:
    * on a une deux tables : gen_element et gen_element_data
    * gen_element_data contient 3 colonnes : id_element, key, value
    * Cette fonction va chercher toutes les informations en mode colonne pour toutes les key -> value trouvé:
    * gen_element.id_element ; gen_element.lib ; [key1] ; [value1] ; [key2] ; [value2] ...
    * pour tout key contenu dans $this->properties
    * @param mixed id_element, int ou array. Si int, c'est pas valeur de la pk qui est retourné si array, mode recherche
    * @return array
    * exemple de conf :
    *   $properties = array (
    *        'primary_key'          => 'id_object'
    *       ,'parent_tablename'     => 'gen_object'
    *       ,'child_tablename'      => 'gen_object_data'
    *       ,'child_keyfield'       => 'title'
    *       ,'child_valuefield'     => 'value'
    *       ,'keys'                 => array(
    *            'mark'=>array('lib'=>'Test','type'=>'text')
    *           ,'model'=>array('lib'=>'Test2','type'=>'select','arrayValues'=>array('1'=>'Val1','2'=>'Val2')
    *           ,'departure'=>array('lib'=>'Test3')
    *       )
    *   );
    */
    public function getKeyValueStore($id_element='', $properties = array()){
        if(!empty($properties) && isset($this->KVS_Properties)){
            $this->KVS_Properties = $properties;
        }
        # si en mode recherche
        if(is_array($id_element) && count($id_element) > 0) {
            $search = $id_element;
            $id_element = '';
            $where = " WHERE ".$this->getSQLCriteres($search);
        }
        else{
            $where = '';
        }
        if(!empty($id_element)){
            $id_element = (int) $id_element;
            $sql = 'SELECT '.$this->KVS_Properties['parent_tablename'].'.* ';
            foreach($this->KVS_Properties['keys'] as $key=>$prop){
                if(!is_array($prop))$key=$prop;
                $sql .= ',(SELECT GROUP_CONCAT('.$this->KVS_Properties['child_valuefield'] . ' SEPARATOR \'||\')
                            FROM '.$this->KVS_Properties['child_tablename'].'
                            WHERE '.$this->KVS_Properties['primary_key'].' = '.$id_element.'
                            AND '.$this->KVS_Properties['child_keyfield'].'="'.$key.'"
                            GROUP BY title
                          ) as "'.$key.'"';
            }
            $sql .= ' FROM '.$this->KVS_Properties['parent_tablename'] .'
                      WHERE '.$this->KVS_Properties['primary_key'].' = '.$id_element;
        }
         else{
            $sql = 'SELECT * ';
            foreach($this->KVS_Properties['keys'] as $key=>$prop){
                if(!is_array($prop))$key=$prop;
                $sql .= ',(SELECT '.$this->KVS_Properties['child_valuefield'].'
                            FROM '.$this->KVS_Properties['child_tablename'].'
                            WHERE '.$this->KVS_Properties['child_tablename'].'.'.$this->KVS_Properties['primary_key'].'
                                    ='.$this->KVS_Properties['parent_tablename'].'.'.$this->KVS_Properties['primary_key'].'
                            and '.$this->KVS_Properties['child_keyfield'].'="'.$key.'"
                          ) AS '.$key;
            }
            $sql .= ' FROM '.$this->KVS_Properties['parent_tablename'] ;
        }
        if(!empty($where)){
            $sql = 'SELECT * from ('.$sql.') as flat '.$where;
        }
        if($this->simulation === true) return $sql;
        return $this->GetRows($this->Query($sql));
    }

    /**
     * Get log events status
     *
     * @return boolean
     */
    public function getLogEvents() 
    {
        return $this->_logEvents;
    }
    /**
     * Set log events status
     *
     * @var boolean $status
     * @return GenClass
     */
    public function setLogEvents($status)
    {
        $this->_logEvents = (bool) $status;
        return $this;
    }
    /**
     * Get log origin
     *
     * @return boolean
     */
    public function getLogOrigin() 
    {
        return $this->_logOrigin;
    }
    /**
     * Set log origin
     *
     * @var boolean $status
     * @return GenClass
     */
    public function setLogOrigin($origin)
    {
        if(in_array($origin, array('OPERATOR', 'SYSTEM'))) {
            $this->_logOrigin = $origin;
        }
        return $this;
    }
    /**
     * Set result raw results status
     *
     * @params boolean $flag
     */
    public function setReturnRawResults($flag) 
    {
        $this->_returnRawResults = (bool) $flag;
    }
    /**
     * Get result raw results status
     *
     * @return boolean
     */
    public function getReturnRawResults() 
    {
        return $this->_returnRawResults;
    }
    /**
     * Generate a thumb for the supplied path
     */
    public function generateThumb($params)
    {
        if(!isset($params['path'])) return;
        $path = $params['path'];
        if(!is_file(implode(DIRECTORY_SEPARATOR, array(USR_DIR, 'admin', $path)))) return;
        $ext = strrchr($path, ".");
        $thumbPath = str_replace($ext, '.thumb'.$ext, $path);
        if(is_file(implode(DIRECTORY_SEPARATOR, array(USR_DIR, 'admin', $thumbPath)))) return;
        
        $finalName = implode(DIRECTORY_SEPARATOR, array(USR_DIR, 'admin', $path));
        $thumb = @exif_thumbnail($finalName, $width, $height, $thumbType);
        if(!$thumb) {
            $thumb = @resizeImg($finalName, 200, 150);
        }
        if($thumb) {
            file_put_contents(implode(DIRECTORY_SEPARATOR, array(USR_DIR, 'admin', $thumbPath)), $thumb);
        }
        return $thumb;
    }
    /**
     * Set admin ID
     *
     * @param integer $adminId
     */
    public function setAdminId($adminId) 
    {
        $this->_adminId = $adminId;
    }
    /**
     * Get setted admin ID
     *
     * @return integer
     */
    public function getAdminId() 
    {
        return $this->_adminId;
    }
    /**
     * Get search crypted columns
     *
     * @return array
     */
    public function getSearchCryptedColumns() 
    {
        return $this->_searchCryptedColumns;
    }
    /**
     * Clean a field name for back office purpose
     * Removes alias and table name
     * Example : gen_table.fieldname AS myfield is returned as fieldname
     *
     * @return string cleaned field name
     */
    public function cleanFieldName($field) 
    {
        $aliasPos = stripos($field, ' as ');
        if(false !== $aliasPos) {
            $field = substr($field, 0, $aliasPos);
        }
        $dotPos = strpos($field, '.');
        if(false !== $dotPos) {
            $field = substr($field, ($dotPos + 1));
        }
        return $field;
    }

    public function fetchField($rs, $field): ?string
    {
        if ($rs instanceof PDOStatement) {
            $row = $rs->fetch(PDO::FETCH_ASSOC);
            return $row ? $row[$field] : NULL;
        } else {
            return $rs->fields[$field];
        }
    }

    public function addAutomaticFilteringMethod(string $callable): void
    {
        $callableArray = explode('::', $callable);
        if (is_array($callableArray)
            && !empty($this->collector[$callableArray[0]])
            && method_exists($this->collector[$callableArray[0]], $callableArray[1])
        ) {
            $this->automaticFilteringMethods[] = [$this->collector[$callableArray[0]], $callableArray[1]];
            return;
        } elseif (is_callable($callable)) {
            $this->automaticFilteringMethods[] = $callable;
            return;
        }
        GaboxLogger::getInstance()->warning("Provided argument is not a callable: " . $callable, [
            'action' => 'genclass::addAutomaticFilteringMethod',
        ]);
    }

    public function isAutomaticFilteringEnabled(): bool
    {
        return $this->automaticFilteringEnabled;
    }

    public function setAutomaticFilteringEnabled(bool $automaticFilteringEnabled): void
    {
        $this->automaticFilteringEnabled = $automaticFilteringEnabled;
    }
}
