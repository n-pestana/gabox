<?php

namespace Gabox\Event;

use Gabox\Monolog\GaboxLogger;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\EventDispatcher\EventDispatcher as SymfonyEventDispatcher;

/**
 * Gabox EventDispatcher based on Symfony EventDispatcher
 */
class EventDispatcher extends SymfonyEventDispatcher
{
    /**
     * This function returns a KV array containing all defined event names.
     * These events are defined :
     *      - in Gabox\Event\GenclassEvents class
     *      - in a class {CollectorName}Events defined in an Event directory placed at the same level as the collector class
     */
    public function getAllThrownEvents()
    {
        global $gen;

        // First take the cache result if defined
        if (isset($gen->collector['caches'])) {
            $allThrownEvents = $gen->collector['caches']->getVar('all-thrown-events');
        } else {
            $allThrownEvents = get_internal_cache('all-thrown-events');
        }
        if (!$allThrownEvents) {
            $allThrownEvents = [];
            try {
                // Find events defined for each collector
                foreach ($gen->collector as $collectorName => $collector) {
                    if (class_exists(ucfirst($collectorName) . 'Events')) {
                        $reflectionClass = new ReflectionClass(ucfirst($collectorName) . 'Events');
                        if (!in_array('Gabox\Event\ListEvents', array_keys($reflectionClass->getInterfaces()))) {
                            GaboxLogger::getInstance()->error(
                                ucfirst($collectorName) . 'Events must implements ListEvents interface.', [
                                    'action' => 'GenClass::getAllThrownEvents'
                                ]
                            );
                        } else {
                            // For each event name defined as a constant, we call a getLib() method to get its title
                            foreach ($reflectionClass->getConstants() as $eventName) {
                                $allThrownEvents[$eventName] = '[' . ucfirst($collectorName) . '] ' . call_user_func([ucfirst($collectorName) . 'Events', 'getLib'], $eventName);
                            }
                        }
                    }
                }
                // Complete the array with Genclass Events
                if (class_exists('Gabox\Event\GenclassEvents')) {
                    $reflectionClass = new ReflectionClass('Gabox\Event\GenclassEvents');
                    foreach ($reflectionClass->getConstants() as $eventName) {
                        $allThrownEvents[$eventName] = '[GenClass] ' . call_user_func(['Gabox\Event\GenclassEvents', 'getLib'], $eventName);
                    }
                }
            } catch (ReflectionException $e) {
                GaboxLogger::getInstance()->error('Error when getting available events', [
                    'action' => 'EventDispatcher::getAllThrownEvents',
                ]);
            }
            // Set the cache
            if (isset($gen->collector['caches'])) {
                $gen->collector['caches']->setVar('all-thrown-events', $allThrownEvents);
            } else {
                set_internal_cache('all-thrown-events', $allThrownEvents);
            }
        }

        asort($allThrownEvents);
        return $allThrownEvents;
    }

    /**
     * Wrapper for the symfony event dispatcher method.
     * Check if the event name is defined before calling dispatch.
     *
     * @param object $event
     * @param string|null $eventName
     * @return object
     */
    public function dispatch(object $event, string $eventName = null): object
    {
        if (!in_array($eventName, array_keys($this->getAllThrownEvents()))) {
            GaboxLogger::getInstance()->error('Nonexistent event name: ' . $eventName, [
                'action' => 'EventDispatcher:dispatch'
            ]);
        }
        return parent::dispatch($event, $eventName);
    }
}