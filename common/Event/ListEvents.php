<?php

namespace Gabox\Event;

interface ListEvents
{
    public static function getLib(string $eventName):string;
}