<?php

namespace Gabox\Event;

use Dompdf\Exception;
use Gabox\Monolog\GaboxLogger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Activation de la gestion des événements pour l'objet utilisant ce trait
 * L'objet doit alors définir une propriété 'listenedEvents' contenant un tableau avec les événements à écouter
 * Ce tableau contient en clé le nom de l'événement et en valeur la méthode à appeler
 * Le nom de la méthode peut être :
 *   - soit le nom d'une méthode de l'objet en cours
 *   - soit le nom d'une méthode de Listener devant implémenter l'interface 'Gabox\Event\ListenerInterface'
 * Dans le cas d'un listener autonome (méthode à privilégier), ce dernier doit être défini dans un dossier Event
 * au même niveau que la classe du collector afin qu'il soit autoloadé
 */
trait EventRegistererTrait
{
    /**
     * Cette méthode doit être appelée dans le constructeur de l'objet (comme c'est le cas sur la Genclass)
     *
     * @return void
     */
    public function initListenedEvents()
    {
        try {
            if (!empty($this->listenedEvents)) {
                // On enregistre tous les événements définis
                foreach ($this->listenedEvents as $eventName => $eventCallbacks) {
                    if (!is_array($eventCallbacks)) {
                        $eventCallbacks = [$eventCallbacks];
                    }
                    foreach ($eventCallbacks as $eventCallback) {
                        // Cas d'un callable situé en dehors de l'objet (Listener autonome)
                        // Le listener doit implémenter Gabox\Event\ListenerInterface
                        if (strpos($eventCallback, '::') !== false) {
                            $eventCallbackInfo = explode('::', $eventCallback);
                            if (!in_array('Gabox\Event\ListenerInterface', class_implements($eventCallbackInfo[0]))) {
                                GaboxLogger::getInstance()->error('Listener ' . $eventCallbackInfo[0] . ' must implement Gabox\Event\ListenerInterface', [
                                    'action' => 'initListenedEvents',
                                ]);
                            }
                            $this->getEventDispatcher()->addListener($eventName, [call_user_func([$eventCallbackInfo[0], 'getInstance']), $eventCallbackInfo[1]]);
                        } else {
                            // Cas d'une méthode directement dans l'objet en cours
                            $this->getEventDispatcher()->addListener($eventName, [$this, $eventCallback]);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            GaboxLogger::getInstance()->error('Error registering events of collector ' . get_class($this), [
                'action' => 'initListenedEvents',
            ]);
        }
    }

    public function getEventDispatcher(): EventDispatcherInterface
    {
        // On prend par défaut l'eventDispatcher de l'objet, sinon celui de la Genclass
        if (isset($this->eventDispatcher)) {
            return $this->eventDispatcher;
        } else {
            global $gen;
            return $gen::$eventDispatcher;
        }
    }
 }