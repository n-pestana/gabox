<?php

namespace Gabox\Event;

interface ListenerInterface
{
    public static function getInstance(): self;
}