<?php

namespace Gabox\Event;

use Symfony\Contracts\EventDispatcher\Event;

class CollectorItemEvent extends Event
{
    protected array $item;
    protected string $collectorName;

    public function __construct(array $item, string $collectorName)
    {
        $this->item = $item;
        $this->collectorName = $collectorName;
    }

    public function getItem(): array
    {
        return $this->item;
    }

    public function getCollectorName(): string
    {
        return $this->collectorName;
    }
}