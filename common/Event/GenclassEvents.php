<?php

namespace Gabox\Event;

/**
 * Classe regroupant les noms de tous les événements générés par Gabox
 */
final class GenclassEvents implements ListEvents
{
    /**
     * Dispatché lors de l'insertion en base d'un item de collector
     */
    public const COLLECTOR_ITEM_CREATED = 'genclass.collector_item.created';

    /**
     * Dispatché lors de l'insertion en base d'un item de collector
     */
    public const COLLECTOR_ITEM_UPDATED = 'genclass.collector_item.updated';

    /**
     * Dispatché lors de l'insertion en base d'un item de collector
     */
    public const COLLECTOR_ITEM_DELETED = 'genclass.collector_item.deleted';

    private static array $eventsLibs = [
        self::COLLECTOR_ITEM_CREATED => "Création d'un item de collector",
        self::COLLECTOR_ITEM_UPDATED => "Mise à jour d'un item de collector",
        self::COLLECTOR_ITEM_DELETED => "Suppression d'un item de collector",
    ];

    public static function getLib(string $eventName):string
    {
        return self::$eventsLibs[$eventName] ?? $eventName;
    }
}