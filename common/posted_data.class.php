<?php class Posted_data extends  gen_posted_data{
    var $posted = '';
    var $errors = [];
    var $id_site = '';
    private bool $isValid = false;

    
	function setPost($post,$fields,$id_site){

          if(!$this->isValid($fields)){
            return false; 
          }
          unset($fields['g-recaptcha-response']);

          $this->db->BeginTrans();
          $sql = "INSERT INTO gen_posted(form_name,ip, agent, localisation,id_loc,id_country,id_site) VALUES ( '".$post['name']."', '".$post['ip']."',".$this->db->qstr($post['agent']).",".$this->db->qstr($post['localisation']).",".ID_LOC.",".ID_COUNTRY.",".ID_SITE.")";
          $this->Query($sql);
          $sql = "SELECT last_insert_id( ) as id ;";
          $rs = $this->Query($sql);
          $this->db->CommitTrans();
          $set= $this->getRows($rs);
          if(!isset($set[0]) && !isset($set[0]['id'])) return false;
          
          $form_id = $set[0]['id'];
          $sql = '';
          foreach($fields as $k=>$v){
            if(is_array($v)) {
                $v = implode(', ', $v);
            }
            $sql = "insert into gen_posted_data (id_posted,post_value, post_name) values (".$form_id.",".$this->db->qstr($v).",".$this->db->qstr($k)."); ";
            $this->Query($sql);
          }

          if ($post['name'] == 'contact') {
            $mailVals = $fields;
            $this->collector['mails']->sendMail('contact', ID_LOC, $mailVals);
          }

          return $form_id;//true; 
	}

    /**
     * Is the form valid ?
     *
     * @param array $data
     * @return boolean $isValid
     */
    public function isValid(array $data = []): bool
    {
	if (!is_null($this->isValid)) {
            return $this->isValid;
        }
        $this->posted = $data;
        $resp = $this->checkCaptcha($data);
        $this->isValid = empty($resp['errors']);
        return $this->isValid;
    }
    /**
     * Is the field valid ?
     *
     * @param string $field
     * @param mixed $val
     * @return array $response
     */
    public function isFieldValid($field, $val)
    {
        return true;
    }

    /**
    * Check google ReCaptcha response
    *
    * @param array $params
    * @return array
    */
    public function checkCaptcha($params = array())
    {
        if (empty($this->collector['displayer']->conf['sites']['key_gg_captcha_secret']) ||
                empty($this->collector['displayer']->conf['sites']['key_gg_captcha_url']) ||
                empty($this->collector['displayer']->conf['sites']['key_gg_captcha_score']) ||
                empty($this->collector['displayer']->conf['sites']['key_gg_captcha_sitekey'])) {
            return [];
        }
        
        $checkParams = array(
            'secret'     => $this->collector['displayer']->conf['sites']['key_gg_captcha_secret']
            , 'response' => $params['g-recaptcha-response']
            , 'remoteip' => $_SERVER['REMOTE_ADDR']
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->collector['displayer']->conf['sites']['key_gg_captcha_url']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $checkParams);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        $info = curl_getinfo($ch);

        if ($info['http_code'] != 200) {
            $output = "No cURL data returned for $url [". $info['http_code']. "]";
            if (curl_error($ch)) {
                $output .= "\n". curl_error($ch);
            }
            error_log($output);
        }

        curl_close($ch);
        $final = json_decode($response, true);
        if($final['success'] !== true) {
            $this->errors['captcha'] = 'Captcha Error : ' . implode(', ', $final['error-codes']);
        } elseif(array_key_exists('score', $final) && $final['score'] < $this->collector['displayer']->conf['sites']['key_gg_captcha_score']) {
            $this->errors['captcha'] = 'Captcha Error : ' . (array_key_exists('error-codes', $final) ? implode(', ', $final['error-codes']) : 'Score not valid');
        }
        return array('errors' => $this->errors['captcha'] ?? '');
    }

    /**
     * Get errors
     *
     * @var array
     */
    public function getErrors()
    {
        return $this->errors;
    }
    /**
     * Add error to error list
     *
     * @var string $error
     * @return $this
     */
    public function addError($field, $error) 
    {
        $this->errors[$field] = $error;
        return $this;
    }
}
