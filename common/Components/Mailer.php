<?php

namespace Gabox\Components;

use Exception;
use Gabox\Monolog\GaboxLogger;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Gabox Mailer component, based on PHPMailer package
 */
class Mailer
{
    /**
     * Singleton instance of the component
     */
    protected static Mailer $_instance;
    /**
     * PHPMailer instance
     */
    private PHPMailer $_mailer;

    /**
     * Get singleton instance (create it if not instanciated yet)
     */
    public static function getInstance(): self {
        if (!isset(static::$_instance)) {
            static::$_instance = new static();
        }
        return static::$_instance;
    }

    private function __construct()
    {
        $this->_mailer = new PHPMailer(true);
    }

    /**
     * Send an email over SMTP using provided configuration
     *
     * @param array $config Configuration for SMTP, the array must contain these keys : host, username, password
     * @param string $sender Email sender
     * @param array $recipients Recipients emails
     * @param string $subject
     * @param string $message
     * @param string $altMessage
     * @return bool true on success
     */
    //TODO: Add SensitiveParameter attribute to $config parameter when all projects will be on PHP 8.2+
    public function SendSMTPMail(array $config, string $sender, array $recipients, string $subject, string $message, string $altMessage = ''): bool {
        if (empty($config['host']) || empty($config['username']) || empty($config['password'])) {
            GaboxLogger::getInstance()->error(
                'Missing configuration in $config array, you have to provide host, username and password keys', [
                    'action' => 'SendSMTPMail'
                ]
            );
            return false;
        }
        global $gen;
        try {
            if (defined('ID_LOC')) {
                $locale = $gen->collector['locales']->getOne(ID_LOC);
                if ($locale) {
                    $this->_mailer->setLanguage($locale['language']);
                }
            }

            $this->_mailer->clearAddresses();
            $this->_mailer->CharSet = 'utf-8';
            $this->_mailer->Debugoutput = GaboxLogger::getInstance();
            //$this->_mailer->SMTPDebug  = SMTP::DEBUG_SERVER;                             //Enable verbose debug output
            $this->_mailer->isSMTP();
            $this->_mailer->Host       = $config['host'];
            $this->_mailer->SMTPAuth   = true;                                           //Enable SMTP authentication
            $this->_mailer->Username   = $config['username'];
            $this->_mailer->Password   = $config['password'];
            $this->_mailer->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;                    //Enable implicit TLS encryption
            $this->_mailer->Port       = 465; //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            //Recipients
            $this->_mailer->setFrom($sender);
            foreach ($recipients as $recipient) {
                $this->_mailer->addAddress($recipient);
            }

            //Content
            $this->_mailer->isHTML(true);
            $this->_mailer->Subject = $subject;
            $this->_mailer->Body    = nl2br($message);
            if (!empty($altMessage)) {
                $this->_mailer->AltBody = $altMessage;
            }

            $result = $this->_mailer->send();
            if ($result) {
                GaboxLogger::getInstance()->info(
                    t("Le mail a été envoyé"), [
                        'action' => 'SendMail'
                    ]
                );
            } else {
                GaboxLogger::getInstance()->error(
                    t("Erreur lors de l'envoi du mail : " . $this->_mailer->ErrorInfo), [
                        'action' => 'SendMail'
                    ]
                );
            }
            return $result;
        } catch (Exception $e) {
            GaboxLogger::getInstance()->error(
                t("Erreur lors de l'envoi du mail : " . $this->_mailer->ErrorInfo), [
                    'action' => 'SendMail'
                ]
            );
            return false;
        }
    }
}