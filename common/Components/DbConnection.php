<?php

namespace Gabox\Components;

use Gabox\Monolog\GaboxLogger;
use PDO;
use PDOException;
use PDOStatement;

/**
 * PDO Database connection overlay.
 * This class wraps ADODB functions that are unavailable with PDO
 * and was created in order to make Gabox compatible with PDO.
 */
class DbConnection
{
    /**
     * @var PDO The PDO connection to the database
     */
    private PDO $connection;
    /**
     * @var string The last error message that was found in a PDO Statement
     */
    private string $lastStatementErrorMsg;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        try {
            $this->connection = new PDO(
                $config['type'] . ':host=' . $config['host']
                . ';dbname=' . $config['dbname']
                . (isset($config['charset']) ? ';charset=' . $config['charset'] : ''),
                $config['username'],
                $config['password']
            );
            $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            if (isset($config['cachePriority'])) {
                CacheManager::setCachePriority($config['cachePriority']);
            }
        } catch (PDOException $e) {
            die("Erreur de connexion à la base de données :" . $e->getMessage());
        }
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }

    /**
     * Returns an array containing all the names of the tables in the database.
     *
     * @return array|false
     */
    public function MetaTables()
    {
        $query = $this->connection->query('SHOW TABLES');
        return $query->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * Returns an array containing all the columns of the table given in argument.
     *
     * @param string $table Name of the table
     * @return array|false
     */
    public function MetaColumnNames(string $table)
    {
        $table = str_replace("'", '', $this->connection->quote($table));
        if ($columns = $this->connection->query('SHOW COLUMNS FROM ' . $table)) {
            return $columns->fetchAll(PDO::FETCH_COLUMN);
        }
        return false;
    }

    /**
     * Quotes a string to use it in a query directly.
     *
     * @param string $value
     * @return string
     */
    public function qstr(string $value): string
    {
        return $this->connection->quote($value);
    }

    /**
     * Execute the query, using optional bind parameters.
     *
     * @param string $sql
     * @param array $bindvars
     * @return false|PDOStatement
     */
    public function Execute(string $sql, array $bindvars = [])
    {
        $statement = $this->connection->prepare($sql);
        try {
            $result = $statement->execute($bindvars);
        } catch (PDOException $e) {
            GaboxLogger::getInstance()->error($e->getMessage() . ' - ' . $sql, [
                'action' => 'DbConnection::Execute',
            ]);
        }
        if (!isset($result) || $result === false) {
            // Store the statement error in order to let it available in a future call to errorMsg().
            $this->lastStatementErrorMsg = $statement->errorInfo()[2];
            return false;
        } else {
            return $statement;
        }
    }

    /**
     * Returns the last error message of the PDO connection if set, otherwise the last PDO Statement error.
     *
     * @return string|null
     */
    public function errorMsg(): ?string
    {
        return !empty($this->connection->errorInfo()[2])
            ? $this->connection->errorInfo()[2]
            : $this->lastStatementErrorMsg;
    }

    /**
     * Gets all the results corresponding to the sql query in an associative array.
     *
     * @param string $sql
     * @return array|false
     */
    public function getArray(string $sql)
    {
        $statement = $this->Execute($sql);
        if ($statement === false) {
            return false;
        }
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return void
     */
    public function beginTrans(): void
    {
        $this->connection->beginTransaction();
    }

    /**
     * @return void
     */
    public function CommitTrans(): void
    {
        $this->connection->commit();
    }

    /**
     * @return void
     */
    public function RollbackTrans(): void
    {
        $this->connection->rollBack();
    }

    /**
     * Simulates the CacheExecute ADODB method since PDO has no caching system.
     *
     * @param $secs2cache
     * @param $sql
     * @return false|mixed
     */
    public function CacheExecute($secs2cache, $sql=false)
    {
        $result = CacheManager::getInstance()->getValue($sql);
        if ($result === false) {
            $result = CacheManager::getInstance()
                ->setCacheDuration($secs2cache)
                ->setValue($sql, $this->getArray($sql));
        }
        return $result;
    }
}