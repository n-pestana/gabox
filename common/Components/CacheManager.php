<?php

namespace Gabox\Components;

/**
 * This component goal is to manage cache and related rules.
 */
class CacheManager
{
    /**
     * @var CacheManager Singleton instance
     */
    private static self $_instance;
    /**
     * @var int Cache duration in seconds
     */
    private int $cacheDuration = 3600;
    /**
     * @var string String prepended to the cache key
     */
    private string $namePrefix;

    /**
     * @var string db|file Where to store the cache
     */
    private static string $cachePriority = 'db';

    private function __construct()
    {
        $this->namePrefix = $_SERVER['SERVER_NAME'] .
            ((defined('ID_SITE')) ? ID_SITE : '') .
            ((defined('ID_LOC')) ? ID_LOC : '');
    }

    /**
     * @param string $cachePriority db|files The cache storage priority
     * @return void
     */
    public static function setCachePriority(string $cachePriority): void
    {
        if (!in_array($cachePriority, ['db', 'file'])) {
            $cachePriority = 'db';
        }
        self::$cachePriority = $cachePriority;
    }

    public static function getInstance(): self
    {
        if (!isset(static::$_instance)) {
            static::$_instance = new static();
        }
        return static::$_instance;
    }

    /**
     * @return int
     */
    public function getCacheDuration(): int
    {
        return $this->cacheDuration;
    }

    /**
     * @param int $cacheDuration
     * @return CacheManager
     */
    public function setCacheDuration(int $cacheDuration): self
    {
        if ($cacheDuration > 0) {
            $this->cacheDuration = $cacheDuration;
        }
        return $this;
    }

    /**
     * Get the value from cache if available.
     *
     * @param string $key Cache key of the data
     * @return false|mixed Cached data or false if no data or if data has expired
     */
    public function getValue(string $key)
    {
        global $gen;
        // Prioritize the value from caches collector if available, or from cache files, depending on the configuration
        if (static::$cachePriority === 'db' && isset($gen->collector['caches'])) {
            $result = $gen->collector['caches']->getVar(md5($this->namePrefix . $key));
        } else {
            $result = get_internal_cache($this->namePrefix . $key);
        }
        if (empty($result) || (isset($result['max-age']) && $result['max-age'] < time())) {
            return false;
        } else {
            return $result['value'];
        }
    }

    /**
     * Set the value in the cache
     *
     * @param string $key Cache key of the data
     * @param $value mixed The value to store in cache
     * @return mixed The value to store in cache
     */
    public function setValue(string $key, $value)
    {
        global $gen;

        if (static::$cachePriority === 'db' && isset($gen->collector['caches'])) {
            $gen->collector['caches']->setVar(md5($this->namePrefix . $key), [
                'max-age' => time() + $this->getCacheDuration(),
                'value' => $value,
            ]);
        } else {
            set_internal_cache($this->namePrefix . $key, [
                'max-age' => time() + $this->getCacheDuration(),
                'value' => $value,
            ]);
        }
        return $value;
    }
}