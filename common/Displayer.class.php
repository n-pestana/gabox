<?php 
class Displayer {

  private $call_func ;
  private $root;
  public $header;
  public $footer;
  public $collector;
  public $display ;
  public $vars; 
  public $blocksVars; 
  public $viewsVars; 
  public $cookies;
  public $id_visitors;
  public $id_members;
  public $conf;
  public $cache_file;
  public $write_cache_now;
  public $www_themes;
  
  function __construct(){
    	$this->display=$this;
  }
  
  function setCallBackFunction($func){
	$this->call_func=$func;
  }
  function Display(){
   $out = $this->header.$this->content.$this->footer;
    if(!empty($this->call_func)){
       if(!function_exists($this->call_func)) die("TEMPLATE call back function not found:".$this->call_func.'');        
       $out = call_user_func_array($this->call_func,array($out));
    }

    # sans BOM
    echo preg_replace("/\xEF\xBB\xBF/", "", $out);

  }

  function fetch($filename, $fetchParams = array()){
        if($this->www_themes){     
            $temp = str_replace('usr/'.USR.'/www/','usr/'.USR.'/www-themes/'.$this->www_themes.'/',$filename);
            if(file_exists($temp)){
                 $filename = $temp;
            }
        }
        if(is_array($fetchParams) && count($fetchParams) > 0) {
            extract($fetchParams);
        }
        ob_start();
        include $filename;
        $contents = ob_get_contents();
        @ob_end_clean();
        return $contents;

  }

  function setVar($key,$value){
    $this->vars[$key] = $value;
  }
  
    public function includeBlock($blockName, $blockVars = [], $returnType = '') {
        if (empty($this->blocksVars[$blockName])) {
            $this->blocksVars[$blockName] = [];
        }
        $this->blocksVars[$blockName] = array_merge($this->blocksVars[$blockName], $blockVars);
        if ($returnType === 'vars') {
            ob_start();
        }
        if (file_exists(CONTROLLER_DIR . 'blocks/' . $blockName . '.php')) {
            include CONTROLLER_DIR . 'blocks/' . $blockName . '.php';
        }
        if (file_exists(VIEWS_DIR . 'blocks/' . $blockName . '.php')) {
            include VIEWS_DIR . 'blocks/' . $blockName . '.php';
        }
        if ($returnType === 'vars') {
            return ob_get_clean();
        }
    }
    public function prepareBlock($blockName, $blockVars = [], $id = null) {
        $content = $this->includeBlock($blockName, $blockVars, 'vars');
        if (is_null($id)) {
            $this->vars['blocks'][$blockName] = $content;
        } else {
            $this->vars['blocks'][$blockName][$id] = $content;
        }
    }
  
    public function includeContent($filters, $vars = []) {
        $this->vars['contents'] = $vars;
        if (isset($this->collector['contents'])) {
            if (defined('ID_SITE')) {
                $filters['id_sites'] = ID_SITE;
            }
            $content = $this->collector['contents']->getOneI18n(array_merge($filters, ['enabled' => 1]));
            if (!empty($content)) {
                $this->vars['contents']['lib'] = $content['lib'];
                for ($i=1; $i<=3; $i++) {
                    $this->vars['contents']["content$i"] = $content["content$i"];
                }
                for ($i=1; $i<=7; $i++) {
                    $this->vars['contents']["text$i"] = $content["text$i"];
                }
                
                for ($i=1; $i<=5; $i++) {
                    $this->vars['contents']["media$i"] = $content["media$i"];
                }
                if(!isset($this->collector['contents_templates'])) return false;
                $template = $this->collector['contents_templates']->getOneCached($content['id_contents_templates']);
                if (file_exists(VIEWS_DIR . 'contents/' . $template['type'] . 's/' . $template['name'] . '.php')) {
                    include(VIEWS_DIR . 'contents/' . $template['type'] . 's/' . $template['name'] . '.php');
                } else {
                    echo 'Content template not found:' . VIEWS_DIR . 'contents/' . $template['type'] . 's/' . $template['name'] . '.php';
                }
            }
        }
  }
}
