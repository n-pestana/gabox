<?php

use Gabox\Components\DbConnection;

$temp = array();

$conf_file = '/etc/db-config.php';

if(isset($_SERVER['env'])){
    if(file_exists(GABOX_USR_DIR.'/etc/db-config.'.$_SERVER['env'].'.php')){
        $conf_file = '/etc/db-config.'.$_SERVER['env'].'.php';
    }
}

if(file_exists(GABOX_USR_DIR.$conf_file)){
    require(GABOX_USR_DIR.$conf_file);
    $temp['host']   = $host;
    $temp['username']   = $username;
    $temp['password']   = $password;
    $temp['dbname']     = (isset($_SERVER['DB_NAME'])) ? $_SERVER['DB_NAME'] : $dbname;
    $temp['charset']    = $charset;
    $temp['type']    = $type;
    $temp['driver'] = $driver ?? 'ado';
    $temp['cachePriority'] = $cachePriority ?? null;
    unset($host,$username,$password,$dbname,$charset,$type, $driver, $cachePriority);
}
# si installation :
if ((
    empty($temp['host'])
    || empty($temp['username'])
    || empty($temp['password'])
    || empty($temp['dbname'])
    || empty($temp['charset'])
    || empty($temp['type'])
    ) && !isset($_POST['GaboXInstall'])
) {
    include(INC.'/install.php');
    die();
} elseif (isset($_POST['GaboXInstall'])) {
   include(INC.'/install.php'); 
   die();
}

if ($temp['driver'] === 'ado') {
    if ($temp['type'] === 'mysql') {
        $temp['type'] = "mysqli";
    }
    $obj['db'] = ADONewConnection((empty($temp['type'])) ? 'mysqli' : $temp['type']);
    $c = $obj['db']->Connect($temp['host'], $temp['username'], $temp['password'], $temp['dbname'], $temp['charset']);
    if ($c === false){
        die("Erreur de connexion à la base de données :" . $obj['db']->_errorMsg);
    }
    if (isset($temp['charset'])) {
        $obj['db']->setCharset($temp['charset']);
    }
    $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
    $ADODB_CACHE_DIR  = GABOX_USR_DIR.'/var/cache/db/';
} else {
    $obj['db'] = new DbConnection($temp);
}

unset($temp);
